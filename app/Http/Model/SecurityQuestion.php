<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class SecurityQuestion extends Model
{
    ////Set the table name
    protected $table = 'security_questions';
    protected $primaryKey = 'security_questions_id';
    public $timestamps = true;
    protected $fillable = array(
        'questions',
        'updated_at',
        'created_at'
    );
    

    public function getFullQuestionList(){
        return self::all();
    }
}
