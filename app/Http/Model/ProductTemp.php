<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class ProductTemp extends Model
{
    //
    protected $table = 'product_services';
    protected $primaryKey = 'product_services_id';
    public $timestamps = true;
    protected $fillable = array(
        'product_service_title',
        'product_service_desc',
        'category_id',
        'product_service_attributes',
        'product_rating',
        'created_at',
        'updated_at'
    );
}
