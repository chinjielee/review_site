<?php

namespace App\Http\Model;
use Auth;
use DB;

use Illuminate\Database\Eloquent\Model;

class ProductQuestion extends Model
{
    //
    const STATUS_PUBLISHED = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_BLOCKED = 3;
    protected $table = 'product_questions';
    protected $primaryKey = 'product_questions_id';
    protected $fillable = array(
        'product_services_id',
        'user_id',
        'question_description',
        'anonymous',
        'created_at',
        'updated_at'
    );
    public $timestamps = true;

    public function product() {
        return $this->belongsTo('App\Http\Model\ProductService', 'product_service_id', 'product_service_id');
    }
    
    public function user() {
        return $this->belongsTo('App\Http\Model\User', 'user_id', 'user_id');
    }

    public function answers() {
        return $this->hasMany('App\Http\Model\ProductAnswer', 'product_questions_id', 'product_questions_id')->where('status', 1);
    }

    public function getCountQuestionByCurrentUser(){
        return self::where('user_id', '=', Auth::user()->user_id)->count();
    }

    public function getQuestionByCurrentUser(){
        return self::select(DB::raw('DATE_FORMAT(product_questions.created_at, "%m/%d/%Y") as date'),'product_questions.status','product_services.product_service_title','product_questions.question_description','product_questions.product_questions_id','product_questions.product_services_id','product_questions.user_id','product_services.product_slug')->leftjoin('product_services', 'product_questions.product_services_id', '=', 'product_services.product_services_id')->where([['product_questions.user_id', '=', Auth::user()->user_id],['product_questions.created_at', '>=',DB::raw("DATE_SUB(NOW(),INTERVAL 5 YEAR)")]])->get();
    }

    public function deleteQuestionByCurrentUser($key){
        $start = strpos($key,"-1")+2;
        $end = strrpos($key,"-");
        $length = $end - $start;
        $product_questions_id = substr($key, $start, $length);
        return DB::table('product_questions')->where([['product_questions_id', '=', $product_questions_id],['user_id', '=', Auth::user()->user_id],])->limit(1)->update(['status' => '2']);
    }
    
    public function getProductQuestionRelatedOwnerWithProductDetail($questionId){
        return self::select('product_questions.user_id as user_id','product_services.product_service_title as product_service_title')->join('product_services','product_questions.product_services_id','product_services.product_services_id')->where('product_questions_id', $questionId)->first();
    }
}
