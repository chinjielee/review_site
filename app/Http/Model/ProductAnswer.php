<?php

namespace App\Http\Model;
use Auth;
use DB;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\NotificationNewAnswer;
use App\Http\Model\ProductQuestion;

class ProductAnswer extends Model
{
    //
    const STATUS_PUBLISHED = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_BLOCKED = 3;
    
    protected $table = 'product_answers';
    protected $primaryKey = 'product_answers_id';
    public $timestamps = true;
    protected $fillable = array(
        'product_questions_id',
        'user_id',
        'answer',
        'anonymous',
        'status',
        'created_at',
        'updated_at'
    );

    public function question() {
        return $this->belongsTo('App\Http\Model\ProductQuestion', 'product_questions_id', 'product_questions_id');
    }
    
    public function user(){
        return $this->belongsTo('App\Http\Model\User', 'user_id', 'user_id');
    }

    public function questionsByUser()
    {
        return $this->hasManyThrough('App\Http\Model\ProductQuestionAnswer','App\Http\Model\ProductService');
    }

    public function getCountCommentByCurrentUser(){
        return self::where('user_id', '=', Auth::user()->user_id)->count();
    }

    public function getCommentByCurrentUser(){
        return self::select(DB::raw('DATE_FORMAT(product_answers.created_at, "%m/%d/%Y") as date'),'product_answers.status','product_services.product_service_title','product_answers.answer','product_answers.product_questions_id','product_answers.product_answers_id','product_answers.user_id','product_services.product_slug')->leftjoin('product_questions', 'product_answers.product_questions_id', '=', 'product_questions.product_questions_id')->leftjoin('product_services', 'product_questions.product_services_id', '=', 'product_services.product_services_id')->where([['product_answers.user_id', '=', Auth::user()->user_id],['product_answers.created_at', '>=', DB::raw("DATE_SUB(NOW(),INTERVAL 5 YEAR)")]])->get();
    }

    public function deleteCommentByCurrentUser($key){
        $start = strpos($key,"-1")+2;
        $end = strrpos($key,"-");
        $length = $end - $start;
        $product_answers_id = substr($key, $start, $length);
        return DB::table('product_answers')->where([['product_answers_id', '=', $product_answers_id],['user_id', '=', Auth::user()->user_id],])->limit(1)->update(['status' => '2']);
    }

    public function getRelatedUserByQuestionId($questionId){
        return self::select('user_id')->where('product_questions_id',$questionId)->groupBy('user_id')->get()->keyBy('user_id')->toArray();
    }

    public function notifyRelatedOwner($arrInput, $anonymous = false) {
        // Get product Review owner
        $objProductQuestion = new ProductQuestion();
        $objProductAnswer = new ProductAnswer();
        $objUser = new User();
        $ansUser = Auth::user();
        if($anonymous){
            $ansUser->first_name = "Somone";
        }
        
        $objNotificationSetting = new NotificationSetting();
        $productQuestionDetail = $objProductQuestion->getProductQuestionRelatedOwnerWithProductDetail($arrInput['product_questions_id']);
        $arrRelatedUser = $objProductAnswer->getRelatedUserByQuestionId($arrInput['product_questions_id']);
        $arrRelatedUser[$productQuestionDetail->user_id] = array("user_id" => $productQuestionDetail->user_id);
        $arrRelatedUser = $this->_processUserId($arrRelatedUser);
        $arrUser = $objUser->getMultipleUser($arrRelatedUser);
        $arrUserNotificationSetting = $objNotificationSetting->getMultipleUserNotificationSetting($arrRelatedUser);
        // Loop all required notify users
        foreach($arrUser as $user){
            if($user['user_id'] !== $ansUser->user_id) {
                if(isset($arrUserNotificationSetting[$user['user_id']])){
                    if($arrUserNotificationSetting[$user['user_id']]['notify_question'])
                    {
                        $notifyMessage = $ansUser->first_name." has comment on ".$productQuestionDetail->product_service_title;
                        $user->notify(new NotificationNewAnswer($arrInput['url'], $ansUser->first_name, $notifyMessage));    
                    }
                } else {
                    $notifyMessage = $ansUser->first_name."  has comment on ".$productQuestionDetail->product_service_title;
                    $user->notify(new NotificationNewAnswer($arrInput['url'], $ansUser->first_name, $notifyMessage));
                }
            }
        }
    }


    private function _processUserId($arr) {
        $newArr = array();
        foreach($arr as $a => $v) {
            $newArr[$a] = $a;
        }
        return $newArr;
    }
}
