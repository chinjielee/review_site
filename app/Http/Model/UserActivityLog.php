<?php
namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Auth;
use DB;

class UserActivityLog extends Model
{
    //
    const ACTIVITY_TYPE_SIGNUP = 1;
    const ACTIVITY_TYPE_LOGIN = 2;
    const ACTIVITY_TYPE_LOGOUT = 3;
    const ACTIVITY_TYPE_UPVOTE = 4;
    const ACTIVITY_TYPE_DOWNVOTE = 5;
    const ACTIVITY_TYPE_REVIEW = 6;
    const ACTIVITY_TYPE_QUESTION = 7;
    const ACTIVITY_TYPE_ANSWER = 8;

    protected $table = 'activity_log';
    protected $primaryKey = 'activity_log_id';
    public $timestamps = true;
    protected $fillable = array(
        'user_id',
        'link',
        'type',
        'remark',
        'created_at',
        'updated_at'
    );

    public function getUserActivityLogByUserId($userId)
    {
        return self::select(DB::raw('DATE_FORMAT(created_at, "%m/%d/%Y") as date'),DB::raw('DATE_FORMAT(created_at, "%H:%i:%s") as time'),'link','remark','type')->where('user_id', '=', $userId)->orderBy('created_at','desc')->get();
    }

    public function insertUserActivityLog($arrInput)
    {
        return self::create($arrInput);
    }

    public function logUserSignup($arrInput)
    {
        $arrInput['type'] = self::ACTIVITY_TYPE_SIGNUP;
        return self::create($arrInput);
    }

    public function logUserLogin($arrInput)
    {
        $arrInput['user_id'] = Auth::user()->user_id;
        $arrInput['type'] = self::ACTIVITY_TYPE_LOGIN;
        return self::create($arrInput);

    }

    public function logUserLogout($arrInput)
    {
        if(isset(Auth::user()->user_id) && !empty(Auth::user()->user_id)){
            $arrInput['user_id'] = Auth::user()->user_id;
        }else{
            $arrInput['user_id'] = 0;
        }
        $arrInput['type'] = self::ACTIVITY_TYPE_LOGOUT;
        return self::create($arrInput);
    }

    public function logUserDownVote($arrInput)
    {
        $arrInput['user_id'] = Auth::user()->user_id;
        $arrInput['type'] = self::ACTIVITY_TYPE_DOWNVOTE;
        return self::create($arrInput);

    }

    public function logUserUpVote($arrInput)
    {
        $arrInput['user_id'] = Auth::user()->user_id;
        $arrInput['type'] = self::ACTIVITY_TYPE_UPVOTE;
        return self::create($arrInput);

    }

    public function logUserReview($arrInput)
    {
        $arrInput['user_id'] = Auth::user()->user_id;
        $arrInput['type'] = self::ACTIVITY_TYPE_REVIEW;
        return self::create($arrInput);

    }

    public function logUserPostQuestion($arrInput)
    {
        $arrInput['user_id'] = Auth::user()->user_id;
        $arrInput['type'] = self::ACTIVITY_TYPE_QUESTION;
        return self::create($arrInput);

    }

    public function logUserAnsQuestion($arrInput)
    {
        $arrInput['user_id'] = Auth::user()->user_id;
        $arrInput['type'] = self::ACTIVITY_TYPE_ANSWER;
        return self::create($arrInput);

    }
}
