<?php

namespace App\Http\Model;
use Auth;
use DB;

use Illuminate\Database\Eloquent\Model;
use App\Http\Model\User;
use App\Http\Model\ProductService;
use Carbon\Carbon;

class ProductReview extends Model
{
    //
    const STATUS_PUBLISHED = 1;
    const STATUS_CANCELLED = 2;
    const STATUS_BLOCKED = 3;
    
    const INVOICE_IMAGE_PATH = 'images/invoice';
    const REVIEW_IMAGE_PATH = 'images/review';

    protected $table = 'product_reviews';
    protected $primaryKey = 'product_reviews_id';
    public $timestamps = true;
    protected $fillable = array(
        'product_services_id',
        'temp_product_services_id',
        'review_title',
        'comments',
        'key_highlight',
        'review_image',
        'up_vote',
        'down_vote',
        'rating',
        'anonymous',
        'purchased_from',
        'purchased_date',
        'invoice_image',
        'created_at',
        'updated_at'
    );

    public function product() {
        return $this->hasOne('App\Http\Model\ProductService', 'product_service_id', 'product_service_id');
    }
    
    public function user(){
        return $this->belongsTo('App\Http\Model\User', 'user_id', 'user_id');
    }


    public function columnMapping($arrInput) {
        $returnInput = array();
        $returnInput['product_services_id'] = isset($arrInput['product'])?$arrInput['product'] :0;
        $returnInput['temp_product_services_id'] = isset($arrInput['temp_product'])?$arrInput['temp_product'] :0;
        $returnInput['review_title'] = isset($arrInput['title'])?$arrInput['title'] :"";
        $returnInput['comments'] = isset($arrInput['detail'])?$arrInput['detail'] :"";
        $returnInput['key_highlight'] = isset($arrInput['highlight'])?$arrInput['highlight'] :"";
        $returnInput['review_image'] = isset($arrInput['product_image'])?$arrInput['product_image'] :"";
        $returnInput['up_vote'] = isset($arrInput['up_vote'])?$arrInput[''] :0;
        $returnInput['down_vote'] = isset($arrInput['down_vote'])? $arrInput['down_vote'] :0;
        $returnInput['rating'] = isset($arrInput['rating'])? $arrInput['rating'] :"";
        $returnInput['anonymous'] = isset($arrInput['anonymous'])? $arrInput['anonymous'] :0;
        $returnInput['purchased_from'] = isset($arrInput['purchase_at'])? $arrInput['purchase_at'] :"";
        $returnInput['purchased_date'] = isset($arrInput['dop'])? Carbon::createFromFormat('m/d/Y', $arrInput['dop'])->toDateTimeString() :"";
        $returnInput['invoice_image'] = isset($arrInput['invoice_image'])? $arrInput['invoice_image'] :"";
        $returnInput['anonymous'] = !empty($arrInput['anonymous'])? 1 : 0;
        $returnInput['created_at'] = Carbon::now();
        $returnInput['updated_at'] = Carbon::now();
        return $returnInput;
    }

    public function create($arrInput) {
        return self::insertGetId($arrInput);
    }

    /**
    * Save and move to saved images from temp storage.
    *
    * @param Request $request
    */
    public function reviewImageSave($userId, $reviewId)
    {
        if(empty($userId) || empty($reviewId)) {
             return false;
        } else {
            $invoice_path = public_path(self::INVOICE_IMAGE_PATH);
            $review_path = public_path(self::REVIEW_IMAGE_PATH);
            $displayReviewId = base64_encode($reviewId);
            $des_invoice_path = $invoice_path."/".$displayReviewId."/invoice";
            $des_review_path = $review_path."/".$displayReviewId."/review";
            $temp_invoice_path = $invoice_path."/".$userId."/temp/invoice";
            $temp_review_path = $review_path."/".$userId."/temp/review";
            $strInvoiceImage = '';
            $strReviewImage = '';
            if (file_exists($temp_invoice_path)) {
                foreach (scandir($temp_invoice_path) as $file) {
                    if ('.' === $file) continue;
                    if ('..' === $file) continue;
                    $strInvoiceImage .= ",".$file;
                    // Moving Directory
                    if (!is_dir($des_invoice_path)) {
                        mkdir($des_invoice_path, 0777, true);
                    }
                    rename($temp_invoice_path."/".$file,$des_invoice_path."/".$file);
                }
            }
            if (file_exists($temp_review_path)) {
                foreach (scandir($temp_review_path) as $file) {
                    if ('.' === $file) continue;
                    if ('..' === $file) continue;
                    $strReviewImage .= ",".$file;
                    // Moving Directory
                    if (!is_dir($des_review_path)) {
                        mkdir($des_review_path, 0777, true);
                    }
                    rename($temp_review_path."/".$file,$des_review_path."/".$file);
                }
            }

            // Update Product Review Image
            $strInvoiceImage = substr($strInvoiceImage,1);
            $strReviewImage = substr($strReviewImage,1);
            return $this->updateImagePath($strInvoiceImage, $strReviewImage, $reviewId);
         }
    }

    private function updateImagePath($strInvoiceImage, $strReviewImage, $reviewId) {
        $reviewRow = self::find($reviewId);
        $reviewRow->invoice_image = $strInvoiceImage;
        $reviewRow->review_image = $strReviewImage;
        return $reviewRow->save();
    }

    public function getCountReviewByCurrentUser(){
        return self::where('user_id', '=', Auth::user()->user_id)->count();
    }

    public function getReviewByCurrentUser(){
        return self::select(DB::raw('DATE_FORMAT(product_reviews.created_at, "%m/%d/%Y") as date'),'product_reviews.status','product_services.product_service_title','product_reviews.review_title','product_reviews.rating','product_reviews.product_reviews_id','product_reviews.product_services_id','product_reviews.temp_product_services_id','product_reviews.user_id','product_services.product_slug','product_services.product_services_id')->leftjoin('product_services', 'product_reviews.product_services_id', '=', 'product_services.product_services_id')->where([['product_reviews.user_id', '=', Auth::user()->user_id],['product_reviews.created_at', '>=',DB::raw("DATE_SUB(NOW(),INTERVAL 5 YEAR)")]])->get();
    }

    public function deleteReviewByCurrentUser($key){
        $start = strpos($key,"-1")+2;
        $end = strrpos($key,"-");
        $length = $end - $start;
        $product_reviews_id = substr($key, $start, $length);
        return DB::table('product_reviews')->where([['product_reviews_id', '=', $product_reviews_id],['user_id', '=', Auth::user()->user_id],])->limit(1)->update(['status' => '2']);
    }

    public function updateCount($reviewId, $count){
        $reviewRow = self::find($reviewId);
        $reviewRow->up_vote = $count->up;
        $reviewRow->down_vote = $count->down;
        return $reviewRow->save();
    }

    public function getProductReviewStat($product_service_id){
        return self::selectRaw('count(product_reviews_id) as total, CONCAT(MONTH(`created_at`),"-",YEAR(`created_at`)) as `key` ')
                ->where('product_services_id',$product_service_id)
                ->groupBy('key')->get();
    }

    public function getProductReviewRatingStat($product_service_id){
        return self::select('rating')->where('product_services_id', $product_service_id)->get();
    }

    public function getProductReviewOwner($product_review_id){
        return self::select('user_id')->where('product_reviews_id', $product_review_id)->first();
    }

    public function getProductReviewOwnerWithProductDetail($product_review_id)
    {
        return self::select('product_reviews.user_id as user_id','product_services.product_service_title as product_service_title')->join('product_services','product_reviews.product_services_id','product_services.product_services_id')->where('product_reviews_id', $product_review_id)->first();
    }
}
