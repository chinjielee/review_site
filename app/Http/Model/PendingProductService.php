<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class PendingProductService extends Model
{
    const STATUS_PENDING = 1;
    const STATUS_APPROVE = 2;
    const STATUS_CANCEL = 3;
    const STATUS_REJECT = 4;
    //
    protected $table = 'pending_product_service';
    protected $primaryKey = 'pending_product_service_id';
    public $timestamps = true;
    protected $fillable = array(
        'product_name',
        'category_id',
        'category_name',
        'brand_id',
        'brand_name',
        'product_attributes',
        'created_at',
        'updated_at'
    );

    public function create($request){
        return self::insertGetId($request);
    }

    public function getProductDetailsBySlugWithFullCategoryByTempId($id){
        return self::select('pending_product_service.*', 'c.*', 'c1.categories_id as parent_cat_1_id','c2.categories_id as parent_cat_2_id','c1.category_slug as parent_cat_1', 'c2.category_slug as parent_cat_2')
            ->leftjoin('categories as c', 'pending_product_service.category_id', '=', 'c.categories_id')
            ->leftjoin('categories as c1','c.parent_category_id', '=', 'c1.categories_id')
            ->leftjoin('categories as c2', 'c1.parent_category_id', '=', 'c2.categories_id')
            ->where('pending_product_service_id', '=', $id)
            ->first();
    }

    public function getProductReviewDetailsByTempId($id){
        return self::select('pending_product_service.*', 'r.*')
            ->join('product_reviews as r', 'pending_product_service.pending_product_service_id', '=', 'r.temp_product_services_id')
            ->where('pending_product_service_id', '=', $id)
            ->first();
    }
}
