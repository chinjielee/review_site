<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class Conversation extends Model
{
    //
    protected $table = 'conversation';
    protected $primaryKey = 'conversation_id';
    protected $timestamp = true;
    protected $fillable = [
        'conversation_name',
        'user_id',
        'created_at',
        'updated_at'
    ];
    
    public function message(){
        return $this->hasMany('App\Http\Model\ConversationMessage', 'conversation_id', 'conversation_id')->orderBy('created_at','DESC');
    }

    public function lastMessage(){
        return $this->hasMany('App\Http\Model\ConversationMessage', 'conversation_id', 'conversation_id')->orderBy('created_at','DESC')->first();
    }

    public function createConversation($user_id) {
        $insertArray = array();
        $insertArray['user_id'] = $user_id;
        $insertArray['conversation_name'] = "Testing Name";
        return self::create($insertArray)->conversation_id;
}

    public function getConversationOwner($channel_id) {
        return self::select('user_id')->where('conversation_id', $channel_id)->first();
    }

}
