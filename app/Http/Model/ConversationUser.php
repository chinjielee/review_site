<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Model\Conversation;
use Carbon\Carbon;
class ConversationUser extends Model
{
    //
    protected $table = 'conversations_user';
    protected $primaryKey = 'conversations_user_id';
    public $timestamps = true;
    protected $fillable = array(
            'conversation_id',
            'user_id',
            'created_at',
            'updated_at'
        );
    
    public function conversation($cid) {
        return self::where('conversation_id', $cid);
    }

    public function lastMessage($cid) {
        return $this->hasMany('App\Http\Model\ConversationMessage', 'conversation_id', 'conversation_id')->orderBy('created_at','DESC')->first();
    }

    public function getConversationByUser($arrUser, $owner_id) {
        $countUser = count($arrUser);
        // SELECT conversation_id FROM `conversations_user` WHERE user_id IN (1,2) GROUP BY conversation_id HAVING COUNT(conversation_id) =2
        $conversation_id = self::select('conversation_id')->whereIn('user_id',$arrUser)->groupby('conversation_id')->havingRaw("COUNT(conversation_id) = ".$countUser)->get();
        if(!isset($conversation_id[0])){
            // Create new conversation with user_Id
            $objConversation = new Conversation();
            $conversation_id = $objConversation->createConversation($owner_id);
            // Insert User
            $insertUser = array();
            foreach($arrUser as $user_id){
                array_push($insertUser, array('conversation_id'=>$conversation_id , 'user_id' => $user_id,'created_at'=>Carbon::now()->toDateTimeString(), 'updated_at'=>Carbon::now()->toDateTimeString()));
            }
            self::insert($insertUser);
        } else {
            $conversation_id = $conversation_id[0]->conversation_id;
        }

        return $conversation_id;
    }
    
    public function getUserConversationList($user_id){
        return self::join('conversation', 'conversation.conversation_id', '=', 'conversations_user.conversation_id')
                ->whereIn('conversations_user.user_id',[$user_id])
                ->distinct('conversation.conversation_id')
                ->get(); 
    }

    public function getTargetUser($channel_id, $current_user_id) {
        return self::select(['users.first_name','users.last_name'])
                    ->join('users', 'users.user_id', '=', 'conversations_user.user_id')
                    ->whereNotIn('conversations_user.user_id',[$current_user_id])
                    ->where('conversations_user.conversation_id','=',$channel_id)
                    ->first();
    }

    public function getMultiConversationTargetUser($channel_id, $owner_id) {
        return self::join('users', 'users.user_id', '=', 'conversations_user.user_id')
                    ->whereNotIn('conversations_user.user_id', [$owner_id])
                    ->whereIn('conversations_user.conversation_id',$channel_id)
                    ->groupBy('conversations_user.conversation_id')
                    ->get();
    }

    public function getDetail($arrConversation, $userId) {
        $strConversationId = "";
        foreach($arrConversation as $conv) {
            $strConversationId .= $conv->conversation_id.",";
        }        
        $strConversationId = rtrim($strConversationId,',');
        $arrConversationId = explode(',',$strConversationId);
        // Get Conversation TargetUser 
        $arrTargetUserConversation = $this->getMultiConversationTargetUser($arrConversationId, $userId);
        return $arrTargetUserConversation;
    }


}
