<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class RatingOption extends Model
{
    //Set the table name
    protected $table = 'rating_option';
    protected $primaryKey = 'rating_option_id';
    public $timestamps = true;
    protected $fillable = array(
        'category_id',
        'type',
        'option_json',
        'created_at',
        'updated_at'
    );

    public function getRatingOption($arrCategory, $type='') {
        // return default general rating
        if(empty($type) && empty($arrCategory)) {
            return self::where('type','=','general')->join('categories','categories.categories_id','=','category_id')->orderBy('categories.level', 'desc')->get();
        } else {
            if(!empty($type)){
                return self::where('type','=',$type)->join('categories','categories.categories_id','=','category_id')->orderBy('categories.level', 'desc')->get();
            }
    
            if(!empty($arrCategory)){
                $objRateOption =  self::whereIn('category_id', $arrCategory)->join('categories','categories.categories_id','=','category_id')->orderBy('categories.level', 'desc')->get();
            } else {
                $objRateOption =  self::join('categories','categories.categories_id','=','category_id')->orderBy('categories.level', 'desc')->get();   
            }
        }
        // If Does not match any option
        if (empty($objRateOption) || $objRateOption->count() <= 0 ) {
            return self::where('type','=','general')->join('categories','categories.categories_id','=','category_id')->orderBy('categories.level', 'desc')->get();
        } else {
            return $objRateOption;
        }
    }

    public function editRatingOption($ratingOptionId, $arrInput) {
        $objRating = self::where('rating_option_id','=',$ratingId)->first();
        // If Else Which case need change
        //$objRating->
    }


    public function editRatingOptionCategory($ratingOptionId, $arrInput) {
        $objRating = self::where('rating_option_id','=',$ratingId)->first();
        // If Else Which case need change
        //$objRating->
    }

    public function create($arrInput) {
        return self::create($arrInput);
    }

    public function createGetId($arrInput){
        return self::insertGetId($arrInput);
    }
}
