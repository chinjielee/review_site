<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class DiscussLog extends Model
{
    //
    protected $table = 'discuss_log';
    protected $primaryKey = 'discuss_log_id';
    protected $timestamp = true;
    protected $fillable = [
        'product_service_id',
        'product_service_title',
        'categories_id',
        'day_month_year',
        'created_at',
        'updated_at'
    ];


    public function logDiscussReview($productDetails) {
        $objToBeInsert = array();
        $objToBeInsert['product_service_id'] = $productDetails->product_services_id;
        $objToBeInsert['product_service_title'] = $productDetails->product_service_title;
        $objToBeInsert['categories_id']= $productDetails->category_id;
        $objToBeInsert['day_month_year'] = Carbon::now()->format('d_m_Y');
        return self::create($objToBeInsert);
    }

    
}
