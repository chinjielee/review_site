<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class FeatureImage extends Model
{
    //
    protected $fillable = [
        'details'
    ];
    protected $table = 'feature_image';
    protected $primaryKey = 'feature_image_id';
    public $timestamps = true;

    public function getFeatureImage() {
        return self::select('details')->find(self::max('feature_image_id'));
    }
}
