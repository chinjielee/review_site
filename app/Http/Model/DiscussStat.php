<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class DiscussStat extends Model
{
    //

    public function getTopDiscussProduct() {
        //SELECT *,COUNT(`discuss_log_id`) as cc FROM discuss_log GROUP BY product_service_id ORDER BY cc DESC LIMIT 15 
        return self::raw("SELECT *,COUNT(`discuss_log_id`) as cc FROM discuss_log GROUP BY categories_id ORDER BY cc DESC LIMIT 15");
    }
}
