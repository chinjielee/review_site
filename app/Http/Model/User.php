<?php

namespace App\Http\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NotificationChannels\WebPush\HasPushSubscriptions;
class User extends Authenticatable
{

    use Notifiable,HasPushSubscriptions;
    const HARDCODE_ADMIN_USER_ID = 10;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','mobile_code','mobile', 'gender', 'dob', 'country','email_token'
    ];
    protected $primaryKey = 'user_id';
    protected $table = 'users';

    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    # --------------- Relationships ------------------ #
    public function questions(){
        return $this->hasMany('App\Http\Model\ProductQuestionAnswer');
    }

    public function answers()
    {
        return $this->hasMany('App\Http\Model\ProductQuestionAnswer');
    }

    public function reviews()
    {
        return $this->hasMany('App\Http\Model\ProductReview');
    }

    public function notifySetting($userId) 
    {
        return $this->hasOne('App\Http\Model\NotificationSetting','user_id','user_id')->find($userId);
    }

    public function getFullNameAttribute($value)
    {
        return $this->attributes['first_name']." ".$this->attributes['last_name'];
    }

    public function getUser($userId){
        return $this->where('user_id','=',$userId)->first();
    }

    public function getMultipleUser($arrUserId)
    {
        return $this->whereIn('user_id', $arrUserId)->get();
    }
}
