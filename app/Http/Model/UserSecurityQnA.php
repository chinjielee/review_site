<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class UserSecurityQnA extends Model
{

    ////Set the table name
    protected $table = 'user_security_qna';
    protected $primaryKey = 'user_security_qna_id';
    public $timestamps = true;
    protected $fillable = array(
        'user_id',
        'questions_id',
        'answer',
        'created_at',
        'updated_at'
    );
    
}
