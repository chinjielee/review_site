<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Http\Model\Categories;

class StatMostDiscuss extends Model
{
    //Set the table name
    protected $table = 'stat_most_discuss';
    protected $primaryKey = 'stat_most_discuss_id';
    protected $timestamp = true;
    protected $fillable = [
        'product_service_id',
        'product_service_title',
        'categories_id',
        'day_month_year',
        'created_at',
        'updated_at'
    ];

    // Get Most Discuss
    public function getMostDiscuss() {
        return self::groupBy('product_service_id')->take(12)->get();
    }

    public function getMostDiscussByProduct() {
        $objProductService = new ProductService();
        $arrMostDiscussProduct = self::groupBy('product_service_id')->take(12)->get();
        $arrMostDiscussProduct = $arrMostDiscussProduct->keyBy('product_service_id');
        $arrProductId = array_column($arrMostDiscussProduct->toArray(),'product_service_id');
        $arrProductDetails = $objProductService->getProductDetailsByIdWithFullCategory($arrProductId);
        // foreach($arrProductDetails as $product) {
            // $product['url'] = $objProductService->getProductDetailUrl($product);
            // $product['url'] = "";
        // }
        return $arrProductDetails;
    }

    public function getMostDiscussByCategory() {
        $arrTopStatList = self::groupBy('categories_id')->take(50)->get();
        // Seperate by product and Category 
        $objCat = new Categories();
        $allCatCollection = $objCat->getCategoryGroupByLabel();
        $serviceList = $productList = array();
        $productCatList = array_column($allCatCollection['product']->toArray(),'categories_id');
        $serviceCatList = array_column($allCatCollection['service']->toArray(),'categories_id');
        foreach($arrTopStatList as $statLis => $item){
            if(in_array($item->categories_id,$productCatList)) {
                array_push($productList, $item);
            } 
            if(in_array($item->categories_id,$serviceCatList)){
                array_push($serviceList, $item);
            }
        }
        $arrAllCat = array_merge($productList,$serviceList);
        $arrCatId = array_column($arrAllCat,'categories_id');
        $catDetail = $objCat->getCategoriesDetail($arrCatId)->keyBy('categories_id')->toArray();
        return array($productList,$serviceList,$catDetail);
    }
}
