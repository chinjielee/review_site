<?php

namespace App\Http\Model;
use DB;

use Illuminate\Database\Eloquent\Model;

class NotificationSetting extends Model
{
    protected $fillable = [
        'user_id','notify_review', 'notify_comment', 'notify_question'
    ];
    protected $table = 'notification_setting';
    protected $primaryKey = 'notification_setting_id';
    public $timestamps = true;

    public function getNotificationSettingByUserId($userId)
    {
        $result = self::where('user_id', '=', $userId)->limit(1)->get();
        if ($result->count()) {
            return $result[0];
        }else{
            $newUser = NotificationSetting::create([
                'user_id' => $userId,
                'notify_review' => '1',
                'notify_comment' => '1',
                'notify_question' => '1'
            ]);
            $newUser->save();
            return $newUser;
        }
    }

    public function UpdateNotificationSetting($userId,$notify_review,$notify_comment,$notify_question)
    {
        $result = self::where('user_id', '=', $userId)->limit(1)->update(['notify_review' => $notify_review,'notify_comment' => $notify_comment,'notify_question' =>$notify_question]);
    }

    public function getUserNotificationSetting($userId)
    {
        return self::where('user_id', $userId)->first();
    }
    public function getMultipleUserNotificationSetting($arrUserId){
        return self::whereIn('user_id', $arrUserId)->get()->keyBy('user_id')->toArray();
    }
}
