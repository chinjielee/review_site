<?php
namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class SpamFilter extends Model
{
    //
     ////Set the table name
    protected $table = 'spam_filter';
    protected $primaryKey = 'spam_filter_id';
    public $timestamps = true;
    protected $fillable = array(
        'text',
        'rate',
        'type',
        'updated_at',
        'created_at'
    );

    private function getFullSpamList()
    {
        return self::select('text')->where('status','1')->get();
    }

    public function isOffensiveWord($str)
    {
        $arrSpamList = $this->getFullSpamList();
        $strSpamList = implode('|', $arrSpamList->pluck('text')->toArray());
        if (preg_match("~" . strtoupper($strSpamList) . "~", strtoupper($str), $matches)) {
            if (sizeof($matches) > 0) {
                return true;
            } else {
                return false;
            }
        }

    }
}
