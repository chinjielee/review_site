<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    const IMAGE_PATH = 'images/categories';
    const DEFAULT_IMAGE = 'images/default.jpg';
    const MAX_LIST_PER_LABEL = 100;
    const PRODUCT_ID = 1;
    const SERVICE_ID = 2;
    const MOVIE_ID = 3;

    const STATUS_ACTIVE = 1;
    const STATUS_DEACTIVE = 0;

    const CATEGORY_TOP_BRAND = 5;
    //Set the table name
    protected $table = 'categories';
    protected $primaryKey = 'categories_id';
    public $timestamps = true;
    protected $fillable = array(
            'categories_name',
            'categories_description',
            'image',
            'level',
            'created_at',
            'updated_at'
        );
    //  ------------------   Relationship  -------------------
    public function products()
    {
        return $this->hasMany('App\Http\Model\ProductService','category_id','categories_id');
    }

    // public function productBrands($categoryId){
    //     // return $this->hasManyThrough(
    //     //     'App\Post',
    //     //     'App\User',
    //     //     'country_id', // Foreign key on users table...
    //     //     'user_id', // Foreign key on posts table...
    //     //     'id', // Local key on countries table...
    //     //     'id' // Local key on users table...
    //     // );
    //     return $this->belongsToManyThrough(
    //         'App\Http\Model\Brands',
    //         'App\Http\Model\ProductService', 
    //         'category_id',
    //         'brands_id',
    //         'categories_id',
    //         'brands_id')->where('categories_id', $categoryId)->get();
    // }

    public function withProductCount($categoryId){
        $result = self::withCount([
            'products',
            'products AS products'
        ])->where('parent_category_id', $categoryId)
        ->where('status',self::STATUS_ACTIVE);
        return $result;
    }

    public function getTopLevelCategories() {
        return self::withCount([
            'products',
            'products AS products'
        ])->where('level','1')
        ->where('status',self::STATUS_ACTIVE)
        ->get();
    }

    public function getTopLevelCategoriesStat() {
        return self::where('level','1')
        ->where('status',self::STATUS_ACTIVE)
        ->get();
    }

    public function getChildLevelCategories($category_id) {
        return self::where('parent_category_id',$category_id)
        ->where('status',self::STATUS_ACTIVE)
        ->get();
    }

    public static function getCategoryIdBySlug($categorySlug) {
        return self::select('categories_id','categories_name','level')->where('category_slug',$categorySlug)
        ->where('status',self::STATUS_ACTIVE)
        ->get();
    }

    public function getCategoryListForForm() {
        return self::whereIn('level',array(1,2))
        ->where('status','1')
        ->get();
    }

    public function getCategoryListForFormWithMovie(){
        return self::whereIn('level',array(1,2))->orWhere('categories_name','=','movie')
        ->where('status',self::STATUS_ACTIVE)
        ->get();
    }

    public function getCategoriesDetail($arrIds = array()){
        return self::whereIn('categories_id', $arrIds)->get();
    }

    public function getCategoryTopExplorelist()
    {
        $arrCategoryList = array();
        $arrCategoryList['products'] = self::where('parent_category_id', '1')->where('level','1')->where('status',self::STATUS_ACTIVE)->limit(self::MAX_LIST_PER_LABEL)->get();
        $arrCategoryList['services'] = self::where('parent_category_id', '2')->where('level','1')->where('status',self::STATUS_ACTIVE)->limit(self::MAX_LIST_PER_LABEL)->get();
        $arrCategoryList['movies'] = self::where('parent_category_id', '3')->where('level','1')->where('status',self::STATUS_ACTIVE)->limit(self::MAX_LIST_PER_LABEL)->get();
        return $arrCategoryList;
    }

    public function getProductAndServicesCategoriesList(){
        $arrCategoryList = array();
        $arrCategoryList['product'] = self::where('parent_category_id', '1')->where('level','1')->where('status',self::STATUS_ACTIVE)->limit(self::MAX_LIST_PER_LABEL)->get();
        $arrCategoryList['service'] = self::where('parent_category_id', '2')->where('level','1')->where('status',self::STATUS_ACTIVE)->limit(self::MAX_LIST_PER_LABEL)->get();
        return $arrCategoryList;
    }

    public function getCategoryGroupByLabel()
    {
        $arrCategoryList = array();
        // SELECT * FROM categories WHERE categories_id = 1 OR parent_category_id =1 OR parent_category_id IN (SELECT categories_id FROM categories WHERE parent_category_id = 1) ORDER BY `category_slug` ASC
        $arrCategoryList['product'] = self::where('status',self::STATUS_ACTIVE)
            ->where(function($query){
                $query->where('categories_id',self::PRODUCT_ID)
                ->orWhere('parent_category_id', self::PRODUCT_ID)
                ->orWhereIn('parent_category_id',function($query)
                {
                    $query->select('categories_id')
                        ->from('categories')
                        ->where('parent_category_id',self::PRODUCT_ID);
                });
            })
            ->get();    
        $arrCategoryList['service'] = self::where('status',self::STATUS_ACTIVE)
            ->where(function($query){
                $query->where('categories_id',self::SERVICE_ID)
                ->orWhere('parent_category_id', self::SERVICE_ID)
                ->orWhereIn('parent_category_id',function($query)
                {
                    $query->select('categories_id')
                        ->from('categories')
                        ->where('parent_category_id',self::SERVICE_ID);
                });
            })
            ->get();
        $arrCategoryList['movie'] = self::where('status',self::STATUS_ACTIVE)
            ->where(function($query){
                $query->where('categories_id',self::MOVIE_ID)
                ->orWhere('parent_category_id', self::MOVIE_ID)
                ->orWhereIn('parent_category_id',function($query)
                {
                    $query->select('categories_id')
                        ->from('categories')
                        ->where('parent_category_id',self::MOVIE_ID);
                });
            })
            ->get();
        return $arrCategoryList;   
    }

    public function processImage($images,$pid) {
        $returnImages = array();
        if(!empty($images)){
            if(strpos($images,',') > -1) {
                $arrImage = explode(',',$images);
            } else {
                $arrImage[0] = $images;
            }
            $image_path = url(self::IMAGE_PATH);
            $displayProductId = base64_encode($pid);
            $des_image_path = $image_path."/".$displayProductId."/";
            foreach($arrImage as $img){
                if(empty($img)){
                    array_push($returnImages, ['path'=>url(self::DEFAULT_IMAGE)]);
                } else {
                    array_push($returnImages, ['path'=>$des_image_path.$img]);
                }
            }
        }
        
        return $returnImages;
    }
}
