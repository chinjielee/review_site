<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class ConversationMessage extends Model
{
    //
    protected $table = 'conversation_message';
    protected $primaryKey = 'conversation_message_id';
    protected $fillabled = [
        'conversation_name',
        'user_id',
        'created_at',
        'updated_at'
    ];

    public function getChatMesssage($channel_id){
        return self::where('conversation_id',$channel_id)->orderBy('updated_at', 'desc')->limit(20)->get();
    }

    public function getChatMesssageWithUser($channel_id) {
        return self::select('conversation_message.*','users.first_name','users.last_name','users.user_id','users.email','users.mobile')->where('conversation_id',$channel_id)
                ->join('users', 'users.user_id', '=', 'conversation_message.user_id')
                ->orderBy('updated_at','desc')
                ->take(20)->get();
    }

    public function insertMessage($arrInput){
        $arrInput['updated_at'] = Carbon::now();
        $arrInput['created_at'] = Carbon::now();
        return self::insertGetId($arrInput);
    }
}
