<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
class ProductRating extends Model
{
    protected $table = 'product_ratings';
    protected $primaryKey = 'product_rating_id';
    public $timestamps = true;
    protected $fillable = array(
        'product_services_id',
        'user_id',
        'rating',
        'created_at',
        'updated_at'
    );

    public function insertRating($arrInput) {
        $arrInput['created_at'] = Carbon::now();
        $arrInput['updated_at'] = Carbon::now();
        return self::create($arrInput);
    }

    public function createGetId($arrInput){
        return self::insertGetId($arrInput);
    }


    public function reCountTotalRating($productId) {
        $arrRating = (object)array();
        $arrAllProductRating = self::where('product_services_id', $productId)->get();
        $arrNewRatingCal = array();
        $count = 0;
        foreach($arrAllProductRating as $rating) {
            $arrJsonObjRating = json_decode($rating->rating,true);
            foreach($arrJsonObjRating as $key => $val){
                if(isset($arrNewRatingCal[$key]))
                    $arrNewRatingCal[$key] = ($arrNewRatingCal[$key] + $val);
                else 
                    $arrNewRatingCal[$key] = $val;
            }
            $count ++;
        }
        foreach($arrNewRatingCal as $key => $val){
            $arrNewRatingCal[$key] = $arrNewRatingCal[$key] / $count;
        }
        $arrNewRatingCal['count'] = $count;
        return $arrNewRatingCal;
    }

}
