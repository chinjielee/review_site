<?php
namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class ProductService extends Model
{
    const IMAGE_PATH = 'images/product';
    const DEFAULT_IMAGE = 'images/default.jpg';
    const PRODUCT_CATEGORY_SLUG = 'products';
    const SERVICE_CATEGORY_SLUG = 'services';
    const MOVIE_CATEGORY_SLUG = 'movies';

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;
    const STATUS_DRAFT = 3;

    //Set the table name
    protected $table = 'product_services';
    protected $primaryKey = 'product_services_id';
    public $timestamps = true;
    protected $fillable = array(
        'product_service_title',
        'product_service_desc',
        'category_id',
        'product_service_attributes',
        'brand',
        'status',
        'product_rating',
        'created_at',
        'updated_at'
    );

    //  ------------------   Relationship  -------------------
    public function reviews()
    {
        return $this->hasMany('App\Http\Model\ProductReview', 'product_services_id', 'product_services_id')->where('status', 1)->orderByRaw('up_vote - down_vote DESC');
    }

    public function reviewCount()
    {
        return $this->hasMany('App\Http\Model\ProductReview', 'product_services_id', 'product_services_id')->where('status', 1)->count();
    }

    public function reviewUserCount()
    {
        return sizeof($this->hasMany('App\Http\Model\ProductReview', 'product_services_id', 'product_services_id')->where('status', 1)->groupby('user_id')->get());
    }

    public function questions()
    {
        return $this->hasMany('App\Http\Model\ProductQuestion', 'product_services_id', 'product_services_id')->where('status', 1)->orderBy('created_at', 'DESC');
    }

    public function questionCount()
    {
        return $this->hasMany('App\Http\Model\ProductQuestion', 'product_services_id', 'product_services_id')->where('status', 1)->count();
    }

    public function questionAndAnswerCount()
    {
        return $this->hasManyThrough('App\Http\Model\ProductAnswer', 'App\Http\Model\ProductQuestion','product_services_id', 'product_questions_id')->where('product_answers.status', 1)->where('product_questions.status', 1)->count() + $this->hasMany('App\Http\Model\ProductQuestion', 'product_services_id', 'product_services_id')->where('status', 1)->count();
    }

    public function topBrands()
    {
        return $this->hasMany('App\Http\Model\Brands', 'brands_id', 'brands_id')->take(self::CATEGORY_TOP_BRAND)->get();
    }

    public function questionsByUser()
    {
        return $this->hasManyThrough('App\Http\Model\ProductQuestion', 'App\Http\Model\User')->orderByRaw('up_vote - down_vote DESC');
    }

    public function countReview()
    {
        $posts = self::withCount([
            'reviews',
            'reviews AS product_reviews' => function ($query) {
                $query;
            }
        ])->get();
    }
    //  ------------------   Relationship  -------------------
    //
    public function wildcardSearchProduct($keyword, $limit = 30)
    {
        //join( 'attachments as att', DB::raw( 'att.item_id' ), '=', DB::raw( 'items_alias.id' ) )
        return self::select('product_services.*', 'c.*', 'c1.category_slug as parent_cat_1', 'c2.category_slug as parent_cat_2')
            ->leftjoin('categories as c', 'product_services.category_id', '=', 'c.categories_id')
            ->leftjoin('categories as c1', 'c.parent_category_id', '=', 'c1.categories_id')
            ->leftjoin('categories as c2', 'c1.parent_category_id', '=', 'c2.categories_id')
            ->leftjoin('brands as brands', 'product_services.brands_id', '=', 'brands.brands_id')
            ->where('product_services.status','1')
            ->where(function ($query) use ($keyword) {
                $query->where('product_services.product_service_title', 'like', '%' . $keyword . '%')
                ->orWhere('brands.brand_title','like', '%' . $keyword . '%');
            })
            ->limit($limit)
            ->get();
    }
    // SELECT product_services.*,c.*,c2.categories_name as parent_cat_1,c3.categories_name as parent_cat_2 FROM product_services INNER JOIN categories c ON product_services.category_id = c.categories_id LEFT JOIN categories c2 ON c.parent_category_id = c2.categories_id LEFT JOIN categories c3 ON c2.parent_category_id = c3.categories_id

    public function wildcardSearchProductSpec($keyword, $limit)
    {
        return self::where('product_service_attributes', 'LIKE', '%' . $keyword . '%')
            ->limit($limit)
            ->get();
    }

    public function getProductDetailsById($productServiceId)
    {
        return self::where('product_services_id', '=', $productServiceId)
            ->first();
    }

    public function getProductDetailsBySlug($slug)
    {
        return self::where('product_slug', '=', $slug)
            ->first();
    }

    public function getProductDetailsBySlugWithFullCategory($slug)
    {
        return self::select('product_services.*', 'c.*', 'c1.categories_id as parent_cat_1_id', 'c2.categories_id as parent_cat_2_id', 'c1.category_slug as parent_cat_1', 'c2.category_slug as parent_cat_2')
            ->leftjoin('categories as c', 'product_services.category_id', '=', 'c.categories_id')
            ->leftjoin('categories as c1', 'c.parent_category_id', '=', 'c1.categories_id')
            ->leftjoin('categories as c2', 'c1.parent_category_id', '=', 'c2.categories_id')
            ->where('product_slug', '=', $slug)
            ->where('product_services.status',1)
            ->first();
    }

    public function getProductDetailsByIdWithFullCategory($arrIds)
    {
        return self::select('product_services.*', 'c.*', 'c1.categories_id as parent_cat_1_id', 'c2.categories_id as parent_cat_2_id', 'c1.category_slug as parent_cat_1', 'c2.category_slug as parent_cat_2')
            ->leftjoin('categories as c', 'product_services.category_id', '=', 'c.categories_id')
            ->leftjoin('categories as c1', 'c.parent_category_id', '=', 'c1.categories_id')
            ->leftjoin('categories as c2', 'c1.parent_category_id', '=', 'c2.categories_id')
            ->whereIn('product_services_id', $arrIds)
            ->where('product_services.status',1)
            ->get();
    }


    public function getProductFullDetailsBySlug($slug)
    {
        return self::where('product_slug', '=', $slug)
            ->join('categories', 'categories.categories_id', '=', 'product_services.category_id')
            ->first();
    }

    public function getProductDetailUrl($result)
    {
        if (!empty($result)) {
            if ($result->category_slug != NULL and $result->parent_cat_1 != NULL and $result->parent_cat_2 != NULL) {
                return route(ucfirst($result->parent_cat_2) . ' Detail3', [$result->parent_cat_1, $result->category_slug, $result->product_slug]);
            } elseif ($result->category_slug != NULL and $result->parent_cat_1 != NULL) {
                return route(ucfirst($result->parent_cat_1) . ' Detail2', [$result->category_slug, $result->product_slug]);
            } else {
                return route(ucfirst($result->category_slug) . ' Detail1', [$result->product_slug]);
            }
        } else {
            return "";
        }
    }

    public function getAllProductUrl($result)
    {
        if (!empty($result)) {
            $urlList = array();
            if ($result->category_slug != NULL and $result->parent_cat_1 != NULL and $result->parent_cat_2 != NULL) {
                $urlList['detail'] =  route(ucfirst($result->parent_cat_2) . ' Detail3', [$result->parent_cat_1, $result->category_slug, $result->product_slug])."/";
                $urlList['photo'] =  route(ucfirst($result->parent_cat_2) . ' Photos3', [$result->parent_cat_1, $result->category_slug, $result->product_slug])."/";
                $urlList['stat'] =  route(ucfirst($result->parent_cat_2) . ' Stats3', [$result->parent_cat_1, $result->category_slug, $result->product_slug])."/";
                $urlList['price'] =  route(ucfirst($result->parent_cat_2) . ' Prices3', [$result->parent_cat_1, $result->category_slug, $result->product_slug])."/";
                $urlList['qa'] =  route(ucfirst($result->parent_cat_2) . ' Question and Answer3', [$result->parent_cat_1, $result->category_slug, $result->product_slug])."/";
                $urlList['review'] =  route(ucfirst($result->parent_cat_2) . ' Review3', [$result->parent_cat_1, $result->category_slug, $result->product_slug])."/";
            } elseif ($result->category_slug != NULL and $result->parent_cat_1 != NULL) {
                $urlList['detail'] = route(ucfirst($result->parent_cat_1) . ' Detail2', [$result->category_slug, $result->product_slug])."/";
                $urlList['photo'] = route(ucfirst($result->parent_cat_1) . ' Photos2', [$result->category_slug, $result->product_slug])."/";
                $urlList['stat'] = route(ucfirst($result->parent_cat_1) . ' Stats2', [$result->category_slug, $result->product_slug])."/";
                $urlList['price'] = route(ucfirst($result->parent_cat_1) . ' Prices2', [$result->category_slug, $result->product_slug])."/";
                $urlList['qa'] = route(ucfirst($result->parent_cat_1) . ' Question and Answer2', [$result->category_slug, $result->product_slug])."/";
                $urlList['review'] = route(ucfirst($result->parent_cat_1) . ' Review2', [$result->category_slug, $result->product_slug])."/";
            } else {
                $urlList['detail'] = route(ucfirst($result->category_slug) . ' Detail1', [$result->product_slug])."/";
                $urlList['photo'] = route(ucfirst($result->category_slug) . ' Photos1', [$result->product_slug])."/";
                $urlList['stat'] = route(ucfirst($result->category_slug) . ' Stats1', [$result->product_slug])."/";
                $urlList['price'] = route(ucfirst($result->category_slug) . ' Prices1', [$result->product_slug])."/";
                $urlList['qa'] = route(ucfirst($result->category_slug) . ' Question and Answer1', [$result->product_slug])."/";
                $urlList['review'] = route(ucfirst($result->category_slug) . ' Review1', [$result->product_slug])."/";
            }
            return $urlList;
        } else {
            return array();
        }
    }

    public function getTempProductReviewlUrl($temp_product_service_id)
    {
        return route('Products Temp Review', [$temp_product_service_id]);
    }
    public function getProductDetailUrlBySlug($product_slug,$withDetail = false)
    {
        $result = $this->getProductDetailsBySlugWithFullCategory($product_slug);
        if (!empty($result)) {
            if ($result->category_slug != NULL and $result->parent_cat_1 != NULL and $result->parent_cat_2 != NULL) {
                if($withDetail){
                    $result['url'] = route(ucfirst($result->parent_cat_2) . ' Detail3', [$result->parent_cat_1, $result->category_slug, $result->product_slug]);
                    return $result;
                } else {
                    return route(ucfirst($result->parent_cat_2) . ' Detail3', [$result->parent_cat_1, $result->category_slug, $result->product_slug]);
                }
                
            } elseif ($result->category_slug != NULL and $result->parent_cat_1 != NULL) {
                if($withDetail){
                    $result['url'] = route(ucfirst($result->parent_cat_1) . ' Detail2', [$result->category_slug, $result->product_slug]);
                    return $result;
                } else {
                    return route(ucfirst($result->parent_cat_1) . ' Detail2', [$result->category_slug, $result->product_slug]);
                }
            } else {
                if($withDetail){
                    $result['url'] = route(ucfirst($result->category_slug) . ' Detail1', [$result->product_slug]);
                    return $result;
                } else {
                    return route(ucfirst($result->category_slug) . ' Detail1', [$result->product_slug]);
                }
            }
        } else {
            return "";
        }
    }

    public function getProductListFullDetailByCategoryId($categoryId, $arrFilter = array())
    {
        $objReturn  = self::select('product_services.*', 'c.*', 'c1.categories_id as parent_cat_1_id', 'c2.categories_id as parent_cat_2_id', 'c1.category_slug as parent_cat_1', 'c2.category_slug as parent_cat_2')
        ->leftjoin('categories as c', 'product_services.category_id', '=', 'c.categories_id')
        ->leftjoin('categories as c1', 'c.parent_category_id', '=', 'c1.categories_id')
        ->leftjoin('categories as c2', 'c1.parent_category_id', '=', 'c2.categories_id')
        ->where('category_id', $categoryId)
        ->where('product_services.status', self::STATUS_ACTIVE);
        if(!empty($arrFilter)) {
            foreach($arrFilter as $key => $filter){
                $objReturn->where($key,$filter); 
            }
        }
        return $objReturn;
    }

    public function getTopProductByCategory($categoryId)
    {
        return self::where('category_id', $categoryId)->get();
    }

    public function processOverallRatingDisplay($strRating) {
        $objRatingDisplay = (object)array();
        // Set Default rating object
        $objRatingDisplay->overall = 0;
        $objRatingDisplay->rate_count = 0;
        if(!empty($strRating)){
            $jsonDecodeRatingDisplay = json_decode($strRating);
            if(isset($jsonDecodeRatingDisplay->overall)){
                $objRatingDisplay->overall = $jsonDecodeRatingDisplay->overall;
            }
            if(isset($jsonDecodeRatingDisplay->count)){
                $objRatingDisplay->rate_count = $jsonDecodeRatingDisplay->count;
            }
        }
        return $objRatingDisplay;
    }

    public function getTopProductBrandsByCategorisList($arrCategoryList) {
        return self::select('brands.*')->join('brands','product_services.brands_id','=','brands.brands_id')->whereIn('product_services.category_id', $arrCategoryList)->groupby('product_services.brands_id')->orderByRaw('COUNT(product_services.brands_id) DESC')->get();
    }

    public function processImage($images,$pid) {
        $returnImages = array();
        if(!empty($images)){
            if(strpos($images,',') > -1) {
                $arrImage = explode(',',$images);
            } else {
                $arrImage[0] = $images;
            }
            $image_path = url(self::IMAGE_PATH);
            $displayProductId = base64_encode($pid);
            $des_image_path = $image_path."/".$displayProductId."/";
            foreach($arrImage as $img){
                if(empty($img)){
                    array_push($returnImages, ['path'=>url(self::DEFAULT_IMAGE)]);
                } else {
                    array_push($returnImages, ['path'=>$des_image_path.$img]);
                }
            }
        }
        
        return $returnImages;
    }

    public function processUpdateNewRatingData($slug, $rating, $userId) {
        $productDetail = $this->getProductDetailsBySlug($slug);
        if(!empty($productDetail)){
            $ratingDetail = $productDetail->product_rating;
            $objProductRating = new ProductRating();
            if(!empty($ratingDetail)) {
                $objOriRate = json_decode($ratingDetail);
                $objNewRating = json_decode($rating);
                // Insert into rating log
                $objRatingLogDetail = array();
                $objRatingLogDetail['product_services_id'] = $productDetail->product_services_id;
                $objRatingLogDetail['user_id'] = $userId;
                $objRatingLogDetail['rating'] = $rating;
                $objProductRating->insertRating($objRatingLogDetail);
                // ReCount
                if(empty($objOriRate->count)) {
                    $oriCount = 0;
                } else {
                    $oriCount = $objOriRate->count;
                }
                    
                $oriCount = $oriCount+1;
                foreach($objOriRate as $key => $rate){
                    if(isset($objNewRating->$key)){
                        $objNewRating->$key = $objNewRating->$key + $rate;
                    } else {
                        $objNewRating->$key = $rate;
                    }
                }
                foreach($objNewRating as $rateKey => $rateVal){
                    $objNewRating->$rateKey = $rateVal / 2;
                }
                
                $objRating = $objNewRating;
                $objRating->count = $oriCount;
            } else {
                // Double Reupdate to confirm is it empty
                $oriCal = $objProductRating->reCountTotalRating($productDetail->product_services_id);
                $objRatingCal = $oriCal;
                if(!empty($objRatingCal)){
                    $objRating = $objRatingCal;
                } else {
                    $objRating = json_decode($rating); 
                    $objRating->count = 1;
                }
            }
            $strJsonRate = json_encode($objRating);
            $this->_updateNewRate($strJsonRate,$productDetail->product_services_id);
        }
    }

    private function _updateNewRate($rate,$psid) {
        $objProduct = self::find($psid);
        $objProduct->product_rating = $rate;
        $objProduct->save();
    }
}