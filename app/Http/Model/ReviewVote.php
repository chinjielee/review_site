<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;
use App\Notifications\NotificationNewLike;
use App\Notifications\NotificationNewDislike;
use Auth;
class ReviewVote extends Model
{
    use Traits\HasCompositePrimaryKey;
    //Set the table name
    protected $table = 'review_vote';
    protected $primaryKey = array('product_review_id','user_id');
    public $timestamps = true;
    public $incrementing = false;
    protected $fillable = array(
        'product_review_id',
        'user_id',
        'up',
        'down',
        'created_at',
        'updated_at'
    );

    public function updateCreateUp($product_review, $user_id, $url){
        $this->_notifyOwner($product_review, "up", $url);
        return self::updateOrCreate(['product_review_id' => $product_review, 'user_id' => $user_id],['up'=>1,'down' =>0]);
    }

    public function updateCreateDown($product_review, $user_id, $url){
        $this->_notifyOwner($product_review, "down", $url);
        return self::updateOrCreate(['product_review_id' => $product_review, 'user_id' => $user_id],['up'=>0,'down' =>1]);
    }

    public function getReviewVoteByReview($arrReviewIds, $userId) {
        return self::whereIn('product_review_id', $arrReviewIds)->where('user_id', $userId)->get();
    }

    public function countTotalVote($reviewId) 
    {   
        return self::selectRaw('SUM(up) as up, SUM(down) as down')->where('product_review_id', $reviewId)->first();
    }

    private function _notifyOwner($productReview, $type, $url){
        // Get product Review owner
        $objProductReview = new ProductReview();
        $objUser = new User();
        $productReviewDetail = $objProductReview->getProductReviewOwnerWithProductDetail($productReview);
        $rateUser = Auth::user();
        $user = $objUser->getUser($productReviewDetail->user_id);
        if($user->user_id !== $rateUser->user_id){
            $objNotificationSetting = new NotificationSetting();
            
            $userNotificationSetting = $objNotificationSetting->getUserNotificationSetting($user->user_id);
            if(!empty($userNotificationSetting) && $userNotificationSetting->notify_review){
                if($type == "up"){
                    $notifyMessage = $rateUser->first_name." liked your review on ".$productReviewDetail->product_service_title;
                    $user->notify(new NotificationNewLike($url, $rateUser->first_name, $notifyMessage));
                } else {
                    $notifyMessage = $rateUser->first_name." disagreed your review on ".$productReviewDetail->product_service_title;
                    $user->notify(new NotificationNewDislike($url,$rateUser->first_name, $notifyMessage));
                }
            }
        }
    }
}
