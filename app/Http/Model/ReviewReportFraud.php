<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class ReviewReportFraud extends Model
{
    //
    const STATUS_PENDING = 1;
    //Set the table name
    protected $table = 'review_report_fraud';
    protected $primaryKey = 'review_report_fraud_id';
    public $timestamps = true;
    protected $fillable = array(
        'product_review_id',
        'user_id',
        'status',
        'created_at',
        'updated_at'
    );

    public function reportFraud($product_review, $user_id, $status){
        return self::updateOrCreate(['product_review_id' => $product_review, 'user_id' => $user_id],['status'=> $status]);
    }
    
    public function getReviewFraudByReview($arrReviewIds, $userId) {
        return self::whereIn('product_review_id', $arrReviewIds)->where('user_id', $userId)->where('status','1')->get();
    }
}
