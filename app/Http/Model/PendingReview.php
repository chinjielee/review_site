<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class PendingReview extends Model
{
    //
    protected $table = 'pending_review';
    protected $primaryKey = 'pending_review_id';
    public $timestamps = true;
    protected $fillable = array(
        'user_id',
        'review_title',
        'review_detail',
        'rating',
        'review_image',
        'country',
        'key_highlight',
        'purchased_date',
        'purchased_from',
        'invoice_image',
        'created_at',
        'updated_at'
    );

}
