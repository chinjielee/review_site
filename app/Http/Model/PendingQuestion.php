<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class PendingQuestion extends Model
{
    //
    protected $table = 'pending_question';
    protected $primaryKey = 'pending_question_id';
    public $timestamps = true;

    public function getPendingProductQuestionByUser($product_service_id, $user_id) {
        return $this->where('user_id','=', $user_id)
                ->where('product_services_id','=',$product_service_id)
                ->get();
    }
}
