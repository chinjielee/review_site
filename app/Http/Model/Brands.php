<?php

namespace App\Http\Model;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    //
    protected $table = 'brands';
    protected $primaryKey = 'brands_id';
    public $timestamps = true;
    protected $fillable = array(
            'brand_title',
            'brand_info',
            'images',
            'created_at',
            'updated_at'
        );
    //  ------------------   Relationship  -------------------
    public function products()
    {
        return $this->hasMany('App\Http\Model\ProductService','brands_id','brands_id');
    }

    public function withProductCount($brandId){
        $result = self::withCount([
            'products',
            'products AS products'
        ])->where('brands_id', $brandId)->get();
        return $result;
    }

    public function getAllBrandsNameKeyById(){
        return self::all()->pluck('brand_title','brands_id');
    }

    public function getMultipleBrandDetail($arrBrandsId){
        return self::whereIn('brands_id', $arrBrandsId)->pluck('brand_title','brands_id');
    }

}
