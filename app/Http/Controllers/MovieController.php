<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MovieController extends Controller
{
    //
    public function index()
    {
        return view('movies.index');
    }

    public function detail($cat1,$cat2,$product)
    {
        return view('movies.detail');
    }

    public function review()
    {
        return view('movies.review');
    }

    public function qa()
    {
        return view('movies.qa');
    }

}
