<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\ProductService;
use App\Notifications\NotificationTest;

class SearchController extends Controller
{
    //
    public function search($keyword,Request $request) {
        $objProductService = new ProductService();
        $arrSearchResult = $objProductService->wildcardSearchProduct($keyword);
        foreach($arrSearchResult as $result){
            $arrImages = $objProductService->processImage($result->images,$result->product_services_id);
            if(!empty($arrImages)) {
                $result->images = $arrImages[0]['path'];
            } else {
                $result->images = "";
            }
            if(!empty($result->product_rating)){
                $ratingObj = json_decode($result->product_rating);
                if(isset($ratingObj->overall)){
                    $result->overall_rating = $ratingObj->overall;
                } else {
                    $result->overall_rating = 0;    
                }
                
            } else {
                $result->overall_rating = 0;    
            }
            
        }
        // $request->user()->notify(new NotificationTest);
        // $user = $request->user();
        // // Limit the number of returned notifications, or return all
        // $query = $user->unreadNotifications();
        // $limit = (int) $request->input('limit', 0);
        // if ($limit) {
        //     $query = $query->limit($limit);
        // }

        // $notifications = $query->get()->each(function ($n) {
        //     $n->created = $n->created_at->toIso8601String();
        // });

        // $total = $user->unreadNotifications->count();
        return view('review.search',compact('arrSearchResult','keyword'));
    }
}
