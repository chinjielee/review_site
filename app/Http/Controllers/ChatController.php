<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Chat;
use App\Http\Model\Conversation;
use App\Http\Model\ConversationUser;
use App\Http\Model\ConversationMessage;

class ChatController extends Controller
{
    //
    public function __construct()
    {
      $this->middleware('auth');
    }

    public function getChatMessages(Request $request, $channel_id) {
        $user = Auth::user();
        $current_user_id =$user->user_id;
        $pageInfo = (object)array();
        $pageInfo->page = "Messenger";
        $objConversationDetail = new Conversation();
        $owner_id = $objConversationDetail->getConversationOwner($channel_id);
        $objConversationUserDetail = new ConversationUser();
        if(!empty($owner_id)){
            $target_name = $objConversationUserDetail->getTargetUser($channel_id, $current_user_id);
            $target_name = $this->_processName($target_name);
        } else {
            $target_name = "Unknown";
        }
        $objConversationMessege = new ConversationMessage();
        $arrMessage = $objConversationMessege->getChatMesssageWithUser($channel_id);
        $arrMessage = $arrMessage->reverse();
        $user = Auth::user();
        $displayname = $user->last_name." ".$user->first_name;
        // Get All Conversation Message by message timestamp
        return view('profile.messenger-details',compact('pageInfo','arrMessage','owner_id','target_name','channel_id','displayname'));
    }

    public function postCreateChat(Request $request) {
        $user = Auth::user();
        $arrChatUser = [$request->tu,$user->user_id];
        $objConversationUser = new ConversationUser();
        $conversation = $objConversationUser->getConversationByUser($arrChatUser, $user->user_id);
        return redirect()->route('Messenger Details',[$conversation]);
    }

    public function postSendMsg(Request $request) {
        $arrRequest = $request->all();
        $objRequest = (object)$arrRequest;
        $msg = $objRequest->msg;
        $conversation_id = $objRequest->channel_id;
        $user = Auth::user();
        if(empty($user)){
            return "error";
        } else {
            // Insert the msg into the conversation 
            $arrInsertDetail = array();
            $arrInsertDetail['conversation_id'] = (int)$objRequest->channel_id;
            $arrInsertDetail['user_id'] = $user->user_id;
            $arrInsertDetail['message'] = $objRequest->msg;

            $objConversationMessage = new ConversationMessage();
            $return = $objConversationMessage->insertMessage($arrInsertDetail);
        }
    }

    public function getChat(Request $request){
        $user = Auth::user();
        $arrChatUser = [$request->tu,$user->user_id];
        return view('chat.chat');
    }

    public function _getChat(Request $request){
        $user = Auth::user();
        $arrChatUser = [$request->tu,$user->user_id];
        return view('chat.chat');
    }

    private function _processName($jsonUserName) {
        return $jsonUserName->last_name." ".$jsonUserName->first_name;
    }
    
}
