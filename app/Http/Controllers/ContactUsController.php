<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Mail;

class ContactUsController extends Controller
{
    public function index()
    {
        return view('static.contact-us');
    }

    public function handleForm(Request $request)
    {
        $this->validate(
        	$request, [
        		'name' => 'required', 
                'email' => 'required|email', 
                'contact' => 'required', 
        		'message_body' => 'required|min:20'
        	]
        );
        $data = [
        	'name' => $request->input('name') , 
            'email' => $request->input('email') , 
            'contact' => $request->input('contact') , 
        	'messageBody' => $request->input('message_body') 
       	];
         Mail::send('static.contact-us-email', $data, function ($message){
            $message->from('support@wikabo.com', 'Contact Us Page');
            $message->to('john@wikabo.com', 'Admin')->subject('Wikabo Feedback');
        });
        session()->flash('success', 'Thank you for contacting us. We will do our best to get back to you as soon as possible.');
        return redirect()->back();

    }

}