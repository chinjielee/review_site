<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Intervention\Image\ImageManagerStatic as Image;

class UploadImageController extends Controller
{
    //
    const INVOICE_PATH = 'images/invoice';
    const REVIEW_PATH = 'images/review';
    private $invoice_path;
    private $review_path;

    public function __construct()
    {
        $this->invoice_path = public_path(self::INVOICE_PATH);
        $this->review_path = public_path(self::REVIEW_PATH);
    }

    /**
    * Saving images uploaded through XHR Request.
    *
    * @param  \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
   public function reviewImageStore(Request $request) {
        // TESTING  --  UserID & productId
        if(!empty($request->diresu)){
            $userId = $request->diresu;
        } else {
            return Response::json([
                'message' => 'Image upload Failed, Missing user details'
            ], 400);
        }
        switch($request->type){
            case "review": 
                $upload_path = $this->review_path."/".$userId."/temp/review";
            break;
            case "invoice": 
                $upload_path = $this->invoice_path."/".$userId."/temp/invoice";
            break;
        }

        $photos = $request->file('file');

        if (!is_array($photos)) {
            $photos = [$photos];
        }
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777, true);
        }
        for ($i = 0; $i < count($photos); $i++) {
            $photo = $photos[$i];
            //$name = sha1(date('YmdHis') . str_random(30));
            $name = $request->type."_".pathinfo($photo->getClientOriginalName())['filename'];
            $save_name = $name . '.' . $photo->getClientOriginalExtension();
            
        //Compress File    
            //$resize_name = $name . str_random(2) . '.' . $photo->getClientOriginalExtension();
            // Image::make($photo)
            //     ->resize(250, null, function ($constraints) {
            //         $constraints->aspectRatio();
            //     })
            //     ->save($invoice_path . '/' . $resize_name);

            $photo->move($upload_path, $save_name);
                
            //  --------- Model to insert images --------
            //    $upload = new Upload();
            //    $upload->filename = $save_name;
            //    $upload->resized_name = $resize_name;
            //    $upload->original_name = basename($photo->getClientOriginalName());
            //    $upload->save();
        }
        $uploaded_url = url('/'.$userId."/temp/".$save_name);
        return Response::json([
            'url' => $uploaded_url,
            'save_file' => $save_name,
            'message' => 'Image upload Successfully'
        ], 200);
   }

   /**
    * Remove the images from the storage.
    *
    * @param Request $request
    */
   public function reviewDestroy(Request $request)
   {
       $filename = $request->id;
       if(empty($request->diresu)){
            return Response::json(['message' => 'File failed delete'], 401); 
       } else {
            $userId = $request->diresu;
            switch($request->type) {
                case "invoice":
                    $invoice_path = $this->invoice_path."/".$userId."/temp/invoice/invoice_". $filename;
                    if (file_exists($invoice_path)) {
                        unlink($invoice_path);
                    }
                break;
                case "review":
                    $review_path = $this->review_path."/".$userId."/temp/review/review_". $filename;
                    if (file_exists($review_path)) {
                        unlink($review_path);
                    }
                break;
            }
            // MODEL Deletion
            //    $uploaded_image = Upload::where('original_name', basename($filename))->first();

            //    if (empty($uploaded_image)) {
            //        return Response::json(['message' => 'Sorry file does not exist'], 400);
            //    }
            return Response::json(['message' => 'File successfully delete'], 200);
        }
   }

}