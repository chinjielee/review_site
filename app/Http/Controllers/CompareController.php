<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\ProductService;
use App\Http\Model\Brands;
use function GuzzleHttp\json_encode;


class CompareController extends Controller
{
    //
    public function index()
    {
        $objProduct_1 = $objProduct_2 = $objProduct_3 = "";
        $strCompareProductsAttribute = "";
        $arrBrand = "";
        $intTotalProduct = 0;
        return view('compare.index', compact('objProduct_1', 'objProduct_2', 'objProduct_3','strCompareProductsAttribute','arrBrand','intTotalProduct'));
    }

    public function compare($product_1 = "", $product_2 = "", $product_3 = "")
    {
        $objProductService = new ProductService();
        $objBrands = new Brands();
        $objProduct_1 = $objProduct_2 = $objProduct_3 = "";
        $objCompareAttribute = (object)array();
        $strCompareProductsAttribute = "";
        $arrBrand = "";
        
        $arrProducts = array();
        if (!empty($product_1)) {
            $objProduct_1 = $objProductService->getProductDetailsBySlugWithFullCategory($product_1);
            $temp = $objProductService->processImage($objProduct_1->images,$objProduct_1->product_services_id);
            if(!empty($temp)) {
                $temp2 = $temp[0]['path'];
                $objProduct_1->images = $temp2;
            }
            array_push($arrProducts, $objProduct_1);
        }
        if (!empty($product_2)) {
            $objProduct_2 = $objProductService->getProductDetailsBySlugWithFullCategory($product_2);
            $temp = $objProductService->processImage($objProduct_2->images,$objProduct_2->product_services_id);
            if(!empty($temp)) {
                $temp2 = $temp[0]['path'];
                $objProduct_2->images = $temp2;
            }
            array_push($arrProducts, $objProduct_2);
        }
        if (!empty($product_3)) {
            $objProduct_3 = $objProductService->getProductDetailsBySlugWithFullCategory($product_3);
            $temp = $objProductService->processImage($objProduct_3->images,$objProduct_3->product_services_id);
            if(!empty($temp)) {
                $temp2 = $temp[0]['path'];
                $objProduct_3->images = $temp2;
            }
            array_push($arrProducts, $objProduct_3);
        }
        if(!empty($arrProducts)){
            $objCompareProductsAttribute = $this->_processCompareProductAttribute($arrProducts);
            $strCompareProductsAttribute = str_replace("'","\\'",json_encode($objCompareProductsAttribute));
            $arrBrands = $objBrands->getAllBrandsNameKeyById();
            $arrBrand = json_encode((object)$arrBrands);
        }
        $intTotalProduct = sizeof($arrProducts);
        return view('compare.index', compact('objProduct_1', 'objProduct_2', 'objProduct_3','arrBrand','strCompareProductsAttribute','intTotalProduct'));
    }

    private function _processCompareProductAttribute($arrProducts)
    {
        $objCompareAttribute = (object)array();
        $id = 1;
        foreach($arrProducts as $product) 
        {
            $objProductAttrJson = json_decode($product->product_service_attributes);
            // Compulsary Field 
            if(!isset($objCompareAttribute->rating)){
                $objCompareAttribute->rating = (object)array();
            }   
            if(!isset($objCompareAttribute->rating->$id)){
                $objCompareAttribute->rating->$id = (object)array();
            }
            $objProductService = new ProductService();
            $objRatingDisplay = $objProductService->processOverallRatingDisplay($product->product_rating);
            $objCompareAttribute->rating->$id = $objRatingDisplay->overall;
            
            if(!isset($objCompareAttribute->brand)){
                $objCompareAttribute->brand = (object)array();
            }
            if(!isset($objCompareAttribute->brand->$id)){
                $objCompareAttribute->brand->$id = (object)array();
            }
            $objCompareAttribute->brand->$id = $product->brands_id;
            if(!isset($objCompareAttribute->image)){
                $objCompareAttribute->image = (object)array();
            }
            if(!isset($objCompareAttribute->image->$id)){
                $objCompareAttribute->image->$id = (object)array();
            }
            
            $objCompareAttribute->image->$id = $product->images;
            
            foreach($objProductAttrJson as $key => $attr){            
                $key = strtolower($key);
                // Optional Attributes
                if(!isset($objCompareAttribute->$key)){
                    $objCompareAttribute->$key = (object)array();
                }
                if(!isset($objCompareAttribute->$key->$id)){
                    $objCompareAttribute->$key->$id = (object)array();
                }
                $objCompareAttribute->$key->$id = $attr;
            }
            $id = $id+1;
        }

        // Clean Up the empty field
        $arrCompareAttribute = json_encode($objCompareAttribute);
        $arrCompareAttribute = json_decode($arrCompareAttribute,true);
        foreach($arrCompareAttribute as $attrKey => $attr){
            $loopid = 1;
            foreach($arrProducts as $product){
                if(!isset($arrCompareAttribute[$attrKey][$loopid])){
                    $arrCompareAttribute[$attrKey][$loopid] = "";
                }
                $loopid = $loopid+1;
            }
            ksort($arrCompareAttribute[$attrKey]);
        }
        
        $objCompareAttribute = (object)$arrCompareAttribute;
        return $objCompareAttribute;
    }
}
