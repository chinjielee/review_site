<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Model\SpamFilter;
use App\Http\Model\ProductAnswer;
use App\Http\Model\DiscussLog;
use App\Http\Model\ProductService;
use Illuminate\Support\Facades\Validator;

class ProductAnswerController extends Controller
{
    protected function answerValidator(array $data)
    {
        $messages = [
            'psid.required' => 'Invalid Answer posting',
            'answer.required' => 'Please describe your answer',
            'answer.min' => 'Answer too short, please explain more on your answer'

        ];
        $validator = Validator::make($data, [
            'psid' => 'required|integer',
            'answer' => 'string|required|min:10',
        ], $messages);
        return $validator;
    }
    protected function answerVerifyValidator(array $data)
    {
        $messages = [
            'answer.required' => 'Please describe your answer',
            'answer.min' => 'Answer too short, please explain more on your answer'
        ];
        $validator = Validator::make($data, [
            'answer' => 'string|required|min:10',
        ], $messages);
        return $validator;
    }
    //
    public function postAnswer(Request $request) {
        $validator = $this->answerValidator($request->all());
        $validator->validate();
        $arrInput = $request->input();
        if(isset($arrInput['anonymous'])){
            if($arrInput['anonymous'] == "on"){
                $arrInput['anonymous'] = 1;
            }
        }
        else {
            $arrInput['anonymous'] = 0;
        }
        if(Auth::user()){
            $arrInput['user_id'] = Auth::user()->user_id;
            $objSpamFilter = new SpamFilter();
            if (!$objSpamFilter->isOffensiveWord($arrInput["answer"])) {
                $objProductAnswer = new ProductAnswer();
                if($objProductAnswer->create($arrInput)){
                    // Log Discuss
                    $objProductDetail = new ProductService();
                    $objDiscussLog = new DiscussLog;
                    $productDetail = $objProductDetail->getProductDetailsById($arrInput['psid']);
                    $objDiscussLog->logDiscussReview($productDetail);
                    $objProductAnswer->notifyRelatedOwner($arrInput, $arrInput['anonymous']); 
                    return redirect()->back();
                } else {
                    return redirect()->back()->withInput()->withErrors(['Somethings goes wrong, please try again']);
                }
            } else {
                return redirect()->back()->withInput()->withErrors(['We understand your feeling, try not to use offensive word >.<']);
            }
        } else {
            return redirect()->back()->withInput()->withErrors(['Please Login tp continues posting answer']);
        }     
    }

    public function postAnswerVerify(Request $request) {
        $arrInput = $request->input();
        $objSpamFilter = new SpamFilter();
        if(Auth::user()){
            if (!$objSpamFilter->isOffensiveWord($arrInput["answer"])) {
                return response()->json('ok',200);
            } else {
                $arrError = array(
                    "error_message" => "We understand your feeling, try not to use offensive word >.<",
                    "status" => "error"
                );
                return response()->json(json_encode($arrError),200);
            }
        } else {
            $arrError = array(
                "error_message" => "Login Required",
                "status" => "error"
            );
            return response()->json(json_encode($arrError),200);
        }
 
    }
}
