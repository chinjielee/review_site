<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\SendNotification;

class NotificationController extends Controller
{
    //
    public function getNoti(Request $request){
        $request->user()->notify(new SendNotification);
        return response()->json('Notification sent.', 201);
    }

    public function getNotiAll(Request $request){
        $user = $request->user();
        
        // Limit the number of returned notifications, or return all
        $query = $user->unreadNotifications();
        $limit = (int) $request->input('limit', 0);
        if ($limit) {
            $query = $query->limit($limit);
        }

        $notifications = $query->get()->each(function ($n) {
            $n->created = $n->created_at->toIso8601String();
        });

        $total = $user->unreadNotifications->count();
        return compact('notifications', 'total');
    }
}
