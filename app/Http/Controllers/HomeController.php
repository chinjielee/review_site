<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\FeatureImage;
use App\Http\Model\StatMostDiscuss;
use App\Http\Model\Categories;
use function GuzzleHttp\json_decode;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    //    $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objFeatureImage = new FeatureImage();
        $jsonFeatureImage = $objFeatureImage->getFeatureImage();
        $jsonImage = "";
        if(!empty($jsonFeatureImage)){
            $jsonImage = json_decode($jsonFeatureImage->details);
        }
        // Get Most Discuss Product 
        $objStatMostDiscuss = new StatMostDiscuss();
        $arrMostDiscuss = $objStatMostDiscuss->getMostDiscussByProduct();
        list($arrProduct,$arrService,$arrCat) = $objStatMostDiscuss->getMostDiscussByCategory();
        return view('welcome',compact('jsonImage','arrMostDiscuss','arrProduct','arrService','arrCat'));
    }
}
