<?php

namespace App\Http\Controllers;

use Auth;
use URL;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Model\ProductService;
use App\Http\Model\RatingOption;
use App\Http\Model\Categories;
use App\Http\Model\Brands;
use App\Http\Model\PendingProductService;
use App\Http\Model\ProductReview;
use App\Http\Model\SpamFilter;
use App\Http\Model\User;
use App\Http\Model\UserActivityLog;
use App\Http\Model\ReviewVote;
use App\Http\Model\DiscussLog;
use App\Http\Model\ReviewReportFraud;
use App\Notifications\NotificationNewProduct;

class ReviewController extends Controller
{
    //
    /**
     * Get a validator for an incoming review product request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */

     public function old(){
         return view('review.write');
     }

     protected function reviewCreateValidator(array $data)
     {
         $messages = [
             'product_name.required' => 'Please fill in the product name.',
             'category.notIn' => 'Please choose one of the product category',
             'category.required' => 'Please choose one of the product category'
         ];
         $validator = Validator::make($data, [
             'product_name' => 'required|string|max:255',
             'category' => 'notIn:0|required',
         ],$messages);
         return $validator;
     }

     protected function reviewValidator(array $data)
     {
         $messages = [
             'title.required' => 'Please fill in the review title.',
             'detail.required' => 'Please fill in the review details.'
             // ,
             // 'dop.required' => 'Please fill in purchases date',
             // 'purchase_at.required' => 'Please fill in where it purchases'
         ];
         $validator = Validator::make($data, [
              'title'=> 'required|string|max:255',
              'detail'=> 'required|string'
              // ,
              // 'dop'=> 'date|required',
              // 'purchase_at'=> 'required'
         ],$messages);
         return $validator;
     }

     

    public function index(){
        return view('review.index');
    }

    public function create(Request $request, $category_slug = '') {
        $objProductService = new ProductService();
        $productSlug = $request->product_slug;
        if(empty($productSlug)) {
            $this->clearTempImage(Auth::user()->user_id);
            $objCategories = new Categories();
            $objBrands = new Brands();
            $arrBrands = $objBrands->getAllBrandsNameKeyById();
            $arrCategoryList = $objCategories->getCategoryGroupByLabel();
            $strProductList = json_encode($arrCategoryList['product']);
            $strServiceList = json_encode($arrCategoryList['service']);
            $strMovieList = json_encode($arrCategoryList['movie']);
            return view('review.create', compact('strProductList','strServiceList','strMovieList','arrBrands'));
        } else {
            $objProductDetail = (object)array();
            $objProductDetail = $objProductService->getProductFullDetailsBySlug($productSlug);
            $objProductDetail = $this->_processDisplayAttributeForReview($objProductDetail);    
            // Proceed to Step 2
            return redirect()->route('Review Detail', ['product_slug'=>$request->product_slug]);
        }
    }

    public function review(Request $request, $product_slug="") {
        // New Product
        if(!empty($request->all()['temp_product_id'])) {
            $tempProductId = $request->all()['temp_product_id'];
            $objPendingProduct = new PendingProductService();
            $objProductDetail = $objPendingProduct->getProductDetailsBySlugWithFullCategoryByTempId($tempProductId);
            $objProductDetail->product_service_id = 0;
        } elseif (!empty($request->all()['product_slug']) || $product_slug) {
            $objProductService = new ProductService();
            if(!empty($request->all()['product_slug'])){
                $product_slug = $request->all()['product_slug'];
            }
            if(!empty($product_slug)){
                $product_slug = $product_slug;
            }
            $objProductDetail = $objProductService->getProductDetailsBySlugWithFullCategory($product_slug);
            if(!empty($objProductDetail)) {
                $objProductDetail->temp_product_service_id = 0;
                $arrImages = $objProductService->processImage($objProductDetail->images,$objProductDetail->product_services_id);
                if(!empty($arrImages)) {
                    $objProductDetail->display_image = $arrImages[0]['path'];
                } else {
                    $objProductDetail->display_image = '';
                }
                $objProductDetail->url = $objProductService->getAllProductUrl($objProductDetail);
            } else {
                return redirect()->route('Create Review');    
            }
        } else {
            return redirect()->route('Create Review');
        }
        // If product is found
        if(!empty($objProductDetail)){
            // Process For search rating
            $arrCatList = $this->_processCatListString($objProductDetail);
            // Get Rating Detail
            $objRatingOption = new RatingOption();
            $arrRatingOption = $objRatingOption->getRatingOption($arrCatList);
             if(!empty($arrRatingOption)){
                if(isset($arrRatingOption[0])){
                    $arrRatingOption = json_decode($arrRatingOption[0]->option_json);
                }
            }
            // Reset all temporary upload image
            $this->clearTempImage(Auth::user()->user_id);
            $countryList = array();
            $countryList = $this->CountryList("MY");
            return view('review.detail',compact('arrRatingOption','objProductDetail','countryList'));
        } else {
            return redirect()->route('Create Review')->withErrors('No product found, please submit a new product details.');
        }
    }

    public function thankyou() {
        return view('review.thankyou');
    }

    ############## POST ACTION #######################
    public function postCreate(Request $request) {
        $validator = $this->reviewCreateValidator($request->all());
        $validator->validate();
        //Create a temporary Product 
        $objPendingProduct = new PendingProductService();
        $requestInput = $request->all();
        $arrProcessedInput = $this->_processPendingProductInput($requestInput);
        $tempProductId = $objPendingProduct->create($arrProcessedInput);
        
        if($tempProductId > 0 ){
            // notify admin
            $objUser = new User();
            $adminUser = $objUser->getUser($objUser::HARDCODE_ADMIN_USER_ID);
            $notifyMessage = Auth::user()->first_name." has created new product :".$requestInput['product_name'];
            $adminUser->notify(new NotificationNewProduct($tempProductId, Auth::user()->first_name, $notifyMessage));    

            return redirect()->route('Review Detail', ['temp_product_id'=>$tempProductId]);
        } else {
            return redirect('Create Review')
            ->withErrors($validator)
            ->withInput();
        }
    }
/*
    "_token" => "mHO77iyMhHKLFesAQDO1yx6wbSalUWkW32qwxsye"
      "title" => "Hal"
      "detail" => "asd asd asd asd asd asd asd"
      "rating-Reliability" => "5"
      "rating-User_Friendliness" => "4"
      "rating-Standards" => "2"
      "rating-Professionalism" => "3"
      "rating-Price" => "3"
      "highlight" => "suck"
      "product_image" => null
      "dop" => "12/22/2029"
      "purchase_at" => "PP Mall"
      "country" => "my"
      "invoice_image" => null
*/
    public function postReview(Request $request) {
        $validator = $this->reviewValidator($request->all());
        $validator->validate();
        $objProductReview = new ProductReview();
        $objSpamFilter = new SpamFilter();
        $arrInput = $request->input();
        $arrInput["rating"] = $this->processRewiewRating($request->input());
        if(!$arrInput["rating"]){
            return redirect()->back()->withInput()->withErrors(['Please kindly make rating on the product or services']);
        } else {
            if(!$objSpamFilter->isOffensiveWord($arrInput["detail"]) && !$objSpamFilter->isOffensiveWord($arrInput["title"]) && !$objSpamFilter->isOffensiveWord($arrInput["highlight"])){
                $objProductDetail = new ProductService();
                $arrMappedInput = $objProductReview->columnMapping($arrInput);
                $arrMappedInput['user_id'] = Auth::user()->user_id;
                $inserted_id = $objProductReview->create($arrMappedInput);
                if($inserted_id){
                    // Update the real product Review Rating
                    $objProductDetail->processUpdateNewRatingData($arrInput['product_slug'],$arrInput["rating"],Auth::user()->user_id);
                }
                $blnInserted = $objProductReview->reviewImageSave(Auth::user()->user_id, $inserted_id); 
                if($blnInserted) {
                    $objLog = new UserActivityLog;
                    if($arrMappedInput['product_services_id'] >0){
                        $objDiscussLog = new DiscussLog;
                        $productDetail = $objProductDetail->getProductDetailUrlBySlug($arrInput['product_slug'], true);
                        $reviewUrl = $productDetail['url']."/review";
                        $objDiscussLog->logDiscussReview($productDetail);
                        $arrLogDetail['link'] = $reviewUrl;
                        $objLog->logUserReview($arrLogDetail);
                    }
                    return redirect()->route('Review Thank You');
                }
            } else {
                return redirect()->back()->withInput()->withErrors(['We understand your feeling, try not to use offensive word.']);
            }
        }
    }

    public function postDownVote(Request $request)
    {
        $arrInput = $request->input();
        $arrCreateInput = array();
        $jsonReturn = array();
        if(Auth::user()) {
            $objReviewVote = new ReviewVote();
            $objProductReview = new ProductReview();
            $objActivityLog = new UserActivityLog();
            $objReviewVote->updateCreateDown($arrInput['product_review_id'],Auth::user()->user_id, $arrInput['url']);
            // count and Update productReview
            $countNewTotal = $objReviewVote->countTotalVote($arrInput['product_review_id']);
            $objProductReview->updateCount($arrInput['product_review_id'], $countNewTotal);
            // Log User Activity
            $objLog = new UserActivityLog;
            $reviewUrl = 
            $arrLogDetail['link'] = $arrInput['url'];
            $objLog->logUserReview($arrLogDetail);;
            
            $jsonReturn = array(
                "status" => "done",
                "totalDownCount" => $countNewTotal->down,
                "totalUpCount" => $countNewTotal->up
            );
        } else {
            $jsonReturn = array(
                "error_message" => "Login Required",
                "status" => "error"
            );
        }
        return response()->json(json_encode($jsonReturn),200);
    }

    public function postUpVote(Request $request)
    {
        $arrInput = $request->input();
        $arrCreateInput = array();
        $jsonReturn = array();
        if(Auth::user()) {
            $objReviewVote = new ReviewVote();
            $objProductReview = new ProductReview();
            $objReviewVote->updateCreateUp($arrInput['product_review_id'],Auth::user()->user_id, $arrInput['url']);
            // count and Update productReview
            $countNewTotal = $objReviewVote->countTotalVote($arrInput['product_review_id']);
            $objProductReview->updateCount($arrInput['product_review_id'], $countNewTotal);
            // Log User Activity
            $objLog = new UserActivityLog;
            $arrLogDetail['link'] = $arrInput['url'];
            $objLog->logUserReview($arrLogDetail);
            $jsonReturn = array(
                "status" => "done",
                "totalDownCount" => $countNewTotal->down,
                "totalUpCount" => $countNewTotal->up
            );
        } else {
            $jsonReturn = array(
                "error_message" => "Login Required",
                "status" => "error"
            );
        }
        return response()->json(json_encode($jsonReturn),200);
    }

    public function postReportFraud(Request $request)
    {
        $arrInput = $request->input();
        $arrCreateInput = array();
        $jsonReturn = array();
        if(Auth::user()) {
            $objReviewReportFraud = new ReviewReportFraud();
            $objReviewReportFraud->reportFraud($arrInput['product_review_id'],Auth::user()->user_id,$arrInput['status']);
            $jsonReturn = array(
                "status" => "done",
            );
        } else {
            $jsonReturn = array(
                "error_message" => "Login Required",
                "status" => "error"
            );
        }
        return response()->json(json_encode($jsonReturn),200);
    }

    private function _processDisplayAttributeForReview($detail){
        $jsonDetail = json_decode($detail->product_service_attributes);
        // Brand
        if(isset($jsonDetail->brand)) {
            $detail->brand = $jsonDetail->brand;
        }

        // Website
        if(isset($jsonDetail->website)){
            $detail->website = $jsonDetail->website;
        }

        // Model
        if(isset($jsonDetail->model)){
            $detail->model = $jsonDetail->model;
        }

        return $detail;
    }

    private function _processPendingProductInput($request){
        $arrInput = array();
        $arrInput['product_name'] = $request['product_name'];
        $arrInput['status'] = PendingProductService::STATUS_PENDING;
        $arrInput['created_at'] = Carbon::now();
        $arrInput['updated_at'] = Carbon::now();
        $jsonAttr = (object)array();
        // Brand
        if(isset($request['brand'])){
            if($request['brand'] >= 0){
                $arrInput['brand_id'] = $request['brand'];
            } else {
                $jsonAttr->brand = $request['new_brand'];
                $arrInput['brand_name'] = $request['new_brand'];
            }
        }
        if(isset($request['category'])){
            if($request['category'] > 0){
                $arrInput['category_id'] = $request['category'];
            } else {
                $arrInput['category_id'] = 0;
                $arrInput['category_name'] = $request['new_category'];
            }
        }
        // Website
        if(isset($request['website'])){
            $jsonAttr->website = $request['website'];
        }
        // Model
        if(isset($request['model'])){
            $jsonAttr->model = $request['model'];
        }
        $arrInput['product_attributes'] = "";
        $arrInput['product_attributes'] = json_encode($jsonAttr);
        return $arrInput;
    }

    public function clearTempImage($userId){
        $path = public_path('images/review/'.$userId.'/temp/');
        $this->_rrmdir($path);
        $path = public_path('images/product/'.$userId.'/temp/');
        $this->_rrmdir($path);
    }

    private function _rrmdir($dir) { 
        if (is_dir($dir)) {
          $objects = scandir($dir); 
          foreach ($objects as $object) { 
            if ($object != "." && $object != "..") {
              if (is_dir($dir."/".$object))
                $this->_rrmdir($dir."/".$object);
              else
                unlink($dir."/".$object);
            } 
          }
          rmdir($dir); 
        } 
    }

    private function _processCatListString($objProductDetail){
        $arrCatList = array($objProductDetail->category_id);
        if(!empty($objProductDetail->parent_cat_2_id)){
            array_push($arrCatList,$objProductDetail->parent_cat_2_id);
        }
        if(!empty($objProductDetail->parent_cat_1_id)){
            array_push($arrCatList,$objProductDetail->parent_cat_1_id);
        }
        return $arrCatList;
    }

    private function processRewiewRating($arrInput){
        $countRating = 0;
        $intRating = 0;
        $avgRating = 0;
        $count = 0;
        $json = (object)array();
        $jsonString = "";
        foreach($arrInput as $key=>$value){
            if(preg_match("/rating-(.*)/",$key,$arrMatch)){
                if(empty($value)){
                    $blnEmptyRating = true;
                    $value = 0;
                }
                $count++;
                $countRating = $countRating+5;
                $intRating = $intRating+$value;
                $tempKey = strtolower($arrMatch[1]);
                $json->$tempKey = $value;
            }
        }
        // If do rated on somehthings
        if($countRating >0){
            $avgRating = $intRating / $countRating * $count;
        }

        $json->overall = $avgRating;
        if($blnEmptyRating){
            return false;
        } else {
            return json_encode($json);
        }
        
    }

    public function CountryList($code = ""){
        $countryList = array();
        $countryList["AF"] = array("select" => "" , "value" => "AF", "text" => "AFGHANISTAN" );
        $countryList["AL"] = array("select" => "" , "value" => "AL", "text" => "ALBANIA" );
        $countryList["DZ"] = array("select" => "" , "value" => "DZ", "text" => "ALGERIA" );
        $countryList["AS"] = array("select" => "" , "value" => "AS", "text" => "AMERICAN SAMOA" );
        $countryList["AD"] = array("select" => "" , "value" => "AD", "text" => "ANDORRA" );
        $countryList["AO"] = array("select" => "" , "value" => "AO", "text" => "ANGOLA" );
        $countryList["AI"] = array("select" => "" , "value" => "AI", "text" => "ANGUILLA" );
        $countryList["AG"] = array("select" => "" , "value" => "AG", "text" => "ANTIGUA" );
        $countryList["AR"] = array("select" => "" , "value" => "AR", "text" => "ARGENTINA" );
        $countryList["AM"] = array("select" => "" , "value" => "AM", "text" => "ARMENIA" );
        $countryList["AW"] = array("select" => "" , "value" => "AW", "text" => "ARUBA" );
        $countryList["AU"] = array("select" => "" , "value" => "AU", "text" => "AUSTRALIA" );
        $countryList["AT"] = array("select" => "" , "value" => "AT", "text" => "AUSTRIA" );
        $countryList["AZ"] = array("select" => "" , "value" => "AZ", "text" => "AZERBAIJAN" );
        $countryList["BS"] = array("select" => "" , "value" => "BS", "text" => "BAHAMAS" );
        $countryList["BH"] = array("select" => "" , "value" => "BH", "text" => "BAHRAIN" );
        $countryList["BD"] = array("select" => "" , "value" => "BD", "text" => "BANGLADESH" );
        $countryList["BB"] = array("select" => "" , "value" => "BB", "text" => "BARBADOS" );
        $countryList["BY"] = array("select" => "" , "value" => "BY", "text" => "BELARUS" );
        $countryList["BE"] = array("select" => "" , "value" => "BE", "text" => "BELGIUM" );
        $countryList["BZ"] = array("select" => "" , "value" => "BZ", "text" => "BELIZE" );
        $countryList["BJ"] = array("select" => "" , "value" => "BJ", "text" => "BENIN" );
        $countryList["BM"] = array("select" => "" , "value" => "BM", "text" => "BERMUDA" );
        $countryList["BT"] = array("select" => "" , "value" => "BT", "text" => "BHUTAN" );
        $countryList["BO"] = array("select" => "" , "value" => "BO", "text" => "BOLIVIA" );
        $countryList["BQ"] = array("select" => "" , "value" => "BQ", "text" => "BONAIRE" );
        $countryList["BA"] = array("select" => "" , "value" => "BA", "text" => "BOSNIA AND HERZEGOVINA" );
        $countryList["BW"] = array("select" => "" , "value" => "BW", "text" => "BOTSWANA" );
        $countryList["BR"] = array("select" => "" , "value" => "BR", "text" => "BRAZIL" );
        $countryList["BN"] = array("select" => "" , "value" => "BN", "text" => "BRUNEI DARUSSALAM" );
        $countryList["BG"] = array("select" => "" , "value" => "BG", "text" => "BULGARIA" );
        $countryList["BF"] = array("select" => "" , "value" => "BF", "text" => "BURKINA FASO" );
        $countryList["BI"] = array("select" => "" , "value" => "BI", "text" => "BURUNDI" );
        $countryList["KH"] = array("select" => "" , "value" => "KH", "text" => "CAMBODIA" );
        $countryList["CM"] = array("select" => "" , "value" => "CM", "text" => "CAMEROON" );
        $countryList["CA"] = array("select" => "" , "value" => "CA", "text" => "CANADA" );
        $countryList["IC"] = array("select" => "" , "value" => "IC", "text" => "CANARY ISLANDS, THE" );
        $countryList["CV"] = array("select" => "" , "value" => "CV", "text" => "CAPE VERDE" );
        $countryList["KY"] = array("select" => "" , "value" => "KY", "text" => "CAYMAN ISLANDS" );
        $countryList["CF"] = array("select" => "" , "value" => "CF", "text" => "CENTRAL AFRICAN REPUBLIC" );
        $countryList["TD"] = array("select" => "" , "value" => "TD", "text" => "CHAD" );
        $countryList["CL"] = array("select" => "" , "value" => "CL", "text" => "CHILE" );
        $countryList["CN"] = array("select" => "" , "value" => "CN", "text" => "CHINA" );
        $countryList["CO"] = array("select" => "" , "value" => "CO", "text" => "COLOMBIA" );
        $countryList["CO"] = array("select" => "" , "value" => "CO", "text" => "COLOMBIA" );
        $countryList["KM"] = array("select" => "" , "value" => "KM", "text" => "COMOROS" );
        $countryList["CG"] = array("select" => "" , "value" => "CG", "text" => "CONGO" );
        $countryList["CD"] = array("select" => "" , "value" => "CD", "text" => "CONGO, THE DEMOCRATIC REPUBLIC OF THE" );
        $countryList["CK"] = array("select" => "" , "value" => "CK", "text" => "COOK ISLANDS" );
        $countryList["CR"] = array("select" => "" , "value" => "CR", "text" => "COSTA RICA" );
        $countryList["CI"] = array("select" => "" , "value" => "CI", "text" => "COTE D'IVOIRE" );
        $countryList["HR"] = array("select" => "" , "value" => "HR", "text" => "CROATIA" );
        $countryList["CU"] = array("select" => "" , "value" => "CU", "text" => "CUBA" );
        $countryList["CW"] = array("select" => "" , "value" => "CW", "text" => "CURACAO" );
        $countryList["CY"] = array("select" => "" , "value" => "CY", "text" => "CYPRUS" );
        $countryList["CZ"] = array("select" => "" , "value" => "CZ", "text" => "CZECH REPUBLIC" );
        $countryList["DK"] = array("select" => "" , "value" => "DK", "text" => "DENMARK" );
        $countryList["DJ"] = array("select" => "" , "value" => "DJ", "text" => "DJIBOUTI" );
        $countryList["DM"] = array("select" => "" , "value" => "DM", "text" => "DOMINICA" );
        $countryList["DO"] = array("select" => "" , "value" => "DO", "text" => "DOMINICAN REPUBLIC" );
        $countryList["TP"] = array("select" => "" , "value" => "TP", "text" => "EAST TIMOR" );
        $countryList["EC"] = array("select" => "" , "value" => "EC", "text" => "ECUADOR" );
        $countryList["EG"] = array("select" => "" , "value" => "EG", "text" => "EGYPT" );
        $countryList["SV"] = array("select" => "" , "value" => "SV", "text" => "EL SALVADOR" );
        $countryList["GQ"] = array("select" => "" , "value" => "GQ", "text" => "EQUATORIAL GUINEA" );
        $countryList["ER"] = array("select" => "" , "value" => "ER", "text" => "ERITREA" );
        $countryList["EE"] = array("select" => "" , "value" => "EE", "text" => "ESTONIA" );
        $countryList["ET"] = array("select" => "" , "value" => "ET", "text" => "ETHIOPIA" );
        $countryList["FK"] = array("select" => "" , "value" => "FK", "text" => "FALKLAND ISLANDS (MALVINAS)" );
        $countryList["FO"] = array("select" => "" , "value" => "FO", "text" => "FAROE ISLANDS" );
        $countryList["FJ"] = array("select" => "" , "value" => "FJ", "text" => "FIJI" );
        $countryList["FI"] = array("select" => "" , "value" => "FI", "text" => "FINLAND" );
        $countryList["FR"] = array("select" => "" , "value" => "FR", "text" => "FRANCE" );
        $countryList["GF"] = array("select" => "" , "value" => "GF", "text" => "FRENCH GUIANA" );
        $countryList["GA"] = array("select" => "" , "value" => "GA", "text" => "GABON" );
        $countryList["GM"] = array("select" => "" , "value" => "GM", "text" => "GAMBIA" );
        $countryList["GE"] = array("select" => "" , "value" => "GE", "text" => "GEORGIA" );
        $countryList["DE"] = array("select" => "" , "value" => "DE", "text" => "GERMANY" );
        $countryList["GH"] = array("select" => "" , "value" => "GH", "text" => "GHANA" );
        $countryList["GI"] = array("select" => "" , "value" => "GI", "text" => "GIBRALTAR" );
        $countryList["GR"] = array("select" => "" , "value" => "GR", "text" => "GREECE" );
        $countryList["GL"] = array("select" => "" , "value" => "GL", "text" => "GREENLAND" );
        $countryList["GD"] = array("select" => "" , "value" => "GD", "text" => "GRENADA" );
        $countryList["GP"] = array("select" => "" , "value" => "GP", "text" => "GUADELOUPE" );
        $countryList["GU"] = array("select" => "" , "value" => "GU", "text" => "GUAM" );
        $countryList["GT"] = array("select" => "" , "value" => "GT", "text" => "GUATEMALA" );
        $countryList["GG"] = array("select" => "" , "value" => "GG", "text" => "GUERNSEY" );
        $countryList["GN"] = array("select" => "" , "value" => "GN", "text" => "GUINEA REPUBLIC" );
        $countryList["GW"] = array("select" => "" , "value" => "GW", "text" => "GUINEA BISSAU" );
        $countryList["GY"] = array("select" => "" , "value" => "GY", "text" => "GUYANA" );
        $countryList["HT"] = array("select" => "" , "value" => "HT", "text" => "HAITI" );
        $countryList["HN"] = array("select" => "" , "value" => "HN", "text" => "HONDURAS" );
        $countryList["HK"] = array("select" => "" , "value" => "HK", "text" => "HONG KONG" );
        $countryList["HU"] = array("select" => "" , "value" => "HU", "text" => "HUNGARY" );
        $countryList["IS"] = array("select" => "" , "value" => "IS", "text" => "ICELAND" );
        $countryList["IN"] = array("select" => "" , "value" => "IN", "text" => "INDIA" );
        $countryList["ID"] = array("select" => "" , "value" => "ID", "text" => "INDONESIA" );
        $countryList["IR"] = array("select" => "" , "value" => "IR", "text" => "IRAN, ISLAMIC REPUBLIC OF" );
        $countryList["IQ"] = array("select" => "" , "value" => "IQ", "text" => "IRAQ" );
        $countryList["IE"] = array("select" => "" , "value" => "IE", "text" => "IRELAND" );
        $countryList["IL"] = array("select" => "" , "value" => "IL", "text" => "ISRAEL" );
        $countryList["IT"] = array("select" => "" , "value" => "IT", "text" => "ITALY" );
        $countryList["JM"] = array("select" => "" , "value" => "JM", "text" => "JAMAICA" );
        $countryList["JP"] = array("select" => "" , "value" => "JP", "text" => "JAPAN" );
        $countryList["JE"] = array("select" => "" , "value" => "JE", "text" => "JERSEY" );
        $countryList["JO"] = array("select" => "" , "value" => "JO", "text" => "JORDAN" );
        $countryList["KZ"] = array("select" => "" , "value" => "KZ", "text" => "KAZAKHSTAN" );
        $countryList["KE"] = array("select" => "" , "value" => "KE", "text" => "KENYA" );
        $countryList["KI"] = array("select" => "" , "value" => "KI", "text" => "KIRIBATI" );
        $countryList["KP"] = array("select" => "" , "value" => "KP", "text" => "KOREA, DEMOCRATIC PEOPLE'S REPUBLIC OF" );
        $countryList["XK"] = array("select" => "" , "value" => "XK", "text" => "KOSOVO" );
        $countryList["KW"] = array("select" => "" , "value" => "KW", "text" => "KUWAIT" );
        $countryList["KG"] = array("select" => "" , "value" => "KG", "text" => "KYRGYZSTAN" );
        $countryList["LA"] = array("select" => "" , "value" => "LA", "text" => "LAO PEOPLE'S DEMOCRATIC REPUBLIC" );
        $countryList["LV"] = array("select" => "" , "value" => "LV", "text" => "LATVIA" );
        $countryList["LB"] = array("select" => "" , "value" => "LB", "text" => "LEBANON" );
        $countryList["LS"] = array("select" => "" , "value" => "LS", "text" => "LESOTHO" );
        $countryList["LR"] = array("select" => "" , "value" => "LR", "text" => "LIBERIA" );
        $countryList["LY"] = array("select" => "" , "value" => "LY", "text" => "LIBYA" );
        $countryList["LI"] = array("select" => "" , "value" => "LI", "text" => "LIECHTENSTEIN" );
        $countryList["LT"] = array("select" => "" , "value" => "LT", "text" => "LITHUANIA" );
        $countryList["LU"] = array("select" => "" , "value" => "LU", "text" => "LUXEMBOURG" );
        $countryList["MO"] = array("select" => "" , "value" => "MO", "text" => "MACAU" );
        $countryList["MK"] = array("select" => "" , "value" => "MK", "text" => "MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF" );
        $countryList["MG"] = array("select" => "" , "value" => "MG", "text" => "MADAGASCAR" );
        $countryList["MY"] = array("select" => "" , "value" => "MY", "text" => "MALAYSIA" );
        $countryList["MW"] = array("select" => "" , "value" => "MW", "text" => "MALAWI" );
        $countryList["MV"] = array("select" => "" , "value" => "MV", "text" => "MALDIVES" );
        $countryList["ML"] = array("select" => "" , "value" => "ML", "text" => "MALI" );
        $countryList["MT"] = array("select" => "" , "value" => "MT", "text" => "MALTA" );
        $countryList["MH"] = array("select" => "" , "value" => "MH", "text" => "MARSHALL ISLANDS" );
        $countryList["MQ"] = array("select" => "" , "value" => "MQ", "text" => "MARTINIQUE" );
        $countryList["MR"] = array("select" => "" , "value" => "MR", "text" => "MAURITANIA" );
        $countryList["MU"] = array("select" => "" , "value" => "MU", "text" => "MAURITIUS" );
        $countryList["YT"] = array("select" => "" , "value" => "YT", "text" => "MAYOTTE" );
        $countryList["MX"] = array("select" => "" , "value" => "MX", "text" => "MEXICO" );
        $countryList["FM"] = array("select" => "" , "value" => "FM", "text" => "MICRONESIA, FEDERATED STATES OF" );
        $countryList["MD"] = array("select" => "" , "value" => "MD", "text" => "MOLDOVA, REPUBLIC OF" );
        $countryList["MC"] = array("select" => "" , "value" => "MC", "text" => "MONACO" );
        $countryList["MN"] = array("select" => "" , "value" => "MN", "text" => "MONGOLIA" );
        $countryList["ME"] = array("select" => "" , "value" => "ME", "text" => "MONTENEGRO" );
        $countryList["MS"] = array("select" => "" , "value" => "MS", "text" => "MONTSERRAT" );
        $countryList["MA"] = array("select" => "" , "value" => "MA", "text" => "MOROCCO" );
        $countryList["MZ"] = array("select" => "" , "value" => "MZ", "text" => "MOZAMBIQUE" );
        $countryList["MM"] = array("select" => "" , "value" => "MM", "text" => "MYANMAR" );
        $countryList["NA"] = array("select" => "" , "value" => "NA", "text" => "NAMIBIA" );
        $countryList["NR"] = array("select" => "" , "value" => "NR", "text" => "NAURU" );
        $countryList["NP"] = array("select" => "" , "value" => "NP", "text" => "NEPAL" );
        $countryList["NL"] = array("select" => "" , "value" => "NL", "text" => "NETHERLANDS" );
        $countryList["AN"] = array("select" => "" , "value" => "AN", "text" => "NETHERLANDS ANTILLES" );
        $countryList["XN"] = array("select" => "" , "value" => "XN", "text" => "NEVIS" );
        $countryList["NC"] = array("select" => "" , "value" => "NC", "text" => "NEW CALEDONIA" );
        $countryList["NZ"] = array("select" => "" , "value" => "NZ", "text" => "NEW ZEALAND" );
        $countryList["NI"] = array("select" => "" , "value" => "NI", "text" => "NICARAGUA" );
        $countryList["NE"] = array("select" => "" , "value" => "NE", "text" => "NIGER" );
        $countryList["NG"] = array("select" => "" , "value" => "NG", "text" => "NIGERIA" );
        $countryList["NU"] = array("select" => "" , "value" => "NU", "text" => "NIUE" );
        $countryList["NO"] = array("select" => "" , "value" => "NO", "text" => "NORWAY" );
        $countryList["OM"] = array("select" => "" , "value" => "OM", "text" => "OMAN" );
        $countryList["PK"] = array("select" => "" , "value" => "PK", "text" => "PAKISTAN" );
        $countryList["PW"] = array("select" => "" , "value" => "PW", "text" => "PALAU" );
        $countryList["PA"] = array("select" => "" , "value" => "PA", "text" => "PANAMA" );
        $countryList["PG"] = array("select" => "" , "value" => "PG", "text" => "PAPUA NEW GUINEA" );
        $countryList["PY"] = array("select" => "" , "value" => "PY", "text" => "PARAGUAY" );
        $countryList["PE"] = array("select" => "" , "value" => "PE", "text" => "PERU" );
        $countryList["PH"] = array("select" => "" , "value" => "PH", "text" => "PHILIPPINES" );
        $countryList["PL"] = array("select" => "" , "value" => "PL", "text" => "POLAND" );
        $countryList["PT"] = array("select" => "" , "value" => "PT", "text" => "PORTUGAL" );
        $countryList["PR"] = array("select" => "" , "value" => "PR", "text" => "PUERTO RICO" );
        $countryList["QA"] = array("select" => "" , "value" => "QA", "text" => "QATAR" );
        $countryList["RE"] = array("select" => "" , "value" => "RE", "text" => "REUNION" );
        $countryList["RO"] = array("select" => "" , "value" => "RO", "text" => "ROMANIA" );
        $countryList["RU"] = array("select" => "" , "value" => "RU", "text" => "RUSSIAN FEDERATION" );
        $countryList["RW"] = array("select" => "" , "value" => "RW", "text" => "RWANDA" );
        $countryList["MP"] = array("select" => "" , "value" => "MP", "text" => "SAIPAN" );
        $countryList["WS"] = array("select" => "" , "value" => "WS", "text" => "SAMOA" );
        $countryList["SM"] = array("select" => "" , "value" => "SM", "text" => "SAN MARINO" );
        $countryList["ST"] = array("select" => "" , "value" => "ST", "text" => "SAO TOME AND PRINCIPE" );
        $countryList["SA"] = array("select" => "" , "value" => "SA", "text" => "SAUDI ARABIA" );
        $countryList["SN"] = array("select" => "" , "value" => "SN", "text" => "SENEGAL" );
        $countryList["RS"] = array("select" => "" , "value" => "RS", "text" => "SERBIA" );
        $countryList["SC"] = array("select" => "" , "value" => "SC", "text" => "SEYCHELLES" );
        $countryList["SL"] = array("select" => "" , "value" => "SL", "text" => "SIERRA LEONE" );
        $countryList["SG"] = array("select" => "" , "value" => "SG", "text" => "SINGAPORE" );
        $countryList["SK"] = array("select" => "" , "value" => "SK", "text" => "SLOVAKIA" );
        $countryList["SI"] = array("select" => "" , "value" => "SI", "text" => "SLOVENIA" );
        $countryList["SB"] = array("select" => "" , "value" => "SB", "text" => "SOLOMON ISLANDS" );
        $countryList["SO"] = array("select" => "" , "value" => "SO", "text" => "SOMALIA" );
        $countryList["XS"] = array("select" => "" , "value" => "XS", "text" => "SOMALILAND (NORTH SOMALIA)" );
        $countryList["ZA"] = array("select" => "" , "value" => "ZA", "text" => "SOUTH AFRICA" );
        $countryList["KR"] = array("select" => "" , "value" => "KR", "text" => "SOUTH KOREA" );
        $countryList["ES"] = array("select" => "" , "value" => "ES", "text" => "SPAIN" );
        $countryList["LK"] = array("select" => "" , "value" => "LK", "text" => "SRI LANKA" );
        $countryList["BL"] = array("select" => "" , "value" => "BL", "text" => "ST. BARTHELEMY" );
        $countryList["XE"] = array("select" => "" , "value" => "XE", "text" => "ST. EUSTATIUS" );
        $countryList["KN"] = array("select" => "" , "value" => "KN", "text" => "ST. KITTS" );
        $countryList["LC"] = array("select" => "" , "value" => "LC", "text" => "ST. LUCIA" );
        $countryList["XM"] = array("select" => "" , "value" => "XM", "text" => "ST. MAARTEN" );
        $countryList["VC"] = array("select" => "" , "value" => "VC", "text" => "ST. VINCENT" );
        $countryList["SD"] = array("select" => "" , "value" => "SD", "text" => "SUDAN" );
        $countryList["SR"] = array("select" => "" , "value" => "SR", "text" => "SURINAME" );
        $countryList["SZ"] = array("select" => "" , "value" => "SZ", "text" => "SWAZILAND" );
        $countryList["SE"] = array("select" => "" , "value" => "SE", "text" => "SWEDEN" );
        $countryList["CH"] = array("select" => "" , "value" => "CH", "text" => "SWITZERLAND" );
        $countryList["SY"] = array("select" => "" , "value" => "SY", "text" => "SYRIA" );
        $countryList["PF"] = array("select" => "" , "value" => "PF", "text" => "TAHITI" );
        $countryList["TW"] = array("select" => "" , "value" => "TW", "text" => "TAIWAN" );
        $countryList["TJ"] = array("select" => "" , "value" => "TJ", "text" => "TAJIKISTAN" );
        $countryList["TZ"] = array("select" => "" , "value" => "TZ", "text" => "TANZANIA" );
        $countryList["TH"] = array("select" => "" , "value" => "TH", "text" => "THAILAND" );
        $countryList["TG"] = array("select" => "" , "value" => "TG", "text" => "TOGO" );
        $countryList["TO"] = array("select" => "" , "value" => "TO", "text" => "TONGA" );
        $countryList["TT"] = array("select" => "" , "value" => "TT", "text" => "TRINIDAD AND TOBAGO" );
        $countryList["TN"] = array("select" => "" , "value" => "TN", "text" => "TUNISIA" );
        $countryList["TR"] = array("select" => "" , "value" => "TR", "text" => "TURKEY" );
        $countryList["TM"] = array("select" => "" , "value" => "TM", "text" => "TURKMENISTAN" );
        $countryList["TC"] = array("select" => "" , "value" => "TC", "text" => "TURKS AND CAICOS ISLANDS" );
        $countryList["TV"] = array("select" => "" , "value" => "TV", "text" => "TUVALU" );
        $countryList["UG"] = array("select" => "" , "value" => "UG", "text" => "UGANDA" );
        $countryList["UA"] = array("select" => "" , "value" => "UA", "text" => "UKRAINE" );
        $countryList["AE"] = array("select" => "" , "value" => "AE", "text" => "UNITED ARAB EMIRATES" );
        $countryList["GB"] = array("select" => "" , "value" => "GB", "text" => "UNITED KINGDOM" );
        $countryList["US"] = array("select" => "" , "value" => "US", "text" => "UNITED STATES" );
        $countryList["UM"] = array("select" => "" , "value" => "UM", "text" => "UNITED STATES MINOR OUTLYING ISLANDS" );
        $countryList["UY"] = array("select" => "" , "value" => "UY", "text" => "URUGUAY" );
        $countryList["UZ"] = array("select" => "" , "value" => "UZ", "text" => "UZBEKISTAN" );
        $countryList["VU"] = array("select" => "" , "value" => "VU", "text" => "VANUATU" );
        $countryList["VE"] = array("select" => "" , "value" => "VE", "text" => "VENEZUELA" );
        $countryList["VN"] = array("select" => "" , "value" => "VN", "text" => "VIETNAM" );
        $countryList["VG"] = array("select" => "" , "value" => "VG", "text" => "VIRGIN ISLANDS, BRITISH" );
        $countryList["VI"] = array("select" => "" , "value" => "VI", "text" => "VIRGIN ISLANDS, U.S." );
        $countryList["YE"] = array("select" => "" , "value" => "YE", "text" => "YEMEN" );
        $countryList["ZM"] = array("select" => "" , "value" => "ZM", "text" => "ZAMBIA" );
        $countryList["ZW"] = array("select" => "" , "value" => "ZW", "text" => "ZIMBABWE" );
        $this->setSelected($countryList, $code);
        return $countryList;
    }

    public function setSelected(&$list,$code){
        foreach($list as $key => $data){
            if($data["value"] == $code)
                $list[$key]["select"] = "selected"; 
        }
    }
}
