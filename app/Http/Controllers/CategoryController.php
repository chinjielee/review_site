<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\Categories;
use App\Http\Model\ProductService;
use App\Http\Model\Brands;

class CategoryController extends Controller
{
    const CATEGORY_PER_PAGE = 32;
    const PRODUCT_PER_PAGE =32;
    const PARENT_CAT_1 = 'products';
    const PARENT_CAT_2 = 'services';
    const PARENT_CAT_3 = 'movies';
    //
    public function CategoryIndex()
    {
        $objCategories = new Categories();
        $arrCategories = $objCategories->getTopLevelCategories();
        $categoriesCount = count($arrCategories);
        
        foreach($arrCategories as $category){
            if($category->parent_category_id == 1){
                $category->category_parent_slug = self::PARENT_CAT_1;
            }
            if($category->parent_category_id == 2){
                $category->category_parent_slug = self::PARENT_CAT_2;
            }
            if($category->parent_category_id == 3){
                $category->category_parent_slug = self::PARENT_CAT_3;
            }
            
            if(!empty($category->image)){
                $displayCategoriesId = base64_encode($category->categories_id);
                $category->image = "/".$displayCategoriesId.'/'.$category->image;    
            }
        }
        return view('categories.index',compact('arrCategories','categoriesCount'));
    }

    public function Categorylist(Request $request, $category = 0)
    {
        $objProductService = new ProductService();
        if($category == 0 ){
            if ($request->is('*/products')) {
                //
                $category = $objProductService::PRODUCT_CATEGORY_SLUG;
            }
            if ($request->is('*/services')) {
                //
                $category = $objProductService::SERVICE_CATEGORY_SLUG;
            }
            if ($request->is('*/movies')) {
                //
                $category = $objProductService::MOVIE_CATEGORY_SLUG;
            }
        }
        $subCatCount = 0;
        $label = $request->segment(2);
        list($return_category, $sub_category) = $this->_processDynamicProductRoute($request);
        if(empty($return_category)){
            $return_category = $category;
        }
        $product_slug = "";
        $pageInfo = (object)array();
        $pageInfo->route = $this->_createPageInfoArray($return_category, $sub_category, $product_slug, $label);
        
        $objCategories= new Categories();
        if(!empty($sub_category)){
            $category = $sub_category;
        }
        $categoryDetail = $objCategories::getCategoryIdBySlug($category);
        if($categoryDetail->count() > 0){
            if($categoryDetail[0]->level > 0){
                $showSub = false;
            } else {
                $showSub = true;
            }
            $arrSubCategoryList = $objCategories->withProductCount($categoryDetail[0]->categories_id);
            $arrSubCategoryList= $arrSubCategoryList->paginate(self::CATEGORY_PER_PAGE);
            $subCatCount = $arrSubCategoryList->count();
            if($showSub && $arrSubCategoryList->count() > 0) {
                $arrSubCategoryList = $this->_processCategoryListDetail($category, $arrSubCategoryList, $label);
                $strCategoryTitle = $categoryDetail[0]->categories_name;
                $total = 0;
                foreach($arrSubCategoryList as $temp){
                    if(!empty($temp->image)){
                        $temp->image = $objCategories->processImage($temp->image,$temp->categories_id);
                        $temp->image = $temp->image[0]["path"];
                    }
                    $total += $temp->products_count;
                }
                // Get Famous brand from category
                $arrAllCategoryList = $objCategories->getChildLevelCategories($categoryDetail[0]->categories_id)->toArray();
                $arrAllCategoryList = array_map(function($o){return $o['categories_id'];}, $arrAllCategoryList);
                array_push($arrAllCategoryList,$categoryDetail[0]->categories_id);
                $arrBrands = $objProductService->getTopProductBrandsByCategorisList($arrAllCategoryList);
                return view('categories.sub',compact('arrSubCategoryList','strCategoryTitle','arrBrands','total','pageInfo','subCatCount'));
            }else{
                $arrSubCategoryList = $this->_processCategoryListDetail($category, $arrSubCategoryList, $label);
                $total = 0;
                foreach($arrSubCategoryList as $temp){
                    if(!empty($temp->image)){
                        $temp->image = $objCategories->processImage($temp->image,$temp->categories_id);
                        $temp->image = $temp->image[0]["path"];
                    }
                    $total += $temp->products_count;
                }
            }
            $arrInput = $request->input();
            $arrInput = $this->_processCategoryProductListInput($arrInput);
            $arrProductList = $objProductService->getProductListFullDetailByCategoryId($categoryDetail[0]->categories_id);   
            $arrProductList = $arrProductList->paginate(self::PRODUCT_PER_PAGE);
            $arrBrands = $this->_processBrand($arrProductList);
            $arrProductList = $this->_processCategoryProductListDetail($arrProductList, $arrBrands);
            foreach($arrProductList as $temp){
                if(!empty($temp->images)){
                    $temp->image =  $objProductService->processImage($temp->images,$temp->product_services_id);
                    $temp->image = $temp->image[0]["path"];
                }else{
                    $temp->image = "";
                }
            }
            $strJsonProductsList = json_encode($arrProductList->items());
            $strCategoryTitle = $categoryDetail[0]->categories_name;
            return view('categories.product-list',compact('arrProductList','strCategoryTitle','total','strJsonProductsList','arrBrands','pageInfo','arrSubCategoryList','subCatCount'));
        } else {
            $arrProductList = $objProductService->getProductListFullDetailByCategoryId('0');
            $arrProductList = $arrProductList->paginate(self::PRODUCT_PER_PAGE);
            $arrSubCategoryList = array();
            $arrBrands= array();
            $strCategoryTitle= "";
            $strJsonProductsList = "{}";
            $subCatCount = 0;
            $total= 0;
            return view('categories.product-list',compact('arrProductList','strCategoryTitle','total','strJsonProductsList','arrBrands','pageInfo','arrSubCategoryList','subCatCount'));
        }
    }

    public function CategoryProductlist(Request $request, $category, $sub_category)
    {
        // Get Categories sub categorys 
        $arrInput = $request->input();
        $categoryDetail = Categories::getCategoryIdBySlug($sub_category);
        $objProductService = new ProductService();
        $arrInput = $this->_processCategoryProductListInput($arrInput);
        $arrProductList = $objProductService->getProductListFullDetailByCategoryId($categoryDetail[0]->categories_id, $arrInput);
        $arrProductList = $arrProductList->paginate(self::PRODUCT_PER_PAGE);
        $label = $request->segment(2);
        list($return_category, $sub_category) = $this->_processDynamicProductRoute($request);
        if(empty($return_category)){
            $return_category = $category;
        }
        $product_slug = "";
        $pageInfo = (object)array();
        $pageInfo->route = $this->_createPageInfoArray($return_category, $sub_category, $product_slug, $label);
        
        $arrBrands = $this->_processBrand($arrProductList);
        
        $arrProductList = $this->_processCategoryProductListDetail($arrProductList, $arrBrands);
        
        $strJsonProductsList = json_encode($arrProductList->items());
        $strCategoryTitle = $categoryDetail[0]->categories_name;
        $subCatCount = 0;
        return view('categories.product-list',compact('arrProductList','strCategoryTitle','strJsonProductsList','arrBrands','pageInfo','subCatCount'));
    }


    private function _processCategoryListDetail($category, $arrSubCategories, $label) {
        foreach($arrSubCategories as $subCategory ){
            if($label == $category){
                $subCategory->link = route('Category '.ucfirst($label).' List', [$subCategory->category_slug]);
            } else {
                $subCategory->link = route(ucfirst($label).' Sub Category List', [$category, $subCategory->category_slug]);
            }
        }
        return $arrSubCategories;
    }

    private function _processCategoryProductListDetail($arrProductList, $arrBrands)
    {
        foreach($arrProductList as $product){
            if($arrBrands->count() > 0 ) {
                if(isset($arrBrands[$product->brands_id]['brands_title'])){
                    $product->brand_title = $arrBrands[$product->brands_id]['brands_title'];
                }
            }
            if ($product->category_slug != NULL and $product->parent_cat_1 != NULL and $product->parent_cat_2 != NULL) {
                $product->link = route(ucfirst($product->parent_cat_2) . ' Detail3', [$product->parent_cat_1, $product->category_slug, $product->product_slug]);
            } elseif ($product->category_slug != NULL and $product->parent_cat_1 != NULL) {
                $product->link = route(ucfirst($product->parent_cat_1) . ' Detail2', [$product->category_slug, $product->product_slug]);
            } else {
                $product->link = route(ucfirst($product->category_slug) . ' Detail1', [$product->product_slug]);
            }
            $objProductService = new ProductService();
            $product->images = $objProductService->processImage($product->images,$product->product_services_id);
            $product->images = $product->images[0]["path"];
        }
        return $arrProductList;
    }

    private function _processBrand($arrProductList) {
        $arrProductsList = $arrProductList->items();
        $arrResultBrandList = array_map(function($o){return $o['brands_id'];}, $arrProductsList);
        $newArrayCount = array_count_values($arrResultBrandList);
        $objBrands = new Brands();
        $objBrandsList = $objBrands->getMultipleBrandDetail($arrResultBrandList);
        foreach($newArrayCount as $brandId => $count) {
            if(!empty($brandId)){
                $objBrandsList[$brandId] = array(
                    "brands_id"=> $brandId,
                    "brands_title"=> $objBrandsList[$brandId],
                    "count"=>$newArrayCount[$brandId]
                );
            }
        }
        return $objBrandsList;
    }

    private function _processCategoryProductListInput($arrInput){
        // Input mapping
        $newInput = array();
        if(isset($arrInput['brands'])){
            $newInput['brands_id'] = $arrInput['brands']; 
        }
        return $newInput;
    }

    private function _processDynamicProductRoute($request) {
        $category = $request->route()->parameter('category');
        $sub_category = $request->route()->parameter('sub_category');
        $product_slug = $request->route()->parameter('product_slug');
        return array($category,$sub_category,$product_slug);
    } 

    private function _createPageInfoArray($category, $sub_category, $product_slug, $label) {
        $arrReturnPageInfo = array();
        $arrReturnPageInfo['label'] = $label;
        if(!empty($category)) {
            $arrReturnPageInfo['category'] = $category;
        } else {
            $arrReturnPageInfo['category'] = "";
        }
        if(!empty($sub_category)) {
            $arrReturnPageInfo['sub_category'] = $sub_category;
        } else {
            $arrReturnPageInfo['sub_category'] = "";
        }
        if(!empty($product_slug)) {
            $arrReturnPageInfo['product_slug'] = $product_slug;
        } else {
            $arrReturnPageInfo['product_slug'] = "";
        }
        return $arrReturnPageInfo;
    }
}
