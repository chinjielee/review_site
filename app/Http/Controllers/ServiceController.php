<?php

namespace App\Http\Controllers;
use Auth;
use Illuminate\Http\Request;
use App\Http\Model\ProductService;
use App\Http\Model\PendingQuestion;
use App\Http\Model\User;
use App\Http\Model\ReviewVote;
use App\Http\Model\ProductReview;
use App\Http\Model\ReviewReportFraud;
use App\Traits\WikaboTrait;

class ServiceController extends Controller
{
    use WikaboTrait;    
    const REVIEW_PER_PAGE = 5;
    const QUEST_PER_PAGE = 2;
    const ANS_PER_QUEST = 10;
    const TOP_COMMENT_AMOUNT = 5;
    const MAX_RATING_POINT = 25;

    public function index(){
        $objProductService = new ProductService();
        $arrProductList = $objProductService->getProductListFullDetailByCategoryId($objProductService::SERVICE_CATEGORY_ID,array());
        return view('products.index',compact('arrProductList'));
    }
    
    public function detail(Request $request)
    {
        // Get which label came from
        $label = $request->segment(2);
        list($category, $sub_category, $product_slug) = $this->processDynamicProductRoute($request);
        $objProductService = new ProductService();
        $objProductTopDetail = $this->_productTopDetail($product_slug);
        if($objProductTopDetail) {
            $objProductCountDetail = $this->_productCountDetail($objProductTopDetail->detail->product_services_id);
            $pageInfo = (object)array();
            $pageInfo->route = $this->_createPageInfoArray($category, $sub_category, $product_slug, $label);
            $pageInfo->subpage = 'detail';
            $pageInfo->url = $objProductTopDetail->url;
            $arrDetail = $objProductTopDetail->detail;
            //$arrProductReview = $objProductReview->getProductReview($arrDetail->product_services_id);
            $view_token = date('Y-m-d H:i:s')." ".$arrDetail->product_slug;
            $arrProductSpec = json_decode($arrDetail->product_service_attributes);
            // Process Spec keyword mapping
            $current_recent_view = $this->ProcessRecentViewCookie();
            $arrProductSpec = $this->_processKeyMap($arrProductSpec);
            return view('services.detail', compact('pageInfo', 'arrDetail','arrProductSpec','objProductTopDetail','view_token','current_recent_view','objProductCountDetail'));
        }else {
            return redirect()->route('home');
        }
    }

    public function review(Request $request)
    {
        $label = $request->segment(2);
        list($category, $sub_category, $product_slug) = $this->processDynamicProductRoute($request);
        $objProductService = new ProductService();
        $objReviewVote = new ReviewVote();
        $objReviewReportFraud = new ReviewReportFraud();
        $arrUserVotedReview = array();
        $objProductTopDetail = $this->_productTopDetail($product_slug);
        if($objProductTopDetail) {
            $objProductCountDetail = $this->_productCountDetail($objProductTopDetail->detail->product_services_id);
            $pageInfo = (object)array();
            $pageInfo->route = $this->_createPageInfoArray($category, $sub_category, $product_slug, $label);
            $pageInfo->subpage = 'review';
            $pageInfo->url = $objProductTopDetail->url;
            $arrDetail = (object)array();
            if(isset($objProductTopDetail->detail)){
                $arrDetail = $objProductTopDetail->detail;
            }

            $arrProductReview = $objProductService->find($arrDetail->product_services_id)->reviews()->paginate(self::REVIEW_PER_PAGE);
            $productReviewIds = array_map(function($o){return $o->product_reviews_id;}, $arrProductReview->items());
            $user = Auth::user();

            if($user) {
                $arrUserVotedReview = $objReviewVote->getReviewVoteByReview($productReviewIds, $user->user_id);
                $arrUserVotedReview = $arrUserVotedReview->keyBy('product_review_id');

                $arrUserFraudReview = $objReviewReportFraud->getReviewFraudByReview($productReviewIds, $user->user_id);
                $arrUserFraudReview = $arrUserFraudReview->keyBy('product_review_id');
            }
        
            foreach($arrProductReview as $review) {
                $this->_processReviewFurtherData($review);
            }

            return view('services.review',compact('pageInfo', 'arrDetail','arrProductReview','objProductTopDetail','arrUserVotedReview','arrUserFraudReview','objProductCountDetail'));
        } else {
            return redirect()->route('home');
        }
    }

    public function qa(Request $request)
    {
        $label = $request->segment(2);
        list($category, $sub_category, $product_slug) = $this->processDynamicProductRoute($request);
        $objProductService = new ProductService();
        $objProductTopDetail = $this->_productTopDetail($product_slug);
        if($objProductTopDetail) {
            $objProductCountDetail = $this->_productCountDetail($objProductTopDetail->detail->product_services_id);
            $pageInfo = (object)array();
            $pageInfo->route = $this->_createPageInfoArray($category, $sub_category, $product_slug, $label);
            $pageInfo->subpage = 'Question and Answer';
            $pageInfo->url = $objProductTopDetail->url;
            $arrDetail = (object)array();
            if(isset($objProductTopDetail->detail)){
                $arrDetail = $objProductTopDetail->detail;
            }
            $user = Auth::user();
            // Get QnA details
            $arrProductQuestionAnswer = $objProductService->find($arrDetail->product_services_id)->questions()->paginate(self::QUEST_PER_PAGE);
            foreach($arrProductQuestionAnswer as $question){
                $question->user = $question->user()->get()->first();
                // If logged In, get user logged qna responses
                if($user){
                    if($user->user_id == $question->user->user_id) {
                        $question->user->owner = true;
                    }
                }
                
                $question->answer = $question->answers()->paginate(self::ANS_PER_QUEST, ['*'], 'answer_page');
                foreach($question->answer as $answer){
                $answer->user = $answer->user()->get()->first();  
                    if($user){
                        if($user->user_id == $answer->user->user_id) {
                            $answer->user->owner = true;
                        }
                    }
                }
            }
            // Get user pending question for this products
            $arrPendingQuestion = array();
            if($user) {
                $objPendingQuestion = new PendingQuestion();
                $arrPendingQuestion = $objPendingQuestion->getPendingProductQuestionByUser($arrDetail->product_services_id, $user->user_id);
                $arrPendingQuestion->user = $user;
            }
            return view('services.qa',compact('pageInfo', 'arrDetail','objProductTopDetail','arrProductQuestionAnswer','arrPendingQuestion','objProductCountDetail'));
        } else {
            return redirect()->route('home');
        }
    }

    public function stats(Request $request)
    {
        $label = $request->segment(2);
        list($category, $sub_category, $product_slug) = $this->processDynamicProductRoute($request);
        $objProductService = new ProductService();
        $objProductReview = new ProductReview();
        $objProductTopDetail = $this->_productTopDetail($product_slug);
        if($objProductTopDetail) {
            $objProductCountDetail = $this->_productCountDetail($objProductTopDetail->detail->product_services_id);
            $pageInfo = (object)array();
            $pageInfo->route = $this->_createPageInfoArray($category, $sub_category, $product_slug, $label);
            $pageInfo->subpage = 'stat';
            $pageInfo->url = $objProductTopDetail->url;

            $arrDetail = $objProductTopDetail->detail;
            $statDetail = $objProductReview->getProductReviewStat($arrDetail->product_services_id);
            if(!empty($statDetail)) {
                $statDetail = $this->_preprocessStatData($statDetail);
                $statDetail = json_encode((object)$statDetail->toArray());
            } else {
                $statDetail = json_encode((object)array());
            }
            $product_review_rating = $objProductReview->getProductReviewRatingStat($arrDetail->product_services_id);
            if(!empty($product_review_rating[0])){
                $statDetail2 = array();
                $rating_1 = $rating_2 = $rating_3 = $rating_4 = $rating_5 = 0;
                $rating_1_rate = $rating_2_rate = $rating_3_rate = $rating_4_rate = $rating_5_rate = 0;
                $rating_1_rate_ave = $rating_2_rate_ave = $rating_3_rate_ave = $rating_4_rate_ave = $rating_5_rate_ave = 0;
                $rating_naming = array();
                foreach($product_review_rating as $product_review_rating_details){
                    $indi_rating = json_decode($product_review_rating_details->rating);
                    $count = 1;
                    foreach($indi_rating as $key=>$value){
                        array_push($rating_naming,ucwords($key));
                        if(isset($indi_rating->$key)){
                            ${'rating_'.$count}++;
                            ${'rating_'.$count.'_rate'} += $indi_rating->$key;
                        }
                        $count++;
                        if($count == 6){
                            break;
                        }
                    }

                }
                $rating_1_rate_ave = $rating_1_rate / $rating_1;
                $rating_2_rate_ave = $rating_2_rate / $rating_2;
                $rating_3_rate_ave = $rating_3_rate / $rating_3;
                $rating_4_rate_ave = $rating_4_rate / $rating_4;
                $rating_5_rate_ave = $rating_5_rate / $rating_5;
                $statDetail2 = '{"0":{"rating":'.$rating_1_rate_ave.',"key":"'.$rating_naming[0].' [Total '.$rating_1.' Votes]"},"1":{"rating":'.$rating_2_rate_ave.',"key":"'.$rating_naming[1].' [Total '.$rating_2.' Votes]"},"3":{"rating":'.$rating_3_rate_ave.',"key":"'.$rating_naming[2].' [Total '.$rating_3.' Votes]"},"4":{"rating":'.$rating_4_rate_ave.',"key":"'.$rating_naming[3].' [Total '.$rating_4.' Votes]"},"5":{"rating":'.$rating_5_rate_ave.',"key":"'.$rating_naming[4].' [Total '.$rating_5.' Votes]"}}';
            } else {
                $statDetail2 = '{"0":{"rating":0,"key":"Reliability [Total 0 Votes]"},"1":{"rating":0,"key":"User Friendliness [Total 0 Votes]"},"3":{"rating":0,"key":"Standards [Total 0 Votes]"},"4":{"rating":0,"key":"Professionalism [Total 0 Votes]"},"5":{"rating":0,"key":"Price [Total 0 Votes]"}}';
            }
            return view('services.stats',compact('pageInfo', 'arrDetail','objProductTopDetail','statDetail','statDetail2','objProductCountDetail'));
        } else {
            return redirect()->route('home');
        }
    }

    public function photos(Request $request)
    {
        $label = $request->segment(2);
        list($category, $sub_category, $product_slug) = $this->processDynamicProductRoute($request);
        $objProductService = new ProductService();
        $objProductTopDetail = $this->_productTopDetail($product_slug);
        if($objProductTopDetail) {
            $objProductCountDetail = $this->_productCountDetail($objProductTopDetail->detail->product_services_id);
            $pageInfo = (object)array();
            $pageInfo->route = $this->_createPageInfoArray($category, $sub_category, $product_slug, $label);
            $pageInfo->subpage = 'photo';
            $pageInfo->url = $objProductTopDetail->url;
            $arrDetail = $objProductTopDetail->detail;
            // Get Images List
            $arrImages = $arrDetail->images;
            return view('services.photos',compact('pageInfo', 'arrDetail','objProductTopDetail','objProductCountDetail','arrImages'));
        } else {
            return redirect()->route('home');
        }
    }

    public function prices(Request $request)
    {
        $label = $request->segment(2);
        list($category, $sub_category, $product_slug) = $this->processDynamicProductRoute($request);
        $objProductService = new ProductService();
        $objProductTopDetail = $this->_productTopDetail($product_slug);
        if($objProductTopDetail) {
            $objProductCountDetail = $this->_productCountDetail($objProductTopDetail->detail->product_services_id);
            $pageInfo = (object)array();
            $pageInfo->route = $this->_createPageInfoArray($category, $sub_category, $product_slug, $label);
            $pageInfo->subpage = 'price';
            $pageInfo->url = $objProductTopDetail->url;
            $arrDetail = $objProductTopDetail->detail;
            return view('services.prices',compact('pageInfo', 'arrDetail','objProductTopDetail','objProductCountDetail'));
        } else {
            return redirect()->route('home');
        }
    }

    private function _productTopDetail($product_slug) {
        $objTopDetail = (object) array();
        $objProductService = new ProductService();
        $objTopDetail->detail = $objProductService->getProductDetailsBySlugWithFullCategory($product_slug);
        if(empty($objTopDetail->detail)) {
            $objTopDetail->detail = $objProductService->getProductDetailsBySlug($product_slug);
        }
        // Product not exist
        if(empty($objTopDetail->detail)){
            return false;
        }
        $objTopDetail->topReview = $objProductService->find($objTopDetail->detail->product_services_id)->reviews()->orderBy('up_vote','desc')->orderBy('down_vote','asc')->take(self::TOP_COMMENT_AMOUNT)->get();
        $objTopDetail->rating = $objProductService->processOverallRatingDisplay($objTopDetail->detail->product_rating);
        $objTopDetail->url = $objProductService->getAllProductUrl($objTopDetail->detail);
        // Process image url 
        $objTopDetail->detail->images = $objProductService->processImage($objTopDetail->detail->images,$objTopDetail->detail->product_services_id);
        return $objTopDetail;
    }

    private function _productCountDetail($product_id) {
        $objCount = (object)array();
        $objProductService = new ProductService();
        $product = $objProductService->find($product_id);
        $objCount->questionCount = $product->questionCount();
        $objCount->qnaCount = $product->questionAndAnswerCount();
        $objCount->reviewCount = $product->reviewCount();
        $objCount->reviewUserCount = !empty($product->reviewUserCount()) ? $product->reviewUserCount() : 0;
        return $objCount;
    }

    private function _processKeyMap($arrSpec) {
        if(isset($arrSpec->released) || isset($arrSpec->released_date) || isset($arrSpec->releaseDate) || isset($arrSpec->{'release date'})){
            if(isset($arrSpec->released)){$arrSpec->release_date = $arrSpec->released;}
            if(isset($arrSpec->released_date)){$arrSpec->release_date = $arrSpec->released_date;}
            if(isset($arrSpec->releaseDate)){$arrSpec->release_date = $arrSpec->releaseDate;}
            if(isset($arrSpec->{'release date'})){$arrSpec->release_date = $arrSpec->{'release date'};}
            unset($arrSpec->released);
            unset($arrSpec->released_date);
            unset($arrSpec->releaseDate);
        }
        return $arrSpec;
    }

    private function processDynamicProductRoute($request) {
        $category = $request->route()->parameter('category');
        $sub_category = $request->route()->parameter('sub_category');
        $product_slug = $request->route()->parameter('service');
        return array($category,$sub_category,$product_slug);
    } 

    private function _createPageInfoArray($category, $sub_category, $product_slug, $label) {
        $arrReturnPageInfo = array();
        $arrReturnPageInfo['label'] = $label;
        if(!empty($category)) {
            $arrReturnPageInfo['category'] = $category;
        } else {
            $arrReturnPageInfo['category'] = "";
        }
        if(!empty($sub_category)) {
            $arrReturnPageInfo['sub_category'] = $sub_category;
        } else {
            $arrReturnPageInfo['sub_category'] = "";
        }
        if(!empty($product_slug)) {
            $arrReturnPageInfo['product_slug'] = $product_slug;
        } else {
            $arrReturnPageInfo['product_slug'] = "";
        }
        return $arrReturnPageInfo;
    }

    private function _processReviewFurtherData(&$review){
        $review->user = $review->user()->first();
        if(!empty($review->rating)){
            if(json_decode($review->rating)){
                $review->rating = json_decode($review->rating);
            }
        }
        $user = Auth::user();
        if($user){
            if($user->user_id == $review->user->user_id) {
                $review->user->owner = true;
            } else {
                $review->user->owner = false;
            }

        }
        // Key Highlight
        $arrKeyHighlight = array();
        if(strpos($review->key_highlight,',') > 0) {
            $arrKeyHighlight = explode(',', $review->key_highlight);
        } else {
            $arrKeyHighlight = array($review->key_highlight);
        }
        $review->key_highlight = $arrKeyHighlight;

        // Images 
        $arrImages = array();
        if(!empty($review->review_image)){
            if(strpos($review->review_image,',') > 0) {
                $arrImages = explode(',', $review->review_image);
            } else {
                $arrImages = array($review->review_image);
            }
        }
        $displayReviewId = base64_encode($review->product_reviews_id);
        foreach($arrImages as $key=>$value){
            $arrImages[$key] = "images/review/".$displayReviewId."/review/".$value;
        }
        $review->review_image = $arrImages;
    }

    // Actions
    public function searchProduct($wildcard="", $limit = 10) {
        if(!empty($wildcard)){
            $objProductService = new ProductService();
            $arrResult = $objProductService->wildcardSearchProduct($wildcard, $limit);
            foreach($arrResult as $result){
                $result->product_rating = $objProductService->processOverallRatingDisplay($result->product_rating);
            }
            return response()->json($arrResult);
        } else {
            return response()->json('');
        }
        
    }

    private function _preprocessStatData($arrData) {
        // Process Month display
        foreach($arrData as $data){
            switch($data->key){
                case 1 :
                    $data->key = "Jan";
                    break;
                case 2 :
                    $data->key = "Feb";
                    break;
                case 3 :
                    $data->key = "Mar";
                    break;
                case 4 :
                    $data->key = "Apr";
                    break;
                case 5 :
                    $data->key = "May";
                    break;
                case 6 :
                    $data->key = "Jun";
                    break;
                case 7 :
                    $data->key = "Jul";
                    break;
                case 8 :
                    $data->key = "Aug";
                    break;
                case 9 :
                    $data->key = "Sep";
                    break;
                case 10 :
                    $data->key = "Oct";
                    break;
                case 11 :
                    $data->key = "Nov";
                    break;
                case 12 :
                    $data->key = "Dec";
                    break;
            }
        }
        return $arrData;
    }

    // private function _processQuestionFurtherData(&$review){
    //     $review->user = $review->user()->first();
    //     // Key Highlight
    //     $arrKeyHighlight = array();
    //     if(strpos($review->key_highlight,',') > 0) {
    //         $arrKeyHighlight = explode(',', $review->key_highlight);
    //     } else {
    //         $arrKeyHighlight = array($review->key_highlight);
    //     }
    //     $review->key_highlight = $arrKeyHighlight;

    //     // Images 
    //     $arrImages = array();
    //     if($review->review_image !== ''){
    //         if(strpos($review->review_image,',') > 0) {
    //             $arrImages = explode(',', $review->review_image);
    //         } else {
    //             $arrImages = array($review->review_image);
    //         }
    //     }
        
    //     $review->review_image = $arrImages;

    // }
}
