<?php

namespace App\Http\Controllers\Auth;

use App\Http\Model\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Http\Model\SecurityQuestion;
use App\Http\Model\UserSecurityQnA;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Jobs\SendVerificationEmail;
use App;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = 'profile';
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $messages = [
            'qna1.not_in' => 'Security Question is not selected.',
            //'dob.date' => 'Date of Birth is not a valid date (YYYY-MM-DD).',
            't&c.accepted' => 'Please agree to WIKÅBÖ\'s Terms of Use and Privacy Policy before proceed.'
        ];
        return Validator::make($data, [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:4|confirmed',
            'mobile_code' => 'required',
            'mobile' => 'required|string|min:7',
            'country' => 'required|string',
            'gender' => 'in:Male,Female',
            'qna1' => 'notIn:0',
            't&c' => 'accepted'
        ],$messages);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $dob = "";
        if(!empty($data['dob'])){
            $dob = Carbon::parse($data['dob'])->format('Y-m-d');
        }
        $newUser = User::create([
            'first_name' => $data['firstname'],
            'last_name' => $data['lastname'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'mobile_code' => $data['mobile_code'],
            'mobile' => $data['mobile'],
            'gender' => $data['gender'],
            'country' => $data['country'],
            'dob' => $dob,
            'language' => APP::getlocale()
        ]);
        $newUser->email_token = base64_encode($newUser->user_id."-".$data['email']);
        $newUser->save();
        $newUserId = $newUser->user_id;
        // Loop total qna user inserted
        if(isset($data['qna1']) && $data['qna1'] != "0" && !empty($data['qna-ans1'])){
            UserSecurityQnA::create([
                'user_id' => $newUserId,
                'questions_id'  => $data['qna1'],
                'answer' => strtolower(trim($data['qna-ans1'])),
            ]);
        }
        if(isset($data['qna2']) && $data['qna2'] != "0" && !empty($data['qna-ans2'])){
            UserSecurityQnA::create([
                'user_id' => $newUserId,
                'questions_id'  => $data['qna2'],
                'answer' => strtolower(trim($data['qna-ans2'])),
            ]);
        }
        if(isset($data['qna3']) && $data['qna3'] != "0" && !empty($data['qna-ans3'])){
            UserSecurityQnA::create([
                'user_id' => $newUserId,
                'questions_id'  => $data['qna3'],
                'answer' => strtolower(trim($data['qna-ans3'])),
            ]);
        }
        return $newUser;
    }

    /**
     * Create a new user form.
     *
     * @return \App\User
     */
    public function showRegistrationForm()
    {
        $objSecurityQuestion = new SecurityQuestion();
        $arrSecurityQuestions = $objSecurityQuestion->getFullQuestionList();
        $socialLogin = true;
        return view('auth.register',compact('arrSecurityQuestions','socialLogin'));
    }
    #getFullQuestionList

    /**
    * Handle a registration request for the application.
    *
    * @param \Illuminate\Http\Request $request
    * @return \Illuminate\Http\Response
    */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        event(new Registered($user = $this->create($request->all())));
        dispatch(new SendVerificationEmail($user));
        return view('email.verification');
    }

    /**
    * Handle a registration request for the application.
    *
    * @param $token
    * @return \Illuminate\Http\Response
    */
    public function verify($token)
    {
        $user = User::where('email_token',$token)->first();
        $user->user_status = 1;
        if($user->save()){
            return view('email.emailconfirm',['user'=>$user]);
        }
    }
}
