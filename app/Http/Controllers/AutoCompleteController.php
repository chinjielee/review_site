<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Model\ProductService;

class AutoCompleteController extends Controller
{
    //
    public function product($wildcard,$limit) {
        $objProduct = new ProductService();
        return $objProduct->wildcardSearchProduct($wildcard,$limit);
    }
}
