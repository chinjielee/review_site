<?php

namespace App\Http\Controllers;
use Auth;
use Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Model\NotificationSetting;
use App\Http\Model\UserActivityLog;
use App\Http\Model\ProductReview;
use App\Http\Model\ProductAnswer;
use App\Http\Model\ProductQuestion;
use App\Http\Model\ProductService;
use App\Http\Model\PendingProductService;
use App\Traits\WikaboTrait;
use App\Events\NotificationReadAll;
use App\Http\Model\ConversationUser;

class UserController extends Controller
{
    use WikaboTrait;
    const NOTIFICATION_PER_PAGE = 50;
    const COUNTRY = array('Malaysia','Singapore');
    public function account(){
        $pageInfo = (object)array();
        $pageInfo->page = "Profile";
        $countryList = self::COUNTRY;
        return view('profile.my-profile',compact('pageInfo','countryList'));
    }

    public function activity(){
        $pageInfo = (object)array();
        $objUserActivityLog = new UserActivityLog();
        $arrUserActivityLog = $objUserActivityLog->getUserActivityLogByUserId(Auth::user()->user_id);
        $currentVariableDate = "";
        $tempCounter = 1;
        for($i = sizeof($arrUserActivityLog)-1; $i >= 0; $i--){
            $arrUserActivityLog[$i]->span = 0;
            if(empty($currentVariableDate)){
                $currentVariableDate = $arrUserActivityLog[$i]->date;
                continue;
            }else{
                if($arrUserActivityLog[$i]->date == $currentVariableDate){
                    $tempCounter++;
                }else{
                    $arrUserActivityLog[$i+1]->span = $tempCounter;
                    $currentVariableDate = $arrUserActivityLog[$i]->date;
                    $tempCounter = 1;
                }
            }

            if($i-1 == -1){
                $arrUserActivityLog[$i]->span = $tempCounter;
            }
        }
        $pageInfo->page = "Activity";
        return view('profile.activity-log',compact('pageInfo','arrUserActivityLog'));
    }

    public function answers(){
        $pageInfo = (object)array();
        $pageInfo->page = "Answers";
        return view('profile.my-answers',compact('pageInfo'));
    }

    public function comment(){
        $pageInfo = (object)array();
        $pageInfo->page = "Comment";
        $objProductAnswer = new ProductAnswer();
        $arrProductAnswer = $objProductAnswer->getCommentByCurrentUser();
        $noData = true;
        if(!empty($arrProductAnswer[0])){
            $objProductDetail = new ProductService();
            foreach($arrProductAnswer as $ProductAnswer){
                $productUrl = $objProductDetail->getProductDetailUrlBySlug($ProductAnswer->product_slug);
                $ProductAnswer->key = $ProductAnswer->product_services_id."-1".$ProductAnswer->product_questions_id."-".$ProductAnswer->user_id;
                $ProductAnswer->service_url = $productUrl;
                $ProductAnswer->question_url = $productUrl."/qa";
            }
            $noData = false;
        }
        return view('profile.my-comments',compact('pageInfo','noData','arrProductAnswer'));
    }

    public function dashboard(){
        $pageInfo = (object)array();
        $pageInfo->page = "Dashboard";
        $review_count = $comment_count = $question_count = 0;
        $objProductReview = new ProductReview();
        $objProductAnswer = new ProductAnswer();
        $objProductQuestion = new ProductQuestion();
        $objProductService = new ProductService();
        $review_count = $objProductReview->getCountReviewByCurrentUser();
        $comment_count = $objProductAnswer->getCountCommentByCurrentUser();
        $question_count = $objProductQuestion->getCountQuestionByCurrentUser();
        $value_array = $this->ProcessRecentViewCookieReturnValue();
        $arrProductService = array();
        foreach($value_array as $recent_view){
            $temp_result = $objProductService->getProductDetailsBySlug($recent_view);
            if(!empty($temp_result)){
                $temp_result->service_url = $objProductService->getProductDetailUrlBySlug($recent_view);
                $temp_result->rating = "0.0"; 
                if(isset(json_decode($temp_result->product_rating)->overall)){
                    $temp_result->rating = (json_decode($temp_result->product_rating)->overall);
                }

                if(!isset($temp_result->images) || empty($temp_result->images)){
                    $temp_result->images = "default_service_logo.png"; 
                }else{
                    $temp_result->images = $objProductService->processImage($temp_result->images,$temp_result->product_services_id);
                    $temp_result->images = $temp_result->images[0]["path"];
                }

                array_push($arrProductService,$temp_result);
            }
        }  
        return view('profile.dashboard',compact('pageInfo','review_count','comment_count','question_count','arrProductService'));
    }

    public function notification(Request $request){
        $pageInfo = (object)array();
        $pageInfo->page = "Notification";
        $objProductAnswer = new ProductAnswer();
        if(!empty($request->user()->unreadNotifications()->get())) {
            $request->user()->unreadNotifications()->get()->each(function ($n) {
                $n->markAsRead();
            });
            event(new NotificationReadAll($request->user()->id));
        }
        $arrNotification = $request->user()->notifications()->paginate(self::NOTIFICATION_PER_PAGE);
        return view('profile.notification',compact('pageInfo','arrNotification'));
    }

    public function notificationSetting(){
        $pageInfo = (object)array();
        $pageInfo->page = "Notification Settings";
        $objNotificationSetting = new NotificationSetting();
        $arrNotificationSetting = $objNotificationSetting->getNotificationSettingByUserId(Auth::user()->user_id);
        return view('profile.notification-settings',compact('pageInfo','arrNotificationSetting'));
    }

    public function question(){
        $pageInfo = (object)array();
        $pageInfo->page = "Question";
        $objProductQuestion = new ProductQuestion();
        $arrProductQuestion = $objProductQuestion->getQuestionByCurrentUser();
        $noData = true;
        if(!empty($arrProductQuestion[0])){
            $objProductDetail = new ProductService();
            foreach($arrProductQuestion as $ProductQuestion){
                $productUrl = $objProductDetail->getProductDetailUrlBySlug($ProductQuestion->product_slug);
                $ProductQuestion->key = $ProductQuestion->product_services_id."-1".$ProductQuestion->product_questions_id."-".$ProductQuestion->user_id;
                $ProductQuestion->service_url = $productUrl;
                $ProductQuestion->question_url = $productUrl."/qa";
            }
            $noData = false;
        }
        return view('profile.my-questions',compact('pageInfo','noData','arrProductQuestion'));
    }

    public function review(){
        $pageInfo = (object)array();
        $pageInfo->page = "Review";
        $objProductReview = new ProductReview();
        $arrProductReview = $objProductReview->getReviewByCurrentUser();
        $noData = true;
        if(!empty($arrProductReview[0])){
            $objProductDetail = new ProductService();
            foreach($arrProductReview as $ProductReview){
                if(!empty($ProductReview->product_slug)){
                    $productUrl = $objProductDetail->getProductDetailUrlBySlug($ProductReview->product_slug);
                    $ProductReview->key = $ProductReview->product_services_id."-1".$ProductReview->product_reviews_id."-".$ProductReview->user_id;
                    $ProductReview->service_url = $productUrl;
                    $ProductReview->review_url = $productUrl."/review";
                } elseif(!empty($ProductReview->temp_product_services_id)) {
                    // Get temp product service details
                    
                    $objPendingProductService = new PendingProductService();
                    $temporary = $objPendingProductService->getProductDetailsBySlugWithFullCategoryByTempId($ProductReview->temp_product_services_id);
                    $ProductReview->product_service_title = $temporary->product_name." (Pending)";
                    $productUrl = $objProductDetail->getTempProductReviewlUrl($ProductReview->temp_product_services_id);
                    $ProductReview->key = $ProductReview->product_services_id."-1".$ProductReview->product_reviews_id."-".$ProductReview->user_id;
                    $ProductReview->service_url = $productUrl;
                    $ProductReview->review_url = $productUrl;
                } else {
                    $ProductReview->key = $ProductReview->product_services_id."-1".$ProductReview->product_reviews_id."-".$ProductReview->user_id;
                    $ProductReview->service_url = $productUrl;
                    $ProductReview->review_url = $productUrl."/review";
                }
                if(!empty($ProductReview->rating)) {
                    $objProductRating = json_decode($ProductReview->rating);
                    $ProductReview->rating = $objProductRating;
                }
                
            }
            $noData = false;
        }
        return view('profile.my-reviews',compact('pageInfo','noData','arrProductReview'));
    }

    public function update_avatar(Request $request){
        $this->validate($request,[
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $user = Auth::user();
        $avatarName = $user->user_id.'_avatar'.time().'.'.$request->avatar->getClientOriginalExtension();
        $request->avatar->move(public_path('images/user'), $avatarName);
        $user->avatar = $avatarName;
        $user->save();
        return back()->with('success','You have successfully upload image.');
    }

    public function update_profile(Request $request){
        $rules = [
            'firstname'     => 'required|string|max:255',
            'lastname'      => 'required|string|max:255',
            'mobile_code'   => 'required',
            'mobile'        => 'required|string|min:7',
            'gender'        => 'required|in:Male,Female',
            'language'      => 'required|in:English,Chinese'
        ];
        $messages = [
            //'dob.date'      => 'Date of Birth is not a valid date (YYYY-MM-DD).',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if($validator->fails()){
            view('profile.my-profile',compact('pageInfo'));
            return redirect()->route('My Profile')->withErrors($validator)->withInput();
        }else{
            $user = Auth::user();
            $user->first_name = $request->input('firstname');
            $user->last_name = $request->input('lastname');
            $user->mobile_code = $request->input('mobile_code');
            $user->mobile = $request->input('mobile');
            $user->gender = $request->input('gender');
            $user->dob = !empty($request->input('dob')) ? $request->input('dob'): '2000-01-31';
            $language = $request->input('language');
            switch ($language){
                case "English" : $language = "en"; break;
                case "Chinese" : $language = "cn"; break;
                default : $language = "en"; break;
            }
            $user->language = $language;
            $user->save();
            session()->flash('success', 'Your profile was updated.');
            return redirect()->route('My Profile');
        }
    }

    public function update_address(Request $request){
        $rules = [
            'address'     => 'required|string|max:300',
            'postcode'      => 'required|string|min:4|max:10',
            'state'        => 'required|string|min:3|max:100',
            'country'        => 'required|in:'.implode(',', self::COUNTRY)
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            view('profile.my-profile',compact('pageInfo'));
            return redirect()->route('My Profile')->withErrors($validator)->withInput();
        }else{
            $user = Auth::user();
            $user->address = $request->input('address');
            $user->postcode = $request->input('postcode');
            $user->state = $request->input('state');
            $user->country = $request->input('country');
            $user->save();
            session()->flash('success', 'Your address was updated.');
            return redirect()->route('My Profile');
        }
    }

    public function update_password(Request $request){
        if(!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            return redirect()->back()->withErrors("Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current_password'), $request->get('password')) == 0){
            //Current password and new password are same
            return redirect()->back()->withErrors("New Password cannot be same as your current password. Please choose a different password.");
        }

        $rules = [
            'current_password'  => 'required',
            'password'          => 'required|string|min:4|confirmed',
        ];
        $validator = Validator::make($request->all(), $rules);
        if($validator->fails()){
            view('profile.my-profile',compact('pageInfo'));
            return redirect()->back()->withErrors($validator)->withInput();
        }else{
            $user = Auth::user();
            $user->password = bcrypt($request->input('password'));
            $user->save();
            session()->flash('success', 'Your password was updated.');
            return redirect()->back();
        }
    }

    public function update_review(Request $request){
        $objProductReview = new ProductReview();
        $message = $objProductReview->deleteReviewByCurrentUser($request->key);
        if($message == true){
            $message = "Success";
        }else{
            $message = "Fail";
        }
        return response()->json($message); 
    }

    public function update_comment(Request $request){
        $objProductAnswer = new ProductAnswer();
        $message = $objProductAnswer->deleteCommentByCurrentUser($request->key);
        if($message == true){
            $message = "Success";
        }else{
            $message = "Fail";
        }
        return response()->json($message); 
    }

    public function update_question(Request $request){
        $objProductQuestion = new ProductQuestion();
        $message = $objProductQuestion->deleteQuestionByCurrentUser($request->key);
        if($message == true){
            $message = "Success";
        }else{
            $message = "Fail";
        }
        return response()->json($message); 
    }

    public function update_notification_setting(Request $request){
        $objNotificationSetting = new NotificationSetting();
        $notify_review = ($request->input('notify_review') == "on") ? 1 : 0;
        $notify_comment = ($request->input('notify_comment') == "on") ? 1 : 0;
        $notify_question = ($request->input('notify_question') == "on") ? 1 : 0;
        $result = $objNotificationSetting->UpdateNotificationSetting(Auth::user()->user_id,$notify_review,$notify_comment,$notify_question);
            // session()->flash('success', 'Your password was updated.');
        return redirect()->back();
    }

    public function messenger(Request $request){
        $pageInfo = (object)array();
        $pageInfo->page = "Messenger";
        $user = Auth::user();
        $user_id = $user->user_id;
        // Get User All messsenger conversation
        $objConversationUser = new ConversationUser();
        $arrConversation = $objConversationUser->getUserConversationList($user_id);
        $arrConversationDetail = $objConversationUser->getDetail($arrConversation, $user_id);
        $arrKeybyConverId = $arrConversation->keyBy('conversation_id');
        // Process Output 
        $arrReturnConverDetailAll = array();
        foreach($arrConversationDetail as $converDetail) {
            $arrReturnConverDetail = (object)array();
            $condetail = $arrKeybyConverId[$converDetail->conversation_id];
            if(!empty($condetail->conversation_name)){
                $arrReturnConverDetail->conversation_name = $condetail->conversation_name;
            } else {
                $arrReturnConverDetail->conversation_name = $arrKeybyConverId[$converDetail->conversation_id]->lastMessage($converDetail->conversation_id)->message;
            }
            $arrReturnConverDetail->conversation_id = $condetail->conversation_id;
            $arrReturnConverDetail->user_id = $user_id;
            $arrReturnConverDetail->target_id = $condetail->user_id;
            $arrReturnConverDetail->first_name = $converDetail->first_name;
            $arrReturnConverDetail->last_name = $converDetail->last_name;
            array_push($arrReturnConverDetailAll, $arrReturnConverDetail);
        }
        return view('profile.messenger',compact('pageInfo','arrReturnConverDetailAll'));
    }

}
