<?php
namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Model\SpamFilter;
use App\Http\Model\DiscussLog;
use App\Http\Model\ProductService;
use App\Http\Model\ProductQuestion;

class ProductQuestionController extends Controller
{
    //
    protected function questionValidator(array $data)
    {
        $messages = [
            'psid.required' => 'Invalid Question posting',
            'question_description.required' => 'Please describe your question',
            'question_description.min' => 'Question too short, please describe more on your question'
        ];
        $validator = Validator::make($data, [
            'psid' => 'required|integer',
            'question_description' => 'string|required|min:10',
        ], $messages);
        return $validator;
    }

    protected function questionVerifyValidator(array $data)
    {
        $messages = [
            'question_description.required' => 'Please describe your question',
            'question_description.min' => 'Question too short, please describe more on your question'
        ];
        $validator = Validator::make($data, [
            'question_description' => 'string|required|min:10',
        ], $messages);
        return $validator;
    }

    public function postQuestion(Request $request)
    {
        $validator = $this->questionValidator($request->all());
        $validator->validate();
        $arrInput = $request->input();
        if(isset($arrInput['anonymous'])){
            if($arrInput['anonymous'] == "on"){
                $arrInput['anonymous'] = 1;
            }
        }else {
            $arrInput['anonymous'] = 0;
        }
        $arrInput['user_id'] = Auth::user()->user_id;
        $arrInput['product_services_id'] = $arrInput['psid'];
        $objSpamFilter = new SpamFilter();
        if (!$objSpamFilter->isOffensiveWord($arrInput["question_description"]) ) {
            $objProductQuestion = new ProductQuestion();
            if($objProductQuestion->create($arrInput)){
                // Log Discuss
                $objProductDetail = new ProductService();
                $objDiscussLog = new DiscussLog;
                $productDetail = $objProductDetail->getProductDetailsById($arrInput['product_services_id']);
                $objDiscussLog->logDiscussReview($productDetail);
                return redirect()->back();
            } else {
                return redirect()->back()->withInput()->withErrors(['Somethings goes wrong, please try again']);
            }
        } else {
            return redirect()->back()->withInput()->withErrors(['We understand your feeling, try not to use offensive word >.<']);
        }
    }

    public function postQuestionVerify(Request $request) {
        $arrInput = $request->input();
        $objSpamFilter = new SpamFilter();
        if(Auth::user()){
            if (!$objSpamFilter->isOffensiveWord($arrInput["question_description"])) {
                return response()->json('ok',200);
            } else {
                $arrError = array(
                    "error_message" => "We understand your feeling, try not to use offensive word >.<",
                    "status" => "error"
                );
                return response()->json(json_encode($arrError),200);
            }
        } else {
            $arrError = array(
                "error_message" => "Login Required",
                "status" => "error"
            );
            return response()->json(json_encode($arrError),200);
        }
    }
}
