<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Auth;
use Config;
use Session;

class Locale{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next){
        $lang = "";
        if(Session::has('locale')){
            $lang = Session::get('locale');
        }else if(Auth::check()){
            $lang = Auth::user()->language;
            App::setLocale($lang);
        }
        if (in_array($lang, Config::get('app.locales'))) {
            $locale = $lang;
        }else{
            $locale = Config::get('app.locale');
        }
        App::setLocale($locale);
        return $next($request);
    }
}