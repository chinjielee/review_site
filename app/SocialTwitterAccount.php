<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialTwitterAccount extends Model
{
  protected $fillable = ['user_id', 'provider_user_id', 'provider'];

  public function user()
  {
      return $this->belongsTo(Http\Model\User::class, 'user_id', 'user_id');
  }
}
