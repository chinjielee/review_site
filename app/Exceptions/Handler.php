<?php

namespace App\Exceptions;

use Exception;
use Session;
use Auth;
use Config;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    // public function render($request, Exception $exception)
    // {
    //     $lang = "";
    //     if(Session::has('locale')){
    //         $lang = Session::get('locale');
    //     }else if(Auth::check()){
    //         $lang = Auth::user()->language;
    //         App::setLocale($lang);
    //     }
    //     if (in_array($lang, Config::get('app.locales'))) {
    //         $locale = $lang;
    //     }else{
    //         $locale = Config::get('app.locale');
    //     }
    //     app()->setLocale($locale);
    //     return parent::render($request, $exception);
    // }

    public function render($request, Exception $exception)
    {
        $lang = "";
        // if(Session::has('locale')){
        //     $lang = Session::get('locale');
        // }else if(Auth::check()){
        //     $lang = Auth::user()->language;
        //     App::setLocale($lang);
        // }
        if (in_array($lang, Config::get('app.locales'))) {
            $locale = $lang;
        }else{
            $locale = Config::get('app.locale');
        }
        app()->setLocale($locale);
        if ($request->expectsJson()) {
            return $this->renderJson($request, $exception);
        }
    
        if ($this->shouldReport($exception) && app()->environment('production')) {
            $exception = new HttpException(500, $exception->getMessage(), $exception);
        }
        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest(route('login'));
    }
}
