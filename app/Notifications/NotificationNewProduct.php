<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use NotificationChannels\WebPush\WebPushMessage;
use NotificationChannels\WebPush\WebPushChannel;

class NotificationNewProduct extends Notification
{
    use Queueable;
    private $name = "Someone";
    private $actionUrl = "";
    private $body = "";
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($url, $firstName ="Someone", $msg="")
    {
        //
        $this->name = $firstName;
        $this->actionUrl = $url;
        $this->body = $msg;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $body = !empty($this->body) ?$this->body:$this->name.' answered your questions';
        return (new MailMessage)
                    ->line($body)
                    ->action('Approve Product here', $this->actionUrl)
                    ->line($body);
    }

    /**
        * Get the array representation of the notification.
        *
        * @param  mixed  $notifiable
        * @return array
    */
    public function toArray($notifiable)
    {
        $body = !empty($this->body) ?$this->body:$this->name.' answered your questions';
        return [
            'title' => $body,
            'body' => $body,
            'action_url' => getenv('ADMIN_DOMAIN').'labels/pending/edit/'.$this->actionUrl
        ];
    }

    /**
     * Get the web push representation of the notification.
     *
     * @param  mixed  $notifiable
     * @param  mixed  $notification
     * @return \Illuminate\Notifications\Messages\DatabaseMessage
     */
     public function toWebPush($notifiable, $notification)
     {
         return (new WebPushMessage)
             ->title('New Product')
             ->body('New product been submit by user')
             ->action('View app', 'view_app')
             ->data(['id' => $notification->id]);
     }
}
