<?php 

namespace App\Traits;

trait WikaboTrait
{
    public function ProcessRecentViewCookie(){
        $current_recent_view = "";
        if(isset($_COOKIE['recent_view'])){
            $current_recent_view = $_COOKIE['recent_view']."||";
            $temp_array = (explode("||",rtrim($current_recent_view,"||")));
            while(sizeof($temp_array) > 5){
                $temp_array = array_reverse($temp_array);
                array_pop($temp_array);
                $temp_array = array_reverse($temp_array);
            }
            $current_recent_view = "";
            $value_array = array();
            foreach($temp_array as $recent_view){
                if(!in_array((substr($recent_view,20)),$value_array)){
                    array_push($value_array,(substr($recent_view,20)));
                    $current_recent_view .= $recent_view."||";
                }
            }
        }
        return $current_recent_view;
    }

    public function ProcessRecentViewCookieReturnValue(){
        $value_array = array();
        if(isset($_COOKIE['recent_view'])){
            $current_recent_view = $_COOKIE['recent_view']."||";
            $temp_array = (explode("||",rtrim($current_recent_view,"||")));
            while(sizeof($temp_array) > 5){
                $temp_array = array_reverse($temp_array);
                array_pop($temp_array);
                $temp_array = array_reverse($temp_array);
            }
            
            foreach($temp_array as $recent_view){
                if(!in_array((substr($recent_view,20)),$value_array)){
                    array_push($value_array,(substr($recent_view,20)));
                }
            }
        }
        return $value_array;
    }
}