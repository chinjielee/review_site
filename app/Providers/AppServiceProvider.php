<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Http\Model\Categories;
use Auth;
use URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

       if (env('APP_ENV') === 'production') {
            URL::forceScheme('https');
        }
        //compose all the views....
        view()->composer('*', function ($view) 
        {
            $objCategories = new Categories();
            $share = (object)array();
            $catList = $objCategories->getCategoryTopExplorelist();
            $share->catList = $catList;
            $share->total = 0;
            $user = "";
            if(Auth::user()){
                $user = Auth::user();
                $total = $user->unreadNotifications->count();
                $share->total = $total;
            }
            //...with this variable
            $view->with('share', $share ); 
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
