var total_photos_counter = 0;
Dropzone.options.invoiceDropzone = {
    uploadMultiple: true,
    parallelUploads: 1,
    previewTemplate: document.querySelector('#preview').innerHTML,
    addRemoveLinks: true,
    dictRemoveFile: 'Remove',
    maxFilesize: 2,
    dictFileTooBig: 'Image is larger than 2MB',
    timeout: 10000,
    maxFiles:3,
    acceptedFiles:'image/*',
    url:uploadUrl,
    params: {_token: $('meta[name="csrf-token"]').attr('content'), type:'invoice',diresu:diresuval},
    init: function () {
        this.on("maxfilesexceeded", function(file){
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Maximum 3 images is allowed',
            });
            this.removeFile(file);
            exit();
        });
        this.on("removedfile", function (file) {
            $.post({
                url: delUrl,
                data: {id: file.name, _token: $('meta[name="csrf-token"]').attr('content'), type:'invoice',diresu:diresuval},
                dataType: 'json',
                success: function (data) {
                    total_photos_counter--;
                    $("#counter").text("# " + total_photos_counter);
                }
            });
        });
    },
    success: function (file, done) {
        total_photos_counter++;
        $("#counter").text("# " + total_photos_counter);
    }
};


// ------ Upload product images --- 
Dropzone.options.reviewDropzone = {
    uploadMultiple: true,
    parallelUploads: 3,
    previewTemplate: document.querySelector('#preview').innerHTML,
    addRemoveLinks: true,
    dictRemoveFile: 'Remove',
    maxFilesize:2,
    dictFileTooBig: 'Image is larger than 2MB',
    timeout: 10000,
    maxFiles:3,
    acceptedFiles:'image/*',
    url:uploadUrl,
    params: {_token: $('meta[name="csrf-token"]').attr('content'), type:'review',diresu:diresuval},
    init: function () {
        this.on("removedfile", function (file) {
            $.post({
                url: delUrl,
                data: {id: file.name, _token: $('meta[name="csrf-token"]').attr('content'), type:'review',diresu:diresuval},
                dataType: 'json',
                success: function (data) {
                    total_photos_counter--;
                    $("#counter").text("# " + total_photos_counter);
                }
            });
        });
        this.on("maxfilesexceeded", function(file){
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Maximum 3 images is allowed',
            });
            this.removeFile(file);
            exit();
        });
    },
    success: function (file, done) {
        total_photos_counter++;
        $("#counter").text("# " + total_photos_counter);
    }
};