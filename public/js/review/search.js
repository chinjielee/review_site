if(typeof(search)!== 'undefined' && typeof(searchInput)!== 'undefined'){
  $('#'+search).on('click',function(event){
      event.preventDefault();
      var keyword = $('#'+searchInput).val();
      if(typeof(keyword) !== 'undefined' && keyword != ''){
        window.location = urlSearch+"/"+keyword;
      }
      return false;
  });

  // Execute a function when the user releases a key on the keyboard
  $('#'+searchInput).on('keypress',function(event){
    // Number 13 is the "Enter" key on the keyboard
    if (event.keyCode === 13) {
      event.preventDefault();
      // Cancel the default action, if needed
      // Trigger the button element with a click
      document.getElementById(search).click();
    }
  });
}

$('.top-search-input').on('keypress',function(event){
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
      // Cancel the default action, if needed
      // Trigger the button element with a click
      event.preventDefault();
      document.getElementById('product-search-text-top').click();
  }
}); 

$('#product-search-text-top').on('click',function(event){
  event.preventDefault();
  var keyword = $('#product-search-text-top').val();
  if(typeof(keyword) !== 'undefined' && keyword != ''){
    window.location = urlSearch+"/"+keyword;
  }
  
});