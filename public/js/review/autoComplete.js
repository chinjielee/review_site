var substringMatcher = function(strs) {
    return function findMatches(q, cb) {
      var matches, substringRegex;
  
      // an array that will be populated with substring matches
      matches = [];
  
      // regex used to determine if a string contains the substring `q`
      substrRegex = new RegExp(q, 'i');
  
      // iterate through the pool of strings and for any string that
      // contains the substring `q`, add it to the `matches` array
      $.each(strs, function(i, str) {
        if (substrRegex.test(str)) {
          matches.push(str);
        }
      });
  
      cb(matches);
    };
  };
  
// constructs the suggestion engine
var productService = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  // `states` is an array of state names defined in "The Basics"
  	remote: {
			url: urlAsProduct,
      wildcard: '%QUERY',
      filter: function (products) {
        // $.map converts the JSON array into a JavaScript array
        return $.map(products, function (product) {
            return {
                // NB : replace original_title below with your JSON film key
                value: product.product_service_title
            };
        });
      }
		}
});

var product = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  // `states` is an array of state names defined in "The Basics"
  	remote: {
			url: urlAsProduct,
      wildcard: '%QUERY',
      filter: function (products) {
        // $.map converts the JSON array into a JavaScript array
        return $.map(products, function (product) {
            return {
                // NB : replace original_title below with your JSON film key
                value: product.product_service_title
            };
        });
      }
		}
});

var comment = new Bloodhound({
  datumTokenizer: Bloodhound.tokenizers.whitespace,
  queryTokenizer: Bloodhound.tokenizers.whitespace,
  // `states` is an array of state names defined in "The Basics"
  	remote: {
			url: urlAsProduct,
      wildcard: '%QUERY',
      filter: function (products) {
        // $.map converts the JSON array into a JavaScript array
        return $.map(products, function (product) {
            return {
                // NB : replace original_title below with your JSON film key
                value: product.product_service_title
            };
        });
      }
		}
});

$('#bloodhound .typeahead').typeahead({
  hint: false,
  highlight: true,
  minLength: 3,
  templates: {
    suggestion: '{{value}}'
  }
},
{
  name: 'value',
  displayKey: 'value',
  source: productService,
  templates:{
    header: '<b>Services</b>'
  }
},
{
  name: 'value',
  displayKey: 'value',
  source: product,
  templates:{
    header: '<b>Products</b>',
  }
},
{
  name: 'value',
  displayKey: 'value',
  source: comment,
  templates:{
    header: '<b>Comments</b>'
  }
}
);
