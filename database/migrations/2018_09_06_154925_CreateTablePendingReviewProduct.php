<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePendingReviewProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
        if(!Schema::hasTable('pending_product_service')){
            Schema::create('pending_product_service', function (Blueprint $table) {
                $table->increments('pending_product_service_id')->index();
                $table->string('product_name');
                $table->integer('category_id')->nullable()->default(0);
                $table->string('category_name')->nullable();
                $table->longText('product_attributes'); //JSON Type
                $table->tinyInteger('status',false,false);
                $table->timestamps();
            });
        }
     }
 
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         //
         Schema::dropIfExists('pending_review_product');
     }
}
