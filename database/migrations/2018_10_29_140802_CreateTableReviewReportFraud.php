<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class CreateTableReviewReportFraud extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('review_report_fraud', function($table) {
            $table->increments('review_report_fraud_id');
            $table->integer('product_review_id',false);
            $table->integer('user_id',false);
            $table->tinyInteger('status')->default(0);
            $table->timestamps();
            $table->index('product_review_id', 'user_id');
       });

       DB::table('review_report_fraud')->insert(array(
            array(
                'user_id' => '1',
                'product_review_id' => '5',
                'status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'user_id' => '2',
                'product_review_id' => '5',
                'status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'user_id' => '1',
                'product_review_id' => '3',
                'status' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            )
       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('review_report_fraud');
    }
}
