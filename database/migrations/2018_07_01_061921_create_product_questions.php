<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('product_questions')){
            Schema::create('product_questions', function (Blueprint $table) {
                $table->increments('product_questions_id')->index();
                $table->integer('product_services_id',false);
                $table->integer('user_id',false);
                $table->string('question_description');
                $table->tinyInteger('anonymous',false,false);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_questions', function (Blueprint $table) {
        });
    }
}
