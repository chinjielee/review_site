<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersSecurityQnaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('user_security_qna')){
            Schema::create('user_security_qna', function (Blueprint $table) {
                $table->increments('user_security_qna_id')->index();
                $table->integer('user_id',false);
                $table->integer('questions_id',false);
                $table->string('answer');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_security_qna', function (Blueprint $table) {
            //
        });
    }
}
