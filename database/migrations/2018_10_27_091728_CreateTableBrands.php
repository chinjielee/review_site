<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateTableBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('brands', function($table) {
            $table->increments('brands_id');
            $table->string('brand_title',200);
            $table->string('brand_info',200);
            $table->string('images',200);
            $table->timestamps();
       });

       DB::table('brands')->insert(array(
            array(
                'brands_id' => '1',
                'brand_title' => 'Panasonic',
                'brand_info' => "",
                'images' => "panasonic.jpg",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'brands_id' => '2',
                'brand_title' => 'Sony',
                'brand_info' => "",
                'images' => "sony.jpg",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'brands_id' => '3',
                'brand_title' => 'Apple',
                'brand_info' => "",
                'images' => "apple.jpg",
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            )
       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('brands');
    }
}
