<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class CreateTableRatingOption extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('rating_option')){
            Schema::create('rating_option', function($table) {
                $table->increments('rating_option_id');
                $table->integer('category_id',false)->index();
                $table->string('type',100);
                $table->longtext('option_json');
                $table->timestamps();
           });
           DB::table('rating_option')->insert(array(
                array(
                    'rating_option_id' => 1,
                    'category_id' => 1,
                    'type' => 'general',
                    'option_json' => 'Reliability,User Friendliness,Standards,Professionalism,Price',
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                )
            ));
       }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('rating_option');
    }
}
