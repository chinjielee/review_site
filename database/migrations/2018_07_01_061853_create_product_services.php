<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('product_services')){
            Schema::create('product_services', function (Blueprint $table) {
                $table->increments('product_services_id')->index();
                $table->string('product_service_title');
                $table->string('product_slug', 100)->index();
                $table->string('product_service_desc');
                $table->integer('category_id');
                $table->longText('product_service_attributes');  //JSON Type
                $table->longText('images');
                $table->decimal('product_rating',2,1);
                $table->mediumText('product_rating')->change();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_services', function (Blueprint $table) {
        });
    }
}
