<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterProductReviewAddTempProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('product_reviews', function($table) {
            if(!Schema::hasColumn('product_reviews','product_services_id')){
                $table->integer('product_services_id')->index()->change();
            }
            if(!Schema::hasColumn('product_reviews','temp_product_services_id')){
                $table->integer('temp_product_services_id')->after('product_services_id')->index();
            }
            $table->mediumtext('rating')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
