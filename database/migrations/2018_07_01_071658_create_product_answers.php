<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('product_answers')){
            Schema::create('product_answers', function (Blueprint $table) {
                $table->increments('product_answers_id')->index();
                $table->integer('product_questions_id')->index();
                $table->integer('user_id',false);
                $table->string('answer');
                $table->integer('up_vote');
                $table->integer('down_vote');
                $table->tinyInteger('anonymous',false,false);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_answers', function (Blueprint $table) {
        });
    }
}
