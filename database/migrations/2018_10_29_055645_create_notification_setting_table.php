<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_setting', function (Blueprint $table) {
            $table->increments('notification_setting_id');
            $table->integer('user_id',false);
            $table->tinyInteger('notify_review')->default(0);
            $table->tinyInteger('notify_comment')->default(0);
            $table->tinyInteger('notify_question')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_setting');
    }
}
