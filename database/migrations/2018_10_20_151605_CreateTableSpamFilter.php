<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class CreateTableSpamFilter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if(!Schema::hasTable('spam_filter')){
            Schema::create('spam_filter', function($table) {
                $table->increments('spam_filter_id');
                $table->string('text',200);
                $table->integer('rate',false)->default(0);
                $table->tinyinteger('type',false)->default(0);
                $table->timestamps();
           });

           DB::table('spam_filter')->insert(array(
                array(
                    'spam_filter_id' => '1',
                    'text' => 'fuck',
                    'rate' => 0,
                    'type' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'spam_filter_id' => '2',
                    'text' => 'asshole',
                    'rate' => 0,
                    'type' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                )
           ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //  
        Schema::dropIfExists('spam_filter');
    }
}
