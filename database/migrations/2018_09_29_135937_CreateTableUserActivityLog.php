<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserActivityLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('activity_log')){
            Schema::create('activity_log', function($table) {
                $table->increments('activity_log_id');
                $table->integer('user_id',false)->index();
                $table->string('link',200);
                $table->string('remark',200)->nullable();
                $table->tinyInteger('type')->index();
                $table->timestamps();
           });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
