<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscussLogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('discuss_log', function (Blueprint $table) {
            $table->increments('discuss_log_id');
            $table->integer('product_service_id')->unsigned()->index('product_service_id');
            $table->string('product_service_title',255);
            $table->integer('categories_id')->unsigned()->index('categories_id');
            $table->integer('day_month_year')->index('day_month_year');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('discuss_log');
    }
}
