<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablPendingQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('pending_question')){
            Schema::create('pending_question', function (Blueprint $table) {
                $table->increments('pending_question_id')->index();
                $table->integer('product_services_id',false);
                $table->integer('user_id',false);
                $table->string('question_description');
                $table->tinyInteger('anonymous',false,false);
                $table->tinyInteger('status',false,false);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('pending_question');
    }
}
