<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function($table) {
            if(!Schema::hasColumn('users','address')){
                $table->string('address',300)->after('dob');
            }
            if(!Schema::hasColumn('users','postcode')){
                $table->string('postcode',10)->after('dob');
            }
            if(!Schema::hasColumn('users','state')){
                $table->string('state',100)->after('dob');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function($table) {
            $table->dropColumn('address');
            $table->dropColumn('postcode');
            $table->dropColumn('state');
        });
    }
}
