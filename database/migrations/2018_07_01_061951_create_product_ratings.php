<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductRatings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('product_ratings')){
            Schema::create('product_ratings', function (Blueprint $table) {
                $table->increments('product_rating_id')->index();
                $table->integer('product_services_id');
                $table->integer('user_id');
                $table->tinyInteger('rating',false);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_ratings', function (Blueprint $table) {
        });
    }
}
