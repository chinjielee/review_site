<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class CreateTableReviewVote extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('review_vote', function($table) {
            $table->integer('product_review_id',false);
            $table->integer('user_id',false);
            $table->primary(['product_review_id', 'user_id']);
            $table->tinyInteger('up');
            $table->tinyInteger('down');
            $table->timestamps();
       });

       DB::table('review_vote')->insert(array(
            array(
                'user_id' => '1',
                'product_review_id' => '2',
                'up' => 0,
                'down' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'user_id' => '2',
                'product_review_id' => '2',
                'up' => 1,
                'down' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'user_id' => '1',
                'product_review_id' => '3',
                'up' => 1,
                'down' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            )
       ));
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('review_vote');
    }
}
