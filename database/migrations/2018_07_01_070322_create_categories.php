<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;
class CreateCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('categories')){
            Schema::create('categories', function (Blueprint $table) {
                $table->increments('categories_id')->index();
                $table->string('categories_name');
                $table->string('categories_description');
                $table->string('category_slug');
                $table->string('image');
                $table->Integer('parent_category_id',false)->index();
                $table->tinyInteger('level',false);
                $table->timestamps();
            });

            DB::table('categories')->insert(array(
                array(
                    'categories_name' => 'Products',
                    'categories_description' => 'Products',
                    'category_slug' => 'products',
                    'image' => '',
                    'level' => 0,
                    'parent_category_id' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Services',
                    'categories_description' => 'Services',
                    'category_slug' => 'services',
                    'image' => '',
                    'level' => 0,
                    'parent_category_id' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Movie',
                    'categories_description' => 'Movie',
                    'category_slug' => 'movie',
                    'image' => '',
                    'level' => 0,
                    'parent_category_id' => 0,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Appliances',
                    'categories_description' => 'Appliances',
                    'category_slug' => 'appliances',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Automotive Accessories',
                    'categories_description' => 'Automotive Accessories',
                    'category_slug' => 'automotive-accessories',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Babies & Kids',
                    'categories_description' => 'Babies & Kids',
                    'category_slug' => 'babies-kids',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Cars',
                    'categories_description' => 'Cars',
                    'category_slug' => 'cars',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Electronics',
                    'categories_description' => 'Electronics',
                    'category_slug' => 'electronics',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Finance',
                    'categories_description' => 'Finance',
                    'category_slug' => 'finance',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Furniture & Bedding',
                    'categories_description' => 'Furniture & Bedding',
                    'category_slug' => 'furniture-bedding',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Garden & Power Tools',
                    'categories_description' => 'Garden & Power Tools',
                    'category_slug' => 'garden-power-tools',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Health & Beauty',
                    'categories_description' => 'Health & Beauty',
                    'category_slug' => 'health-beauty',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Home Care',
                    'categories_description' => 'Home Care',
                    'category_slug' => 'home-care',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Insurance',
                    'categories_description' => 'Insurance',
                    'category_slug' => 'insurance',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Kitchenware',
                    'categories_description' => 'Kitchenware',
                    'category_slug' => 'kitchenware',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Motorbikes',
                    'categories_description' => 'Motorbikes',
                    'category_slug' => 'motorbikes',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Pet Products',
                    'categories_description' => 'Pet Products',
                    'category_slug' => 'pet-products',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Services',
                    'categories_description' => 'Services',
                    'category_slug' => 'product-services',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Shops',
                    'categories_description' => 'Shops',
                    'category_slug' => 'shops',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Travel',
                    'categories_description' => 'Travel',
                    'category_slug' => 'travel',
                    'image' => '',
                    'level' => 1,
                    'parent_category_id' => 1,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'TVs',
                    'categories_description' => 'TVs',
                    'category_slug' => 'tvs',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Mobile Phones',
                    'categories_description' => 'Mobile Phones',
                    'category_slug' => 'mobile-phones',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Headphones',
                    'categories_description' => 'Headphones',
                    'category_slug' => 'headphones',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Modems / Routers',
                    'categories_description' => 'Modems / Routers',
                    'category_slug' => 'modems-routers',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Printers',
                    'categories_description' => 'Printers',
                    'category_slug' => 'printers',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Home Audio',
                    'categories_description' => 'Home Audio',
                    'category_slug' => 'home-audio',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Laptops',
                    'categories_description' => 'Laptops',
                    'category_slug' => 'laptops',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Set Top Boxes / PVRs / DVRs',
                    'categories_description' => 'Set Top Boxes / PVRs / DVRs',
                    'category_slug' => 'set-top-boxes-pvrs-dvrs',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Tablets',
                    'categories_description' => 'Tablets',
                    'category_slug' => 'tablets',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Wearable Tech',
                    'categories_description' => 'Wearable Tech',
                    'category_slug' => 'wearable-tech',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Phones',
                    'categories_description' => 'Phones',
                    'category_slug' => 'phones',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
                array(
                    'categories_name' => 'Digital Cameras',
                    'categories_description' => 'Digital Cameras',
                    'category_slug' => 'digital-cameras',
                    'image' => '',
                    'level' => 2,
                    'parent_category_id' => 8,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now()
                ),
            ));
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories', function (Blueprint $table) {
        });
    }
}
