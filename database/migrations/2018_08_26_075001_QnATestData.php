<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class QnATestData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        #  -- Questions -- 
        DB::table('product_questions')->insert(array(
            array(
                'product_services_id' => '1',
                'user_id' => 1,
                'question_description' => 'Water Proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '1',
                'user_id' => 1,
                'question_description' => 'Water Bullet proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '1',
                'user_id' => 1,
                'question_description' => 'Smash proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '1',
                'user_id' => 1,
                'question_description' => 'Got audio jack ??',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '2',
                'user_id' => 1,
                'question_description' => 'Water Proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '2',
                'user_id' => 1,
                'question_description' => 'Water Bullet proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '2',
                'user_id' => 1,
                'question_description' => 'Smash proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '2',
                'user_id' => 1,
                'question_description' => 'Got audio jack ??',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '3',
                'user_id' => 1,
                'question_description' => 'Water Proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '3',
                'user_id' => 1,
                'question_description' => 'Water Bullet proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '3',
                'user_id' => 1,
                'question_description' => 'Smash proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '3',
                'user_id' => 1,
                'question_description' => 'Got audio jack ??',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '4',
                'user_id' => 1,
                'question_description' => 'Water Proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '4',
                'user_id' => 1,
                'question_description' => 'Water Bullet proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '4',
                'user_id' => 1,
                'question_description' => 'Smash proof ?',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_services_id' => '4',
                'user_id' => 1,
                'question_description' => 'Got audio jack ??',
                'anonymous' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            )
        ));
        #  -- Question Answers -- 
        DB::table('product_answers')->insert(array(
            array(
                'product_questions_id' => '1',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '1',
                'user_id' => 1,
                'answer' => 'NO kut, not sure, what you think ? keh asD?ASD?A?SD? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '2',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '2',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think Asdasdasd2nddd2i? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '3',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '3',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '4',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '4',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '5',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '5',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '6',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '6',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '7',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '7',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '8',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '8',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '9',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '9',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '10',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '10',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '11',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '11',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '12',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '12',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '13',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '13',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '14',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '14',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '15',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '15',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '16',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ),
            array(
                'product_questions_id' => '16',
                'user_id' => 1,
                'answer' => 'Yes / NO kut, not sure, what you think ? ',
                'anonymous' => 0,'up_vote'=>0,'down_vote'=>0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            )
        ));
    }

    /**
     * Reverse the migrations.
     * 
     * @return void
     */
    public function down()
    {
        //
    }
}
