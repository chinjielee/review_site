<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableReviewQuestionAnswerAddStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product_reviews', function($table) {
            $table->tinyInteger('status')->after('anonymous')->default(1)->index();
        });
        Schema::table('product_answers', function($table) {
            $table->tinyInteger('status')->after('anonymous')->default(1)->index();
        });
        Schema::table('product_questions', function($table) {
            $table->tinyInteger('status')->after('anonymous')->default(1)->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
