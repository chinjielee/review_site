<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('users')){
            Schema::create('users', function (Blueprint $table) {
                $table->increments('user_id')->index();
                $table->string('first_name');
                $table->string('last_name');
                $table->string('email',150)->unique();
                $table->string('mobile',18);
                $table->text('password');
                $table->enum('gender', ['Male', 'Female']);
                $table->date('dob');
                $table->string('country',100);  
                $table->tinyInteger('user_status')->default(0);
                $table->string('email_token')->nullable();
                $table->string('avatar')->default('avatar.png');
                $table->string('language',2)->default('en');
                $table->rememberToken();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
