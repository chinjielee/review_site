<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductReviews extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('product_reviews')){
            Schema::create('product_reviews', function (Blueprint $table) {
                $table->increments('product_reviews_id')->index();
                $table->integer('product_services_id');
                $table->string('review_title', 100);
                $table->mediumText('comments');
                $table->mediumText('review_image');
                $table->mediumText('key_highlight');
                $table->integer('user_id',false);
                $table->integer('up_vote',false);
                $table->integer('down_vote',false);
                $table->tinyInteger('rating',false);
                $table->tinyInteger('anonymous',false,false);
                $table->timestamp('purchased_date');
                $table->string('purchased_from');
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('product_comments', function (Blueprint $table) {
        });
    }
}
