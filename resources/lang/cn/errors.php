<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'About Us' => '简介',
    'Explore Categories' => '探索类别',
    'language_flag' => 'china-flag',
    'Notification' => '讯息',
    'Notification Setting' => '讯息管理',
    'Profile' => '个人资料',
    'Home' => '主页',
    'Search' => '搜索',
    'Find products, services or questions...' => '查找产品，服务或问题...',
    'Page Not Found' => '网页未找到',
    'Register' => '注册',
    'Sign In' => '登入',
    'Sign Out' => '退出',
    'Write a Review' => '评论',
	'Select Language' => '选择语言',
    'English' => '英语',
    'Chinese' => '中文',
    'Terms of Use' => '使用条款',
    'Posting Guidelines' => '发布指南',
    'Privacy Policy' => '隐私政策',
    'Trademark Policy' => '商标政策',
    'Acceptable Use Policy' => '使用政策',
    'Like our Facebook page to stay in the know on amazing new products' => '喜欢我们的Facebook页面，以了解惊人的新产品',
    'Like us' => '喜欢我们',
    'on Facebook' => 'Facebook页面',
    'Copyright Policy' => '版权政策',
    'Stay connected' => '保持联系',
    'Copyrights &copy; 2018 All Rights Reserved by WIKÅBÖ' => '版权所有&copy;2018 WIKÅBÖ保留所有权利',
    'Ooopps.! The Page you were looking for, could not be found.' => '抱歉,无法找到您正在寻找的页面.',
];
