<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'If the listing you were looking for is not here, you can create it' => '如果您要查找的商家信息不在此处，您可以创建它',
    'What are you reviewing' => '你要评论什么吗',
    'Search any products, services or movies on below to review' => '搜索下面的任何产品，服务或电影进行审核',
    'SEARCH' => '搜索',
    'No Result Found' => '找不到结果',
    'Search any keywords on top search bar' => '在顶部搜索栏上搜索任何关键字',
];
