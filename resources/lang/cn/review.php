<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'Home' => '主页',
    'What are you reviewing' => '你要评论什么',
    'Search any products, services or movies on below to review' => '搜索下面的任何产品，服务或电影进行审核',
	'SEARCH' => '搜索',
    'Example: iPhone, Fridges, Washing Machines, TVs...' => '示例：iPhone，冰箱，洗衣机，电视...',
    'reviews' => '评论',
    'Reviews' => '评论',
    'Write a review' => '写评论',
    'Seach Results for' => '寻找结果:',
    'No Result Found' => '找不到结果',
    'Search any keywords on top search bar' => '在顶部搜索栏上搜索任何关键字',
    'Fill In Details' => '填写详细信息',
    'Upload Photos' => '上传照片',
    'Fill In Product Details' => '填写产品详细信息',
    'Review Detail' => '查看详情',
    'Complete Review' => '完整评论',
    'You successfully submit review' => '您已成功提交评论',
    'Thank you' => '谢谢',
    'Your review will help thousands of people' => '您的评论将帮助成千上万的人',
    'Title Of Your Review' => '你的评论标题',
    'Detailed Reviews' => '详细评论',
    '30 words minimum' => '最少30个字',
    'Key Highlights' => '主要亮点',
    'Rate On Relevant Points' => '相关点率',
    'Upload Related Images' => '上传相关图片',
    'Maximum 3 images' => '最多3张图片',
    'Check' => '检',
    'Created with Sketch' => '用Sketch创建',
    'error' => '错误',
    'Purchase Info' => '购买信息',
    'Date of Purchase' => '购买日期',
    'Purchase at' => '在哪里购买',
    'Country' => '国家',
    'Malaysia' => '马来西亚',
    'Upload Invoice Images' => '上传发票图片',
    'Review Anonymously' => '匿名审核',
    'Go Back' => '回去',
    'Submit Review' => '提交评论',
    'Which would you like to review' => '您想要评论哪个',
    'Review Product' => '查看产品',
    'Review Service' => '查看服务',
    'Review Movie' => '评论电影',
    'Product Details' => '产品详情',
    'Product Name' => '产品名称',
    'Product Category' => '产品分类',
    'Product Brand' => '产品品牌',
    'Product Model' => '产品型号',
    'Next Step' => '下一步',
    'Brand Website' => '品牌网站',
    'Service Details' => '服务细节',
    'Movie Details' => '电影细节',
    'Service Category' => '分类',
    'Service Name' => '名称',
    'Service Brand' => '品牌',
    'Service Model' => '型号',
    'Movie Category' => '分类',
    'Movie Name' => '名称',
    'Movie Brand' => '品牌',
    'Movie Model' => '型号',
    'Add New Brand' => '添加品牌'
];
