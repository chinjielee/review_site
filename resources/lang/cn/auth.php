<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Reset Password' => '重设密码',
    'Home' => '主页',
    'Login' => '登录',
    'Please sign in to your account' => '请登录您的帐户',
    'Email Address' => '电子邮件地址',
    'Password' => '密码',
    'Remember Me' => '记住账号',
    'Forgot Password' => '忘记密码',
    'or Sign in with' => '或者登录',
    'Sign In' => '登入',
    'Create your new account' => '创建新帐户',
    'First Name' => '名字',
    'Last Name' => '姓',
    'Confirm Password' => '确认密码',
    'Country Code' => '国家代码',
    'Mobile' => '手机号码',
    'Country of residence' => '居住国家',
    'Singapore' => '新加坡',
    'Malaysia' => '马来西亚',
    'Female' => '女',
    'Gender' => '性别',
    'Date of birth' => '出生日期',
    'Male' => '男',
    'Security Question' => '安全问题',
    'Choose an Security Questions' => '选择安全问题',
    'Register Now' => '现在注册',
	'Having trouble signing in' => '登陆不进去吗',
    'Continue' => '继续',
    'Messenger' => '信箱',
];
