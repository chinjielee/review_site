<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'Know better, choose better' => '知道更好，选择更好',
    'Can not find what you are looking for? Search here' => '无法找到您想要的？ 在这里搜索',
    'SEARCH' => '搜索',
    'Find products, services or movies' => '查找产品，服务或电影',
];
