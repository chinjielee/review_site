<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'Loading&#8230;' => '载入中&#8230;',
    'Compare specs/details' => '比较规格/细节',
    'Search any products, services or movies on below to compare' => '搜索下面的任何产品，服务或电影进行比较',
    'Home' => '主页',
    'Compare with' => '与之比较',
    'Please enter model name or part of it' => '请输入型号名称或部分名称',
    'Add a mobile phone' => '添加产品/服务或电影',
    'Change with' => '更改',
    'Change' => '更改',
    'Search' => '搜索',
];
