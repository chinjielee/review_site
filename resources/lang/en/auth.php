<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Reset Password' => 'Reset Password',
    'E-Mail Address' => 'E-Mail Address',
    'Password' => 'Password',
    'Confirm Password' => 'Confirm Password',
    'Reset Password' => 'Reset Password',
    'Home' => 'Home',
    'Please sign in to your account' => 'Please sign in to your account',
    'Email Address' => 'Email Address',
    'Password' => 'Password',
    'Remember Me' => 'Remember Me',
    'Forgot Password' => 'Forgot Password',
    'or Sign in with' => 'or Sign in with',
    'Sign In' => 'Sign In',
    'Create your new account' => 'Create your new account',
    'First Name' => 'First Name',
    'Last Name' => 'Last Name',
    'Confirm Password' => 'Confirm Password',
    'Country Code' => 'Country Code',
    'Mobile' => 'Mobile',
    'Country of residence' => 'Country of residence',
    'Singapore' => 'Singapore',
    'Malaysia' => 'Malaysia',
    'Female' => 'Female',
    'Gender' => 'Gender',
    'Date of birth' => 'Date of birth',
    'Male' => 'Male',
    'Security Question' => 'Security Question',
    'Choose an Security Questions' => 'Choose an Security Questions',
    'Register Now' => 'Register Now',
    'Having trouble signing in' => 'Having trouble signing in',
    'Continue' => 'Continue',
    'Messenger' => 'Messenger',
	

];
