<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'About Us' => 'About Us',
    'Contact Us' => 'Contact Us',
    'Explore Categories' => 'Explore Categories',
    'language_flag' => 'united-states-flag',
    'Notification' => 'Notification',
    'Notification Setting' => 'Notification Setting',
    'Profile' => 'Profile',
    'Register' => 'Register',
    'Register Here' => 'Register Here',
    'Not A Member' => 'Not A Member',
    'Sign In' => 'Sign In',
    'Sign Out' => 'Sign Out',
    'Logout' => 'Logout',
    'Write a Review' => 'Write a Review',
    'Select Language' => 'Select Language',
    'English' => 'English',
    'Chinese' => 'Chinese',
    'Terms of Service' => 'Terms of Service',
    'Terms of Use' => 'Terms of Use',
    'Posting Guidelines' => 'Posting Guidelines',
    'Privacy Policy' => 'Privacy Policy',
    'Our Policy' => 'Our Policy',
    'Trademark Policy' => 'Trademark Policy',
    'Acceptable Use Policy' => 'Acceptable Use Policy',
    'Disclaimer and Limitation of Liability' => 'Disclaimer and Limitation of Liability',
    'Like our Facebook page to stay in the know on amazing new products' => 'Like our Facebook page to stay in the know on amazing new products',
    'Like us' => 'Like us',
    'Follow us' => 'Follow us',
    'on Facebook' => 'on Facebook',
    'on Instagram' => 'on Instagram',
    'on Twitter' => 'on Twitter',
    'Copyright Policy' => 'Copyright Policy',
    'Stay connected' => 'Stay connected',
    'Copyrights &copy; 2018 All Rights Reserved by WIKÅBÖ' => 'Copyrights &copy; 2018 All Rights Reserved by WIKÅBÖ',
    'Messenger' => 'Messenger',
    'Dashboard' => 'Dashboard',
];
