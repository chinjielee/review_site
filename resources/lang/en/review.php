<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'Home' => 'Home',
    'What are you reviewing' => 'What are you reviewing',
    'Search any products, services or movies on below to review' => 'Search any products, services or movies on below to review',
    'SEARCH' => 'SEARCH',
    'Example: iPhone, Fridges, Washing Machines, TVs...' => 'Example: iPhone, Fridges, Washing Machines, TVs...',
    'reviews' => 'reviews',
    'Reviews' => 'Reviews',
    'Write a review' => 'Write a review',
    'Seach Results for' => 'Seach Results for',
    'No Result Found' => 'No Result Found',
    'Search any keywords on top search bar' => 'Search any keywords on top search bar',
    'Fill In Details' => 'Fill In Details',
    'Upload Photos' => 'Upload Photos',
    'Fill In Product Details' => 'Fill In Product Details',
    'Review Detail' => 'Review Detail',
    'Complete Review' => 'Complete Review',
    'You successfully submit review' => 'You successfully submit review',
    'Thank you' => 'Thank you',
    'Your review will help thousands of people' => 'Your review will help thousands of people',
    'Title Of Your Review' => 'Title Of Your Review',
    'Detailed Reviews' => 'Detailed Reviews',
    '30 words minimum' => '30 words minimum',
    'Key Highlights' => 'Key Highlights',
    'Rate On Relevant Points' => 'Rate On Relevant Points',
    'Upload Related Images' => 'Upload Related Images',
    'Maximum 3 images' => 'Maximum 3 images',
    'Check' => 'Check',
    'Created with Sketch' => 'Created with Sketch',
    'error' => 'error',
    'Purchase Info' => 'Purchase Info',
    'Date of Purchase' => 'Date of Purchase',
    'Purchase at' => 'Purchase at',
    'Country' => 'Country',
    'Malaysia' => 'Malaysia',
    'Upload Invoice Images' => 'Upload Invoice Images',
    'Review Anonymously' => 'Review Anonymously',
    'Go Back' => 'Go Back',
    'Submit Review' => 'Submit Review',
    'Which would you like to review' => 'Which would you like to review',
    'Review Product' => 'Review Product',
    'Review Service' => 'Review Service',
    'Review Movie' => 'Review Movie',
    'Product Details' => 'Product',
    'Product Name' => 'Product Name',
    'Product Category' => 'Product Category',
    'Product Brand' => 'Product Brand',
    'Product Model' => 'Product Model',
    'Next Step' => 'Next Step',
    'Brand Website' => 'Website',
    'Service Details' => 'Service Details',
    'Service Category' => 'Category',
    'Service Name' => 'Name',
    'Service Brand' => 'Brand',
    'Service Model' => 'Model',
    'Movie Details' => 'Movie Details',
    'Movie Category' => 'Category',
    'Movie Name' => 'Name',
    'Movie Brand' => 'Brand',
    'Movie Model' => 'Model',
    'Add New Brand' => 'Add New Brand'
];
