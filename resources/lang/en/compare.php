<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'Loading&#8230;' => 'Loading&#8230;',
    'Compare specs/details' => 'Compare specs/details',
    'Search any products, services or movies on below to compare' => 'Search any products, services or movies on below to compare',
    'Home' => 'Home',
    'Compare with' => 'Compare with',
    'Please enter model name or part of it' => 'Please enter model name or part of it',
    'Add a mobile phone' => 'Add a Product/Service/Movie',
    'Change with' => 'Change with',
    'Change' => 'Change',
    'Search' => 'Search',
];
