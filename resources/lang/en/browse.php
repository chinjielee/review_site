<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'If the listing you were looking for is not here, you can create it' => 'If the listing you were looking for is not here, you can create it',
    'What are you reviewing' => 'What are you reviewing',
    'Search any products, services or movies on below to review' => 'Search any products, services or movies on below to review',
    'SEARCH' => 'SEARCH',
    'No Result Found' => 'No Result Found',
    'Search any keywords on top search bar' => 'Search any keywords on top search bar',
];
