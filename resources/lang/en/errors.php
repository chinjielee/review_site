<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'About Us' => 'About Us',
    'Explore Categories' => 'Explore Categories',
    'language_flag' => 'united-states-flag',
    'Notification' => 'Notification',
    'Notification Setting' => 'Notification Setting',
    'Profile' => 'Profile',
    'Home' => 'Home',
    'Search' => 'Search',
    'Find products, services or questions...' => 'Find products, services or questions...',
    'Page Not Found' => 'Page Not Found',
    'Register' => 'Register',
    'Sign In' => 'Sign In',
    'Sign Out' => 'Sign Out',
    'Write a Review' => 'Write a Review',
    'Select Language' => 'Select Language',
    'English' => 'English',
    'Chinese' => 'Chinese',
    'Terms of Use' => 'Terms of Use',
    'Posting Guidelines' => 'Posting Guidelines',
    'Privacy Policy' => 'Privacy Policy',
    'Trademark Policy' => 'Trademark Policy',
    'Acceptable Use Policy' => 'Acceptable Use Policy',
    'Like our Facebook page to stay in the know on amazing new products' => 'Like our Facebook page to stay in the know on amazing new products',
    'Like us' => 'Like us',
    'on Facebook' => 'on Facebook',
    'Copyright Policy' => 'Copyright Policy',
    'Stay connected' => 'Stay connected',
    'Copyrights &copy; 2018 All Rights Reserved by WIKÅBÖ' => 'Copyrights &copy; 2018 All Rights Reserved by WIKÅBÖ',
    'Ooopps.! The Page you were looking for, could not be found.' => 'Ooopps.! The Page you were looking for, could not be found.',
];
