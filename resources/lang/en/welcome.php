<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'Know better, choose better' => 'Know better, choose better',
    'Can not find what you are looking for? Search here' => 'Can not find what you are looking for? Search here',
    'SEARCH' => 'SEARCH',
    'Find products, services or movies' => 'Find products, services or movies',
    'Let’s Be Efficient' => 'Let’s Be Efficient',
    'Most Discussed Products / Services' => 'Most Discussed Products / Services',
];
