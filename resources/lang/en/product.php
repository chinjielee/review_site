<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Menu Language Lines
    |--------------------------------------------------------------------------
    */

    'Review Rating' => 'Review Rating',
    'Helpful Reviews' => 'Helpful Reviews',
    'Review Now' => 'Review Now',
    'View all reviews' => 'View all reviews',
    'Overall Rating' => 'Overall Rating',
    'Write a review' => 'Write a review',
    'Ask a question' => 'Ask a question',
    'reviews' => 'reviews',
    'Product Details' => 'Product',
    'View more details on official website' => 'View more details on official website',
    'Official Website' => 'Official Website',
    'What are you reviewing' => 'What are you reviewing',
    'Search any products, services or movies on below to review' => 'Search any products, services or movies on below to review',
    'reviews' => 'reviews',
    'SEARCH' => 'SEARCH',
    'Reviews' => 'Reviews',
    'Q&amp;A' => 'Q&amp;A',
    'Price' => 'Price',
    'Compare' => 'Compare',
    'Stats' => 'Stats',
    'Product Photos' => 'Photos',
    'Total' => 'Total',
    'people shared their reviews' => 'people shared their reviews',
    'Anonymous' => 'Anonymous',
    'Reviewed Date' => 'Reviewed Date',
    'Overall Rating' => 'Overall Rating',
    'click here for details rating' => 'click here for details rating',
    'Rating Details' => 'Rating Details',
    'Close' => 'Close',
    'Key Highlight' => 'Key Highlight',
    'Date Purchased' => 'Date Purchased',
    'Purchased at' => 'Purchased at',
    'votes' => 'votes',
    'this review was helpful' => 'this review was helpful',
    'Similar opinion? Write a review to help others' => 'Similar opinion? Write a review to help others',
    'pages' => 'pages',
    'Your review will help thousands of people' => 'Your review will help thousands of people',
    'Share Your Review Now' => 'Share Your Review Now',
    'Pending Questions' => 'Pending Questions',
    'questions and answers' => 'questions and answers',
    'Questions &amp; Answers' => 'Questions &amp; Answers',
    'other replies' => 'other replies',
    'Replies' => 'Replies',
    'Show All' => 'Show All',
    'Answer Now' => 'Answer Now',
    'Want to be part of this discussion' => 'Want to be part of this discussion',
    'Login Now' => 'Login Now',
    'Answer anonymously' => 'Answer anonymously',
    'Cancel' => 'Cancel',
    'Would like to ask any question' => 'Would like to ask any question',
    'Post Your Question Now' => 'Post Your Question Now',
    'Ask anonymously' => 'Ask anonymously',
    'Minimize replies' => 'Minimize replies',
    'Write your answer here' => 'Write your answer here',
    'Post Now' => 'Post Now',
    'View' => 'View',
    'photos' => 'photos',
    'Home' => 'Home',
    'Product' => 'Product',
    'Service' => 'Service',
    'Movie' => 'Movie',
    'Total Review' => 'Total Review',
    'Pending Approval' => 'Pending Approval'
];
