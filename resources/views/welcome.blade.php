@extends( 'layouts.app' ) 
<!-- Additional CSS --> 
@section( 'headCss' )
<style>
			.twitter-typeahead {
				position: inherit!important;
				display: block !important;
			}
			
			.tt-dataset-value {
				line-height: 15px;
				padding: 10px;
			}
			
			.tt-menu {
				background-color: #fff;
				width: 100%;
				text-align: left;
				max-height: 150px;
				overflow-y: auto;
			}
			
			.tt-dataset-value:hover {
				background: rgba(0, 101, 204, 0.1);
				width: 100%;
			}
			.clients-grid li {
				display: -webkit-box;
				display: -ms-flexbox;
				display: flex;
				height: 100%;
			}
			.clients-grid li a, .clients-grid li img {
				-webkit-box-flex: 1;
				-ms-flex: 1 1 auto;
				flex: 1 1 auto;
				height: 100% !important;
			}
		</style>
@endsection 
<!-- Additional JS --> 
@section( 'headJs' )

		@endsection
		@section( 'content' )
<section id="" style="background: url({{ asset('img/light_gradient_violet_bg.jpg') }}) center; position: relative;">
  <div class="container clearfix">
    <div class="heading-block center nobottomborder topmargin-lg bottommargin-lg">
      <h1 data-animate="fadeInUp" class="nott"><strong>@lang('welcome.Let’s Be Efficient')</strong></h1>
      <span>
      <form action="#" method="post" role="form" class="landing-wide-form clearfix">
        <div id='bloodhound' class="col_four_fifth nobottommargin norightmargin">
          <input id='product-search-text' type="text" class="form-control input-lg not-dark typeahead" value="" placeholder="@lang('welcome.Find products, services or movies')...">
        </div>
        <div class="col_one_fifth col_last nobottommargin">
          <button class="btn btn-lg btn-blue btn-block nomargin" id="product-search" value="submit" type="submit" style="">@lang('welcome.SEARCH')</button>
        </div>
      </form>
      </span> </div>
  </div>
  <a href="#" data-scrollto="#content" data-offset="100" class="one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a> </section>
<section id="content">
  <div class="content-wrap">
      <div class="row clearfix align-items-stretch">
        <div class="col-lg-6 center col-padding">
          <div class="heading-block nobottomborder bottommargin-xs"> <span class="before-heading color">Most Discussed</span>
            <h3>Products</h3>
          </div>
          <ul class="clients-grid grid-4 bottommargin clearfix text-center">     
            @foreach($arrProduct as $product)
              <li><a href="{{route('Category Products List',$arrCat[$product['categories_id']]['category_slug'])}}">{{$arrCat[$product['categories_id']]['categories_name']}}</a></li>
            @endforeach
          </ul>
        </div>
        <div class="col-lg-6 center col-padding">
          <div class="heading-block nobottomborder bottommargin-xs"> <span class="before-heading color">Most Discussed</span>
            <h3>Services</h3>
          </div>
          <ul class="clients-grid grid-4 bottommargin clearfix text-center">
            @foreach($arrService as $service)
              <li><a href="{{route('Category Services List',$arrCat[$service['categories_id']]['category_slug'])}}">{{$arrCat[$service['categories_id']]['categories_name']}}</a></li>
            @endforeach
          </ul>
        </div>
      </div>
  </div>
</section>
@endsection
		@section( 'tailJs' ) 
<script>
				let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
				let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
				let search = 'product-search';
				let searchResult = 'product-result';
				let searchInput = 'product-search-text';
			</script> 
@endsection