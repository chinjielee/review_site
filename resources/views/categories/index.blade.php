@extends( 'layouts.app' )
@section('headCss')
<style>
	.default-category-background{
		background:black;
		height:425px;
		width:350px;
	}
	.portfolio-desc h3{
		overflow-wrap: break-word;
    	padding: 10px;
		color:white;
	}
	.default-category-background.white-back-black-text{
		background:white;
		height:400px;
		width:350px;
	}
	h3.white-back-black-text{
		color:black;
	}
	.custom-pagination ul{
		float:right;
		margin:0;
	}
	.category-image-height{
		height:425px;
		width:350px;
	}
	.category-image-wrap{

	}
	.category-image{

	}
</style>
@endsection
@section( 'content' )

<!-- Page Title
===
===
===
=== === === === === === === === === === === === -->
<section id="page-title" class="page-title-mini">

	<div class="container clearfix">
		<ol class="breadcrumb">
			<li><a href="#">Home</a>
			</li>
			<li class="breadcrumb-item"><a href="#">Products</a>
			</li>
			<li class="active"><strong>{{ Route::currentRouteName() }}</strong>
			</li>
		</ol>
	</div>

</section> <!-- #page-title end -->

<!-- Page Title === === === === === === === === === === === === === === === -->

<section id="slider" class="slider-parallax" style="background: url({{ asset('img/review_bg.jpg') }}) center;">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="heading-block center nobottommargin">
				<h1 class="nott white">@lang('categories.All Categories')</h1>
				<span class="white">{{$categoriesCount}} @lang('categories.Desc All Categories')</span>
			</div>
		</div>
	</div>
</section>
<!-- #page-title end -->

<!-- Content
===
===
===
=== === === === === === === === === === === === -->
<section id="content">

	<div class="content-wrap toppadding-sm">

		<div class="container clearfix">

			<div class="line topmargin-sm bottommargin-sm"></div>
			
			<div id="portfolio" class="portfolio grid-container category_wrapper clearfix">
				@foreach($arrCategories as $category)
					<div class="portfolio-item pf-media pf-icons" data-animate="fadeInUp" data-delay="0">
						<div class="portfolio-image category-image">
							<a class='category-image-wrap' href="{!!$category->link!!}">
								@if(!empty($category->image))
									<img class='category-image-height' src="{{ asset('images/categories/')}}{{$category->image}}" alt="{{$category->categories_name}}">
								@else
									<div class="default-category-background"></div>
								@endif
							</a>
							<div class="portfolio-overlay">
								<a href="{{route('Category '.ucfirst($category->category_parent_slug).' List',['category'=>$category->category_slug])}}" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
							</div>
						</div>
						<div class="portfolio-desc center">
							<h3>{{$category->categories_name}}</h3>
							<span>{{$category->products_count}} products</span>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>

</section> <!-- #content end -->


@endsection


<!--
<script>
	// Infinity Scroll
	jQuery( window ).on( 'load', function () {

		var $container = $( '.infinity-wrapper' );

		$container.infiniteScroll( {
			path: '.load-next-posts',
			history: false,
			status: '.page-load-status',
		} );

		$container.on( 'load.infiniteScroll', function ( event, response, path ) {
			var $items = $( response ).find( '.infinity-loader' );
			// append items after images loaded
			$items.imagesLoaded( function () {
				$container.append( $items );
				$container.isotope( 'insert', $items );
				setTimeout( function () {
					SEMICOLON.initialize.resizeVideos();
					SEMICOLON.initialize.lightbox();
					SEMICOLON.widget.loadFlexSlider();
				}, 1000 );
			} );
		} );

	} );
</script>
-->