@extends( 'layouts.app' )
@section('headCss')
<style>
	.default-category-background{
		background:black;
		height:425px;
		width:350px;
	}
	.portfolio-desc h3{
		overflow-wrap: break-word;
    	padding: 10px;
		color:white;
	}
	.default-category-background.white-back-black-text{
		background:white;
		height:400px;
		width:350px;
	}
	h3.white-back-black-text{
		color:black;
	}
	.custom-pagination ul{
		float:right;
		margin:0;
	}
</style>
@endsection
@section( 'content' )

<!-- Page Title
=== === === === === === === === === === === === -->
<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li><a href="#">Home</a>
			</li>
			<li class="breadcrumb-item"><a href="{{route(ucfirst($pageInfo->route['label']).' All List')}}">{{ucfirst($pageInfo->route['label'])}}</a>
			</li>
			<li class="active"><strong>{{ Route::currentRouteName() }}</strong>
			</li>
		</ol>
	</div>

</section> <!-- #page-title end -->

<!-- Page Title === === === === === === === === === === === === === === === -->

<section id="slider" class="slider-parallax" style="background: url({{ asset('img/review_bg.jpg') }}) center;">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="heading-block center nobottommargin">
				<h1 class="nott white">{{$strCategoryTitle}}</h1>
				<span class="white">Check out {{$total}} of best {{$strCategoryTitle}} categories of the year.</span>
			</div>
		</div>
	</div>
</section>
<!-- #page-title end -->

<!-- Content -->
<section id="content">
	<div class="content-wrap toppadding-sm">
		<div class="container clearfix">
			<div class="row clearfix">
				<div class="col-md-8 custom-pagination">
					{{$arrSubCategoryList->links()}}
				</div>
			</div>
			<div class="line topmargin-sm bottommargin-sm"></div>
			<div id="portfolio" class="portfolio grid-container category_wrapper clearfix">
			@foreach($arrSubCategoryList as $category)
				<article class="portfolio-item pf-media pf-icons" data-animate="fadeInUp" data-delay="0">
					<div class="portfolio-image category-image">
						<a class='category-image-wrap' href="{!!$category->link!!}">
						@if(!empty($category->image))
							<img class='category-image-height' src="{{$category->image}}" alt="{{$category->categories_name}}">
						@else
							<div class="default-category-background {{$category->display}}"></div>
						@endif
						</a>
						<div class="portfolio-overlay">
							<a href="{!!$category->link!!}" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
						</div>
					</div>
					<div class="portfolio-desc center">
						<h3 class="{{$category->display}}">{{$category->categories_name}}</h3>
						<span>{{$category->products_count}} products</span>
					</div>
				</article>
			@endforeach
			</div>
		</div>
	</div>
	<div class="divider divider-center"><a href="#" data-scrollto="#header"><i class="icon-chevron-up"></i></a>
	</div>
	<div class="content-wrap notoppadding">
		<div class="container clearfix">
		<div class="row clearfix">
			<div class="heading-block center bottommargin-sm">
				<h2>Most Popular Brands</h2>
			</div>
		</div>
		<div id="oc-clients" class="owl-carousel image-carousel carousel-widget" data-margin="60" data-nav="false" data-autoplay="5000" data-pagi="false" data-items-xs="2" data-items-sm="3" data-items-md="4" data-items-lg="5" data-items-xl="6">
			@foreach($arrBrands as $brand)
				<div class="oc-item center"><a href="#"><img src="{{ asset('images/brand_image')}}/{{$brand->images}}" alt="{{$brand->brand_title}}"></a>
				</div>	
			@endforeach
		</div>
	</div>
</section> <!-- #content end -->

@endsection