@extends( 'layouts.app' )
@section( 'headCss' )
	<style>
		.default-product-background{
			color:white;
			background:black;
			height:200px;
			width:200px;
		}
		.custom-pagination ul{
			float:right;
			margin:5px;
		}
		i.icon-thumbs-up2{
			font-size: 0.92em;
			text-shadow: none;
		}
		.rating-row-rate{
			font-size:12px;
			margin-top:-5px;
		}
		.rating-row-title{
			line-height:30px;
		}
		.rating-row{
			min-height:30px;
		}
	</style>
@endsection
@section( 'content' )
<!-- Page Title============ === === === === === === === === === === === -->
<section id="page-title" class="page-title-mini">

	<div class="container clearfix">
		<ol class="breadcrumb">
			<li><a href="#">Home</a>
			</li>
			<li class="breadcrumb-item"><a href="{{route(ucfirst($pageInfo->route['label']).' All List')}}">{{ucfirst($pageInfo->route['label'])}}</a>
			</li>
			<li class="active"><strong>{{ Route::currentRouteName() }}</strong>
			</li>
		</ol>
	</div>

</section> <!-- #page-title end -->

<!-- Page Title === === === === === === === === === === === === === === === -->

<section id="slider" class="slider-parallax" style="background: url({{ asset('img/review_bg.jpg') }}) center;">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="heading-block center nobottommargin">
				<h1 class="nott white">{{$strCategoryTitle}}</h1>
				<span class="white">Your review will help thousands of people!</span>
			</div>
		</div>
	</div>
</section>
<!-- #page-title end -->

	<div class="body-overlay"></div>

	<div id="side-panel">

		<div id="side-panel-trigger-close" class="side-panel-trigger"><a href="#"><i class="icon-line-cross"></i></a></div>

		<div class="side-panel-wrap">
		</div>

	</div>
	
<!-- Content -->
<section id="content">

	<div class="content-wrap toppadding-sm">
		<div id="el">
			<div class="container clearfix">
				@if($arrProductList->total() > 0)
				<div class="sidebar nobottommargin hidden-sm hidden-xs">
					<div class="sidebar-widgets-wrap">
						<div class="widget widget-filter-links clearfix" v-for="facet in searchResult.data.aggregations">
							<h4 class="nott"><strong>Select @{{ facet.title }}</strong></h4>
							<ul class="custom-filter" data-container="#shop" data-active-class="active-filter">
							<li class="widget-filter-reset active-filter"><a href="#" data-filter="*">Clear</a>
							</li>
							<li v-for="bucket in facet.buckets" >
								<a @click='checkIt' v-model="filters[facet.name]" class="filter bucket-toggle" v-bind:value="bucket.key" v-bind:data-click="'checkbox-'+bucket.key">@{{ bucket.key }}</a><span>@{{ bucket.doc_count }}</span>
								<input v-bind:id="'checkbox-'+bucket.key" class="checkbox" type="checkbox" v-model="filters[facet.name]" v-bind:value="bucket.key" v-el="'checkbox-'+bucket.key" ref="'click'+bucket.key">
							</li>	
						</div>
					</div>
				</div>
				
				<div class="postcontent col_last clearfix">
					<div class="row clearfix">
						<div class="col-md-3 col-sm-4">
							@if($pageInfo->route['label'] != "movies")
							<a href="#" id='btn_compare' class="button button-3d button-rounded button-blue nott btn-block text-center"><i class="icon-line-columns"></i>Compare</a>
							@endif
						</div>
						
						<div class="col-md-3 col-sm-4 visible-sm visible-xs">
							<a href="#" class="button button-3d button-rounded button-light side-panel-trigger nott btn-block text-center">Select Brands</a>
						</div>
						
						<div class="col-md-6 custom-pagination">
							{{$arrProductList->links()}}						
						</div>
					</div>
					<div class="line topmargin-sm bottommargin-sm"></div>
					<div class="row clearfix bottommargin-sm">
						<div class="col-md-3 col-sm-4 col-xs-6 sf-Apple center bottommargin-sm cat_list_wrapper" v-for="item of searchResult.data.items">
							<input v-bind:id="'product-' +item.product_services_id" v-bind:data-title="item.product_slug" class="checkbox-style" name="checkbox_product" type="checkbox">
							<label v-bind:for="'product-' +item.product_services_id" class="checkbox-style-3-label checkbox-large product-checkbox" style="position: absolute; z-index: 1;"></label>
							<a v-bind:href=item.link>
								<!-- <div class="default-product-background"></div> -->
								<img v-bind:src="item.images">
								
								<h5 class="topmargin-xs nobottommargin">@{{item.product_service_title}}</h5>
								<input id="rating-overall" type="number" class="show-rating-wikabo" v-bind:value= 'item.product_rating.overall' max="5" data-stars="5" data-size="xs">
							</a>
						</div>
					</div>
				</div>
				@else
				<div class="text-center">
					<i class="icon-line-search" style="font-size: 90px;opacity: 0.3;"></i>
					<h1 class="nobottommargin" style="opacity: 0.3;">No Result Found</h1>
					<h4>There's no item listed currently. Explore more at <a href="{{route(ucfirst($pageInfo->route['label']).' All List')}}" class="border_bottom_dotted">{{ucfirst($pageInfo->route['label'])}}</a>.</h4>
				</div>
				@endif
				@if($subCatCount > 0)
				<div class="divider divider-center"><a href="#" data-scrollto="#header"><i class="icon-chevron-up"></i></a>
				</div>
					<div class="content-wrap notoppadding">
						<div class="container clearfix">
						<div class="row clearfix">
							<div class="heading-block center bottommargin-sm">
								<h4>Sub Categories</h4>
							</div>
						</div>
						<div class="row clearfix">
							@foreach($arrSubCategoryList as $category)
								<article class="portfolio-item pf-media pf-icons">
									<div class="portfolio-image category-image">
										<a class='category-image-wrap' href="{!!$category->link!!}">
										{{$category->categories_name}}
										</a>
									</div>
								</article>
							@endforeach
						</div>
					</div>
				@endif			
			</div>
		</div>
	</div>

</section> <!-- #content end -->
@endsection
@section('tailJs')
	<script src="{{ asset('js/lib/vue.min.js') }}"></script>
	<script src="{{ asset('js/lib/itemsjs.min.js') }}"></script>
	<script src="{{ asset('js/sweetalert.min.js') }}"></script>
	<script>
		var urlCompare = "{{route('Compare',['','',''])}}/";
		$(document).ready(function(){
			$('.product-checkbox').hide();
		});
		
		$( window ).load(function() {
			$('input[name="checkbox_product"]').each(function() {
				this.checked = false;
			});
		});

		$(document).on('vue-loaded', function () {
			$('#btn_compare').on('click', function(){
				var products = [];
				var count =0;
				if($("input[name='checkbox_product']:checked").length == 0){
					if($('.product-checkbox').is(":visible")) {
						swal({
							type: 'info',
							title: 'Oops...',
							text: 'Please choose an items to start compare.',
						});	
						return false;
					} else {
						$('.product-checkbox').show();
						$('#btn_compare').html('<i class="icon-line-columns"></i> Compare Now');
						return false;
					}
				}
				else if($("input[name='checkbox_product']:checked").length > 3){
					swal({
						type: 'error',
						title: 'Oops...',
						text: 'Only can compare 3 product in a time.',
					});
				} else {
					$.each($("input[name='checkbox_product']:checked"), function(){
						if(count >= 3){
							return false;
						}
						urlCompare += $(this).data('title')+"/";
						count ++;
					});
					window.location.href=urlCompare;
				}
				
			});
			$(".show-rating-wikabo").rating({
				containerClass: 'is-heart',
				filledStar: '<i class="icon-thumbs-up2"></i>',
				emptyStar: '<i class="icon-thumbs-up"></i>',
				showClear:0,
				showCaption:0,
				displayOnly:1
			});
		});
		var jsonResult = {!!$strJsonProductsList!!};
		
		
	</script>
@include('categories.facet')
@endsection