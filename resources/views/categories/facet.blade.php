<script>
    $('document').ready(function() {
        var configuration = {
            searchableFields: ['product_service_title', 'brand_title', 'product_service_attributes'],
            sortings: {
            product_service_title: {
                field: 'Product name',
                order: 'asc'
              }
            },
            aggregations: {
              brand_title: {
                title: 'Brands',
                size: 10
              },
              categories_name: {
                title: 'Categories',
                size: 10
              }
            }
          }
        for(var i = 0 ; i < jsonResult.length;i++){
            if(jsonResult[i].product_rating != "" || typeof(jsonResult[i].product_rating) == "undefinded")
                jsonResult[i].product_rating = JSON.parse(jsonResult[i].product_rating);
        }
        itemsjs = itemsjs(jsonResult, configuration);
        itemsjs.search();
        var vm = new Vue({
            el: '#el',
            mounted() {
                $(document).trigger('vue-loaded');
            },
            data: function () {
                // making it more generic
                var filters = {};
                Object.keys(configuration.aggregations).map(function(v) {
                    filters[v] = [];
                })

                return {
                query: '',
                // initializing filters with empty arrays
                filters: filters,
                }
            },
            methods: {
                reset: function () {
                    var filters = {};
                    Object.keys(configuration.aggregations).map(function(v) {
                        filters[v] = [];
                    })

                    this.filters = filters;
                    this.query = '';
                },
                checkIt:  function() {
                    this.$refs.clickSony.click()
                }
            },
            computed: {
                searchResult: function () {
                var result = itemsjs.search({
                    query: this.query,
                    filters: this.filters
                })
                return result;
                }
            }
        });
    });

</script>