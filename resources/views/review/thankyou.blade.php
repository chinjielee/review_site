@extends( 'layouts.app' )

@section( 'content' )

<!-- Page Title === === === === === === === === === === === === === === === -->
<section id="page-title" class="page-title-mini hidden-xs">
	<div id="processTabs">
		<ul class="process-steps process-3 nomargin clearfix">
			<li class="active">
				<a href="" class="i-circled i-bordered i-alt divcenter">1</a>
				<h5 class="nomargin">@lang('review.Fill In Details')</h5>
			</li>
			<li class="active">
				<a href="" class="i-circled i-bordered i-alt divcenter">2</a>
				<h5 class="nomargin">@lang('review.Upload Photos')</h5>
			</li>
			<li class="active">
				<a href="" class="i-circled i-bordered i-alt divcenter"><i class="icon-line-check"></i></a>
				<h5 class="nomargin">@lang('review.Complete Review')</h5>
			</li>
		</ul>
	</div>
</section> <!-- #page-title end -->

<!-- Content======================== === === === === === === === -->

<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="nomargin clearfix col-lg-10 divcenter">
				<div class="panel panel-default nobottommargin">
					<div class="panel-body">
						<div class="divcenter col-md-8">
							<div class="alert alert-success center">@lang('review.You successfully submit review').</div>
							<div class="center" style="opacity: 0.3;">
								<i class="icon-line-circle-check" style="font-size: 90px;"></i>
								<h1 class="nobottommargin">@lang('review.Thank you')</h1>
								<h3>@lang('review.Your review will help thousands of people')</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@endsection