@extends( 'layouts.app' )
@section('headCss')
<link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/jquery_taginput.css') }}">
<style>
	.swal2-popup {
		font-size: 1.6rem !important;
	}
    .review-detail-area{
        resize: vertical;
    }
    i.icon-thumbs-up2{
		font-size: 0.92em;
		text-shadow: none;
	}
	.rating-row-rate{
		font-size:12px;
		margin-top:-5px;
	}
	.rating-row-title{
		line-height:30px;
	}
	.rating-row{
		min-height:30px;
	}
</style>
@endsection
@section( 'content' )

<!-- Page Title === === === === === === === === === === === === === === === -->
<section id="page-title" class="page-title-mini hidden-xs">
	<div id="processTabs">
		<ul class="process-steps process-3 nomargin clearfix">
			<li class="active">
				<a href="" class="i-circled i-bordered i-alt divcenter">1</a>
				<h5 class="nomargin">@lang('review.Fill In Product Details')</h5>
			</li>
			<li class="active">
				<a href="" class="i-circled i-bordered i-alt divcenter">2</a>
				<h5 class="nomargin">@lang('review.Review Detail')</h5>
			</li>
			<li>
				<a href="" class="i-circled i-bordered i-alt divcenter"><i class="icon-line-check"></i></a>
				<h5 class="nomargin">@lang('review.Complete Review')</h5>
			</li>
		</ul>
	</div>
</section> <!-- #page-title end -->

<!-- Content==================
===
=== === === === === === === === -->
<style>
.fancy-title.title-dotted-border {
	background: url({{ asset('img/dotted.png') }}) center repeat-x;
}
a.review-btn {
	min-width: 200px
}
a.review-active {
	background-color: #004d9a !important;
	color: #FFF !important;
	border-color: transparent !important;
	text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2) !important;
}
@media (max-width: 767px) {
.star-rating .caption {
	display: none;
}
a.review-btn {
	min-width: 100px
}
}
</style>

<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="nomargin clearfix col-lg-10 divcenter">
				<div class="panel panel-default nobottommargin">
					<div class="panel-body">
                        @if(!empty($objProductDetail->display_image))
                        <div class="col-md-2 col-sm-3 col-xs-5 divcenter bottommargin-sm">
                            <img src="{{$objProductDetail->display_image}}" alt="{{$objProductDetail->product_service_title}}">
                        </div>
                        @endif
                        <div class="fancy-title title-dotted-border title-center">
                            <h3>{{$objProductDetail->product_name}}{{$objProductDetail->product_service_title}} - <span>@lang('review.Reviews')</span></h3>
                        </div>
                        @if(Session::get('errors')||count( $errors ) > 0)
                            <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <i class="icon-remove-sign"></i> {{ $error }} </br>
                                @endforeach
                            </div>
                        @endif
                        <form id="review_form" name="review_form" class="nobottommargin row" action="{{ route('Post Review Detail') }}" method="post">
                            {{ csrf_field() }}
                            <input type="hidden" name="temp_product" value= "{{$objProductDetail->pending_product_service_id}}">
                            <input type="hidden" name="product" value= "{{$objProductDetail->product_services_id}}">
                            <input type="hidden" name="product_slug" value= "{{$objProductDetail->product_slug}}">
                            <div class="row">                                
                                <div class="col-md-6">
                                    <div class="col_full bottommargin-xs">
                                        <label for="title">@lang('review.Title Of Your Review'):</label>
                                        <input type="text" id="title" name="title" value="{{ old('title') }}" class="sm-form-control" placeholder="Review Title" required>
                                    </div>
                                    <div class="col_full bottommargin-xs">
                                        <label for="detail">@lang('review.Detailed Reviews'):</label>
                                        <textarea class="form-control review-detail-area" id="detail" name="detail" rows="6" minlength="30" required>{{ old('detail') }}</textarea>
                                        <!--Default show below details-->
                                        <small class="form-text text-muted">@lang('review.30 words minimum')</small>
                                    </div>
                                    
                                        <div class="col_full bottommargin-xs">
                                            <label for="highlight">@lang('review.Key Highlights'): </label>
                                            <input id="highlight" name="highlight" value="{{ old('highlight') }}" type="text" data-role="tagsinput" placeholder="Insert a key highlight">
                                        </div>
                                </div>
                                <div class="col-md-6">
                                        <div class="col_full bottommargin-sm">
                                           <div class="mb-2">
                                            <label for="text">@lang('review.Rate On Relevant Points'): </label>
                                            <!-- Create an dynamic loop getting the rating format -->
                                            @foreach($arrRatingOption as $option)
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">{{$option}}</p>
                                                </div>
                                                <div class="col-xs-7">
                                                    <input id="input-{{$option}}" name="rating-{{$option}}" type="number" class="rating-form" max="5" data-stars="5" data-step="0.5" data-size="xs">
                                                </div>
                                            </div>
                                            @endforeach
											</div>
                                        </div>
                                        
                                    <div class="col_full bottommargin-sm">
                                        <label for="text">@lang('review.Upload Related Images') <small class="nott">(@lang('review.Maximum 3 images')</small>: </label>
                                        <div id="review_dropzone" class="dropzone">
                                            <input type="file" name="product_image" class="hidden" multiple>
                                        </div>
                                        <div id="preview" style="display: none;">
                                            <div class="dz-preview dz-file-preview">
                                                <div class="dz-image"><img data-dz-thumbnail /></div>
                                                <div class="dz-details">
                                                    <div class="dz-size"><span data-dz-size></span></div>
                                                    <div class="dz-filename"><span data-dz-name></span></div>
                                                </div>
                                                <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                                <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                                <div class="dz-success-mark">
                                                    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                                        <title>@lang('review.Check')</title>
                                                        <desc>@lang('review.Created with Sketch').</desc>
                                                        <defs></defs>
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                                            <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                        </g>
                                                    </svg>
                                                </div>
                                                <div class="dz-error-mark">
                                                    <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                        <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                                        <title>@lang('review.error')</title>
                                                        <desc>@lang('review.Created with Sketch').</desc>
                                                        <defs></defs>
                                                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                                            <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                                                                <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                            </g>
                                                        </g>
                                                    </svg>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="fancy-title title-dotted-border title-center topmargin">
                                <h3><span>@lang('review.Purchase Info')</span></h3>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12 bottommargin-xs">
                                    <div class="col_full bottommargin-xs">
                                        <label for="dop">@lang('review.Date of Purchase'):</label>
                                        <input type="text" value="{{old('dop')}}" name="dop" id="dop" class="sm-form-control tleft today" data-date-end-date="0d" placeholder="MM/DD/YYYY" autocomplete="nope">
                                    </div>
                                    <div class="col_full bottommargin-xs">
                                        <label for="purchase_at">@lang('review.Purchase at'): </label>
                                        <input type="text" name="purchase_at" value="{{old('purchase_at')}}" id="purchase_at" class="sm-form-control tleft today" placeholder="ABC mall">
                                    </div>
                                    <div class="col_full bottommargin-xs">
                                        <label for="country">@lang('review.Country'): </label>
                                        <select class="selectpicker" name="country">
                                            @foreach($countryList as $country)
                                            <option value="{{strtolower($country['value'])}}" {{$country['select']}}>{{ucfirst(strtolower($country['text']))}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 bottommargin-xs">
                                    <label for="text">@lang('review.Upload Invoice Images') <small class="nott">(@lang('review.Maximum 3 images'))</small>: </label>
                                    <div id="invoice_dropzone" class="dropzone">
                                        <input type="file" name="invoice_image" class="hidden" multiple>
                                    </div>
                                    <div id="preview" style="display: none;">
                                        <div class="dz-preview dz-file-preview">
                                            <div class="dz-image"><img data-dz-thumbnail /></div>
                                            <div class="dz-details">
                                                <div class="dz-size"><span data-dz-size></span></div>
                                                <div class="dz-filename"><span data-dz-name></span></div>
                                            </div>
                                            <div class="dz-progress"><span class="dz-upload" data-dz-uploadprogress></span></div>
                                            <div class="dz-error-message"><span data-dz-errormessage></span></div>
                                            <div class="dz-success-mark">
                                                <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                    <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                                    <title>@lang('review.Check')</title>
                                                    <desc>@lang('review.Created with Sketch').</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                                        <path d="M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" stroke-opacity="0.198794158" stroke="#747474" fill-opacity="0.816519475" fill="#FFFFFF" sketch:type="MSShapeGroup"></path>
                                                    </g>
                                                </svg>
                                
                                            </div>
                                            <div class="dz-error-mark">
                                                <svg width="54px" height="54px" viewBox="0 0 54 54" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                                                    <!-- Generator: Sketch 3.2.1 (9971) - http://www.bohemiancoding.com/sketch -->
                                                    <title>@lang('review.error')</title>
                                                    <desc>@lang('review.Created with Sketch').</desc>
                                                    <defs></defs>
                                                    <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" sketch:type="MSPage">
                                                        <g id="Check-+-Oval-2" sketch:type="MSLayerGroup" stroke="#747474" stroke-opacity="0.198794158" fill="#FFFFFF" fill-opacity="0.816519475">
                                                            <path d="M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z" id="Oval-2" sketch:type="MSShapeGroup"></path>
                                                        </g>
                                                    </g>
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col_full form-check">
                                <input class="checkbox-style form-check-input" name="anonymous" type="checkbox" id="anonymous">
                                <label class="form-check-label checkbox-style-3-label" for="anonymous">
                                    @lang('review.Review Anonymously')
                                </label>
                            </div>
                            <hr>
                            <div class="col_full center nobottommargin">
                                <label style="text-transform: inherit; font-weight: normal;">By clicking “SUBMIT REVIEW”, I accept WIKÅBÖ review’s Guideline and Terms of Usage.</label>
                            </div>
                            <div class="divcenter">
                                <div class="col-md-3 col-sm-4 com-xs-8 col-md-offset-3 col-sm-offset-2 topmargin-sm bottommargin-sm">
                                @if(isset($objProductDetail->url))
                                    <a class="button button-3d button-circle button-white button-light button-large nomargin btn-block nott" href="{{$objProductDetail->url['detail']}}" id="back-step"><i class="icon-line-arrow-left"></i>@lang('review.Go Back')</a>
                                @else
                                    <button class="button button-3d button-circle button-white button-light button-large nomargin btn-block nott" id="back-step" onclick="goBack();return false;"><i class="icon-line-arrow-left"></i> @lang('review.Go Back')</button>
                                @endif
                                </div>
                                <div class="col-md-3 col-sm-4 com-xs-8 topmargin-sm bottommargin-sm">
                                    <button class="button button-3d button-circle button-blue button-large nomargin btn-block nott" id="review-form-submit" name="review-form-submit" value="review"> @lang('review.Submit Review') <i class="icon-line-arrow-right"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
            </div>
         </div>
    </section>
@endsection

@section('tailJs')
    <script>
        let delUrl = '{{route('review.remove')}}';
        let uploadUrl = "{{ route('review.upload') }}";
        let diresuval = '{{ Auth::user()->user_id }}';
        $(document).ready(function(){
            $("#highlight").tagsInput({
                placeholder: 'Insert a key highlight',
                width: 'auto',
                maxTags: 5,
                itemValue: 'highlight'
            });
            // Rating form 
            $(".rating-form").rating({
                containerClass: 'is-heart',
                filledStar: '<i class="icon-thumbs-up2"></i>',
                emptyStar: '<i class="icon-thumbs-up"></i>',
                showClear:0,
                showCaption:0
            });

            // Handle on the form submittion : 
            $('#review-form-submit').click(function(){
                $("#review_form").submit();    
            });

            $("div#invoice_dropzone").dropzone({});
            $("div#product_dropzone").dropzone({});
            $( "#dop" ).datepicker();

            
			$("#review_form").submit(function(){
				if($("#review_form").valid()){
					form.submit();
				} else {
					return false;
				}
            });
			$("#review_form").validate({
				onsubmit: true,
				rules: {
					title: "required",
					detail: "required"
                    // ,
                    // dop:"required",
                    // purchase_at:"required"
				},
				messages:{
					title: "Please fill in the product name.",
					detail:"Please choose one of the product category"
                    // ,
                    // dop:"Please fill in Purchases date",
                    // purchase_at:"Please fill in where it purchases"
				}
            });
        });
        function goBack() {
            window.history.back();
        }
    </script>
    <script src="{{ asset('js/lib/jquery.tagsinput.min.js') }}"></script>
    <script src="{{ asset('js/lib/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('js/dropzone.min.js') }}"></script>
	<script src="{{ asset('js/dropzone-config.js') }}"></script>
	<script src="{{ asset('js/sweetalert.min.js') }}"></script>
@endsection