@extends( 'layouts.app' )

@section( 'content' )

<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">@lang('review.Home')</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong>
			</li>
		</ol>
	</div>
</section>

<!-- Content==================
=== === === === === === === === === -->
<div id="page-menu clearfix">
	<div id="page-menu-wrap clearfix">
	<a href="{{ route('Create Review') }}" class="button button-full button-blue center tright" style="padding: 15px 0px;">
<div class="container clearfix" style="font-size: 18px;">
	If the listing you were looking for isn't here, you can <strong>create</strong> it! <i class="icon-caret-right" style="top:4px;"></i>
</div>
</a>
	</div>
	</div>
<section id="slider" class="slider-parallax" style="background: url({{ asset('img/review_bg.jpg') }}) center;">
	<div class="content-wrap nobottompadding">
		<div class="container clearfix">
			<div class="heading-block center">
				<h1 class="topmargin white">@lang('review.What are you reviewing')?</h1>
				<span class="white">@lang('review.Search any products, services or movies on below to review')</span>
				<span>
					<form action="{{route('Search Result',[""])}}" method="get" role="form" class="landing-wide-form clearfix">
						<div id='bloodhound' class="col_four_fifth nobottommargin norightmargin">
							<input id='product-search-text' type="text" class="form-control input-lg not-dark typeahead" value="" placeholder="@lang('review.Example: iPhone, Fridges, Washing Machines, TVs...')">
						</div>
						<div class="col_one_fifth col_last nobottommargin">
							<button class="btn btn-lg btn-blue btn-block nomargin" id="product-search" value="submit" type="submit" style="">@lang('review.SEARCH')</button>
						</div>
					</form>
				</span>
			</div>
		</div>
	</div>

</section>

<!--Stagging No Search Result-->
<section id="content" class="hide">
	<div class="content-wrap">
		<div class="container center clearfix" style="opacity: 0.3;">
		<i class="icon-line-search" style="font-size: 90px;"></i>
			<h1 class="nobottommargin">@lang('review.No Result Found')</h1>
			<h3>@lang('review.Search any keywords on top search bar')</h3>
		</div>
	</div>
</section>

<!--Stagging Search Result-->
<section id="content" class="hide">

	<div class="content-wrap">

		<div class="container clearfix">

				<h3 class="center">Seach Results for « iphone »</h3>

				<div class="line topmargin-sm bottommargin-sm"></div>

				<div class="row clearfix">
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
				</div>
				
				<div class="row clearfix">
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
				</div>
				
				<div class="row clearfix">
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span><span class="star"><i class="icon-star3"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
				</div>
			</div>
		</div>
</section>
@endsection
@section('tailJs')
<script>
	let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
	let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
	let search = 'product-search';
	let searchResult = 'product-result';
	let searchInput = 'product-search-text';
</script>
@endsection