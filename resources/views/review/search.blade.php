@extends( 'layouts.app' )
@section( 'headCss' )
	<style>
		i.icon-thumbs-up2{
			font-size: 0.92em;
			text-shadow: none;
		}
		.rating-row-rate{
			font-size:12px;
			margin-top:-5px;
		}
		.rating-row-title{
			line-height:30px;
		}
		.rating-row{
			min-height:30px;
		}
	</style>
@endsection
@section( 'content' )

<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">@lang('review.Home')</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong>
			</li>
		</ol>
	</div>
</section>

<!-- Content==================
=== === === === === === === === === -->
<div id="page-menu clearfix">
	<div id="page-menu-wrap clearfix">
	<a href="{{ route('Create Review') }}" class="button button-full button-blue center tright toppadding-xs bottompadding-xs">
<div class="container clearfix" style="font-size: 18px;">
	If the listing you were looking for isn't here, you can <strong>create</strong> it!  <i class="icon-caret-right" style="top:4px;"></i>
</div>
</a>
	</div>
	</div>
	@if(!empty($arrSearchResult))
		<section id="content">
		<div class="content-wrap">
			<div class="container clearfix">

				<h3 class="center">@lang('review.Seach Results for') {{$keyword}}</h3>

				<div class="line topmargin-sm bottommargin-sm"></div>

				<div class="row clearfix">
	@endif
@forelse($arrSearchResult as $result)
	<!--Stagging Search Result-->
	<div class="col-md-2 col-sm-3 col-xs-6 center bottommargin">
		@if($result->category_slug != NULL AND $result->parent_cat_1 != NULL AND $result->parent_cat_2 != NULL)
			<a href="{!!route(ucfirst($result->parent_cat_2).' Detail3', [$result->parent_cat_1 , $result->category_slug, $result->product_slug] )!!}"><img src="@if(empty($result->images)) {{ asset('img/empty-compare.jpg') }} @else {{ $result->images }} @endif" alt="{{$result->product_service_title}}">
		@elseif($result->category_slug != NULL AND $result->parent_cat_1 != NULL  AND $result->parent_cat_1 != "")
			<a href="{!!route(ucfirst($result->parent_cat_1).' Detail2', [$result->category_slug, $result->product_slug])!!}"><img src="@if(empty($result->images)) {{ asset('img/empty-compare.jpg') }} @else {{ $result->images }} @endif" alt="{{$result->product_service_title}}">
		@elseif($result->category_slug != NULL AND $result->category_slug != "")
			<a href="{!!route(ucfirst($result->category_slug).' Detail1', [$result->product_slug] )!!}"><img src="@if(empty($result->images)) {{ asset('img/empty-compare.jpg') }} @else {{ $result->images }} @endif" alt="{{$result->product_service_title}}">
		@endif
			<h5 class="topmargin-sm nobottommargin">{{$result->product_service_title}}<br>
			</h5>
			<input id="rating-overall" type="number" class="show-rating-wikabo" value= '{{$result->overall_rating}}' max="5" data-stars="5" data-size="xs">
			<a href="{{route('Review Product Detail',['product_slug'=>$result->product_slug])}}" class="button button-3d button-rounded button-light button-mini nott">@lang('review.Write a review')</a>
		</a>
	</div>
@empty
	
	<!--Stagging No Search Result-->
	<section id="content">
		<div class="content-wrap">
			<div class="container center clearfix" style="opacity: 0.3;">
			<i class="icon-line-search" style="font-size: 90px;"></i>
				<h1 class="nobottommargin">@lang('review.No Result Found')</h1>
				<h3>@lang('review.Search any keywords on top search bar')</h3>
			</div>
		</div>
	</section>
@endforelse
@if(!empty($arrSearchResult))
			</div>
		</div>
	</div>
</section>
@endif
@endsection
@section('tailJs')
<script>
	let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
	let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
	let search = 'product-search';
	let searchResult = 'product-result';
	$(".show-rating-wikabo").rating({
			containerClass: 'is-heart',
			filledStar: '<i class="icon-thumbs-up2"></i>',
			emptyStar: '<i class="icon-thumbs-up"></i>',
			showClear:0,
			showCaption:0,
			displayOnly:1
		});
</script>
@endsection