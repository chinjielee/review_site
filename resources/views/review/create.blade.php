@extends( 'layouts.app' )
@section( 'headCss' )
<link rel="stylesheet" href="{{ asset('css/dropzone.min.css') }}">
<style>
	.swal2-popup {
		font-size: 1.6rem !important;
	}
	.review-type-active {
		background-color: #009d9a !important;
	}
	.review-type-active span{
		color:white;
	}
</style>
@endsection
@section( 'content' )

<!-- Page Title=========================== === === === === === === -->
<section id="page-title" class="page-title-mini hidden-xs">
	<div id="processTabs">
	<ul class="process-steps process-3 nomargin clearfix">
		<li class="active">
			<a href="" class="i-circled i-bordered i-alt divcenter">1</a>
			<h5 class="nomargin">@lang('review.Fill In Product Details')</h5>
		</li>
		<li>
			<a href="" class="i-circled i-bordered i-alt divcenter">2</a>
			<h5 class="nomargin">@lang('review.Review Detail')</h5>
		</li>
		<li>
			<a href="" class="i-circled i-bordered i-alt divcenter"><i class="icon-line-check"></i></a>
			<h5 class="nomargin">@lang('review.Complete Review')</h5>
		</li>
	</ul>
	</div>
</section> <!-- #page-title end -->

<!-- Content============================== === === === === === -->
<style>
.fancy-title.title-dotted-border {
	background: url({{ asset('img/dotted.png') }}) center repeat-x;
}
a.review-btn {
	min-width: 200px
}
a.review-active {
	background-color: #004d9a !important;
	color: #FFF !important;
	border-color: transparent !important;
	text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2) !important;
}
@media (max-width: 767px) {
.star-rating .caption {
	display: none;
}
a.review-btn {
	min-width: 100px
}
}
</style>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="nomargin clearfix col-lg-10 divcenter">
				<div class="panel panel-default nobottommargin">
					<div class="panel-body">
					<form id="create_product_form" name="create_product_form" class="nobottommargin row" action="{{ route('Post Product Review') }}" method="post">
						<div class="center bottommargin-sm">
							<h2 class="bottommargin-xs">@lang('review.Which would you like to review')</h2>
							<div class="button button-desc button-blue button-border button-rounded center review-btn">
								<div class="nomargin"><input id="category_product" class="checkbox-style category-label" name="category_label" type="radio" value="product"><label for="category_product" class="checkbox-style-3-label"></label>
								</div><span>@lang('review.Review Product')</span>
							</div>
							<div class="button button-desc button-blue button-border button-rounded center review-btn">
								<div class="nomargin"><input id="category_business" class="checkbox-style category-label" name="category_label" type="radio" value="service"><label for="category_business" class="checkbox-style-3-label"></label>
								</div><span>@lang('review.Review Service')</span>
							</div>
							<div class="button button-desc button-blue button-border button-rounded center" style="min-width: 200px">
								<div class="nomargin"><input id="category_movie" class="checkbox-style category-label" name="category_label" type="radio" value="movie"><label for="category_movie" class="checkbox-style-3-label"></label>
								</div><span>@lang('review.Review Movie')</span>
							</div>
						</div>
						<div class="col_full divcenter bottommargin-sm">
							<div class="fancy-title title-dotted-border title-center">
								<h3><span id='review_header'>@lang('review.Product Details')</span></h3>
							</div>
							@if(Session::get('errors')||count( $errors ) > 0)
								<div class="alert alert-danger">
									@foreach ($errors->all() as $error)
										<i class="icon-remove-sign"></i> {{ $error }} </br>
									@endforeach
								</div>
							@endif

							<div class="row">
								<div class="col-md-12">
								{{ csrf_field() }}
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label id="lbl_name" for="register-firstname">@lang('review.Product Name'):</label>
										<input type="text" id="product_name" name="product_name" value="{{ old('product_name')}}" class="sm-form-control">
									</div>
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs" id='category_sel'>
										<label id="lbl_category" for="category">@lang('review.Product Category'): </label>
										<select class="selectpicker required" name="category" id='category'>
											<option value="">-</option>
										</select>
										<span></span>
									</div>
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs hidden" id='category_input'>
										<input type="text" id="model" name="new_category" value="" class="sm-form-control">
									</div>
									<!-- <div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<input type="checkbox" id='newBrands' name='new_brand'> Add New Brand
									</div> -->
									<div id='brand_opt'>
										<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs" id='brand_sel'>
											<label id="lbl_brand" for="brand">@lang('review.Product Brand'):</label>
											<select class="selectpicker" name="brand" id="brand">
												<option value="0">-</option>
												<option value="-1">-- @lang("review.Add New Brand") --</option>
												@foreach($arrBrands as $key => $brands)
													<option value="{{$key}}">{{$brands}}</option>
												@endforeach
											</select>
										</div>
										<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs hidden" id='brand_input'>
											<input type="text" id="model" name="new_brand" value="{{ old('new_brand')}}" class="sm-form-control">
										</div>
									</div>
									<div id='model_opt'>
										<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs" id= 'model_input'>
											<label id="lbl_model" for="model">@lang('review.Product Model'):</label>
											<input type="text" id="model" name="model" value="{{ old('model')}}" class="sm-form-control">
										</div>
									</div>
									
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="website">@lang('review.Brand Website'):</label>
										<input type="text" id="website" name="website" value="{{ old('website')}}" class="sm-form-control">
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4 com-xs-8 divcenter topmargin-sm bottommargin">
				<button class="button button-3d button-circle button-blue button-large nomargin btn-block nott" id="create_product_form_submit" name="create_product_form_submit" value="create">@lang('review.Next Step') <i class="icon-line-arrow-right"></i></button>
			</div>
			</form>
		</div>
	</div>
</section>
@endsection
@section('tailJs')
    <script>
		var productList = JSON.parse('{!!$strProductList!!}');
		var serviceList = JSON.parse('{!!$strServiceList!!}');
		var movieList = JSON.parse('{!!$strMovieList!!}');
		$(document).ready(function(){
			$('#category').prop('disabled', true);
			$("#create_product_form").submit(function(){
				if($("#create_product_form").valid()){
					this.submit();
				} else {
					return false;
				}
			});
			var validator = $("#create_product_form").validate({
				onsubmit: true,
				rules: {
					product_name: "required",
					category: "required",
					category_label: "required"
				},
				messages:{
					product_name: "Please fill in the product name.",
					category:"Please choose one of the product category",
					category_label:"Please choose one type of review"
				}
			});

			$(".category-label").on('click',function(){
				var $this = $(this);
				$('.button').removeClass('review-type-active')
				switch(this.value){
					case 'product':
						bindCategoriesList(productList);
						$this.parents('.button').addClass('review-type-active');
						$('#review_header').html('@lang('review.Product Details')');
						$('#lbl_name').html('@lang('review.Product Name')');
						$('#lbl_category').html('@lang('review.Product Category')');
						$('#lbl_brand').html('@lang('review.Product Brand')');
						$('#lbl_model').html('@lang('review.Product Model')');
						$('#brand_opt').removeClass('hidden');
						$('#model_opt').removeClass('hidden');
					break;
					case 'service':
						bindCategoriesList(serviceList);
						$this.parents('.button').addClass('review-type-active');
						$('#review_header').html('@lang('review.Service Details')');
						$('#lbl_name').html('@lang('review.Service Name')');
						$('#lbl_category').html('@lang('review.Service Category')');
						$('#lbl_brand').html('@lang('review.Service Brand')');
						$('#lbl_model').html('@lang('review.Service Model')');
						$('#brand_opt').removeClass('hidden');
						$('#model_opt').removeClass('hidden');
					break;
					case 'movie':
						bindCategoriesList(movieList);
						$this.parents('.button').addClass('review-type-active');
						$('#review_header').html('@lang('review.Movie Details')');
						$('#lbl_name').html('@lang('review.Movie Name')');
						$('#lbl_category').html('@lang('review.Movie Category')');
						$('#lbl_brand').html('@lang('review.Movie Brand')');
						$('#lbl_model').html('@lang('review.Movie Model')');
						$('#brand_opt').addClass('hidden');
						$('#model_opt').addClass('hidden');
					break;
					default:
						bindCategoriesList(array());
						$this.parents('.button').addClass('review-type-active');
						$('#review_header').html("@lang('review.Product Details')");
						$('#lbl_name').html('@lang('review.Product Name')');
						$('#lbl_category').html('@lang('review.Product Category')');
						$('#lbl_brand').html('@lang('review.Product Brand')');
						$('#lbl_model').html('@lang('review.Product Model')');
						$('#brand_input').removeClass('hidden');
				}
			});


			// Prefill old data
			let category_label = "{{old('category_label')}}";
			let brand = "{{old('brand')}}";
			let category = "{{old('category')}}";
			
			if(category_label !== "" && category_label != 'null' && category_label != null) {
				switch(category_label)	{
					case 'product' :
						document.getElementById("category_product").click();
						break;
					case 'service' :
						document.getElementById("category_business").click();
						break;
					case 'movie' :
						document.getElementById("category_movie").click();
						break;
				}
			}
			if(brand !== "" && brand != 'null' && brand != null) {
				$('#brand option[value='+brand+']').attr('selected','selected');
			}
			if(category !== "" && category != 'null' && category != null) {
				$('#category option[value='+category+']').attr('selected','selected');
			}
			$('.selectpicker').selectpicker('refresh');
		});
		function bindCategoriesList(jsonList) {
			// Reset
			$('#category').find('option')
				.remove()
				.end()
				.append('<option value="0">--</option>')
				.append('<option value="-1">Add New Category</option>')
			var i = 0;
			
			for(var i=0; i < Object.keys(jsonList).length;i++){
				$('#category').append('<option value="'+jsonList[i].categories_id+'">'+jsonList[i].categories_name+'</option>');
			}
			$('#category').prop('disabled',false);
			$('.selectpicker').selectpicker('refresh');
		}
		
		$('#newBrands').on('change',function() {
			if($(this).is(':checked')){
				$('#brand_input').removeClass('hidden');
				$('#brand_sel').addClass('hidden');
			} else {
				$('#brand_input').addClass('hidden');
				$('#brand_sel').removeClass('hidden');
			}
		})
		$('#brand').on('change',function(){
			var choiceValue = $('#brand').find(":selected").val();
			if(choiceValue < 0){
				$('#brand_input').removeClass('hidden');
			} else {
				$('#brand_input').addClass('hidden');
			}
		});
		$('#category').on('change',function(){
			var choiceValue = $('#category').find(":selected").val();
			if(choiceValue < 0){
				$('#category_input').removeClass('hidden');
			} else {
				$('#category_input').addClass('hidden');
			}
		});
		
	</script>
	<script src="{{ asset('js/lib/jquery.validate.min.js') }}"></script>
	<script src="{{ asset('js/sweetalert.min.js') }}"></script>
@endsection