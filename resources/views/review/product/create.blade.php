@extends( 'layouts.app' )

@section( 'content' )

<!-- Page Title=========================== === === === === === === -->
<section id="page-title" class="page-title-mini hidden-xs">
	<div id="processTabs">
	<ul class="process-steps process-3 nomargin clearfix">
		<li class="active">
			<a href="" class="i-circled i-bordered i-alt divcenter">1</a>
			<h5 class="nomargin">Fill In Product Details</h5>
		</li>
		<li>
			<a href="" class="i-circled i-bordered i-alt divcenter">2</a>
			<h5 class="nomargin">Review Detail</h5>
		</li>
		<li>
			<a href="" class="i-circled i-bordered i-alt divcenter"><i class="icon-line-check"></i></a>
			<h5 class="nomargin">Complete Review</h5>
		</li>
	</ul>
	</div>
</section> <!-- #page-title end -->

<!-- Content============================== === === === === === -->
<style>
.fancy-title.title-dotted-border {
	background: url({{ asset('img/dotted.png') }}) center repeat-x;
}
a.review-btn {
	min-width: 200px
}
a.review-active {
	background-color: #004d9a !important;
	color: #FFF !important;
	border-color: transparent !important;
	text-shadow: 1px 1px 1px rgba(0, 0, 0, 0.2) !important;
}
@media (max-width: 767px) {
.star-rating .caption {
	display: none;
}
a.review-btn {
	min-width: 100px
}
}
</style>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="nomargin clearfix col-lg-10 divcenter">
				<div class="panel panel-default nobottommargin">
					<div class="panel-body">
						<div class="col-md-2 col-sm-3 col-xs-5 divcenter bottommargin-sm">
							<img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
						</div>
						<div class="fancy-title title-dotted-border title-center">
						@if(!empty($objProductDetail))
							<h3> {{$objProductDetail->product_service_title}} - <span>Fill in details</span></h3>
						@else
							<h3><span>Fill in details</span></h3>
						@endif
						</div>
						@if(Session::get('errors')||count( $errors ) > 0)
							<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<i class="icon-remove-sign"></i> {{ $error }} </br>
							@endforeach
							</div>
						@endif
						<div class="row">
							<div class="col-md-6">
								@if(empty($objProductDetail))
									<form id="register-form" name="register-form" class="nobottommargin row" action="" method="post">
										<input type="hidden" name="_token" value={{ csrf_token() }}>
										<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
											<label for="product_name">Product Name:</label>
											<input type="text" id="product_name" name="product_name" value="" class="sm-form-control">
										</div>
										<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
											<label for="product_category">Product Category: </label>
											<select class="selectpicker" id="product_category" name="product_category">
												<option>-</option>
												<option>-</option>
											</select>
										</div>
										<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
											<label for="product_brand">Product Brand:</label>
											<select class="selectpicker" id="product_brand" name="product_brand">
												<option>-</option>
												<option>-</option>
											</select>
										</div>
										<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
											<label for="product_model">Product Model:</label>
											<select class="selectpicker" id ="product_model" name="product_model">
												<option>-</option>
												<option>-</option>
											</select>
										</div>
										<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
											<label for="product_website">Brand Website:</label>
											<input type="text" id="product_name" name="product_name" value="" class="sm-form-control">
										</div>
									</form>
								@else
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="product_name">Product Name:</label>
										<label class="sm-form-control" disabled>{{$objProductDetail->product_service_title}}</label>
									</div>
									@if(isset($objProductDetail->categories_description))
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="product_category">Product Category: </label>
										<label class="sm-form-control" disabled>{{$objProductDetail->categories_description}}</label>
									</div>
									@endif
									@if(isset($objProductDetail->brand))
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="product_brand">Product Brand:</label>
										<label class="sm-form-control" disabled>{{$objProductDetail->brand}}</label>
									</div>
									@endif
									@if(isset($objProductDetail->model))
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="product_model">Product Model:</label>
										<label class="sm-form-control" disabled>{{$objProductDetail->model}}</label>
									</div>
									@endif
									@if(isset($objProductDetail->website))
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="product_website">Brand Website:</label>
										<label class="sm-form-control" disabled>{{$objProductDetail->website}}</label>
									</div>
									@endif
								@endif
							</div>
							<div class="col-md-6">
								<form id="register-form" name="register-form" class="nobottommargin" action="{{route('Post Product Review',[""])}}" method="post">
									<input type="hidden" name="_token" value={{ csrf_token() }}>
									<div class="col_full">
										<label for="register-firstname">Title Of Your Review:</label>
										<input type="text" id="" name="" value="" class="sm-form-control" placeholder="Review Title">
									</div>
									<hr>
									<div class="col_full">
										<label for="text">Rate On Relevant Points: </label>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">Reliability</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" max="5" data-stars="5" data-step="1" data-size="sm">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">User Friendliness</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" min="1" max="5" data-size="sm">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">Standards</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" min="1" max="5" data-size="sm">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">Professionalism</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" min="1" max="5" data-size="sm">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">Price</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" min="1" max="5" data-size="sm">
											</div>
										</div>
									</div>
								
							</div>
							<hr>
							<div class="col-xs-12">
								<label for="text">Detailed Reviews:</label>
								<textarea class="form-control" id="" rows="9"></textarea>
								<!--Default show below details-->
								<small class="form-text text-muted">30 words minimum</small>
								<!--When typing show below details-->
								<!--
								<small class="form-text text-muted">1 words (30 words minimum)</small>
								-->
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4 com-xs-8 divcenter topmargin-sm bottommargin">
				<button class="button button-3d button-circle button-blue button-large nomargin btn-block nott" id="login-form-submit" name="login-form-submit" value="login">Next Step <i class="icon-line-arrow-right"></i></button>
			</div>
		</div>
	</div>
</section>


<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="nomargin clearfix col-lg-10 divcenter">
				<div class="panel panel-default nobottommargin">
					<div class="panel-body">
						<div class="center bottommargin-sm">
							<h2 class="bottommargin-xs">Which would u like to create for review</h2>
							<a href="#" class="button button-desc button-blue button-border button-rounded center review-btn review-active">
								<div class="nomargin"><input id="checkbox-7" class="checkbox-style" name="checkbox-7" type="checkbox" checked=""><label for="checkbox-7" class="checkbox-style-3-label"></label>
								</div><span>Review Product</span>
							</a>
							<a href="#" class="button button-desc button-blue button-border button-rounded center review-btn">
								<div class="nomargin"><input id="checkbox-7" class="checkbox-style" name="checkbox-7" type="checkbox"><label for="checkbox-7" class="checkbox-style-3-label"></label>
								</div><span>Review Business</span>
							</a>
							<!--
				<a href="#" class="button button-desc button-blue button-border button-rounded center" style="min-width: 200px">
					<div class="nomargin"><input id="checkbox-7" class="checkbox-style" name="checkbox-7" type="checkbox"><label for="checkbox-7" class="checkbox-style-3-label"></label>
					</div><span>Review Movie</span>
				</a>
					-->
						</div>
						<div class="fancy-title title-dotted-border title-center">
							<h3><span>Fill in details</span></h3>
						</div>
						<div class="alert alert-danger">
							<i class="icon-remove-sign"></i> The firstname field is required when lastname is not present.
						</div>
						<div class="row">
							<div class="col-md-6">
								<form id="register-form" name="register-form" class="nobottommargin row" action="" method="post">
									<input type="hidden" name="_token">
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="register-firstname">Product Name:</label>
										<input type="text" id="" name="" value="" class="sm-form-control">
									</div>
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="password">Product Category: </label>
										<select class="selectpicker" name="country">
											<option>-</option>
											<option>-</option>
										</select>
									</div>
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="password_confirmation">Product Brand:</label>
										<select class="selectpicker" name="country">
											<option>-</option>
											<option>-</option>
										</select>
									</div>
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="register-form-mobile">Product Model:</label>
										<select class="selectpicker" name="country">
											<option>-</option>
											<option>-</option>
										</select>
									</div>
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="register-form-mobile">Brand Website:</label>
										<input type="text" id="" name="" value="" class="sm-form-control">
									</div>
									<div class="col-md-12 col-sm-6 col-xs-12 bottommargin-xs">
										<label for="register-firstname">Title Of Your Review:</label>
										<input type="text" id="" name="" value="" class="sm-form-control" placeholder="Review Title">
									</div>
								</form>
							</div>
							<div class="col-md-6">
								<form id="register-form" name="register-form" class="nobottommargin" action="" method="post">
									<input type="hidden" name="_token">
									<div class="col_full">
										<label for="text">Rate On Relevant Points: </label>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">Reliability</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" max="5" data-stars="5" data-step="1" data-size="sm">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">User Friendliness</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" min="1" max="5" data-size="sm">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">Standards</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" min="1" max="5" data-size="sm">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">Professionalism</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" min="1" max="5" data-size="sm">
											</div>
										</div>
										<div class="row">
											<div class="col-xs-4">
												<p style="margin-bottom: 0px;margin-top: 15px;line-height: normal;">Price</p>
											</div>
											<div class="col-xs-7">
												<input id="input-7" type="number" class="rating" min="1" max="5" data-size="sm">
											</div>
										</div>
									</div>
									<hr>
									<div class="col_full">
										<label for="text">Detailed Reviews:</label>
										<textarea class="form-control" id="" rows="9"></textarea>
										<!--Default show below details-->
										<small class="form-text text-muted">30 words minimum</small>
										<!--When typing show below details-->
										<!--
								<small class="form-text text-muted">1 words (30 words minimum)</small>
								-->
									</div>

								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3 col-sm-4 com-xs-8 divcenter topmargin-sm bottommargin">
				<button class="button button-3d button-circle button-blue button-large nomargin btn-block nott" id="login-form-submit" name="login-form-submit" value="login">Next Step <i class="icon-line-arrow-right"></i></button>
			</div>

		</div>
	</div>
</section>

@endsection