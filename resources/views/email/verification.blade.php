@extends('layouts.app')

@section('content')

<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page"><strong>{{ ucwords(Route::currentRouteName()) }}</strong>
			</li>
		</ol>
	</div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="col-md-8 divcenter nobottommargin clearfix">
				<h1 class="center">Registration Confirmed</h1>
				<div class="panel panel-default nobottommargin">
					<div class="panel-body text-center">
						<i class="icon-email" style="font-size: 90px;opacity: 0.3;"></i>
						<br>
						<p>You have successfully registered your account, an email is sent to you for verification. Click <a href="{{url('/login')}}" class="border_bottom_dotted">here</a> to Login.</p>
					</div>
				</div>			
			</div>
		</div>
	</div>
</section><!-- #content end -->
@endsection