@extends('layouts.app')

@section('content')

<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">Home</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page"><strong>{{ ucwords(Route::currentRouteName()) }}</strong>
			</li>
		</ol>
	</div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="col-md-8 divcenter nobottommargin clearfix">
				<h1 class="center">Account Verification Completed</h1>
				<div class="panel panel-default nobottommargin">
					<div class="panel-body text-center">
						<i class="icon-line-circle-check" style="font-size: 90px;opacity: 0.3;"></i>
						<br>
						<p>Your Email is successfully verified. Click <a href="{{url('/login')}}" class="border_bottom_dotted">here</a> to login.</p>
					</div>
				</div>			
			</div>
		</div>
	</div>
</section><!-- #content end -->
@endsection