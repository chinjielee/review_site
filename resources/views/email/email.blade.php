<h1>{{ config('app.name', 'WIKÅBÖ') }} Account Activation</h1>
Dear {{ $name}},
<br><br>
Please click <a href="{{url('/verifyemail/'.$email_token)}}"> here</a> to verify your account.
<br><br>
Regards,
<br><br>
{{ config('app.name', 'WIKÅBÖ') }} Team