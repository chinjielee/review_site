@extends('layouts.app')

@section('content')

@include('profile.navbar')

<style>
	a.list-group-item.active_page {
		color: #000;
		font-weight: bold;
		background-color: #f5f5f5;
		border-color: #dddddd;
	}
	.entry-meta li:before {
		content: '--';
	}
	.media-body h3 {
		margin-top: 5px !important;
	}
	.profile_action a.button {
		float: right;
		margin-top: 0px !important;
	}
	@media (max-width: 1199px) {
		.media-body h3 {
			margin-top: 5px !important;
			font-size: 18px;
		}
	}
	
	@media (max-width: 767px) {
		.media-body h3 {
			margin-top: 5px !important;
		}
	.profile_action a.button {
		float: none;
		margin-top: 15px !important;
	}
	}
</style>

<!-- Content === === === === === === === === === === === === === === === -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row clearfix">
				@include( 'profile.slidebar' )
				<div class="col-md-9 col-sm-12">
					<div class="fancy-title title-bottom-border">
						<h3>{{ Route::currentRouteName() }}</h2>
					</div>
					<div class="row clearfix">
						<div class="col-md-8">
							<img src="{{ url('/images/user/'.Auth::user()->avatar) }}" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" style="max-width: 84px;">
							<div class="heading-block noborder nomargin">
								<h4 class="nomargin nott" style="padding-top: 5px;">{{ Auth::user()->fullname }} <small>({{ Auth::user()->country }})</small></h4>
								<small>{{ Auth::user()->updated_at }}</small>
							</div>
						</div>
						<div class="col-md-4 profile_action">
							<a href="/profile/my-account" class="button button-border button-rounded button-fill fill-from-bottom button-small button-blue nott nomargin"><span>@lang('profile.Edit Profile')</span></a>
						</div>
					</div>
					<div class="clear"></div>
					<div class="divider notopmargin"></div>
					<div class="fancy-title title-double-border">
						<h4>@lang('profile.Summary Review')</h4>
					</div>
					<div class="row clearfix">
						<div class="col-md-4 col-sm-4">
							<div class="card">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2 text-center bg-primary bg-darken-2">
											<i class="icon-line2-star font-large-2"></i>
										</div>
										<div class="p-2 bg-gradient-x-primary white media-body">
											<h3 class="white nomargin">@lang('profile.Reviews')</h3>
											<h4 class="white nomargin">{{$review_count}}</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="card">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2 text-center bg-primary bg-darken-2">
											<i class="icon-line2-note white font-large-2"></i>
										</div>
										<div class="p-2 bg-gradient-x-primary white media-body">
											<h3 class="white nomargin">@lang('profile.Comments')</h3>
											<h4 class="white nomargin">{{$comment_count}}</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4 col-sm-4">
							<div class="card">
								<div class="card-content">
									<div class="media align-items-stretch">
										<div class="p-2 text-center bg-primary bg-darken-2">
											<i class="icon-line2-speech white font-large-2"></i>
										</div>
										<div class="p-2 bg-gradient-x-primary white media-body">
											<h3 class="white nomargin">@lang('profile.Questions')</h3>
											<h4 class="white nomargin">{{$question_count}}</h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="divider notopmargin"></div>
					<div class="fancy-title title-double-border">
						<h4>@lang('profile.Recently Viewed')</h4>
					</div>
					<div class="row clearfix">
						<div class="col-sm-4">
							@if(isset($arrProductService[0]))
							<div class="spost clearfix">
								<div class="entry-image">
									<a href="#"><img src="{{$arrProductService[0]->images}}" alt="Image"></a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h4><a href="{{$arrProductService[0]->service_url}}">{{$arrProductService[0]->product_service_title}}</a></h4>
									</div>
									<ul class="entry-meta">
										<li style="color: #fde16d;">
											@for ($i = 0; $i < floor($arrProductService[0]->rating); $i++)
												<i class="icon-star3 nomargin"></i>
											@endfor
											@if(fmod($arrProductService[0]->rating,1) != 0)
											<i class="icon-star-half-full"></i>
											@endif
										</li>
										<li class="color">{{number_format($arrProductService[0]->rating, 2, '.', ',')}}</li>
									</ul>
								</div>
							</div>
							@endif
							@if(isset($arrProductService[3]))
							<div class="spost clearfix">
								<div class="entry-image">
									<a href="#"><img src="{{$arrProductService[3]->images}}" alt="Image"></a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h4><a href="{{$arrProductService[3]->service_url}}">{{$arrProductService[3]->product_service_title}}</a></h4>
									</div>
									<ul class="entry-meta">
										<li style="color: #fde16d;">
											@for ($i = 0; $i < floor($arrProductService[3]->rating); $i++)
												<i class="icon-star3 nomargin"></i>
											@endfor
											@if(fmod($arrProductService[3]->rating,1) != 0)
											<i class="icon-star-half-full"></i>
											@endif
										</li>
										<li class="color">{{number_format($arrProductService[3]->rating, 2, '.', ',')}}</li>
									</ul>
								</div>
							</div>
							@endif
						</div>
						<div class="clearfix visible-xs" style="margin-top: 20px; padding-top: 20px; border-top: 1px dashed #E5E5E5;"></div>
						<div class="col-sm-4">
							@if(isset($arrProductService[1]))
							<div class="spost clearfix">
								<div class="entry-image">
									<a href="#"><img src="{{$arrProductService[1]->images}}" alt="Image"></a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h4><a href="{{$arrProductService[1]->service_url}}">{{$arrProductService[1]->product_service_title}}</a></h4>
									</div>
									<ul class="entry-meta">
										<li style="color: #fde16d;">
											@for ($i = 0; $i < floor($arrProductService[1]->rating); $i++)
												<i class="icon-star3 nomargin"></i>
											@endfor
											@if(fmod($arrProductService[1]->rating,1) != 0)
												<i class="icon-star-half-full"></i>
											@endif
										</li>
										<li class="color">{{number_format($arrProductService[1]->rating, 2, '.', ',')}}</li>
									</ul>
								</div>
							</div>
							@endif
							@if(isset($arrProductService[4]))
							<div class="spost clearfix">
								<div class="entry-image">
									<a href="#"><img src="{{$arrProductService[4]->images}}" alt="Image"></a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h4><a href="{{$arrProductService[4]->service_url}}">{{$arrProductService[4]->product_service_title}}</a></h4>
									</div>
									<ul class="entry-meta">
										<li style="color: #fde16d;">
											@for ($i = 0; $i < floor($arrProductService[4]->rating); $i++)
												<i class="icon-star3 nomargin"></i>
											@endfor
											@if(fmod($arrProductService[4]->rating,1) != 0)
											<i class="icon-star-half-full"></i>
											@endif
										</li>	
										<li class="color">{{number_format($arrProductService[4]->rating, 2, '.', ',')}}</li>
									</ul>
								</div>
							</div>
							@endif
						</div>
						<div class="clearfix visible-xs" style="margin-top: 20px; padding-top: 20px; border-top: 1px dashed #E5E5E5;"></div>
						<div class="col-sm-4">
							@if(isset($arrProductService[2]))
							<div class="spost clearfix">
								<div class="entry-image">
									<a href="#"><img src="{{$arrProductService[2]->images}}" alt="Image"></a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h4><a href="{{$arrProductService[2]->service_url}}">{{$arrProductService[2]->product_service_title}}</a></h4>
									</div>
									<ul class="entry-meta">
										<li style="color: #fde16d;">
											@for ($i = 0; $i < floor($arrProductService[2]->rating); $i++)
												<i class="icon-star3 nomargin"></i>
											@endfor
											@if(fmod($arrProductService[2]->rating,1) != 0)
											<i class="icon-star-half-full"></i>
											@endif
										</li>
										<li class="color">{{number_format($arrProductService[2]->rating, 2, '.', ',')}}</li>
									</ul>
								</div>
							</div>
							@endif
							@if(isset($arrProductService[5]))
							<div class="spost clearfix">
								<div class="entry-image">
									<a href="#"><img src="{{$arrProductService[5]->images}}" alt="Image"></a>
								</div>
								<div class="entry-c">
									<div class="entry-title">
										<h4><a href="{{$arrProductService[5]->service_url}}">{{$arrProductService[5]->product_service_title}}</a></h4>
									</div>
									<ul class="entry-meta">
										<li style="color: #fde16d;">
											@for ($i = 0; $i < floor($arrProductService[5]->rating); $i++)
												<i class="icon-star3 nomargin"></i>
											@endfor
											@if(fmod($arrProductService[5]->rating,1) != 0)
											<i class="icon-star-half-full"></i>
											@endif
										</li>
										<li class="color">{{number_format($arrProductService[5]->rating, 2, '.', ',')}}</li>
									</ul>
								</div>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- #content end -->
@endsection