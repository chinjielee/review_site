@extends('layouts.app')

@section('content')

@include('profile.navbar')

<style>
	a.list-group-item.active_page {
		color: #000;
		font-weight: bold;
		background-color: #f5f5f5;
		border-color: #dddddd;
	}
	
	.entry-meta {
		margin: 10px 0 15px 0;
		list-style: none;
		clear: both;
	}
	
	.entry-meta li:before {
		content: '--';
	}
	
	.media-body h3 {
		margin-top: 5px !important;
	}
	
	.profile_action a.button {
		float: right;
		margin-top: 0px !important;
	}
	
	.text-truncate {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
		display: block;
	}
	
	.details_url {
		max-width: 280px;
	}
	
	.entry-meta li i {
		font-size: 18px;
	}
	
	.table> tbody> tr> td {
		vertical-align: middle;
	}
	
	.review_rating_xs {
		clear: both;
		padding-top: 5px;
	}
	
	@media (max-width: 1199px) {
		.media-body h3 {
			margin-top: 5px !important;
			font-size: 18px;
		}
		.details_url {
			max-width: 200px;
		}
		.i-small.i-rounded,
		.i-small.i-circled,
		.i-small.i-bordered {
			margin: 4px 4px 4px 0;
		}
		code {
			font-size: 70%;
		}
	}
	
	@media (max-width: 767px) {
		.media-body h3 {
			margin-top: 5px !important;
		}
		.profile_action a.button {
			float: none;
			margin-top: 15px !important;
		}
		.details_url {
			max-width: 150px;
		}
		.text-truncate {
			overflow: hidden;
			display: -webkit-box;
			-webkit-box-orient: vertical;
			text-overflow: ellipsis;
			-webkit-line-clamp: 2;
			white-space: normal;
		}
	}
</style>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row clearfix">
				@include( 'profile.slidebar' )
				<div class="col-md-9 col-sm-12">
					<div class="fancy-title title-bottom-border">
						<h3>{{ Route::currentRouteName() }}</h3>
					</div>
					@if($noData == true)
					<div class="center clearfix" style="opacity: 0.3;">
						<i class="icon-line2-doc" style="font-size: 90px;"></i>
						<h1 class="nobottommargin">@lang('profile.No Comments Found')</h1>
						<h3>@lang('profile.You have not posted any comments yet')</h3>
					</div>
					@else
					<table class="table table-hover">
						<thead>
							<tr>
								<th class="hidden-sm hidden-xs">@lang('profile.Date')</th>
								<th>@lang('profile.Status')</th>
								<th class="hidden-xs hidden-sm">@lang('profile.Product/Service')</th>
								<th>@lang('profile.Comments')</th>
								<th class="hidden-xs">@lang('profile.Action')</th>
							</tr>
						</thead>
						<tbody>
							@foreach($arrProductAnswer as $ProductAnswer)
							<tr>
								<td class="hidden-sm hidden-xs"><code>{{$ProductAnswer->date}}</code>
								</td>
								@if($ProductAnswer->status == "1")
								<td><span class="badge badge-success">@lang('profile.Published')</span>
								@elseif($ProductAnswer->status == "2")
								<td><span class="badge badge-info">@lang('profile.Removed')</span>
								@else
								<td><span class="badge badge-danger">@lang('profile.Blocked')</span>
								@endif
								<code class="visible-sm visible-xs">{{$ProductAnswer->date}}</code>
								</td>
								<td class="hidden-xs hidden-sm">
									<a href="{{$ProductAnswer->service_url}}" class="text-truncate">{{$ProductAnswer->product_service_title}}</a>
								</td>
								<td class="details_url">
									<a href="{{$ProductAnswer->question_url}}" class="text-truncate">{{$ProductAnswer->answer}}</a>
									<div class="visible-xs review_rating_xs">
										<a href="{{$ProductAnswer->question_url}}"><i class="i-rounded i-bordered i-small icon-line-eye" data-toggle="tooltip" data-placement="top" title="@lang('profile.View')"></i></a> 
										<!-- <a href=""><i class="i-rounded i-bordered i-small icon-line2-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>  -->
										@if($ProductAnswer->status == "1")
										<a href="#" onclick="confirmation('{{$ProductAnswer->key}}')"><i class="i-rounded i-bordered i-small icon-line-trash" data-toggle="tooltip" data-placement="top" title="@lang('profile.Delete')"></i></a>
										@endif
									</div>
								</td>
								<td class="hidden-xs">
									<div>
										<a href="{{$ProductAnswer->question_url}}"><i class="i-rounded i-bordered i-small icon-line-eye" data-toggle="tooltip" data-placement="top" title="@lang('profile.View')"></i></a> 
										<!-- <a href=""><i class="i-rounded i-bordered i-small icon-line2-pencil" data-toggle="tooltip" data-placement="top" title="Edit"></i></a>  -->
										@if($ProductAnswer->status == "1")
										<a href="#" onclick="confirmation('{{$ProductAnswer->key}}')"><i class="i-rounded i-bordered i-small icon-line-trash" data-toggle="tooltip" data-placement="top" title="@lang('profile.Delete')"></i></a>
										@endif
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					@endif
					<div class="divider notopmargin"></div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section( 'tailJs' )
<script>
function confirmation(key){
	if(key){
		swal({
		    type: 'warning',
		    title: '@lang('profile.Delete Confirmation')',
		    text: '@lang('profile.Are you sure you want to remove this posted comment?')',
		    showCancelButton: true,
		    confirmButtonText: '@lang('profile.Yes, I am sure')',
		    cancelButtonText: "@lang('profile.Cancel')"
		}).then(function(isConfirm) {
			console.log(isConfirm);
	      if(isConfirm.value===true){
	        $.ajax({
            	type: "POST",
            	dataType : 'json', 
	            url: "{{route('Comment Update')}}",
	            data: {
	            	"_token": "{{ csrf_token() }}",
	            	"key" : key
	            },
	            success: function( respond ) {
	                $("#ajaxResponse").append(respond);
			        swal({
			        	type: 'info',
			         	title: 'Remove '+respond+"!"
			        }).then(function(){
			       		location.reload();
			        });
	            }
	        });
	      }
	    });
	}
}
</script>
<script src="{{ asset('js/sweetalert.min.js') }}"></script>
@yield( 'tailJs' )