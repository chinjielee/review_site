@extends('layouts.app')

@section('content')

@include('profile.navbar')

<style>
	a.list-group-item.active_page {
		color: #000;
		font-weight: bold;
		background-color: #f5f5f5;
		border-color: #dddddd;
	}
	
	.entry-meta {
		margin: 10px 0 15px 0;
		list-style: none;
		clear: both;
	}
	
	.entry-meta li:before {
		content: '--';
	}
	
	.media-body h3 {
		margin-top: 5px !important;
	}
	
	.profile_action a.button {
		float: right;
		margin-top: 0px !important;
	}
	
	.text-truncate {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
		display: block;
	}
	
	.details_url {
		max-width: 280px;
	}
	
	.entry-meta li i {
		font-size: 18px;
	}
	
	.table> tbody> tr> td {
		vertical-align: middle;
	}
	
	.review_rating_xs {
		clear: both;
		padding-top: 5px;
	}
	.notification_details_wrapper p {
		display: inline-block;
	}
	.notification_details_wrapper small {
		opacity: 0.5;
		float: right;
	}
	.pagination-custom{
		text-align:right !important;
	}

	.pagination-custom .pagination {
		margin-top:0;
	}

	@media (max-width: 1199px) {
		.media-body h3 {
			margin-top: 5px !important;
			font-size: 18px;
		}
		.details_url {
			max-width: 200px;
		}
		.i-small.i-rounded,
		.i-small.i-circled,
		.i-small.i-bordered {
			margin: 4px 4px 4px 0;
		}
		code {
			font-size: 70%;
		}
	}
	
	@media (max-width: 767px) {
		.media-body h3 {
			margin-top: 5px !important;
		}
		.profile_action a.button {
			float: none;
			margin-top: 15px !important;
		}
		.details_url {
			max-width: 150px;
		}
		.text-truncate {
			overflow: hidden;
			display: -webkit-box;
			-webkit-box-orient: vertical;
			text-overflow: ellipsis;
			-webkit-line-clamp: 2;
			white-space: normal;
		}
	.notification_details_wrapper small {
		opacity: 0.5;
		float: none;
	}
	}
</style>

<section id="content" style="margin-bottom: 0px;">

	<div class="content-wrap">

		<div class="container clearfix">
			@include( 'profile.slidebar' )
			<div class="col-md-9 col-sm-12">
				<div class="fancy-title title-bottom-border">
					<h3 class="bottommargin-xs">Your Notification 
					<small><a href="{{route('Notification Setting')}}" class="pull-right" style="margin-top: 10px;">Notification Setting</a></small></h3>
				</div>
				<div class="list-group">
					@foreach($arrNotification as $notification)
						@if(isset($notification->data['action_url']))
							<a href="{{$notification->data['action_url']}}" class="list-group-item list-group-item-action flex-column align-items-start notification_details_wrapper">
						@else
							<a class="list-group-item list-group-item-action flex-column align-items-start notification_details_wrapper" onclick="return false;">
						@endif
						@if($notification->type == 'App\Notifications\NotificationNewComment')
							<p class="nomargin"><i class="i-rounded i-circled i-small icon-line2-speech notopmargin nobottommargin bg-blue"></i>{{$notification->data['body']}}</p>
						@elseif($notification->type == 'App\Notifications\NotificationNewAnswer')
							<p class="nomargin"><i class="i-rounded i-circled i-small icon-line2-speech notopmargin nobottommargin bg-blue"></i>{{$notification->data['body']}}</p>
						@elseif($notification->type == 'App\Notifications\1')
							<p class="nomargin notification_details_wrapper"><i class="i-rounded i-circled i-small icon-line2-star notopmargin nobottommargin bg-blue"></i>{{$notification->data['body']}}</p>
						@elseif($notification->type == 'App\Notifications\2')
							<p class="nomargin notification_details_wrapper"><i class="i-rounded i-circled i-small icon-line2-note notopmargin nobottommargin bg-blue"></i>{{$notification->data['body']}}</p>
						@elseif($notification->type == 'App\Notifications\NotificationNewLike')
							<p class="nomargin notification_details_wrapper"><i class="i-rounded i-circled i-small icon-line2-like notopmargin nobottommargin bg-success"></i>{{$notification->data['body']}}</p>
						@elseif($notification->type == 'App\Notifications\NotificationNewDislike')
							<p class="nomargin notification_details_wrapper"><i class="i-rounded i-circled i-small icon-line2-dislike notopmargin nobottommargin bg-danger"></i>{{$notification->data['body']}}</p>								
						@elseif($notification->type == 'App\Notifications\5')
							<p class="nomargin notification_details_wrapper"><i class="i-rounded i-circled i-small icon-line-check notopmargin nobottommargin bg-success"></i>{{$notification->data['body']}}</p>
						@else
							<p class="nomargin"><i class="i-rounded i-circled i-small icon-line2-speech notopmargin nobottommargin bg-blue"></i>{{$notification->data['body']}}</p>
						@endif
							<small><strong>{{$notification->created_at->diffForHumans()}}</strong></small>
						</a>
					@endforeach				
				</div>
				@if($arrNotification->total() != 0)
				<div class="row clearfix topmargin-sm bottommargin-sm">
					<div class="col-sm-6">
						<p class="nomargin">
						<strong>{{$arrNotification->currentPage()}} of {{$arrNotification->lastPage()}} </strong> pages
					</div>
					<div class="col-sm-6 pagination-custom">
						{{$arrNotification->links()}}
					</div>
				</div>
				@else
				<div class="row clearfix topmargin-sm bottommargin-sm">
					<div class="col-sm-12">
						<p class="nomargin">
						<strong>No Notification Found. </strong>
						</p>
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</section>

@endsection