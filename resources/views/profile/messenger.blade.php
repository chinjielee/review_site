@extends( 'layouts.app' )

@section( 'content' )

@include( 'profile.navbar' )
	<style>
		.mail-inbox-select-all {
			margin: 0.83333em 2.08333em 0 0;
			font-size: 0.85714em;
			display: inline-block;
		}
		.inbox-wrapper a small {
			opacity: 0.5;
			float: right;
		}
		.inbox-wrapper tr.unread {
			font-weight: bold;
		}
		.inbox-wrapper tr.read {
			font-weight: normal;
		}
		table.inbox-wrapper tr td:first-child {
			padding-left: 0px;
		}
		.inbox-wrapper tr.read a {
			color: #000;
		}
		
		.inbox-wrapper tr.read a:hover {
			color: #0066cc;
		}
	</style>

<section id="content" style="margin-bottom: 0px;">

	<div class="content-wrap">

		<div class="container clearfix">
			@include( 'profile.slidebar' )
			<div class="col-md-9 col-sm-12">
				<div class="fancy-title title-bottom-border">
					<h3 class="bottommargin-xs">Messenger</h3>
				</div>
				<table class="mail-list table table-responsive inbox-wrapper">
					<tbody>
						@forelse($arrReturnConverDetailAll as $conversation)
						<tr class="unread">
							<td class="mail-list-user" style="width: 70px;">
							<img src="http://localhost:8000/img/avatar.png" class="avatar avatar-60 photo avatar-default" height="50" width="50" style="margin: 0;">
							</td>
							<td class="mail-list-name"><a href="{{route('Messenger Details',$conversation->conversation_id)}}">{{$conversation->last_name}} {{$conversation->first_name}}</a>
							</td>
							<td class="mail-list-time" style="width: 100px;"><small></small></td>
						</tr>
						@empty
						<div class="center clearfix" style="opacity: 0.3;">
							<i class="icon-line2-doc" style="font-size: 90px;"></i>
							<h1 class="nobottommargin">@lang('profile.No Messenger Found')</h1>
							<h3>@lang('profile.You have not involve in any messenger yet')</h3>
						</div>
						@endforelse
						
					</tbody>
				</table>
				<hr>
				<!-- <div class="row clearfix topmargin-sm bottommargin-sm">
					<div class="col-sm-6">
						<p class="nomargin">
						<strong>1 of 1 </strong> pages
					</p></div>
					<div class="col-sm-6 pagination-custom">
						
					</div>
				</div> -->
			</div>
		</div>
	</div>
</section>

@endsection