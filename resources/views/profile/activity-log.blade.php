@extends('layouts.app')

@section('content')

@include('profile.navbar')

<style>
	a.list-group-item.active_page {
		color: #000;
		font-weight: bold;
		background-color: #f5f5f5;
		border-color: #dddddd;
	}
	
	.entry-meta {
		margin: 10px 0 15px 0;
		list-style: none;
		clear: both;
	}
	
	.entry-meta li:before {
		content: '--';
	}
	
	.media-body h3 {
		margin-top: 5px !important;
	}
	
	.profile_action a.button {
		float: right;
		margin-top: 0px !important;
	}
	
	.text-truncate {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
		display: block;
	}
	
	.details_url {
		max-width: 280px;
	}
	
	.entry-meta li i {
		font-size: 18px;
	}
	
	.table> tbody> tr> td {
		vertical-align: middle;
	}
	
	.review_rating_xs {
		clear: both;
		padding-top: 5px;
	}
	
	@media (max-width: 1199px) {
		.media-body h3 {
			margin-top: 5px !important;
			font-size: 18px;
		}
		.details_url {
			max-width: 200px;
		}
		.i-small.i-rounded,
		.i-small.i-circled,
		.i-small.i-bordered {
			margin: 4px 4px 4px 0;
		}
		code {
			font-size: 70%;
		}
	}
	
	@media (max-width: 767px) {
		.media-body h3 {
			margin-top: 5px !important;
		}
		.profile_action a.button {
			float: none;
			margin-top: 15px !important;
		}
		.details_url {
			max-width: 150px;
		}
		.text-truncate {
			overflow: hidden;
			display: -webkit-box;
			-webkit-box-orient: vertical;
			text-overflow: ellipsis;
			-webkit-line-clamp: 2;
			white-space: normal;
		}
	}
</style>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row clearfix">
				@include( 'profile.slidebar' )
				<div class="col-md-9 col-sm-12">
					<div class="fancy-title title-bottom-border">
						<h3>{{ Route::currentRouteName() }}</h3>
					</div>
					<table class="table table-hover">
						<thead>
							<tr>
								<th>@lang('profile.Date')</th>
								<th class="hidden-xs">@lang('profile.Time')</th>
								<th>@lang('profile.Activity')</th>
							</tr>
						</thead>
						<tbody>
							@foreach($arrUserActivityLog as $UserActivityLog)
							<tr>
								@if($UserActivityLog->span > 1)
								<td rowspan="{{$UserActivityLog->span}}">
									<code>{{$UserActivityLog->date}}</code>
								</td>
								@elseif($UserActivityLog->span == 1)
								<td>
									<code>{{$UserActivityLog->date}}</code>
								</td>
								@else
								@endif
								<td class="hidden-xs">{{$UserActivityLog->time}}</td>
								@if($UserActivityLog->type == 1)
								<td><i class="i-rounded hidden-xs i-circled i-small icon-line-head notopmargin nobottommargin"></i> @lang('profile.Registered successfully').</td>
								@elseif($UserActivityLog->type == 2)
								<td><i class="i-rounded hidden-xs i-circled i-small icon-line-head notopmargin nobottommargin"></i> @lang('profile.Logged in to the account').</td>
								@elseif($UserActivityLog->type == 3)
								<td><i class="i-rounded hidden-xs i-circled i-small icon-line2-logout notopmargin nobottommargin"></i> @lang('profile.Logged out account').</td>
								@elseif($UserActivityLog->type == 4)
								<td><i class="i-rounded hidden-xs i-circled i-small icon-line2-like notopmargin nobottommargin bg-success"></i> @lang('profile.Voted a reviews on') {{$UserActivityLog->remark}} was helpful.</td>
								@elseif($UserActivityLog->type == 5)
								<td><i class="i-rounded hidden-xs i-circled i-small icon-line2-dislike notopmargin nobottommargin bg-danger"></i> @lang('profile.Downvote a reviews on') {{$UserActivityLog->remark}} wasn't helpful.</td>
								@elseif($UserActivityLog->type == 6)
								<td><i class="i-rounded hidden-xs i-circled i-small icon-line2-star notopmargin nobottommargin bg-blue"></i> @lang('profile.Posted a reviews on') {{$UserActivityLog->remark}}.</td>
								@elseif($UserActivityLog->type == 7)
								<td class="hidden-xs"><i class="i-rounded hidden-xs i-circled i-small icon-line2-speech notopmargin nobottommargin bg-blue"></i> @lang('profile.Posted a questions on') {{$UserActivityLog->remark}}.</td>
								@elseif($UserActivityLog->type == 8)
								<td><i class="i-rounded hidden-xs i-circled i-small icon-line2-note notopmargin nobottommargin bg-blue"></i> @lang('profile.Posted a comments on') {{$UserActivityLog->remark}}.</td>
								@endif
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- #content end -->
@endsection