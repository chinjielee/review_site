@extends('layouts.app')

@section('content')

@include('profile.navbar')

<style>
	a.list-group-item.active_page {
		color: #000;
		font-weight: bold;
		background-color: #f5f5f5;
		border-color: #dddddd;
	}
	
	.entry-meta {
		margin: 10px 0 15px 0;
		list-style: none;
		clear: both;
	}
	
	.entry-meta li:before {
		content: '--';
	}
	
	.media-body h3 {
		margin-top: 5px !important;
	}
	
	.profile_action a.button {
		float: right;
		margin-top: 0px !important;
	}
	
	.text-truncate {
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
		display: block;
	}
	
	.details_url {
		max-width: 280px;
	}
	
	.entry-meta li i {
		font-size: 18px;
	}
	
	.table> tbody> tr> td {
		vertical-align: middle;
	}
	
	.review_rating_xs {
		clear: both;
		padding-top: 5px;
	}
	
	@media (max-width: 1199px) {
		.media-body h3 {
			margin-top: 5px !important;
			font-size: 18px;
		}
		.details_url {
			max-width: 200px;
		}
		.i-small.i-rounded,
		.i-small.i-circled,
		.i-small.i-bordered {
			margin: 4px 4px 4px 0;
		}
		code {
			font-size: 70%;
		}
	}
	
	@media (max-width: 767px) {
		.media-body h3 {
			margin-top: 5px !important;
		}
		.profile_action a.button {
			float: none;
			margin-top: 15px !important;
		}
		.details_url {
			max-width: 150px;
		}
		.text-truncate {
			overflow: hidden;
			display: -webkit-box;
			-webkit-box-orient: vertical;
			text-overflow: ellipsis;
			-webkit-line-clamp: 2;
			white-space: normal;
		}
	}
</style>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row clearfix">
				@include( 'profile.slidebar' )
				<div class="col-md-9 col-sm-12">
					<div class="fancy-title title-bottom-border">
						<h3>{{ Route::currentRouteName() }}</h3>
					</div>
				 	<form id="update_notification_setting" action="{{ route('Notification Setting Update') }}" method="post">
				    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<div class="card bottommargin-xs">
							<div class="card-body card-default">
								<label>@lang('profile.Review status'):</label><br>
								<div class="row">
									<div class="col-md-12 col-6">
									<input class="bt-switch" name="notify_review" id="notify_review" type="checkbox"
									@if(!empty($arrNotificationSetting))
										@if($arrNotificationSetting['notify_review'] == 1)
											checked
										@endif 
									@endif
									data-on-text="<i class='icon-line-check'></i>" data-off-text="<i class='icon-line-cross'></i>"  data-on-color="success" data-off-color="white"> <span style="margin-left: 15px;">@lang('profile.Receive daily digest emails when a review or comment of yours has been commented on').</span>
									</div>
								</div>
							</div>
						</div>
						<div class="card bottommargin-xs">
							<div class="card-body card-default">
								<label>@lang('profile.Comments'):</label><br>
								<div class="row">
									<div class="col-md-12 col-6">
										<input class="bt-switch" name="notify_comment" id="notify_comment" type="checkbox" 
										@if($arrNotificationSetting->notify_comment == 1)
										checked
										@endif 
										data-on-text="<i class='icon-line-check'></i>" data-off-text="<i class='icon-line-cross'></i>"  data-on-color="success" data-off-color="white"> <span style="margin-left: 15px;">@lang('profile.Receive daily digest emails when a review or comment of yours has been commented on').</span>
									</div>
								</div>
							</div>
						</div>
						<div class="card bottommargin-xs">
							<div class="card-body card-default">
								<label>@lang('profile.Questions'):</label><br>
								<div class="row">
									<div class="col-md-12 col-6">
										<input class="bt-switch" name="notify_question" id="notify_question" type="checkbox" 
										@if($arrNotificationSetting->notify_question == 1)
										checked
										@endif
									 	data-on-text="<i class='icon-line-check'></i>" data-off-text="<i class='icon-line-cross'></i>"  data-on-color="success" data-off-color="white"> <span style="margin-left: 15px;">@lang('profile.Receive daily digest emails when a review or comment of yours has been commented on').</span>
									</div>
								</div>
							</div>
						</div>
						<button type="submit" class="button button-3d button-rounded button-fill fill-from-bottom button-blue nott nomargin"><span>@lang('profile.Save Changes')</span></button>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section( 'tailJs' )
<script>
	jQuery(".bt-switch").bootstrapSwitch();
</script>
@endsection