<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">@lang('profile.Home')</a>
			</li>
			<li class="breadcrumb-item"><a href="/profile">@lang('profile.Profile')</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong>
			</li>
		</ol>
	</div>
</section>