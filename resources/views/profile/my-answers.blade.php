@extends('layouts.app')

@section('content')

@include('profile.navbar')

<style>
	a.list-group-item.active_page {
		color: #000;
		font-weight: bold;
		background-color: #f5f5f5;
		border-color: #dddddd;
	}
</style>

<!-- Content
===
=== === === === === === === === === === === === === === -->
<section id="content">

	<div class="content-wrap">

		<div class="container clearfix">

			<div class="row clearfix">

				@include( 'profile.slidebar' )

				<div class="col-md-9">


					<div class="fancy-title title-bottom-border">
						<h3>{{ Route::currentRouteName() }}</h3>
					</div>


					<div class="row clearfix">

						<div class="col-md-8">
							<img src="http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" alt="Avatar" style="max-width: 84px;">

							<div class="heading-block noborder nomargin">
								<h4 class="nomargin nott" style="padding-top: 5px;">John Doe <small>(Malaysia)</small></h4>
								<small>April 24, 2012 at 10:46 am</small>
							</div>

						</div>
						<div class="col-md-4">
							<a href="#" class="button button-border button-rounded button-fill fill-from-bottom button-small button-blue nott pull-right nomargin"><span><i class="icon-line-image"></i> Upload Profile Photo</span></a>
						</div>
					</div>
					<div class="clear"></div>


					<div class="divider notopmargin"></div>

				</div>


			</div>

		</div>

	</div>

</section>

<!-- #content end -->

@endsection
