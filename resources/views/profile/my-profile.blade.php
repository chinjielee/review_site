@extends('layouts.app')

@section('content')

@include('profile.navbar')

<style>
	a.list-group-item.active_page {
		color: #000;
		font-weight: bold;
		background-color: #f5f5f5;
		border-color: #dddddd;
	}
	.entry-meta li:before {
		content: '--';
	}
	.media-body h3 {
		margin-top: 5px !important;
	}
	.profile_action a.button {
		float: right;
		margin-top: 0px !important;
	}
	@media (max-width: 1199px) {
		.media-body h3 {
			margin-top: 5px !important;
			font-size: 18px;
		}
	}
	
	@media (max-width: 767px) {
		.media-body h3 {
			margin-top: 5px !important;
		}
		.profile_action a.button {
			float: none;
			margin-top: 15px !important;
		}
	}
</style>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row clearfix">
				@include( 'profile.slidebar' )
				<div class="col-md-9 col-sm-12">
					<div class="fancy-title title-bottom-border">
						<h3>{{ Route::currentRouteName() }}</h3>
					</div>
					<div class="row clearfix">
						<div class="col-md-8">
							<img src="{{ url('/images/user/'.Auth::user()->avatar) }}" class="alignleft img-circle img-thumbnail notopmargin nobottommargin" style="max-width: 84px;">
							<div class="heading-block noborder nomargin">
								<h4 class="nomargin nott" style="padding-top: 5px;">{{ Auth::user()->fullname }} <small>({{ Auth::user()->country }})</small></h4>
								<small>{{ Auth::user()->updated_at }}</small>
							</div>
						</div>
						<div class="col-md-4 profile_action">
							<a href="#" onclick="$('#avatarFile').click();"class="button button-border button-rounded button-fill fill-from-bottom button-small button-blue nott nomargin"><span><i class="icon-line-image"></i> @lang('profile.Upload Profile Photo')</span>
							</a>
						</div>
						<div class="row justify-content-center">
						    <form id="uploadAvatarForm" action="{{ route('My Profile Photo') }}" method="post" enctype="multipart/form-data">
						    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
						        <div class="form-group">
						            <input type="file" onchange="photoSubmit.click()"class="hidden" accept=".gif, .png, .jpg, .pdf" name="avatar" id="avatarFile" aria-describedby="fileHelp">
						        </div>
						        <button id="photoSubmit" type="submit" class="btn btn-primary hidden">@lang('profile.Submit')</button>
						    </form>
						</div>
					</div>
					<div class="clear"></div>
					<div class="divider notopmargin"></div>
					<div class="row clearfix">
						<div class="col-lg-12">
							<div class="tabs tabs-bordered tabs-tb clearfix" id="tabs-profile">
								<ul class="tab-nav clearfix">
									<li><a href="#tab-profile">@lang('profile.Edit Profile')</a>
									</li>
									<li><a href="#tab-address">@lang('profile.Edit Address')</a>
									</li>
									<li><a href="#tab-password">@lang('profile.Changes Password')</a>
									</li>
								</ul>
								<div class="tab-container">
									@if(Session::get('errors')||count( $errors ) > 0)
										<div class="alert alert-danger" onclick="$(this).hide()">
										@foreach ($errors->all() as $error)
											<i class="icon-remove-sign"></i> {{ $error }} </br>
										@endforeach
										</div>
									@endif
									@if($flash = session('success'))
										<div class="alert alert-success" onclick="$(this).hide()">
											{{ $flash }}
										</div>
									@endif
									<!-- PROFILE -->
									<div class="tab-content clearfix" id="tab-profile">
										<div class="divider notopmargin"></div>
										<form method="post" action="{{ route('Profile Update') }}" class="clearfix">
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
											<div class="form-row">
												<div class="form-group col-sm-6">
													<label for="register-firstname">@lang('profile.First Name'):</label>
													<input type="text" id="register-firstname" name="firstname" value="{{ Auth::user()->first_name }}" class="sm-form-control {{ $errors->has('firstname') ? ' error' : '' }}" />
												</div>
												<div class="form-group col-sm-6">
													<label for="register-lastname">@lang('profile.Last Name'):</label>
													<input type="text" id="register-lastname" name="lastname" value="{{Auth::user()->last_name}}" class="sm-form-control {{ $errors->has('lastname') ? ' error' : '' }}" />
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-3">
													<label for="register-form-mobile_code">@lang('profile.Country Code'):</label>
													<select id="register-form-mobile_code" class="selectpicker" name="mobile_code">
													<option {{ (Auth::user()->mobile_code == '60' ? "selected":"")}}>60</option>
													<option {{ (Auth::user()->mobile_code == '65' ? "selected":"") }}>65</option>
												</select>
												</div>

												<div class="form-group col-sm-3">
													<label for="register-form-mobile">@lang('profile.Mobile'):</label>
													<input type="number" id="register-form-mobile" name="mobile" value="{{Auth::user()->mobile}}" class="sm-form-control {{ $errors->has('mobile') ? ' error' : '' }}" />
												</div>

												<div class="form-group col-sm-6">
													<label>@lang('profile.Email Address'):</label>
													<input type="text" class="sm-form-control" id="" placeholder="{{ Auth::user()->email }}" disabled>
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-6">
													<label for="inputState">@lang('profile.Gender'):</label>
													<select id="inputState" class="form-control" name="gender">
														<option {{ (Auth::user()->gender == 'Female' ? "selected":"") }}>Female</option>
														<option {{ (Auth::user()->gender =='Male' ? "selected":"") }}>Male</option>
													</select>
												</div>
												<div class="form-group col-sm-6">
													<label for="login-form-dob">@lang('profile.Date of birth'):</label>
													<input type="text" value="{{ Auth::user()->dob }}" class="sm-form-control tleft format {{ $errors->has('dob') ? ' error' : '' }}" name="dob" placeholder="YYYY-MM-DD">
												</div>
											</div>
											<div class="form-row">
												<div class="form-group col-sm-6">
													<label for="inputState">@lang('profile.Preferred Language'):</label>
													<select id="inputState" class="form-control" name="language">
														<option {{ (Auth::user()->language == 'en' ? "selected":"") }}>English</option>
														<option {{ (Auth::user()->language =='cn' ? "selected":"") }}>Chinese</option>
													</select>
												</div>
											</div>
											<div class="topmargin-sm">
												<div class="col-md-12">
													<button type="submit" class="button button-3d button-rounded button-fill fill-from-bottom button-blue nott nomargin"><span>@lang('profile.Save Changes')</span></button>
												</div>
											</div>
										</form>
										<div class="divider notopmargin"></div>
									</div>
									<!-- ADDRESS -->
									<div class="tab-content clearfix" id="tab-address">
										<div class="divider notopmargin"></div>
											<form method="post" action="{{ route('Address Update') }}" class="clearfix">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<div class="form-row">
													<div class="form-group col-md-4">
														<label for="login-form-country">@lang('profile.Country')</label>
													<select id="country" class="selectpicker" name="country">
														@foreach($countryList as $country)
															<option {{ (Auth::user()->country == "$country" ? "selected":"") }}>{{$country}}</option>
														@endforeach
													</select>
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-12">
														<label for="address">@lang('profile.Street Address'):</label>
														<input type="text" value="{{Auth::user()->address}}" class="sm-form-control" id="address" name="address" placeholder="">
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="state">@lang('profile.State'):</label>
															<input type="text" value="{{Auth::user()->state}}" class="sm-form-control" id="state" name="state" placeholder="">
													</div>
													<div class="form-group col-md-6">
														<label for="postcode">@lang('profile.Postcode'):</label>
														<input type="text" value="{{Auth::user()->postcode}}" class="sm-form-control" id="postcode" name="postcode" placeholder="">
													</div>
												</div>
												<div class="topmargin-sm">
													<div class="col-md-12">
														<button type="submit" class="button button-3d button-rounded button-fill fill-from-bottom button-blue nott nomargin">
																	<span>@lang('profile.Save Changes')</span>
														</button>
													</div>
												</div>
											</form>
										<div class="divider notopmargin"></div>
									</div>
									<!-- PASSWORD -->
									<div class="tab-content clearfix" id="tab-password">
										<div class="divider notopmargin"></div>
											<form method="post" action="{{ route('Password Update') }}" class="clearfix">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<div class="form-row clearfix">
													<div class="form-group col-md-6">
														<label for="inputEmail4">@lang('profile.Current Password'):</label>
														<input type="password" class="sm-form-control {{ $errors->has('current_password') ? ' has-error' : '' }}" id="current_password" placeholder="" name="current_password" required>
														@if ($errors->has('current_password'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('current_password') }}</strong>
						                                    </span>
						                                @endif
													</div>
												</div>
												<div class="form-row">
													<div class="form-group col-md-6">
														<label for="login-form-dob">@lang('profile.New Password'):</label>
														<input type="password" class="sm-form-control {{ $errors->has('password') ? ' has-error' : '' }}" id="password" placeholder="" name="password" required>
														@if ($errors->has('password'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('password') }}</strong>
						                                    </span>
						                                @endif
													</div>
													<div class="form-group col-md-6">
														<label for="inputPassword4">@lang('profile.Confirm new password'):</label>
														<input type="password" class="sm-form-control {{ $errors->has('password_confirmation') ? ' has-error' : '' }}" id="password-confirm" placeholder="" name="password_confirmation" required>
														@if ($errors->has('password_confirmation'))
						                                    <span class="help-block">
						                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
						                                    </span>
						                                @endif
													</div>
												</div>
												<div class="topmargin-sm">
													<div class="col-md-12">
														<button type="submit" class="button button-3d button-rounded button-fill fill-from-bottom button-blue nott nomargin">
															<span>@lang('profile.Save Changes')</span>
														</button>
													</div>
												</div>
											</form>
											<div class="divider notopmargin"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<!-- #content end -->

@endsection