@extends( 'layouts.app' )

@section( 'content' )

@include( 'profile.navbar' )
	<style>
		.mail-inbox-select-all {
			margin: 0.83333em 2.08333em 0 0;
			font-size: 0.85714em;
			display: inline-block;
		}
		
		.inbox-wrapper a small {
			opacity: 0.5;
			float: right;
		}
		
		.inbox-wrapper tr.unread {
			font-weight: bold;
		}
		
		.inbox-wrapper tr.read {
			font-weight: normal;
		}
		
		table.inbox-wrapper tr td:first-child {
			padding-left: 0px;
		}
		
		.inbox-wrapper tr.read a {
			color: #666;
		}
		
		.inbox-wrapper tr.read a:hover {
			color: #000;
		}
	</style>

<section id="content" style="margin-bottom: 0px;">

	<div class="content-wrap">

		<div class="container clearfix">
			@include( 'profile.slidebar' )
			<div class="col-md-9 col-sm-12">
				<div class="fancy-title title-bottom-border">
					<h3 class="bottommargin-xs">
					Messenger - {{$target_name}}
					<small><a href="{{route('Messenger')}}" class="pull-right" style="margin-top: 10px;">All Messenger</a></small>
					</h3>
				</div>
				<div id="chat_message_display">
				@foreach($arrMessage as $message)
					<div class="list-group">
						<div class="list-group-item list-group-item-action flex-column align-items-start">
							<div>
								<img src="http://localhost:8000/img/avatar.png" class="avatar avatar-60 photo avatar-default nomargin rightmargin-xs" height="50" width="50">
								<h4 class="mb-2" style="display: inline-block;">{{$message->first_name}} {{$message->last_name}}</h4>
								<small class="text-muted pull-right">{{$message->updated_at->diffForHumans()}}</small>
							</div>
							<hr class="topmargin-xs bottommargin-xs">
							<p class="mb-1">{{$message->message}}</p>
						</div>
					</div>
				@endforeach
				</div>
				<form id='send_msg'>
					<div class="col_full bottommargin-xs topmargin">
						<label >Send To: {{$target_name}} </label>
						<textarea id='chat_msg' name="chat_msg" rows="7" tabindex="4" class="form-control"></textarea>
					</div>

					<div class="col_full bottommargin-xs">
						<button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-3d button-rounded button-fill fill-from-bottom button-blue nott nomargin">Submit Now</button>
					</div>
				</form>
				<div class="hide">
					<div class="list-group" id='chat_box'>
						<div class="list-group-item list-group-item-action flex-column align-items-start">
							<div>
								<img src="http://localhost:8000/img/avatar.png" class="avatar avatar-60 photo avatar-default nomargin rightmargin-xs" height="50" width="50">
								<h4 class="mb-2 display-name" style="display: inline-block;"></h4>
								<small class="text-muted pull-right display-time"></small>
							</div>
							<hr class="topmargin-xs bottommargin-xs">
							<p class="mb-1 display-msg"></p>
						</div>
					</div>
				</div>
		</div>
	</div>
</section>
@endsection

@section('tailJs')
		<script> 
			$('form#send_msg').submit(function(e){
				e.preventDefault(); //Prevent the normal submission action
				var input = $("#chat_msg").val();
				$("#chat_msg").val('');
				$.ajax({
					method: "POST",
					url: '{{route("postSendMsg")}}',
					data: {
						_token: $('meta[name="csrf-token"]').attr('content'),
						channel_id: {{$channel_id}},
						msg:input
					},
				  }).done(function(data) {
					var display = appendMessageDisplay(input);
					$('#chat_message_display').append(display);
				  });
			});

			function appendMessageDisplay(msg) {
				var clonedChatBox = $('#chat_box').clone();
				var d = new Date();
				var time = d.toLocaleTimeString();
				clonedChatBox.find('.display-msg').html(msg);
				clonedChatBox.find('.display-name').html('{{$displayname}}');
				clonedChatBox.find('.display-time').html(time);
				return clonedChatBox;
			}
		</script>
@endsection