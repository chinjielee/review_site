<div class="col-md-3 hidden-sm hidden-xs clearfix">
	<div class="list-group">
		<a href="{{route('Dashboard')}}" class="list-group-item list-group-item-action clearfix  
			@if($pageInfo->page == 'Dashboard')
				active
			@endif">
			<i class="icon-line-layout" style="margin-right: 5px;"></i> @lang('profile.Dashboard')
		</a>
		<a href="{{route('My Profile')}}" class="list-group-item list-group-item-action clearfix 
			@if($pageInfo->page == 'Profile')
				active
			@endif">
			<i class="icon-line-head" style="margin-right: 5px;"></i> @lang('profile.My Profile')
		</a>
		<a href="{{route('My Reviews')}}" class="list-group-item list-group-item-action clearfix 
			@if($pageInfo->page == 'Review')
				active
			@endif">
			<i class="icon-line2-star" style="margin-right: 5px;"></i> @lang('profile.My Reviews')
		</a>
		<a href="{{route('My Comments')}}" class="list-group-item list-group-item-action clearfix 
			@if($pageInfo->page == 'Comment')
				active
			@endif">
			<i class="icon-line2-note" style="margin-right: 5px;"></i> @lang('profile.My Comments')
		</a>
		<a href="{{route('My Questions')}}" class="list-group-item list-group-item-action clearfix 
			@if($pageInfo->page == 'Question')
				active
			@endif">
			<i class="icon-line2-speech" style="margin-right: 5px;"></i> @lang('profile.My Questions')
		</a>
		<a href="{{route('Activity Log')}}" class="list-group-item list-group-item-action clearfix 
			@if($pageInfo->page == 'Activity')
				active
			@endif">
			<i class="icon-line2-list" style="margin-right: 5px;"></i> @lang('profile.Activity Log')
		</a>
		<a href="{{route('Notification Setting')}}" class="list-group-item list-group-item-action clearfix 
			@if($pageInfo->page == 'Notification Settings')
				active
			@endif">
			<i class="icon-line-cog" style="margin-right: 5px;"></i> @lang('profile.Notification Settings')
		</a>
		<a href="{{ route('logout')}}" class="list-group-item list-group-item-action clearfix">
			<i class="icon-line2-logout" style="margin-right: 5px;" ></i> @lang('profile.Logout')
		</a>
	</div>
</div>