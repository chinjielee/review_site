@extends('layouts.app')
@section('content')
<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
		</ol>
	</div>
</section>
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					<div class="text-center">
			<h3>Contact Us</h3>
				<p>
				Our Location :
				16 Raffles Quay, 
				Hong Leong Building,
				#41-07, Singapore 048581
				</p>
			</div>
			<div>
				@if($flash = session('success'))
					<div class="alert alert-success" onclick="$(this).hide()">
						{{ $flash }}
					</div>
				@endif
				<form action="{{ url('contact-us')}}" method="post"> 
					{{ csrf_field() }} 
					<div class="form-group"> 
						<label>Name</label> 
						<input type="text" name="name" class="form-control"> 
					</div> 
					@if ($errors->has('name'))
					<span class="help-block">
						<strong>{{ $errors->first('name') }}</strong>
					</span>
					@endif
					<div class="form-group"> 
						<label>E-mail</label> 
						<input type="email " name="email" class="form-control"> 
					</div> 
					@if ($errors->has('email'))
					<span class="help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif
					<div class="form-group"> 
						<label>Contact Number</label> 
						<input type="number " name="contact" class="form-control"> 
					</div> 
					@if ($errors->has('contact'))
					<span class="help-block">
						<strong>{{ $errors->first('contact') }}</strong>
					</span>
					@endif
					<div class="form-group"> 
						<label>Message</label> 
						<textarea name="message_body" class="form-control"></textarea> 
					</div> 
					@if ($errors->has('message_body'))
					<span class="help-block">
						<strong>{{ $errors->first('message_body') }}</strong>
					</span>
					@endif
					<div class="text-center">
					<button type="submit" class="btn btn-blue btn-lg">Submit</button> 
				</div>
				</form>
			</div>
		</div>
	</div>
		</div>
	</div>
</section>
@endsection