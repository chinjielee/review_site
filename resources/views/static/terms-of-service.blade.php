@extends('layouts.app')

@section('content')


<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>WIKABO Policy<br> 
							<small>Posted on: 1st December 2018</small>
						</h3>
						
					<p>Welcome to WIKABO! WIKABO is a platform to gain and share knowledge, enabling people to learn from others, gain insightful resources for better understanding purposes and connect with people from all walks of life by unlocking the power of sharing. </p>
					
					<p>These terms and conditions as listed in below ( collectively “WIKABO Terms”) sets forth the agreement (“Agreement”) between you and WIKTUBE Pte. Ltd. (“WIKABO” “we”, “our” or “us”). It governs your use of the products and services we offer through our internet, online and applications (collectively the “WIKABO Platform”).</p>
					
					<p>Please make sure to read it, because, by using the WIKABO Platform, you expressly consent and agreed to the terms listed below.</p>
					
					<ol class="leftmargin-sm">
						<li class="bottommargin-xs">
							<a href="{{ route('Term of Use') }}" ><h4 class="nomargin">Terms of Usage</h4>
						</li>
						<li class="bottommargin-xs">
							<a href="{{ route('Privacy Policy') }}" ><h4 class="nomargin">Privacy Policy</h4>
						</li>
						<li class="bottommargin-xs">
							<a href="{{ route('Trademark Policy') }}" ><h4 class="nomargin">Trademark Policy</h4>
						</li>
						<li class="bottommargin-xs">
							<a href="{{ route('Copyright Policy') }}" ><h4 class="nomargin">Copyright Policy</h4>
						</li>
						<li class="bottommargin-xs">
							<a href="{{ route('Posting Guideline')}}" ><h4 class="nomargin">Recommended Guidelines</h4>
						</li>
						<li class="bottommargin-xs">
							<a href="{{ route('Disclaimer and Limitation of Liability') }}" ><h4 class="nomargin">Disclaimer and Limitation of Liability</h4>
						</li>
						<li class="bottommargin-xs">
							<a href="{{ route('Cookie Policy') }}" ><h4 class="nomargin">Cookie Policy</h4></a>
						</li>
					</ol>

				</div>

			</div>

		</section><!-- #content end -->

@endsection
