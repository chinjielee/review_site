@extends('layouts.app')

@section('content')


<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>Disclaimer and Limitation of Liability<br> 
							<small><strong>PLEASE READ THIS SECTION CAREFULLY SINCE IT LIMITS THE LIABILITY OF WIKABO ENTITIES TO YOU.</strong></small>
						</h3>
					<p>Updated : 2nd August 2020</p>
					<p>WIKTUBE PTE. LTD. (Company Registration Number 201923089C registered in Republic of Singapore), and its subsidiaries, affiliates, related companies, suppliers, licensors and partners, and the respective Officers, Directors, Shareholders, Employees, Agents and Representatives hereinafter collectively referred to as “WIKABO”. Each provision below applies to the maximum extent permitted under applicable law. For clarity, applicable law shall be Law of Republic of Singapore.</p>
					
					
					<ol type="a" class="leftmargin-sm">
						<li class="bottommargin-xs">
						WIKABO Platform, provides you with our contents, datas and materials and the communication opportunity with others on the basis of without warranty of any kind, express or implied. Without limiting the foregoing, WIKABO hereby expressly disclaim all warranties and accountabilities pertaining to the accuracy, reliability, fit for purposes from usage of WIKABO platform by individuals, organisations and its representatives respectively.
						</li>
						<li class="bottommargin-xs">
							WIKABO does not warrant with respect to, and expressly disclaims all liability for:
							<ol type="roman" class="leftmargin-sm">
								<li>content posted by any user or third party; </li>
								<li>any third-party website, third-party product, or third-party service listed on or accessible to you through the WIKABO platform, including service provider or professional contributor; </li>
								<li> the quality or conduct of any third party that you encounter in connection with your use of the WIKABO platform; or </li>
								<li>unauthorized access, use or alteration of your content. WIKABO makes no warranty that: <br>
								<ol type="a" class="leftmargin-sm">
									<li>WIKABO platform does and/or will meet your requirements; </li>
									<li>WIKABO platform is and/or will be uninterrupted, timely, secure, or error free; </li>
									<li>the results or information that you may obtain from the use of the WIKABO platform, a professional contributor, or any other user is and/or will be accurate and/or reliable; or </li>
									<li>the quality of any products, services, information, or other material obtained and/or purchased by you through the WIKABO platform is and/or will be satisfactory.</li>
									<li>negative publications, reviews and ratings contributed by users on your product, services, materials and any other form of consumables whether either in tangible form or intangible form.</li>
								</ol>
								</li>  
							</ol>
						</li>
						<li class="bottommargin-xs">You agree that to the maximum extent permitted by law, WIKABO shall not be liable to you under any form of liability. Without limiting the foregoing, you agree that, to the maximum extent permitted by law, WIKABO specifically will not be liable for any indirect, incidental, consequential, special, or exemplary damages, loss of profits, business interruption, reputational harm, or loss of data (even if we have been advised of the possibility of such damages or such damages are foreseeable) arising out of or in any way connected with your use of, or inability to use the WIKABO platform.</li>
						
						<li class="bottommargin-xs">Your sole remedy for any dissatisfaction with the WIKABO platform is to refrain from using WIKABO platform.</li>
						
						<li class="bottommargin-xs">Without limiting the foregoing, WIKABO’s maximum aggregate liability to you for losses or damages that you suffer in connection with the WIKABO platform or this agreement is limited to the amount of One Singapore Dollar (SGD 1.00) only.
						</li>
						

					</ol>

				</div>

			</div>

		</section><!-- #content end -->

@endsection
