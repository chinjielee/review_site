@extends('layouts.app')

@section('content')


<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>Trademark Policy<br> 
							<small>Updated on: 2nd August 2020</small>
						</h3>
						
					<p>At WIKABO, we expect our users to respect the intellectual property rights of others. All capitalized terms used in the WIKABO Trademark Policy have the same meaning as those defined in the WIKABO’s Terms of Service, which incorporates this policy by reference into its terms.</p>
					
					<h4 class="nomargin">1)	Discouragement of Trademark Infringement on WIKABO</h4>
					<p>Trademarks should not be used in any way howsoever which causes confusion about the source of goods or sponsorship. Upon discovery of any infringement of trademarks of others, such may result in the removal or suspension, whether in whole or in part, of Your Content from our website. It is also our policy, under appropriate circumstances and at our discretion, to suspend, disable and/or terminate accounts of such users who repeatedly commits, offends and/or charged with infringement of trademarks or other intellectual property rights of others.</p>
					
					<h4 class="nomargin">2) Recourse for Trademark Owners</h4>
					<p>Any trademark owner who reasonably believe that his/her trademark has been infringed on WIKABO, kindly contact us by providing us with the following information in order for us to provide you with an adequate respond:</p>
					<ol type="1" class="leftmargin">
						<li>
						Your contactable information such as your full name, address, contact number, and email address.
						</li>
						<li>
						Credentials for the justification of trademark ownership.
						</li>
						<li>
						Your relationship with the owner of the trademark rightsYour relationship to the owner of the trademark rights
						</li>
						<li>
						Reasonably sufficient information for us to identify of the location on the WIKABO Platform (e.g., URL link) where you believe infringement has occurred
						</li>
						<li>
						A summary of your reasonable believe on the infringement of your trademark rights
						</li>
						<li>
						A confirmation from you that all statements and/or allegations on the infringement are true and accurate; that you reasonably believe in good faith that the use of the intellectual property, described in the manner of your complaint, is not consented, permitted and/or authorized by the owners, its agent, or the law; and that you are the owner, or representative acting on behalf of the owner, of an exclusive intellectual property that allegedly is infringed.
						</li>
					</ol>
					<p>For your convenience, we provide you with this contact form to complete if you wish to report a trademark infringement.</p>
					
					<h4>We strongly suggest the use of our contact form for a more expedient respond. The use of any alternative method may result in a delay in processing your request.</h4>
				</div>
				
			</div>

		</section><!-- #content end -->
@endsection
