@extends('layouts.app')

@section('content')


<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>Acceptable Use Policy<br> 
							<small>Last updated: December 18, 2017</small>
						</h3>
						
					<p>Quora is a place that empowers people to share and grow the world’s knowledge. We believe if people don’t feel safe they can’t contribute to knowledge or listen to another person’s point of view. The following rules help everyone on Quora have a safe experience</p>
					
					<h4 class="nomargin">Be Nice, Be Respectful</h4>
					<p>Follow Quora's Be Nice, Be Respectful policy (“BNBR”), which includes the following:<br>
						<strong><em>Engage with Others in a Constructive Way</em></strong><br>
						Assume everyone on Quora is here to make the platform a great resource with diverse backgrounds, beliefs, and opinions. It is OK to disagree, but please be civil, respectful, and considerate to other posters.<br>
						<strong><em>No Hate Speech</em></strong><br>
						Quora is a place for civil discourse and does not tolerate content that attacks or disparages an individual or group based on race, gender, religion, nationality, ethnicity, political group, sexual orientation or another similar characteristic. Any generalizations about these topics should be phrased as neutrally as possible.<br>
						<strong><em>No Harassment and Bullying</em></strong><br>
						Abusive behavior directed towards private individuals is not allowed. Repeated and unwanted contact constitutes a form of harassment. Content should not make threats against others or advocate violence, including self-harm.
					</p>
					
					<h4 class="nomargin">Respect the Rights of Others</h4>
					<p><strong><em>Identity and Deceptive Activity</em></strong><br>
					Your Quora profile should use your real name or legally incorporated business nameand use accurate and truthful credentials. Do not use Quora to impersonate another person, act as another entity without authorization, or create multiple accounts.<br>
						<strong><em>Intellectual Property and Personal Rights</em></strong><br>
					Do not post content that infringes any intellectual property or personal right of another party. Writing taken from another source should be properly attributed and block quoted. Learn More.
					</p>
					
					<h4 class="nomargin">Respect the Quora Platform</h4>
					<p><strong><em>No Spam</em></strong><br>
					Do not use Quora to post or attract spam. Learn More<br>
					<strong><em>No Malicious or Illegal Activity</em></strong><br>
					Do not contribute viruses, malware, and other malicious programs or engage in activity that harms or interferes with the operation of Quora or seeks to circumvent security or authentication measures. Do not use Quora to engage in unlawful activity or to promote illegal acts.<br>
					<strong><em>Abide by Other Quora Policies</em></strong><br>
					Follow Quora's other policies, which Quora may add or change from time to time. Learn more
					</p>
					
					<h4 class="nomargin">Reporting Problems</h4>
					<p>If you see something on Quora that you believe violates our guidelines or policies, please report it to us using the in-product reporting tool, or via our contact form. <br>	
					Violating the Acceptable Use Policy may result in the removal of content, or in the limitation or termination of a user’s access to Quora. Learn More
					</p>
					
				</div>
				
			</div>

		</section><!-- #content end -->
@endsection
