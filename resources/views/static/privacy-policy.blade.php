@extends('layouts.app')

@section('content')
<style>
li p {
	margin-bottom: 5px;
}

</style>
<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>Privacy Policy<br> 
							<small>Updated on: 2nd August 2020</small>
						</h3>
						
					<p>Your privacy is very important to us and we take it seriously. Our WIKABO Privacy Policy (hereinafter referred to as the “Privacy Policy”) describes our policies and procedures on the collective usage of our platform, disclosure, and sharing of your information when you use the WIKABO Platform. Your information will not be used or shared with anyone unless stated otherwise in our Privacy Policy. This Privacy Policy applies to activities by WIKTUBE Pte. Ltd., its affiliates and subsidiaries (hereinafter collectively referred to as “WIKABO,” “we” or “us”). Capitalized terms that are not defined in this Privacy Policy are further defined in our <a href="https://wikabo.com/terms-of-use" target="blank">Terms of Usage</a></p>
					<h4>1) What Do We Collect?</h4>
						<p>We collect information directly from individuals, organisations, and /or third-parties automatically through the WIKABO Platform.</p>
						<ol type="a" class="leftmargin-sm">
							<li>
								<strong>Profile Information</strong><br>
								Once created an account and inputting profile information on the WIKABO Platform, we collect your name, contact information, and other information you provide in it. Your name, photos, and any other information that you choose to add onto your profile will be available for viewing to all users on the WIKABO Platform. Once you activated the profile in WIKABO platform, other users will be able to see your profile certain information, your activity on the WIKABO Platform, such as the comments, reviews, photos, feedback, questions and answers that you post, your followers and who you followed, topics in connection to your comments, feedback, questions and answer. Alternative options is to comment using anonymous functions which limit your details available publicly. For more information on what you can opt to display, see the below section on “Your Option”.
							</li>
							<li>
								<strong>Communications and Contents</strong><br>
								We collect and store the information, contents that you post in the WIKABO Platform, including your reviews, datas, comments, questions, answers, photos, videos, communication records (via email, messaging, phone, either the WIKABO platform channel or other form of communications. Unless otherwise you have posted your content anonymously, your content, such as dates and all associated comments are publicly available on the WIKABO Platform, along with your name. This may be recorded by search engines and may be republished elsewhere on the Internet in accordance with our <a href="https://wikabo.com/terms-of-use" target="blank">Terms of Usage</a>. For more information on what you can opt to display, see the below section on “Your Option”.
							</li>
						</ol>

					<h4>2) How We Collect Your Activity Information Automatically?</h4>
						We use cookies, log files, and other tracking technologies available to automatically collect information about your activities such as your online behavioral activity, searches, page views, date and time of your visit, and all other information about your usage of WIKABO Platform. We may collect and store information provided by your device in connection with your use of the WIKABO Platform such as type of device used, your choice of internet browser, language, IP address, mobile carrier, unique device identifier, access location, and requested and referring URLs. We will still be able to receive information even if you view contents or interact as a guest user on the WIKABO Platform, without first having created an account or log in. More information is available on the Cookies, and Tracking section below.
						<ol type="a" class="leftmargin-sm">
							<li>
								<strong>Third Parties</strong></br>
								<ul class="leftmargin-sm">
									<li>When other users share or disclose information about you while mentioning you in their content, reply to Your Content, or share information about Your Content via other sites or services. </li>
									<li>Users of our Ad Services and other third-parties share information with us such as a cookie ID, device ID, demographic or interest data, information on content viewed or actions taken by you on a third party website, online service or app. </li>
									<li>Users of our Ad Services upload certain customer list information which contains email or phone number to create customized audience segments for certain campaigns, in order for them to reach audiences consisting of existing customers and others who shares similar characteristics in the WIKABO Platform. </li>
								</ul>
								<p>For more information, see the WIKABO Ads section below.</p>
							</li>
							<li>
							<strong>Social Network Service Providers and Linked Networks</strong><br>
								<p>If you choose to connect your WIKABO account to another online service provider, example; social networking service (hereinafter referred to as “Social Network Service Provider(s)”), you allow us to share and to receive data and information from the Social Network Service Provider(s) such as your log-in information and all other information. You may choose to sign up or log in directly from our WIKABO Platform or through a linked network like Facebook, Twitter, LinkedIn, Google and others (hereinafter collectively referred to as “Linked Network”). If you elect to sign up through a Linked Network, you will be notified and requested to enable the sharing of your contacts with WIKABO. Upon your acceptance to this sharing, we will be using these information to enhance your experience and may engage your contacts by notifying them that you have joined WIKABO. You and your contacts may find and follow each other on the WIKABO Platform. Information that we may collect from each Social Network Service Provider(s) differs from WIKABO due to the difference in control and treatment over the usage and sharing of your information. For more information on Social Network Service Provider(s) on information sharing practices, please refer to each Social Network Service Provider policies.</p>
							</li>
							<li>
								<strong>Ad Services</strong><br>
								<p>The WIKABO Platform includes the WIKABO advertising services (hereinafter referred to as “Ad Services”), which may collect information on the user’s activity and browsing history within the WIKABO Platform and across third-party sites and online services, including those that contain our ad pixels (“Pixels”), widgets, plug-ins, buttons, or related services. Our Ad Services collect browsing information – such as IP address and location, date and time stamp, user agent, WIKABO cookie ID (if applicable), URL, and time zone, and other information about user activities on the WIKABO Platform as well as third party sites and services that have embedded our Pixels, widgets, plug-ins, buttons, or related services. For more information about our Pixels and how they are used, see the section below about our WIKABO Ads.</p>
							</li>
						</ol>
					</h4>
					
					<h4>3) How We Use Your Information</h4>
					<p>We do not share, provide, disclose, give away and/or sell your personal identification information such as your name and contact information to third parties for their use for marketing purposes.</p>
					<p>WIKABO uses the information we collect:</p>
					<ol type="a" class="leftmargin-sm">
						<li><p>To provide you the services we offer on the WIKABO Platform, communicate with you pertaining to your usage of the WIKABO Platform, respond to your inquiries, provide troubleshooting, and for other customer service purposes.</p></li>
						<li><p>To tailor the content and information that we may send or display to you at the WIKABO Platform, to offer location customization, and personalized help and instructions, and to otherwise personalize your experiences while using the WIKABO Platform.</p></li>
						<li><p>To display interest-based advertising to you at the WIKABO Platform, to improve our advertising and measurement systems so we can present relevant ads to you, and to measure and/or determine the effectiveness and reach of our ads and services. For more information, see the Ad Services section below.</p></li>
						<li><p>For marketing and promotional purposes, to send you news and newsletters, special offers, and promotions, or to otherwise contact you concerning products or information which we think may interest you, including information on third party products and services.</p></li>
						<li><p>To track the success of our communications and marketing, and to gather metrics about the Ad Services.</p></li>
						<li><p>To better understand how users access and use the WIKABO Platform, including the Ad Services, both on an aggregated and individualized basis, and for other research and analytical purposes.</p></li>
						<li><p>To evaluate and improve the WIKABO Platform, including the Ad Services, and to develop new products and services.</p></li>
						<li><p>To comply with legal obligations, as part of our general business operations, and for other business administration purposes.</p></li>
						<li><p>Where we believe necessary to investigate, prevent, take action or cooperate with the relevant authorities on matters involving illegal activities, suspected fraud, situations involving potential threats to the safety of any person or violations of our Terms of Usage and/or this Privacy Policy.</p></li>
					</ol>
					
					<h4>4) How We Share Your Information:</h4>
						We share information as set forth below, and where individuals have otherwise consented:
						<ol type="a" class="leftmargin-sm">
							<li>
								<strong>Service Providers</strong><br>
								<p>We may share your information with third party service provider(s) who utilise this information to provide and/or perform services for us, such as payment processors, hosting providers, auditors, advisors, consultants, customer service and support providers, as well as those who assist us in providing the Ad Services.</p>
							</li>
							<li>
								<strong>Affiliates and Subsidiaries</strong><br>
								<p>Your personal information collected by us may be accessed by or shared with subsidiaries and affiliates of WIKTUBE, Pte. Ltd., which usage and disclosure of such information is subjected to this Privacy Policy.</p>
							</li>
							<li>
								<strong>Business Transfers</strong><br>
								<p>We may disclose or transfer information, including personal data, as part of any merger, sale, and transfer of our assets, acquisition or restructuring of all or part of our business, bankruptcy, or similar event.</p>
							</li>
							<li>
								<strong>Legally Required</strong><br>
								<p>We may disclose your information if we are required to do so by law.</p>
							</li>
							<li>
								<strong>Protection of Rights</strong><br>
								<p>We may disclose information where we believe it to be necessary to respond to claims brought against us or, to comply with legal process (e.g., subpoenas or warrants), enforce or administer our agreements and terms, for fraud prevention, risk assessment, investigation, and protect the rights, property or safety of WIKABO, its users, or others.</p>
							</li>
						</ol>
					</h4>
					
					<h4>5) Advertisement Metrics</h4> 
					<p>We may share with users of our Ad Services aggregate statistics, metrics and other reports about the performance of their ads or content in the WIKABO Platform such as the number of unique user views, demographics illustrating the users who saw their ads or content, conversion rates, and information on the date and time. We certainly do not share IP addresses or personal information. We may further allow our advertisers to use Pixels on the WIKABO Platform in order to collect information regarding the performance of their ads.</p>
					
					<h4>6) Anonymized and Aggregated Data</h4> 
					<p>We may share aggregate or de-identified information with third parties for research, marketing, analytics and other purposes, provided such information does not refer specifically to a particular individual.</p>
					
					<h4>7) Cookies, Pixels and Tracking</h4> 
						<p>We and our third party providers use cookies, clear GIFs/pixel tags, JavaScript, local storage, log files, and other mechanisms to automatically collect, store and record information about your usage and browsing activities on the WIKABO Platform and across other third party sites or online services. We may accumulate, consolidate and combine all the information that we collect about our users. A summary of the cookies tracking is stated herein below.</p>
						<ul type="a" class="leftmargin-sm">	
							<li>
								<p>Cookies. These are relatively small files with a unique identifier that are placed on your device through our platform. They allow us to remember users who have logged in, to discover and enables us to know how users navigate through and use the WIKABO Platform, and to display personalized content and targeted ads (including on third party sites and applications).</p>
							</li>
							<li>
								<p>Pixels, web beacons, clear GIFs. These are tiny graphics with a unique identifier which has a similar function as cookies, that we use to track user(s)’ online movements at the WIKABO Platform and the web pages of users of our Ad Services, and to personalize content. We also use these in our emails which enables us to know when they have been opened or forwarded, so we can  determine and/or confirm the effectiveness of our communications.</p>
							</li>
							<li>
								<p>Analytics Tools. We may use internal and third party analytics tools, including <a href="https://support.google.com/analytics/answer/6004245?hl=en" target="_blank">Google Analytics</a>.The third party analytics companies we work with may combine the information collected by us with the other information independently collected by them from other websites and/or other online products and services. Their collection and use of information is subject to their own privacy policies.</p>
							</li>
						</ul>
					</h4>
					
					<h4>8) Incognito Mode </h4>
					<p>Please note we do not change system behavior within the WIKABO Platform in response to browser requests of not to be tracked.</p>
					
					<h4>9) WIKABO Ads</h4>
						<ol type='a' class ='leftmargin-sm'>
							<li>We display personalized ads and content to you on the WIKABO Platform. In order to present such relevant ads and personalized content to our users, we may peruse the information on the user’s activity that we have collected via the WIKABO Platform as well as from third party sites using our Pixels, widgets, plug-ins, or buttons that are embedded on third party sites. We also use this information to determine the effectiveness of advertising and to provide metrics about ad performance to advertising customers. For example, with you visiting websites that sell cars, we may present to you car related ads or content on the WIKABO Platform. We may report to the user of our Ad Services the statistics of WIKABO users who has seen a particular ad and certain aggregated demographic information about the viewers of the ad.</li>
							<li>Users of our Ad Services may also provide us with information as part of their ad campaigns. For example, they may share their customer list information (e.g., email or contact number of their current and prospective customers) with us in order to create customized audiences for their ad campaigns. We only use this customer list information to facilitate the particular user’s campaign (including ad metrics and reporting to that user) or for fraud detection and security purposes. We do not disclose these customer lists to third parties (other than our services providers) unless required by law. We also do not disclose the names or identities of customers who were successfully reached in such campaigns to other relevant Ad Services user.</li>
							<li>We may also work with third parties such as network advertisers to serve advertisements on the WIKABO Platform and on third-party websites or other media (e.g., social networking platforms), such as Google AdSense (more info <a href="https://support.google.com/adsense/answer/142293?hl=en" target="_blank">here</a>) and Facebook Audience Network (more info <a href="https://www.facebook.com/help/568137493302217" target="_blank">here</a>). . These third parties may use cookies, JavaScript, web beacons (including clear GIFs), and other tracking technologies to determine the effectiveness of their ads and to personalize advertising content to you.</li>
							<li>While you cannot opt out of advertising on the WIKABO Platform, you may opt out of much interest-based advertising on third party sites and through third party ad networks (including Facebook Audience Network and Google AdSense). For more information, visit <a href='http://www.aboutads.info/choices' target="_blank">www.aboutads.info/choices</a> Opting out means that you will no longer receive any personalized ads from any third parties ad networks which you have chose to opt out from, However, if you delete cookies or change or accessing our website using another devices, your opt out may no longer be applicable and/or effective.</li>
						</ol>
					
					<h4>10) How We Protect Your Information</h4> 
						The security of your information is important to us. WIKABO has made provisions and implemented safeguards to protect the information we collect. However, no website or Internet transmission is completely secure. We urge that you to take steps to keep your personal data safe, such as selecting a strong password and keeping it private, logging out of your user account, and closing your web browser immediately after you have finished using the WIKABO Platform.
					
					<h4>11) Access and Amend Your Information </h4> 
						You may amend and/or update your account information at any time by logging in to your account. You may also make certain other adjustments to settings or the display of your information in the manner described in more detail in the following section about Your Choices.
					
					<h4>12) Your Options</h4> 
						You have the discretion to decline to submit information through the WIKABO Platform, however that may result in us not being able to provide certain services to you. You also have the choice and control over the types of notifications and communications which we may send to you, limit the sharing of your information within the WIKABO Platform, and otherwise amend certain privacy settings. Below are further choices which you are entitled to:
						<ul type="a" class="leftmargin-sm">
							
							<li>
								<p><strong>Anonymous Posts</strong>.You may post certain content, including questions and answers, anonymously, your name does not appear alongside the content, and WIKABO does not associate such content with your user ID and other profile data.</p>
							</li>
							<li>
								<p><strong>Your Content</strong>. You may at any time amend or remove the answers that you have posted. Any questions you have posted remain on the WIKABO Platform and be subjected to amendment by other users. Any of Your Content deleted from our website will automatically reflect the same at third party sites which contains Your Content that has been shared via WIKABO’s sharing features; however we have no control over the removal or amendments of Your Content once it has been manually shared by others. Each and every amendment made by you will appear in your profile activity and are viewable by other users.</p>
							</li>
							<li>
								<p><strong>Adult Content</strong>. you may choose to receive adult content under your profile’s privacy settings.</p>
							</li>
						</ul>						
					
					<h4>13) Emails and Communications</h4>
						<p>When you have signed up for an account or created a profile at our WIKABO Platform, as part of the service, you will receive the WIKABO digest comprising of content which we believe may match your interests. As permitted by law, you are also opting to receive other emails from us which may interest you as a user of the WIKABO Platform. You can manage your email and notice preferences through your account profile settings, under your Emails and Notifications settings. However, kindly note that you are not entitled to opt out from receiving certain administrative or legal notices from us. If you opt out from receiving emails about recommendations or other information which we think may interest you, we may still send you transactional e-mails about your account or any services you have requested or received from us. It may take up to 10 business days for us to process your opt-out requests.Further, by using the WIKABO Platform, you agree to receive feedback and communications from third-parties within the WIKABO Platform, including any comment over your postings by third parties within the WIKABO Platform. In the event that you do not intend to allow others to comment on any of your posts and/or answers or send you any messages on the WIKABO Platform, you may manage from the Privacy Settings in your profile.</p>
					
					<h4>14) Followers</h4>
						<p>You can block another WIKABO user from following you by selecting the setting for this in the other user’s profile. You can, at any time, change whether or not to follow other users.</p>
					
					<h4>15) Topics</h4>
						<p>You can change the topics that you follow or that your profile lists as areas that you are familiar with.</p>

					<h4>16) Credentials</h4>
					<p>You can change your credentials that are displayed in your profile or in connection with a specific answer.</p>
					
					<h4>17) Indexed Search </h4>
					<p>
						In your privacy settings, you can control whether to allow your profile and name to be indexed by search engines. Changes to privacy settings do not apply retrospectively; In the event that, for example, your name (e.g., answers and profile) has been indexed by search engines, it will remain indexed for a period of time even after you have switched off the index feature, as implementing change immediately is beyond our control.
					</p>
					
					<h4>18) Deleted or Deactivated Account</h4>
					<p>If you choose Delete Your Account in your privacy settings, all of Your Content will be removed from public visibility on the WIKABO Platform, and it may not be restored by us, even if you change your mind. If you choose Deactivate Your Account, you will forthwith no longer receive any communications from us, and users will not be able to interact with you. However, Your Content will remain on the WIKABO Platform. You may, at any time, reactivate the account which you have deactivated by re-logging in.</p>
						
					<h4>19) Targeted Advertising</h4>
						<p>For information about targeted advertising choices, see the WIKABO Ads section above.</p>
					
					<h4>20) Linked Networks</h4>
					<p>
						You may connect or disconnect your Linked Networks, such as Facebook, through the Account Settings tab in your profile settings. Once you have disconnected it, we will no longer receive any information forthwith from that Linked Network and we may retain only the information we have already collected.
					</p>
					
					<h4>21) Transferring Your Data</h4>
					<p>
						WIKABO is registered in Republic of Singapore, and has operations, entities and service providers in the Singapore and potentially throughout the world. As such, we and our service providers may transfer your personal data to, or access it from, countries which may not provide equivalent levels of data protection as your home country. We will take steps to ensure that your personal data receives an adequate level of protection in the country in which we process it, including through appropriate written data processing terms and/or data transfer agreements.
					</p>

					<h4>22) Children’s Privacy</h4>
					<p>
						We do not knowingly collect or solicit personal information from anyone under 13 years old (or under 14 years old for anyone living in Spain or South Korea) or knowingly allow such persons to register. If it has come to our knowledge that we have collected personal information from a child under the relevant age without parental consent, we shall take steps to dispose of that information. 
					</p>
					
					<h4>23) Links to Other Web Sites</h4>
					<p>
						The WIKABO Platform may contain links to third party sites or online services. Please be advised that we are not responsible for the privacy practices of such third party websites as those may have different privacy policies and terms of use and are not associated with us.
					</p>
					
					<h4 class="nomargin">24) Contact Us</h4>
					<p>If you have any queries concerning our practices or this Privacy Policy, please contact us at <a href='mailto:contact@WIKABO.com'/></a> or send mail to :<br>
						
						WIKTUBE Pte. Ltd.<br>
						Attn: Privacy<br>
						16 Raffles Quay, #41-07<br>
						Hong Leong Building,<br>
						Singapore 048581</p>
				
					<h4>25) Changes to Our Privacy Policy</h4>
					<p>
						If we do change our privacy policies and procedures, we will post those changes on this page. If we make any changes to this Privacy Policy that materially changes how we treat your personal information, we will endeavor to provide you with reasonable notice of such changes, such as via prominent notice in the WIKABO Platform or to the email address of in our record, and where required by law, we will obtain your consent or give you the opportunity to opt out of such changes.
					</p>	
				</div>
				
			</div>

		</section><!-- #content end -->

@endsection
