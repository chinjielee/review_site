@extends('layouts.app')

@section('content')


<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>Terms of Usage<br> 
							<small>Updated: 2nd August 2020</small>
						</h3>
					<ol type ='1'>
						<li class="bottommargin-sm">
							<h4 class="nomargin">Interpretation</h4></br>
							<p>
								WIKTUBE Pte. Ltd., and its subsidiaries, affiliates, related companies, suppliers, licensors and partners, and their respective Officers, Directors, Shareholders, Employees, Agents and Representatives.
							</p>
							<p>
								Organisation : A profitable or non-profitable registered entity, club, association, cooperative, union, government ministry and organisation, council, education related institution, and religious related institution with its activities registered with local Government registrar locally or internationally with proof of valid organisation registration number. 
							</p>
							<p>
								Individual : A person is of at least 14 years old calculated from the date of birth. Prior to your registration, your parent or legal guardian must have read, agree, and consent to this Terms of Service and confirm acceptance of this Agreement on your behalf and bear all responsibility pertaining to your use.
							</p>
							<p>
								User : An Individual or Organisation that registers and uses WIKABO Platform.
							</p>
							<p>
								WIKABO Platform : A website/platform where we offer our Products and/or services through the internet, online and applications.
							</p>
						</li>
						
						<li class="bottommargin-sm">
							<h4 class="nomargin">Using the WIKABO Platform</h4>
							<p>
								<strong>Registration.</strong> When you set up a profile on the WIKABO Platform, you will be requested to provide certain general information regarding yourself or your organisation. Individual or Organisation agrees to provide us with clear, accurate and genuine information when you create your account on the WIKABO Platform. Your information shall be kept and used strictly in accordance to our Privacy Policy. Individuals or Organisations are advised to take care in safeguarding the safety and confidentiality of their own password.
							</p>
							<p>
								<strong>Privacy Policy.</strong> Our privacy practices are set forth in our <a href="https://www.wikabo.com/privacy-policy">Privacy Policy</a>. By using the WIKABO Platform, you hereby accept our <a href="https://www.wikabo.com/privacy-policy">Privacy Policy</a>
							</p>
							<p>
								<strong>Guideline.</strong> Your contributions to the WIKABO and/or interaction with others on the WIKABO Platform shall follow the <a href="http://www.wikabo.com/policy/guidelines">Guidelines</a>at all times.
							</p>
							<p>
								<strong>Termination.</strong> We may at our discretion suspend, disable and/or terminate your WIKABO account if it is discovered that you have violate any of the WIKABO Policies and/or Guidelines set forth herein and/or in any of our policies or guidelines or for any other reason.
							</p>
							<p>
								<strong>Changes to the WIKABO Platform.</strong> We are always trying to improve your experience on the WIKABO Platform. We may add, remove or change our features from time to time and we may do so without further reference to you.
							</p>
							<p>
								<strong>Feedback.</strong> We do appreciate and welcome any feedback and suggestions on how to further improve the WIKABO Platform. Feel free to submit your feedback at <a href="http://www.wikabo.com/contact-us">wikabo.com/contact</a>. By submitting your feedback, you agree to grant us the right, at our discretion, to use, disclose and otherwise utilise the feedback, in whole or part, freely and without any consideration and/or compensation to you.
							</p>
							<p>
								<strong>Data retention.</strong>Kindly note that all data collected by WIKABO (“Data”) from the contribution and usage of the WIKABO Platform by you and/or organisations will be used for but not limited to WIKABO Platform improvements and research purposes and it shall remain as the property of WIKTUBE Pte. Ltd., its subsidiaries, and affiliates. You grant WIKTUBE Pte. Ltd. a royalty free, rights and compensation free from the improvements and research from the perusal of your data. Data does not include Your Content and is separate from Our Content and Materials. 
							</p>
							<p>
								<strong>Engagement and promotional contents.</strong> By signing up with WIKABO Platform, we will engage with users by means but not limited to;
								<ol type="i" class="leftmargin-sm">
									<li>
										Promotional contents through contactable manner (example : emails, private messages and etc) 
									</li>
									<li>
										advertisements
									</li>
								</ol>
							</p>
						</li>
						
						<li class="bottommargin-sm">
							<h4 class="nomargin">Your Content</h4>
							<ol type="a" class="leftmargin-sm">
								<li>
								<strong>Definition of Your Content</strong>
									WIKABO Platform enables you to add posts, texts, photos, videos, links, and files to share with others. All material that you upload, publish or display to others via the WIKABO Platform will be collectively referred to as “Your Content.” You acknowledge, agree and consent that, as part of using the WIKABO Platform, Your Content may be viewed by the general public and stored in WIKABO platform perpetually. 
								</li>
								<li>
									<strong>Ownership</strong>
									You, or your licensors (if any), retain copyright and other intellectual property ownership over Your Content, subject to the non-exclusive rights granted to us below.
								</li>
								<li>
									<strong>License and Permission to Use Your Content.</strong>
									<ol type="i" class="leftmargin">
										<li>
											By submitting, posting, or displaying Your Content on the WIKABO Platform, you grant WIKABO, partners, employees, directors, management and its affiliated companies a non-exclusive, worldwide, royalty free, fully paid up, transferable, sublicensable (through multiple tiers), license to use, copy, reproduce, process, adapt, modify, create derivative works from, publish, transmit, store, display and distribute, and otherwise use Your Content in connection with the operation or use of the WIKABO Platform or the promotion, advertising or marketing thereof, in any and all media or distribution methods (currently available or  developed in the foreseeable future). You agree that this license includes the right for WIKABO to make Your Content available to other companies, organizations, business partners, or individuals who collaborate with WIKABO for the syndication, broadcast, research, distribution or publication of Your Content through other media or distribution methods. This license also includes the right for other users of the WIKABO Platform to use and modify Your Content, subject to our Terms of Service. If you disagree with this clause, your sole remedy is to stop using WIKABO Platform. 
										</li>
										<li>
											Once you post an answer to a question or review in WIKABO Platform, you may, at any time, amend or remove your answer from public display except when answer is posted anonymously by you. Vice versa, the questions and /or comments you posted may, at any time, be amended or removed by WIKABO at any time. Any amendments made by you may be visible to other users. The right for WIKABO to copy, display, transmit, publish, perform, distribute, store, modify, and otherwise use any question posted by you, and sublicense those rights to others, is perpetual and irrevocable explicitly granted by you.
										</li>
										<li>
											You acknowledge, agree and consent that WIKABO may preserve Your Content and may also disclose Your Content and related information if required to do so by law or you reasonably believe in good faith that such preservation or disclosure is reasonably necessary to: 
											<ul>
												<li>comply with legal process, applicable laws or government requests;</li>
												<li>enforce these Terms of Usage; </li>
												<li>respond to any allegations claiming that Your Content violates the rights of third party(s); </li>
												<li>detect, prevent, or otherwise address fraud, security or technical issues; or </li>
												<li>protect the rights, property, or personal safety of WIKABO, its users, or the public.</li>
											</ul>
										</li>
										<li>
											You understand and agree that we may, at our discretion modify, adapt, or create derivative works from Your Content in order to transmit, display or distribute it over computer networks, devices, service providers, and in various media. We may also at any time remove or refuse to publish Your Content, in whole or in part.
										</li>
										<li>
											You further  grant us the permission and authority to act as your non exclusive agent to bring enforcement action at our discretion against any unauthorized use by third party(s) of any of Your Content outside of the WIKABO Platform or in violation of our Terms of Usage.
										</li>										
									</ol>
								</li>
								<li>
									<strong>Your Responsibilities for Your Content</strong>
									By posting Your Content on the WIKABO Platform, you represent and warrant to us that: 
										<ol type="i" class="leftmargin">
											<li>
												You have the ownership rights, or you have obtained all necessary licenses and/or permissions from any parties, to use Your Content and grant us the rights to use Your Content in accordance to the provision herein mentioned, and;
											</li>
											<li>
												that the posting of Your Content does not violate any intellectual property or personal rights of others or applicable law or regulation or WIKABO’s Guidelines, Copyright Policy , Trademark Policy, any other published WIKABO’s policies, and you shall take all reasonable steps to avoid causing any form of such infringement. You accept full responsibility to avoid any infringement of the intellectual property or personal rights of others or violation of laws and regulations caused in association You also agree to pay all royalties, fees, and any other monies owed to any person by reason of Your Content.
											</li>
										</ol>
								</li>
								<li>
									<strong>Our Content and Materials</strong>
									<ol type="a" class="leftmargin">
										<li>
											<strong>Definition of Our Content and Materials</strong>
											All intellectual property in and/or related to the WIKABO Platform (particularly inclusive of, but not limited to our software, the WIKABO marks, the WIKABO logo, our User Interface, our User Experience design but excluding Your Content) is the property of WIKTUBE Pte. Ltd., or its subsidiaries and affiliates if applicable, or content posted by other WIKABO users licensed to us (hereinafter collectively referred to as “Our Content and Materials”).
										</li>
										<li>
											<strong>Data</strong>
											All data gathered by WIKABO (“Data”) from the contribution and usage of the WIKABO Platform by you and/or organisations shall be the property of WIKTUBE Pte. Ltd., its subsidiaries, and affiliates. For clarity, Data does not include Your Content and is separate from Our Content and Materials.
										</li>
										<li>
											<strong>Our License to You.</strong>
											<ol type="a" class="leftmargin">
												<li>
													We grant you a limited, non-exclusive license to use and access Our Content and Materials and Data made available to you on the WIKABO Platform in connection with your use of the WIKABO Platform, subject to the terms and conditions herein mentioned.
												</li>
												<li>
													WIKABO gives you a worldwide, royalty-free, revocable, non-assignable and non-exclusive license to re-post Our Content and Materials anywhere on the internet provided that:
													<ul type="a" class="leftmargin">
														<li>
															The creator of the Content has not explicitly marked the Content as not for reproduction on the WIKABO Platform; 
														</li>
														<li>
															you do not modify any of the content within; 
														</li>
														<li>
															you attribute WIKABO by name in readable text and with a human and machine-followable link (an HTML {{stripslashes('<a>')}} anchor tag) linking back to the original source of the content on https://www.wikabo.com/ on every page of your post which contains Our Content and Materials
														</li>
														<li>
															upon request, either by WIKABO or a user, you remove the user's name from content which the user has subsequently made anonymous; 
														</li>
														<li>
															upon request, either by WIKABO or by a user who contributed to the content, you make a reasonable effort to update a particular piece of content to the latest version on the WIKABO Platform; and 
														</li>
														<li>
															upon request, either by WIKABO or by a user who contributed to the content, you make a reasonable attempt to remove content that has been deleted or marked as not for reproduction on the WIKABO Platform; 
														</li>
														<li>
															you do not republish more than a small portion of Our Content and Materials. In exercising of these rights, you agree that you do not assert any connection with, sponsorship or endorsement by WIKABO, or any WIKABO user, without separate, express prior written permission from us.
														</li>
														<li>
															We may terminate our license granted to you at any time for any reason. Our rights will be exercised and non obligatory distribution refusal on any content on the WIKABO Platform or to remove the contents except for the rights and license granted under these terms, we reserve all other rights and grant no other rights or licenses, implied or otherwise, to another.
														</li>
													</ul>
												</li>
											</ol>
										</li>
										<li>
											<strong>Permitted uses.</strong>If you operate a search engine, web crawler, bot, scraping tool, data mining tool, bulk downloading tool, wget utility, or similar data gathering or extraction tool, you may access the WIKABO Platform, subject to the following additional rules: 
											<ol>
												<li>
													you must use a descriptive user agent header; 
												</li>
												<li>
													you must follow robots.txt at all times; 
												</li>
												<li>
													your access must not adversely affect the functionality of the WIKABO Platform; and 
												</li>
												<li>
													you must clearly inform how we may contact you, whether in your user agent string, or by providing a reachable contact number or email on your website.
												</li>
											</ol>
										</li>
										<li>
											<strong>No Endorsement or Verification.</strong> Kindly note that the WIKABO Platform grants you access to third-party content, products and services, and it provides you with opportunity to engage with third parties. Participation or availability on the WIKABO Platform does not amount to endorsement or verification by us. We make no warranties or representations as to the accuracy, completeness, or timeliness of any content posted on the WIKABO Platform by any user(s).
										</li>
										<li>
											<strong>Ownership</strong> You acknowledge and agree that Our Content and Materials remain the property of WIKABO's users or WIKABO. The content, information and services made available on the WIKABO Platform are protected by the Law of Singapore and international copyright, trademark, patent and other laws, and you acknowledge that these rights are valid and enforceable.
										</li>
									</ol>
								</li>
							</ol>
						</li>
						<li class="bottommargin-sm">
							<h4 class="nomargin">Service Provider</h4> You may enable another online service provider, such as a social networking service (hereinafter referred to as “Service Provider”), to be integrated directly into your account on the WIKABO Platform. By enabling an integrated service, you are allowing us to pass to, and/or receive from, the Service Provider your log-in information and other user data. For more information about WIKABO’s use, storage, and disclosure of information related to you and your use of integrated services within WIKABO, please see our Privacy Policy. Note that your use of any Service Provider and the handling of your data and information by them is governed solely by their terms of use, privacy policies, and other policies.	
						</li>
						
						<li class="bottommargin-sm">
							<h4 class="nomargin">WIKABO Platform Engagements and Promotions</h4>
							<ol type="a" class="leftmargin-sm">
								<li>
								<strong>Email</strong>. By signing up at the WIKABO Platform, it includes promotions of emails of reviews and questions and answers that may be of interest to you. You may opt-out from receiving the emails, and adjust other communication settings, by going to “Email and Notification” settings in your account profile, as further described in our Privacy Policy.
								</li>
								<li>
								<strong>Advertisements</strong>. The WIKABO Platform may include advertisements, which may be targeted to content or information on the WIKABO Platform, queries made through the WIKABO Platform, or other information, in an effort to make them relevant to you. The types and extent of advertising by WIKABO are subject to change. In consideration for WIKABO granting you access to and use of the WIKABO Platform, you agree, permits and/or allow WIKABO and its third party providers and partners to place such advertising on the WIKABO Platform. If you wish to become an advertiser, you will be required to enter into another agreement with us which a separate and supplemental terms and conditions about providing advertising services on the WIKABO Platform.
								</li>
								<li>
								<strong>Legal, Medical &amp; Other Professional Contributors</strong>. Some users who post content are members of legal, medical, and other licensed professions (hereinafter collectively referred to as “Professional Contributors”). Content posted by Professional Contributors should be not be fully and solely relied upon and taken as a substitute advice of a professional. You are encouraged to obtain your own advice from a professional based on your specific situation. WIKABO shall not be liable for any advises given by the contributor has provided.
								</li>
								<li>
								<strong>Web resources and third-party services.</strong> The WIKABO Platform also offer you the opportunity to visit links to other websites or to engage with third-party products or services. You assume all risk arising from your visit and/or use of such websites or resources.
								</li>
								<li>
								<strong>Services that Require Separate Agreement.</strong> Certain features or services may require you to enter into a separate and supplemental written agreement with us prior to use.
								</li>
								<li>
								Services that Require Separate Agreement. Certain features or services may require that you enter into a separate and supplemental written agreement prior to use.
								</li>
							</ol>
						</li>
						
						<li class="bottommargin-sm">
							<h4 class="nomargin">Reporting Violations of Your Intellectual Property Rights, WIKABO Policies, or Applicable Laws.</h4>
							<p>We have a special procedure for reporting violations of your intellectual property rights or other violations of WIKABO policies or applicable laws.</p>
							<ol type="a" class="leftmargin-sm">
								
								<li>
									<strong>Copyright Policy and Trademark Policy</strong>We have adopted and implemented a Copyright Policy and Trademark Policy. For more information, including detailed information about how to submit a request for takedown if you believe a content on the WIKABO Platform infringes your intellectual property rights, please refer to our Copyright Policy and Trademark Policy. Kindly contact us on the infringement claim with all documentary evidence and we shall take necessary action accordingly.
								</li>
								<li>
									<strong>Reports of Other Violations</strong>If you reasonably believe any content on the WIKABO Platform violates WIKABO’s Guideline or otherwise violates applicable law (apart from copyright or trademark violations) or other WIKABO policies, you may contact us at https://wikabo.com/contact-us
								</li>
								<p>We have no obligation to delete and/or remove content which you personally finds it objectionable or offensive. We endeavor to respond promptly to requests for content removal, consistent with our policies and applicable law.</p>
							</ol>
						</li>
						<li class="bottommargin-sm">
							<h4 class="nomargin">Indemnification</h4>
							<p>You agree to fully indemnify or keep WIKABO indemnified from all claims (directly and/or indirectly) and costs (including reasonable attorneys’ fees) arising out of or related to: </br>
								i) your use of the WIKABO Platform, </br>
								ii) Your Content, </br>
								iii) your conduct or interactions with other users of the WIKABO Platform, or </br>
								iv) your breach of any part of this Agreement. We will promptly notify you of any such claim and will provide you (at your expense) with reasonable assistance in defending the claim. You will allow us to participate in the defense and will not attempt to privately settle any such claim without our prior written consent.</p>
						</li>
						<li class="bottommargin-sm">
							<h4 class="nomargin">Dispute Resolution</h4>
							<p>This Agreement and any action arising out of your use of the WIKABO Platform will be governed by the laws of the Republic of Singapore. In the event that a contradiction arise, the laws of the Republic of Singapore shall prevail. Unless submitted to arbitration as set forth in the following paragraph, all claims, legal proceedings or litigation arising in connection with your use of the WIKABO Platform will be brought solely in Singapore and you consent to the jurisdiction of and venue in such courts and waive any objection as to inconvenient forum.</p>
							<p>For any claim (excluding claims for injunctive or other equitable remedy) under this Agreement where the total amount of the award sought is less than SGD 10,000.00, the party requesting such remedy may elect to resolve the dispute through a binding non-appearance-based arbitration. The party electing such arbitration will initiate the arbitration through an established alternative dispute resolution ("ADR") provider mutually agreed upon by the parties. The ADR provider and the parties must comply with the following rules: </p>
							<ol type ='a' class='leftmargin-sm'>
								<li>the arbitration shall be conducted by telephone, online and/or solely based on written submissions, as per the choice of the party initiating the arbitration;</li>
								<li>the arbitration need not require the presence of the parties or witnesses unless otherwise mutually agreed by the parties; and </li>
								<li>any judgment on the award entered by the arbitrator is binding upon the parties and may be enforced in any court of competent jurisdiction.</li>
							</ol>
						</li>
						<li class="bottommargin-sm">
							<h4 class="nomargin">General Terms</h4>
							<ol type ='a' class='leftmargin-sm'>
								<li><strong>Changes to these Terms </strong>We may, at any time, amend this Agreement (including any policies, such as the Privacy Policy , Guideline , Copyright Policy, and Trademark Policy that are incorporated into this Agreement) at our exclusive discretion. If we amend material terms of this Agreement, such amendment shall be immediately effective prior to our notice to you of the amendment. We also reserves the sole discretion to select the form of notification used to inform you of such amendment. You may, at any time, view the Agreement and our main policies here. Your failure and/or omission to cancel and/or terminate your account, or cease use of the WIKABO Platform, after receiving notification of the amendment, will amount to your acceptance of the amended terms. If you do not agree to the amendments or to any of the terms in this Agreement, your sole remedy is to cancel and/or terminate your account and to cease use of the WIKABO Platform.</li>
								<li><strong>Governing Law and Jurisdiction </strong>WIKABO is an entity registered in Singapore and you hereby agree that any disputes, claims, suit, proceeding arising from the usage of our WIKABO Platform shall be governed under the Law of Singapore only. </li>
								<li><strong>Use of WIKABO Platform outside of Singapore </strong>WIKABO expressly disclaims any representation or warranty that the WIKABO Platform complies with all applicable laws and regulations outside of Singapore. If you use the WIKABO Platform from outside of Singapore voluntarily or involuntarily, you expressly understood and agree that you will be responsible for compliance with the respective country’s laws, regulations, or customs whichever applicable in connection with your use of the WIKABO Platform.</li>
								<li><strong>Applications and Mobile Devices </strong>When you access the WIKABO Platform through a WIKABO application, you acknowledge that the provisions in this Agreement shall only bind upon you and WIKABO, and not with another application service provider or application platform provider (for example Apple Inc. or Google Inc.), for the latter are subjected to their own terms. While you access the WIKABO Platform through a mobile device or devices, your wireless carrier’s standard charges, data rates, and other fees may apply.</li>
								<li><strong>Assignment or novation </strong>You are not allowed to assign or novate this Agreement (or any of your rights or obligations under this Agreement) without our prior written consent from us. Any attempted assignment or transfer without complying with the foregoing shall be treated as a breach of this agreement. We may freely assign or transfer this Agreement at our discretion. This Agreement inures to the benefit of and is binding upon the parties and their respective legal representatives, successors-in-title, heirs and assigns.</li>
								<li><strong>Electronic Communications </strong>You consent to receive communications from us by email in accordance with this Agreement and applicable law. You acknowledge and agree that all agreements, notices, disclosures and other communications that we provide to you electronically will satisfy any legal requirement where such communications has to be in writing.</li>
								<li><strong>Entire Agreement/ Severability </strong>This Agreement supersedes all prior terms, agreements, discussions and writings regarding the WIKABO Platform and constitutes the entire agreement between you and us pertaining to the WIKABO Platform (save and except to services which require a separate written agreement to be made with us, in addition to this Agreement). If any one or more of the provisions contained herein shall for any reason be held to be unenforceable illegal or otherwise invalid in any respect under the law governing this Agreement or its performances, such unenforceability illegality or otherwise invalidity shall not affect any other provisions of this Agreement and this Agreement shall then be construed as if such unenforceable illegal or otherwise invalid provisions had never been contained herein.</li>
								<li><strong>Notices </strong>All notices permitted or required under this Agreement, unless specified otherwise in this Agreement, must be sent in writing in the manner as follows in order to be valid: 
									(i) notice(s) from us to you, must be sent via email to the address associated with your account, and 
     								(ii) notice(s) from you to us, must be sent via contact@wikabo.com. Notice(s) will be deemed served (a) upon you, once it is emailed, and (b) to us, upon receipt by us.
								</li>
								<li><strong>Relationship</strong>This Agreement does not create a joint venture, agency, partnership, or other form of joint enterprise between you and us. Except as expressly provided herein, neither party has the right, power, or authority to create any obligation or duty, express or implied, on behalf of another.</li>
								<li><strong>Waiver</strong>No waiver of any terms will be deemed a further or continuing waiver of such term or any other term. Our failure or omission to assert a right or provision under this Agreement will not constitute a waiver of such right or provision.</li>
							</ol>
						</li>
					</ol>			
				</div>

			</div>

		</section><!-- #content end -->

@endsection
