@extends('layouts.app')

@section('content')


<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>Copyright Policy<br> 
							<small>Updated on: 2nd August 2020</small>
						</h3>
						
					<p>At WIKABO, we expect our users to respect the intellectual property rights of others. All capitalized terms used in the WIKABO Copyright Policy have the same meaning as defined in the WIKABO Terms of Service, which incorporates this policy by reference into its terms.</p>
					
					<h4 class="nomargin">1)	Discouragement of Copyright Infringement</h4>
					<p>We ask users of WIKABO to avoid infringing copyrights of others. Upon discovery of any infringement of copyrights of others, such may result in the removal or suspension, whether in whole or in part, of Your Content from our website. It is our policy, under appropriate circumstances and at our discretion, to suspend, disable and/or terminate accounts of such users who repeatedly commits, offends and/or charged with infringement of copyrights or other intellectual property rights of others.</p>
					
					<h4 class="nomargin">2) Recourse for Copyright Owners: Reporting Complaints with a Copyright Act Notice</h4>
					<p>We also provide a mechanism for copyright owners, or those authorized to act on behalf of an owner of a copyright, to report infringing use of their content on the WIKABO Platform. We will respond expeditiously to claims of copyright infringement that are reported to us in accordance to the Copyright Act 1987. We have included herein the procedure for a proper complaint below. We also provide herewith the Singapore Copyright Act for your easy reference:<a href="https://sso.agc.gov.sg/Act/CA1987" target="_blank">https://sso.agc.gov.sg/Act/CA1987</a></p>
					<p>In order to provide us with a complaint notice required by the Copyright Act (hereinafter referred to as the “CA Notice”), kindly furnish us the following for our further action:
					</p>
					<ol type="1" class="leftmargin">
						<li>
						Your contactable information such as your full name, address, contact number, and email address. 
						</li>
						<li>
						Identify the copyright work that you claim has been infringed. If multiple copyright works are involved you may provide a representative list of the copyright works that you claim have been infringed.
						</li>
						<li>
						Provide reasonably sufficient information for us to identify the location on the WIKABO Platform (e.g., URL link) where you believe infringement has occurred and are requesting the same to be removed or disabled.
						</li>
						<li>
						Include the following statements in the CA Notice:
							<p>
								I declare that I have a good faith belief that the disputed use of the copyrighted material or reference link to such material is not authorized by the copyright owner, its agent, or the law (e.g., as a fair use).
							</p>
							<p>
								I declare that the information in this CA Notice is accurate and, under penalty of perjury, that I am the owner or authorized to act on behalf of the owner of an exclusive copyright that is allegedly infringed.
							</p>
						</li>
						<li>
						Include a physical or electronic signature (e.g. typing your full name)
						</li>
						<li>
						Deliver your CA Notice, completed together with the supporting documents to WIKABO, at:
						<p>
							Attn: Copyright Dispute<br>
						 	contact@wikabo.com<br>
						Or<br>
						Attn: Copyright Dispute <br>
						WIKTUBE Pte. Ltd.
						16 Raffles Quay, #41-07 Hong Leong Building,
						 Singapore 048581						
						</p>

						</li>
					</ol>
					<p>For your convenience, we provide you with this <a href="https://www.wikabo.com/contact" target="_blank">contact form</a> to complete and send via email if you wish to report copyright infringement.</p>

					<p class="text-danger"><b><i>We strongly suggest the use of our contact form for a more expedient respond. The use of any alternative method may result in a delay in processing your request.</i></b></p>

					<h4 class="nomargin">3) If There Is a Copyright Complaint Against You</h4>
						<p>Please note that there are potentially serious consequences for any fraudulent or bad faith submissions of CA notifications or counter notifications. Such consequences may be dealt with in accordance to the Laws of Singapore. Therefore, before responding with a counter-notification, you should be certain that you are indeed the actual rights holder of the content removed or that you have a reasonable belief in good faith that content was erroneously removed. It is important to make sure that you understand the repercussions of submitting a false counter notification.</p>						

					<h4 class="nomargin">4) How to File a CA Counter-Notification</h4>
						<p>If you receive a notification that a posting or portion of Your Content has been removed or suspended due to a copyright complaint, this indicate that we have removed it at the request of the content owner. If you believe that such removal was a mistake, you may file a CA counter-notification (the proper procedure for such counter notification is provided as below). Upon our receipt of a proper counter-notification, we shall forward the same to the Complainant who alleged violation of copyright. If we do not receive any form of notice from the original Complainant within seven (7) business days, indicating that he is and/or attempting to seek a court order to prevent further infringement of the content at issue, we will remove all records of the complaint from your account and, we may restore the content that was removed.</p>
					A proper counter notification should be sent in reply to the email notification received by you, or by sending it to WIKABO's Copyright Agent as mentioned above, and it should contain the following:
					<ol type="1" class="leftmargin">
						<li>Your name, address, contact number and email address
						</li>
						<li>The URLs of the content that was removed or disabled by us (you can copy and paste the link provided in the notification email)
						</li>
						<li>The following statements:
							<p>I consent to the jurisdiction of Singapore Court for which my address is located, or if my address is outside of Singapore, for any judicial district in which WIKTUBE Pte. Ltd. is located. I will accept service of process from the claimant of copyright of infringement.</p>
							<p>I declare, under penalty of perjury, that I have a good faith belief that the content was removed as a result of a mistake or misidentification of the material to be removed or disabled.</p>
						</li>
						<li>
							Your physical or electronic signature (e.g., typing your full name)
						</li>
					</ol>

				</div>
				
			</div>

		</section><!-- #content end -->

@endsection
