@extends('layouts.app')

@section('content')

<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		
<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>About Us<br> 
						</h3>
						
					<p>Whatever we do, we need information or date to determine the decision making. That’s what we do on daily basis.</p>
					<p>We WIKABO, believe that everything should have a proper data metrics information in order to form a justifiable decision and reducing disappointment as much as possible leading to savings tonnes costs. </p>
					<p>Our vision is to transform the way decision making is made and with using the right transparent and powerful data metrics approach. </p>
					
					
				</div>
				
			</div>

		</section><!-- #content end -->
	
@endsection
