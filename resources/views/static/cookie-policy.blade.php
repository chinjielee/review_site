@extends('layouts.app')

@section('content')


<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
					<h3>Cookie Policy<br> 
						<small>Updated on: 2nd August 2020
						</small>
					</h3>
						
					<p>Cookies are relatively small files placed onto your device by certain websites that you visit or e-mails which you open.</p>
					<p>
					By using WIKABO website/platform, you hereby consent to our use of cookies to collect and use your personal data for the purposes of collecting information on the usage of our website in order to give you a personalised and more streamlined online experience.
					</p>
					<p>
					The personal data that we collect may include information on how you use our website, your choice of Internet browser, the type of device used to access our website and your access location. Our Privacy Policy shall apply over the collection and use of such personal data.
					</p>
					
					<h4 class="nomargin"><b>Purpose</b></h4>
					We collect and use cookies on our website for the following purposes: </br>
					<ol>
						<li>
							<strong>Technical</strong></br>
							<p>Cookies allow our website to function at its most efficient, including but not limited to:
								<ul>
									<li>
										Controlling website traffic to provide our users with consistent and reliable access
									</li>
									<li>
										Storing our users’ acceptance of our Terms and Conditions, Privacy Policy and Cookie Policy
									</li>
								</ul>
							</p>
						</li>
						<li><strong>Ease of use</strong></br>
							<p>Cookies make navigating our website easier for you, including but not limited to:
								<ul>
									<li>
										Remembering your language and country preferences, and presenting relevant content based on your viewing behaviour at our website
									</li>
								</ul>
							</p>
						</li>
						<li><strong>Analytic</strong></br>
							<p>Cookies are used to collect statistical data on how you navigate and perform transactions on our website, including but not limited to:
								<ul>
									<li>
										Gathering feedback on your satisfaction through questionnaires and/or surveys
									</li>
									<li>
										Gaining insights on how you navigate on our website for further improvements to be made
									</li>
								</ul>
							</p>
						</li>
						<li class="leftmargin"><strong>Marketing</strong></br>
							<p>Cookies allow us to present appropriate marketing messages to you, including but not limited to:
								<ul>
									<li>
										Allowing our website to serve up different versions of a page for marketing purposes.
									</li>
									<li>
										Allowing third parties to display appropriate advertisement to track its effectiveness
									</li>
									<li>
										Displaying relevant advertisement based on your viewing behaviour on our website and certain other websites
									</li>
									<li>
										Displaying our advertisements. including advertisements of other entities at third party websites which you visit.
									</li>
								</ul>
							</p>
						</li>

						<p>We also use web analytics services, such as Google Analytics. For example, Google utilises the data they collect to track and examine the use of www.wikabo.com which is then used to prepare reports and its activities and share them with other Google services. Google may use the data they collect to contextualise and personalise the advertising on its own network. For more details, see Google’s Privacy Policy.</p>

						<li><strong>Disabling Cookies</strong></br>
							<p>You may refuse to accept and prevent cookies from being placed on your device by changing the settings of your Internet browser. Please refer to the links below for the respective guide on how to turn off or delete cookies 

							</br><a href = "https://support.google.com/chrome/answer/95647?co=GENIE.Platform=Desktop&hl=en" >Chrome</a></br>
							<a href = "https://support.mozilla.org/en-US/kb/cookies-information-websites-store-on-your-computer" >Firefox</a></br>
							<a href = "https://support.microsoft.com/en-us/help/17442/windows-internet-explorer-delete-manage-cookies#ie=ie-11" >Internet Explorer</a></br>
							<a href = "https://support.apple.com/kb/PH21411?locale=en_US&viewlocale=en_US" >Safari</a></br>
							If you choose not to allow the use of cookies, certain areas of our website may not function properly or become inaccessible.
							If you delete your cookies, we will not be able to know if you have opted out of behavioural advertising on our website; recognise if you have agreed to our Terms and Conditions, Privacy Policy and Cookie Policy; or remember your language preferences. Banners from our third-party websites will reappear and you will once again be prompt to accept the use of cookies and provide other cookie-related information.
							More information on behavioural advertising and deleting or controlling cookies are available at each respective country Media / Communication authority guidelines. 
							
							</p>
						</li>
						<li><strong>Online behavioural Analytic and Advertising</strong></br>
							<p>Online behavioural advertising is also known as interest-based advertising. It serves advertisement which are more relevant to your interests based on your online behavioural (analytics). Example : websites visit, product or services search, or place of interest search.This enables us to display advertisement that is relevant, useful and beneficial to you. Advertisement can also be delivered to you based on the website content you have just been viewing. This is known as retargeting.
							</p>
							<p>When you visit our website and thereafter navigate to other websites, we may leverage the cookies we have in place and our partnerships with online advertisers and social media networks in order to serve you with targeted advertisement about the WIKABO platform on other websites and/or to limit the number of times you see a particular advertisement.
							We collect the following information about you for such purposes: IP address or other unique identifiers, mobile carrier, time zone setting, operating system and platform, information about your journey on the website or mobile application, and information about any sites you have visited on the website or mobile application.
							You may opt out or customised the use of cookies if you do not wish to be retargeted or have your advertising to be tailored.
							</p>
							<p>
							Opting out does not mean that you will no longer receive online advertisement. It merely means that the company(s) from which you opted out will no longer serve you advertisement that is tailored to your web preferences and usage patterns. which may result in you seeing more advertisement irrelevant to your preferences.
							</p>
						</li>
						<li><strong>Our Partnership With Advertising Analytics</strong></br>
							<p>We may have a partnership with advertising networks to capture data from you through third party cookies. This allows our partners to display targeted and relevant messages from selected third party websites based on your viewing behaviour on our website and certain other websites.</p>
							<p>We share your data with our partners with strict confidentiality only for the intention of linking you with relevant advertisement which may be your interest. You may opt out of such advertisement at any time. However, we have no control over the use of cookies by our partners.</p>
						</li>
					</ol>
				</div>
				
			</div>

		</section><!-- #content end -->

@endsection
