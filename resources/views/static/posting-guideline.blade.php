@extends('layouts.app')

@section('content')

<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
<div class="container clearfix">
<ol class="breadcrumb">
<li class="breadcrumb-item"><a href="/">Home</a></li>
<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong></li>
</ol>
</div>
</section><!-- #page-title end -->
		

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
						<h3>Recommended Guidelines<br>
							<small>Updated on: 2nd August 2020</small>
					</h3>
					
					<p>WIKABO is a place which empowers people with the ability and opportunity to share and grow the world’s knowledge by unlocking the power of transparency, web intelligence and limitless sharing of knowledge. We believe everyone should have fair access to information needed to forge qualitative decisions and learnings.</p>
					<p>All input of information, comments and/or reviews are purely based on each individual’s preference even if it is meant for personal and/or business reviews and WIKABO will not tolerate any false, abusive and/or fraudulent information or review inputs.</p>
					<p>WIKABO encourages every user to follow the following guidelines in order to make this place an informative, brilliant, convenient and educational platform. For more detailed policies, kindly visit each policy listed.</p>
					<p>By using WIKABO platform, users agreed to indemnify and keep WIKABO indemnified at all times against any forms of claims and/or loss resulted directly or indirectly by the use of this platform. The only remedy to such disagreement is to refrain from using the WIKABO platform.</p>

					
					<ol>
						<li class="bottommargin-sm">
							<h4 class="nomargin">Be factual</h4>
							<p>Review, Comments and Feedback on Products, Services and Response to Questions and Answer must be Factual. WIKABO does not allow and/or approve any fake reviews and false allegations to be made against another. Any unlawful incitation may be reported to the relevant authorities if necessary.</p>
						</li>
						<li class="bottommargin-sm">
							<h4 class="nomargin">Be Responsible</h4>
							<p>Every user is obliged to shoulder their own responsibilities when they decide to leave any comments, reviews, feedback, questions and answers. WIKABO does not and will not engage, involve and/or interfere in any defence and argument against each user’s opinion.</p>
						</li>
						<li class="bottommargin-sm">
							<h4 class="nomargin">Respect Each Other</h4>
							<p>Everyone on WIKABO is here to make the platform a great resource for themselves. It is permissible to disagree amongst each other, but we urge you to be factual, civilised, considerate and respectful to the individuals / businesses / service providers and other contributors. WIKABO strongly emphasize in equality as our platform is a place for everybody to learn and share. We do not accept any toleration over contents which demonstrates any form of discrimination based on age, race, colour, gender, religion, nationality, ethnicity, political group, sexual orientation or any other similar characteristic. Statement, Discussions, Topics, Reviews should be phrased as neutrally as much as possible.</p>
							<p>Abusive behavior directed towards private individuals is not allowed. Repeated and unwanted contact constitutes a form of harassment. Content should not contain any threats or advocate, any form of harassment, violence against others, including self-harm.</p>
							<ol type="a" class="leftmargin-xs">
								<li><strong>Deceptive and Intrusion of Other’s Identity</strong><br>
									<p class="bottommargin-xs">Your registered profile should be created using your real name or legally incorporated business name based on accurate and truthful credentials. Impersonation of another, act as an entity without authorization, or creation of multiple accounts are absolutely not allowed at WIKABO.
									</p>
								</li>
								<li><strong> Intellectual Property and Personal Rights</strong><br>
									<p class="bottommargin-xs">Kindly refrain from posting any content that infringes intellectual property or personal rights of others. Intellectual Property taken from other sources should be properly attributed back to the rightful owner. <a href="https://www.wikabo.com/trademark-policy" target="_blank">Learn More</a> about Intellectual Property.
									</p>
								</li>
							</ol>
						</li>
						<li class="bottommargin-sm">
							<h4 class="nomargin">No Spam</h4>
							<p class="bottommargin-xs">Kindly refrain from using WIKABO to post or attract spam which may cause disturbance to other users. WIKABO will enforce the removal of spams. <a href="https://www.wikabo.com/terms-of-use" target="_blank">Learn More</a></p>
						</li>
						<li>
							<h4 class="nomargin">No Malicious or Illegal Activity</h4>
								<p class="bottommargin-xs">Kindly refrain from introducing viruses, malware, and other malicious programs or engage in activity that harms or interferes with the operation of WIKABO or seeks to circumvent security or authentication measures. Do not use WIKABO to engage and/or involve in unlawful activity or to promote illegal acts.</p>
						</li>
						<li>
							<h4 class="nomargin">Abide by Other WIKABO Policies</h4>
							<p class="bottommargin-xs">Kindly follow WIKABO’s other policies, which WIKABO may add, remove or change from time to time without further reference to you.<a href="https://www.wikabo.com/privacy-policy" target="_blank">Learn more</a></p>
						</li>
					</ol>
						<h4 class="nomargin">Reporting Problems</h4>
						<p class="bottommargin-xs">If you do notice anything on WIKABO that you believe violates our guidelines or policies, kindly report it to us via our <a href="https://www.wikabo.com/contact" target="_blank">contact form</a>.</p>
						<p class="bottommargin-xs">Violating the <i>General Guideline</i> may result in the removal of content, and/or limitation or termination of a user’s access to WIKABO.</p>
				</div>
				
			</div>

		</section><!-- #content end -->
		
@endsection
