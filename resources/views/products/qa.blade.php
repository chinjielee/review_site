@extends( 'layouts.app' )
@section('headCss')
<style>
.pagination-custom{
	text-align:right !important;
}

.pagination-custom .pagination {
	margin-top:0;
}

.showAllAns{
	max-height:fit-content !important;
}
.ownQnA{
	border: 1px solid #0066cc;
}

</style>
@endsection
@section( 'content' )
<!-- Content
=============================================-->
@include( 'products.product_breadcrumb' )
@include('products.top_summary_detail')
@include( 'products.product_nav_bar' )
<section id="content" class="nobottommargin">

	<div class="content-wrap" style="padding: 40px 0px;">
	<div class="container clearfix">
		@if(Session::get('errors')||count( $errors ) > 0)
			<div class="alert alert-danger">
			@foreach ($errors->all() as $error)
				<i class="icon-remove-sign"></i> {{ $error }} </br>
			@endforeach
			</div>
		@endif
		@if(count($arrPendingQuestion) > 0)
			<div class="title-block">
				<h3><span>@lang('product.Pending Questions') ({{count($arrPendingQuestion)}})</span></h3>
				<span>@lang('product.Total') {{count($arrPendingQuestion)}} @lang('product.questions and answers').</span>
			</div>
			
			<ol class="commentlist qa-item noborder nomargin nopadding clearfix">
			@foreach($arrPendingQuestion as $objPendingQuestion)
				<li class="comment bottommargin-xs">
					<div class="comment-wrap clearfix">
						<div class="comment-meta">
							<div class="comment-author vcard">
								<span class="comment-avatar clearfix ">
								<img src="{{ asset('img/avatar.png') }}" height="60" width="60"></span>
							</div>
						</div>
						<div class="comment-content clearfix">
							<div class="comment-author clearfix">
								<h4 class="nomargin" style="display: inline-block;font-weight: bold;"><a href="#" rel="external nofollow" class="url">
									@if($objPendingQuestion->anonymous != 1)
										{{$arrPendingQuestion->user->first_name}}</a>
									@else
										@lang('product.Anonymous')</a>
									@endif
									
								</h4>
								<div class="share_btn_group pull-right" style="display: inline-block;">
									<a href="#" class="social-icon si-rounded si-small si-trash nobottommargin">
										<i class="icon-trash"></i>
										<i class="icon-trash"></i>
									</a>
								</div>
								<span>{{$objPendingQuestion->updated_at->diffForHumans()}}</span>
							</div>
							<hr>
							<p><strong>{{$objPendingQuestion->question_description}}</strong></p>
						</div>
				</li>
			@endforeach
			</ol>
			</div>
		@endif
		<div class="container clearfix">
			<div class="title-block">
				<h3><span>@lang('product.Questions &amp; Answers') ({{$objProductCountDetail->questionCount}})</span></h3>
				<span>@lang('product.Total') {{$objProductCountDetail->qnaCount}} @lang('product.questions and answers').</span>
			</div>

			<ol class="commentlist qa-item noborder nomargin nopadding clearfix">
			@foreach($arrProductQuestionAnswer as $objQuestion)
				<!-- Questions -->
				<li class="comment bottommargin-xs">
					<div class="comment-wrap clearfix">
						<div class="comment-meta">
							<div class="comment-author vcard">
								<span class="comment-avatar clearfix @if($objQuestion->user->owner) ownQnA @endif">
								<img src="{{ asset('img/avatar.png') }}" height="60" width="60"></span>
							</div>
						</div>
						<div class="comment-content clearfix">
							<div class="comment-author clearfix">
								<h4 class="nomargin" style="display: inline-block;font-weight: bold;"><a href="#" rel="external nofollow" class="url">
									@if($objQuestion->anonymous != 1)
										{{$objQuestion->user->first_name}}
									@else 
										@lang('product.Anonymous')
									@endif
									</a>
								</h4>
								<div class="share_btn_group pull-right" style="display: inline-block;">
										
								<!--<a href="#" class="social-icon si-rounded si-small si-twitter nobottommargin">
									<i class="icon-twitter"></i>
									<i class="icon-twitter"></i>
								</a>
								<a href="#" class="social-icon si-rounded si-small si-facebook nobottommargin">
									<i class="icon-facebook"></i>
									<i class="icon-facebook"></i>
								</a>-->
								<a href="#">
									<i class="i-rounded i-bordered i-small icon-warning-sign notopmargin"></i>
								</a>
										

										</div>
								<span>{{$objQuestion->updated_at->diffForHumans()}}</span>
							</div>
							<hr>
							<p><strong>{{$objQuestion->question_description}}</strong></p>
						</div>
						<div class="clear"></div>
						<ul class="children qa-content">
							<!-- Answer -->
							@foreach($objQuestion->answer as $answer)
								<li class="comment byuser comment-author-_smcl_admin odd alt depth-2" id="li-comment-3">
									<div id="comment-3" class="comment-wrap clearfix" style="background-color: #f5f5f5;">
										<div class="comment-meta">
											<div class="comment-author vcard">
												<span class="comment-avatar clearfix @if($answer->user->owner) ownQnA @endif">
												<img src="{{ asset('img/avatar.png') }}" height="40" width="40"></span></span>
											</div>
										</div>
										<div class="comment-content clearfix">
											<div class="comment-author"><a href="#" rel="external nofollow" class="url">
												@if($answer->anonymous != 1)
													{{$answer->user->first_name}}
												@else
													@lang('product.Anonymous')
												@endif
												
												</a>
											<span>{{$answer->updated_at->diffForHumans()}}</span>
											</div>
											<p>{{$answer->answer}}</p>
										</div>
										<div class="clear"></div>
									</div>
								</li>
							@endforeach
						</ul>
						@if($objQuestion->answer->total() > 1)
						<div class="center"><a href="" class="border_bottom_dotted" onclick="return showAllQa(this,event,{{$objQuestion->answer->total()}});"><strong> @lang('product.Show All') {{$objQuestion->answer->total()}} @lang('product.Replies')</strong></a></div>
						@endif
						<div class="section helpful_wrapper topmargin-sm nobottommargin nopadding">
							<p class="nomargin padding-xs clearfix">
								<!-- <span>-1</span>
								<i class="i-circled i-light icon-thumbs-down2 i-small nomargin" style="float: none;"></i> 
								<i class="i-circled i-light icon-thumbs-up2 i-small nomargin" style="float: none;"></i> 
								<span>5 votes <span class="hidden-sm hidden-xs">this review was helpful.</span> </span> -->
								<span class="pull-right">
								@if(!empty(Auth::user()->user_id))
									<a href="#" class="button button-border button-small button-rounded button-fill fill-from-bottom button-black nott pull-right nomargin" data-question-id='{{$objQuestion->product_questions_id}}' data-toggle="modal" data-target="#answer_now" onclick="answerQuestion(this);return false;"><span>@lang('product.Answer Now')</span></a>
									<span class="pull-right hidden-sm hidden-xs" style="margin-top: 5px; margin-right: 5px;">@lang('product.Want to be part of this discussion')?</span>
									</span>	
								@else
									<a href="{{route('login')}}" class="button button-border button-small button-rounded button-fill fill-from-bottom button-black nott pull-right nomargin" ><span>@lang('product.Login Now')</span></a>
									<span class="pull-right hidden-sm hidden-xs" style="margin-top: 5px; margin-right: 5px;">@lang('product.Want to be part of this discussion')?</span>
									</span>	
								@endif
								
							</p>
						</div>
						<div id="answer_now" class="modal fade">
							<div class="modal-dialog">
								<div class="modal-body">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal">×</button>
											<h4 class="modal-title" id="myModalLabel">@lang('product.Answer Now')</h4>
										</div>
										<form id='answer_now_form' action="{{route('postAnswer')}}" method="post">
										{{ csrf_field() }}
										<div class="modal-body">
											<textarea id="answer" class="form-control" rows="5" name="answer" placeholder="@lang('product.Write your answer here')"></textarea>
										</div>
										<div class="modal-footer text-center">
											@lang('product.Answer anonymously') <input type='checkbox' name='anonymous'> 
											<div id='ans_question_error' class="hide text-danger"></div>
											<input  id='ans_question' type="submit" class="btn btn-blue btn-rounded ripple" value="@lang('product.Post Now')" onclick="return false;">
											<input type='hidden' id='question_id' name='product_questions_id' value=''>
											<input type='hidden' name='psid' value='{{$arrDetail->product_services_id}}'>
											<input type='hidden' name='url' value='{{url()->current()}}'>
											<button type="button" class="btn btn-default btn-rounded ripple" data-dismiss="modal">@lang('product.Cancel') </button>
										</form>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
				@endforeach
			</ol>
			@if($arrProductQuestionAnswer->total() > 0)
			<div class="row clearfix topmargin-sm bottommargin-sm">
				<div class="col-sm-6">
					<strong>{{$arrProductQuestionAnswer->currentPage()}} of {{$arrProductQuestionAnswer->lastPage()}} </strong> @lang('product.pages')
				</div>
				<div class="col-sm-6 pagination-custom">
					{{$arrProductQuestionAnswer->links()}}
				</div>
			</div>
			@endif
		<div class="promo promo-light bottommargin">
		<h4 class="nomargin">@lang('product.Would like to ask any question')?</h4>
		@if(!empty(Auth::user()->user_id))
			<a href="#" target="_blank" class="button button-blue button-3d button-rounded nott" data-toggle="modal" data-target="#post_qa_now"><i class="icon-line2-speech"></i> @lang('product.Post Your Question Now')</a>
		@else
			<a href="{{route('login')}}" target="_blank" class="button button-blue button-3d button-rounded nott"><i class="icon-line2-speech"></i> @lang('product.Login Now')</a>
		@endif
			</div>
			<div id="post_qa_now" class="modal fade">
				<div class="modal-dialog">
					<div class="modal-body">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">×</button>
								<h4 class="modal-title" id="myModalLabel">@lang('product.Post Your Question Now')</h4>
							</div>
							<form id='ask_now_form' action="{{route('postQuestion')}}" method="post">
								{{ csrf_field() }}
								<div class="modal-body">
									<textarea id="question_description" name="question_description" class="form-control" rows="5" placeholder="Post your question here"></textarea>
								</div>
								<div class="modal-footer text-center">
									@lang('product.Ask anonymously') <input type='checkbox' name='anonymous'> 
									<div id='ask_question_error' class="hide text-danger"></div>
									<input id='ask_question' type="submit" class="btn btn-blue btn-rounded ripple" value="@lang('product.Post Now')">
									<input type='hidden' name='psid' value='{{$arrDetail->product_services_id}}'>
									<input type='hidden' name='url' value='{{url()->current()}}'>
									<button type="button" class="btn btn-default btn-rounded ripple" data-dismiss="modal">@lang('product.Cancel') </button>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
	</div>
	</div>
</section>
<script>
		function answerQuestion(ele){
			console.log($(ele).data('question-id'));
			$('#question_id').val($(ele).data('question-id'));
		}
</script>
@endsection
@section( 'tailJs' )
<script>
	var verifyAnswer = '{{route("postAnswerVerify")}}';
	var verifyQuestion = '{{route("postQuestionVerify")}}';
	var strShowLessQaText = "<strong>@lang('product.Minimize replies')</strong>";
		function showAllQa(ele, event, total) {
			var strShowMoreQaText = "<strong> @lang('product.Show All') "+total+" @lang('product.Replies')</strong>";
			event.preventDefault();
			var ansEle = $(ele).parent().siblings().closest('ul');
			if(ansEle.hasClass('showAllAns')){
				ansEle.removeClass('showAllAns');
				$(ele).html(strShowMoreQaText);
			} else {
				ansEle.addClass('showAllAns');
				$(ele).html(strShowLessQaText);
			}
			return false;
		}
		// Verify Answer
		$('#ans_question').on('click',function(){
			$.ajax({
				method: "POST",
				url: verifyAnswer,
				json:true,
				data: {_token: $('meta[name="csrf-token"]').attr('content'),
						answer: $('#answer').val()
				}
			})
			.done(function( result ) {
				if(result !== "ok"){
					result = JSON.parse(result);
					if(result.status == "error"){
						$('#ans_question_error').html(result.error_message).removeClass('hide');
					}
				} else {
					$('#answer_now_form').submit();
				}
			})
			.error(function(err, result, wat) {
				$('#ans_question_error').html("Somethings goes wrong").removeClass('hide');
			})
			.always(function() {
				return false;
			});
			return false;
		});
		// Verify Questions
		$('#ask_question').on('click',function(){
			$.ajax({
				method: "POST",
				url: verifyQuestion,
				json:true,
				data: {
					_token: $('meta[name="csrf-token"]').attr('content'),
					question_description: $('#question_description').val()
				},
				beforeSend: function(  ) {
					$('#loading').show();
				}
			})
			.done(function( result ) {
				if(result !== "ok"){
					result = JSON.parse(result);
					if(result.status == "error"){
						$('#ask_question_error').html(result.error_message).removeClass('hide');
					}
				} else {
					$('#ask_now_form').submit();
				}
			})
			.error(function(result) {
				$('#ask_question_error').html("Somethings goes wrong").removeClass('hide');
			})
			.always(function() {
				$('#loading').hide();
			});
			return false;
		});
	// }
</script>
@endsection