<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="{{route('home')}}">@lang('product.Home')</a>
			</li>
			<li class="breadcrumb-item"><a href="{{route(ucfirst($pageInfo->route['label']).' All List')}}">{!! ucfirst(str_replace(['_','-'], ' ', $pageInfo->route['label'])) !!}</a>
            </li>
            @if(!empty($pageInfo->route['category']) && !empty($pageInfo->route['label']))
                <li class="breadcrumb-item">
                    <a class="text-capitalize" href="{{route('Category '.ucfirst($pageInfo->route['label']).' List', $pageInfo->route)}}">{!! str_replace(['_','-'], ' ', $pageInfo->route['category']) !!}</a>
                </li>
            @endif
            @if(!empty($pageInfo->route['sub_category']) && !empty($pageInfo->route['label']))
                <li class="breadcrumb-item">
                    <a class="text-capitalize" href="{{route(ucfirst($pageInfo->route['label']).' Sub Category List', $pageInfo->route)}}">{!! str_replace(['_','-'], ' ', $pageInfo->route['sub_category']) !!}</a>
                </li>
            @endif
            @if(!empty($pageInfo->route['product_slug']))
                <li class="breadcrumb-item active" aria-current="page">
                    <strong class="text-capitalize">{!! str_replace(['_','-'], ' ', $pageInfo->route['product_slug']) !!} - {{$pageInfo->subpage}}</strong>
                </li>
            @endif
		</ol>
	</div>
</section>