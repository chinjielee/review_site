@extends( 'layouts.app' )

@section( 'content' )

<!-- Content
===
===
=== === === === === === === === === === === === === -->

<style>
	.pricing-title h3,
	label {
		color: #000;
	}
	
	.section {
		background-color: #F9F9F9;
	}
	
	#page-menu nav li.active a {
		background-color: #0256ae;
	}
	
	.pricing-box.pricing-extended .pricing-features li {
		padding: 0px 0 10px;
		font-size: inherit;
		width: 100%;
	}
	
	.pricing-price {
		color: #000;
		font-weight: 900;
	}
</style>
@include( 'products.product_breadcrumb' )
@include( 'products.top_summary_detail' )
@include( 'products.product_nav_bar' )
	<section id="content" style="margin-bottom: 0px;">

		<div class="content-wrap" style="padding-top: 40px;">
			<div class="container clearfix">
				<div class="title-block">
					<h3><span>@lang('product.Product Photos')</span></h3>
					<span>@lang('product.View') {{$arrDetail->product_service_title}} @lang('product.photos')</span>
				</div>
				@foreach($arrImages as $img)
					<img src="{{$img['path']}}" alt="{{$arrDetail->product_service_title}}">
				@endforeach
			</div>
		</div>

	</section>

@endsection