@extends( 'layouts.app' )

@section( 'content' )
<style>
	.top-review-overflow{
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
	i.icon-thumbs-up2{
		font-size: 0.92em;
		text-shadow: none;
	}
	.rating-row-rate{
		font-size:12px;
		margin-top:-5px;
	}
	.rating-row-title{
		line-height:30px;
	}
	.rating-row{
		min-height:30px;
	}
	@media (max-width: 767px) {
		.list_mainheader img {
			max-width: 180px
		}
	}
</style>
@if(!$error || !empty($objPendingProductReviewDetail))
<div class="section nomargin nobottompadding" style="background: url({{ asset('img/review_bg.jpg') }}) center;">
	<div class="container clearfix">
		<div class="pricing-box pricing-extended clearfix">
			<div class="pricing-desc">
				<div class="pricing-title">
					<div class="clearfix">
						<h3 class="nott nomargin" style="display: inline-block;">{{$objPendingProductReviewDetail->product_name}} (@lang('product.Pending Approval'))</h3>
					</div>
				</div>
				<div class="pricing-features bottommargin-sm">
					<div class="row list_mainheader">
						<div class="col-md-3 col-sm-4 center">
                            <img src="{{ asset('img/empty-compare.jpg') }}" alt="Product Name" class="topmargin-sm">
						</div>
						<div class="col-md-9 col-sm-8 topmargin-sm">
							<div class="row">
								<div class="col-sm-6" style="border-right: 1px dashed #ddd; min-height: 200px;">
									<label for="text">@lang('product.Product Details'): </label></br>
                                    <ul>
                                    @foreach($objPendingProductReviewDetail->product_attributes as $key => $val)
                                        <li><strong>{{ucfirst($key)}}</strong> : {{$val}}</li>
                                    @endforeach
                                    </ul>
								</div>
								<hr class="visible-xs">
                                <div class="col-sm-6" style="border-right: 1px dashed #ddd; min-height: 200px;">
                                    <label for="text">@lang('product.Reviews'): </label></br>
                                    <strong>{{$objPendingProductReviewDetail->review_title}}</strong></br>
                                    <p>{{$objPendingProductReviewDetail->comments}}</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="pricing-action-area">
					<div class="pricing-meta">@lang('product.Overall Rating')</div>
					<div class="pricing-price">
						{{number_format($objPendingProductReviewDetail->rating->overall, 2, '.', ',')}}
					</div>
					<input id="rating-overall" type="number" class="rating rating-input rating-loading show-rating-wikabo" value="{{$objPendingProductReviewDetail->rating->overall}}" max="5" data-stars="5" data-size="xs">
					<div class="pricing-action">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endif
<script>
	$( document ).ready(function() {
		$(".show-rating-wikabo").rating({
			containerClass: 'is-heart',
			filledStar: '<i class="icon-thumbs-up2"></i>',
			emptyStar: '<i class="icon-thumbs-up"></i>',
			showClear:0,
			showCaption:0,
			displayOnly:1
		});
	});
</script>
@endsection