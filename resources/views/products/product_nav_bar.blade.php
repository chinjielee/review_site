<div id="page-menu" class="product_submenu_wrapper">
		<div id="page-menu-wrap">
			<div class="container clearfix">
				<nav class="one-page-menu product_submenu center">
					<ul>
                        <li @if($pageInfo->subpage == 'review')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['review']}}">
								<div>@lang('product.Reviews') ({{$objProductCountDetail->reviewCount}})</div>
							</a>
						</li>
						<li @if($pageInfo->subpage == 'Question and Answer')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['qa']}}">
								<div>@lang('product.Q&amp;A') ({{$objProductCountDetail->qnaCount}})</div>
							</a>
						</li>
						<li @if($pageInfo->subpage == 'detail')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['detail']}}">
								<div>@lang('product.Product Details')</div>
							</a>
						</li>
						@if($arrDetail->parent_cat_1_id != 3 && $arrDetail->parent_cat_2_id != 3)
						<!--<li @if($pageInfo->subpage == 'price')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['price']}}">
								<div>@lang('product.Price')</div>
							</a>
						</li>-->
						<li >
							<a href="{{route('Compare',[$arrDetail->product_slug,'',''])}}">
								<div>@lang('product.Compare')</div>
							</a>
						</li>
						@endif
						<li @if($pageInfo->subpage == 'stat')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['stat']}}">
								<div>@lang('product.Stats')</div>
							</a>
						</li>
						<li @if($pageInfo->subpage == 'photo')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['photo']}}">
								<div>@lang('product.Product Photos')</div>
							</a>
						</li>
					</ul>
				</nav>
				<div id="page-submenu-trigger"><i class="icon-reorder"></i>
				</div>
			</div>
		</div>
	</div>