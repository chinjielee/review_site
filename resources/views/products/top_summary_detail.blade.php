<style>
	.top-review-overflow{
		overflow: hidden;
		text-overflow: ellipsis;
		white-space: nowrap;
	}
	i.icon-thumbs-up2{
		font-size: 0.92em;
		text-shadow: none;
	}
	.rating-row-rate{
		font-size:12px;
		margin-top:-5px;
	}
	.rating-row-title{
		line-height:30px;
	}
	.rating-row{
		min-height:30px;
	}
	@media (max-width: 767px) {
		.list_mainheader img {
			max-width: 180px
		}
	}
</style>
<div class="section nomargin nobottompadding" style="background: url({{ asset('img/review_bg.jpg') }}) center;">
	<div class="container clearfix">
		<div class="pricing-box pricing-extended clearfix">
			<div class="pricing-desc">
				<div class="pricing-title">
					<div class="clearfix">
						<h3 class="nott nomargin" style="display: inline-block;">{{$arrDetail->product_service_title}}</h3>
						<!--<a href="#" class="social-icon si-light si-small si-rounded si-facebook pull-right nobottommargin">
						<i class="icon-facebook"></i>
						<i class="icon-facebook"></i>
						</a>
						<a href="#" class="social-icon si-rounded si-light si-small si-twitter pull-right nobottommargin">
						<i class="icon-twitter"></i>
						<i class="icon-twitter"></i>
						</a>-->
					</div>
				</div>
				<div class="pricing-features bottommargin-sm">
					<div class="row list_mainheader">
						<div class="col-md-3 col-sm-4 center">
							@if (!empty($arrDetail->images))
								<img src="{{$arrDetail->images[0]['path']}}" alt="Product Name" class="topmargin-sm">
							@else 
								<img src="{{ asset('img/empty-compare.jpg') }}" alt="Product Name" class="topmargin-sm">
							@endif
						</div>
						<div class="col-md-9 col-sm-8 topmargin-sm">
							<div class="row">
								<div class="col-sm-6" style="border-right: 1px dashed #ddd; min-height: 200px;">
									<label for="text">@lang('product.Review Rating'): </label></br>
									@foreach($objProductTopDetail->rating as $key => $val)
										@if($key !== "rate_count" && $key !== "overall")
											<div class="row rating-row">
												<div class="col-md-6 col-sm-6 col-xs-6 rating-row-title">
													<p class="nobottommargin">{{$key}}</p>
												</div>
												<div class="col-md-6 col-sm-6 col-xs-6 rating-row-rate">
													<input id="rating-cat-1" type="number" class="rating rating-input rating-loading show-rating-wikabo" value="{{$val}}" max="5" data-stars="5" data-size="xs">
												</div>
											</div>
										@endif
									@endforeach
								</div>
								<hr class="visible-xs">
								<div class="col-sm-6">
									<label for="text">@lang('product.Helpful Reviews'):</label>
									<ul class="clearfix nopadding">
									@forelse($objProductTopDetail->topReview as $review)
										<li class="top-review-overflow">
											<i class="icon-line2-speech"></i> {{$review->comments}}
										</li>	
									@empty			
										<li><a href="{{route('Create Review',['product_slug'=>$arrDetail->product_slug])}}" style="border-bottom: 1px dotted;"><i class="icon-line2-action-redo"></i> @lang('product.Review Now')</a></li>
									@endforelse
									@if(!$objProductTopDetail->topReview->isEmpty())
										<li><a href="{{$pageInfo->url['review']}}" style="border-bottom: 1px dotted;"><i class="icon-line2-action-redo"></i> @lang('product.View all reviews')</a></li>
									@endif
								</ul>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="pricing-action-area">
					<div class="pricing-meta">@lang('product.Overall Rating')</div>
					<div class="pricing-price">
						{{number_format($objProductTopDetail->rating->overall, 2, '.', ',')}}
					</div>
					<input id="rating-overall" type="number" class="rating rating-input rating-loading show-rating-wikabo" value="{{$objProductTopDetail->rating->overall}}" max="5" data-stars="5" data-size="xs">
					<span class="price-tenure nott">({{$objProductTopDetail->rating->rate_count}} @lang('product.reviews'))</span>
					<div class="pricing-action">
						<a href="{{route('Create Review',['product_slug'=>$arrDetail->product_slug])}}" class="button button-3d button-large btn-block nott">@lang('product.Write a review')</a>
						<a href="{{route('Products Question and Answer1',$pageInfo->route)}}" class="button button-3d button-large button-border button-blue btn-block nott">@lang('product.Ask a question')</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$( document ).ready(function() {
		$(".show-rating-wikabo").rating({
			containerClass: 'is-heart',
			filledStar: '<i class="icon-thumbs-up2"></i>',
			emptyStar: '<i class="icon-thumbs-up"></i>',
			showClear:0,
			showCaption:0,
			displayOnly:1
		});
	});
</script>