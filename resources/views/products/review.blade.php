@extends( 'layouts.app' )
@section('headCss')
<style>
.pagination-custom{
	text-align:right !important;
}

.pagination-custom .pagination {
	margin-top:0;
}
.rating-stars span.star{
	font-size:20px;
}
.ownReview{
	border: 1px solid #0066cc !important;
}
.voted{
	background-color: #0066cc;
    color: #FFF;
    text-shadow: 1px 1px 1px rgba(0,0,0,0.3);
    box-shadow: 0 0 0 rgba(0,0,0,0.2);
}
.reported{
	background-color: #e25252 !important;
	color: white;
    border-color: #e25252;
}
</style>
@endsection
@section( 'content' )
<!-- Content====== === === === === === === === === === === === === == -->
@include( 'products.product_breadcrumb' )
@include( 'products.top_summary_detail' )
@include( 'products.product_nav_bar' )
<section id="content" class="nobottommargin">

	<div class="content-wrap" style="padding: 40px 0px;">

		<div class="container clearfix">
			<div class="title-block">
				<h3><span>@lang('product.Reviews') ({{ $arrProductReview->total() }})</span></h3>
				<span>@lang('product.Total') {{$objProductCountDetail->reviewUserCount}} @lang('product.people shared their reviews')</span>
			</div>
			<ol class="commentlist review-item noborder nomargin nopadding clearfix">
			@foreach($arrProductReview as $objReview)
			<li class="comment even thread-even depth-1 noleftmargin" id="li-comment-1">
					<div id="comment-1" class="comment-wrap clearfix">
						<div class="row clearfix">
							<div class="comment-meta avatar_details_wrapper col-md-2 col-sm-3 col-xs-12 center">
								<div class="">
									@if($objReview->anonymous == 1)
										<img src="{{ asset('img/avatar.png') }}" class="avatar avatar-60 photo avatar-default" height="60" width="60"><br>	
										<h4 class="nobottommargin"><a href="#" class="user_name url">@lang('product.Anonymous')</a><br><small></small></h4>	
									@else
										<img src="{{ asset('img/avatar.png') }}" class="avatar avatar-60 photo avatar-default @if($objReview->user->owner) ownReview @endif" height="60" width="60"><br>	
										<h4 class="nobottommargin"><a href="#" class="user_name url">{{$objReview->user->first_name}}</a><br><small>{{$objReview->user->country}}</small></h4>
										<p class="nomargin">@lang('product.reviews')</p>
										@if(!$objReview->user->owner)
											<form method="post"  action="{{route('Post Chat')}}">
												<input type="hidden" name="_token" value="{{ csrf_token() }}">
												<input type="hidden" name="tu" value="{{$objReview->user->user_id}}">
												<p class="nomargin"><a href="#" class="start_conversation_id review-messenger"><i class="material-icons" >mail_outline</i>Messenger</a></p>
											</form>	
										@endif
									@endif
									
								</div>
							</div>
							<div class="divider divider-center visible-xs bottommargin-xs"><i class="icon-cloud"></i>
							</div>
							<div class="col-md-10 col-sm-9 col-xs-12 review_details_wrapper">
								<div class="comment-content clearfix">
									<div class="comment-author clearfix">
										<h3 class="nomargin" style="display: inline-block;font-weight: bold;">{{$objReview->review_title}}</h3>
										<div class="share_btn_group pull-right" style="display: inline-block;">
										
											<!--<a href="#" class="social-icon si-rounded si-small si-twitter nobottommargin">
												<i class="icon-twitter"></i>
												<i class="icon-twitter"></i>
											</a>
											<a href="#" class="social-icon si-rounded si-small si-facebook nobottommargin">
												<i class="icon-facebook"></i>
												<i class="icon-facebook"></i>
											</a>-->
											<a onclick="return false;">
												@if(isset($arrUserFraudReview[$objReview->product_reviews_id]))
													<i id="report_fraud_{{$objReview->product_reviews_id}}" class="i-rounded i-bordered i-small icon-warning-sign notopmargin report-fraud reported" data-review-id="{{$objReview->product_reviews_id}}"></i>
												@else
													<i id="report_fraud_{{$objReview->product_reviews_id}}" class="i-rounded i-bordered i-small icon-warning-sign notopmargin report-fraud" data-review-id="{{$objReview->product_reviews_id}}"></i>
												@endif
											</a>
										</div>
										<span>@lang('product.Reviewed Date'): {{$objReview->updated_at->diffForHumans()}}</span>
									</div>
									<hr class="bottommargin-xs">
									<div class="review_list_rating"> <strong>@lang('product.Overall Rating'): </strong>
									<!-- <div class="rating-stars" style="line-height: 20px;display: inline-block;font-size: 20px;vertical-align: sub;"><span class="filled-stars nomargin" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span>
										</div> -->
										<div class="rating-stars" style="line-height: 20px;display: inline-block;font-size: 20px;vertical-align: sub;">
											<input id="rating-overall" type="number" class="rating rating-input rating-loading show-rating-wikabo" value="{{$objReview->rating->overall}}" max="5" data-stars="5" data-size="xs">
										</div>
										<small style="margin-left: 10px"><a href="#" class="border_bottom_dotted" data-toggle="modal" data-target="#details_rating">(@lang('product.click here for details rating'))</a></small>
										<!--helen add : pls make it function, currently curi other code-->
										<div id="details_rating" class="modal fade">
										<div class="modal-dialog">
										<div class="modal-body">
											<div class="modal-content">
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal">×</button>
													<h4 class="modal-title" id="myModalLabel">@lang('product.Rating Details')</h4>
												</div>
												<div class="modal-body">
													<div>
														@foreach($objReview->rating as $key => $val)
															@if($key !== "rate_count" && $key !== "overall")
																<div class="row rating-row">
																	<div class="col-md-6 col-sm-6 col-xs-6 rating-row-title">
																		<p class="nobottommargin">{{ucwords(str_replace('_',' ',$key))}}</p>
																	</div>
																	<div class="col-md-6 col-sm-6 col-xs-6 rating-row-rate">
																		<input id="rating-cat-1" type="number" class="rating rating-input rating-loading show-rating-wikabo" value="{{$val}}" max="5" data-stars="5" data-size="xs">
																	</div>
																</div>
															@endif
														@endforeach
													</div>
												</div>
												<div class="modal-footer center">
													<button type="button" class="button button-3d button-circle button-white  button-light nott" data-dismiss="modal">@lang('product.Close')</button>
												</div>
											</div>
											</div>
											</div>
										</div>
										<!--/ helen add--> 
									</div>
									<div class="review_list_key">
										<strong>@lang('product.Key Highlight'): </strong>
										@foreach($objReview->key_highlight as $highlight)
											<span class="badge badge-secondary">{{$highlight}}</span> 
										@endforeach
									</div>
									<div class="review_list_purchased">
										<div class="row clearfix">
											<div class="col-md-6">
												<strong>@lang('product.Date Purchased'): </strong> <span>{{$objReview->purchased_date}}</span>
											</div>

											<div class="col-md-6">
												<strong>@lang('product.Purchased at'): </strong> <span>{{$objReview->purchased_from}}</span>
											</div>
										</div>

									</div>
									<p>{{$objReview->comments}}</p>
									<div class="row clearfix">
										@foreach($objReview->review_image as $image)
										<div class="col-md-1 col-sm-2 col-xs-3">
											<a href="{{ asset($image) }}" data-lightbox="image">
												<img src="{{ asset($image) }}" alt="Review Image">
												<div class="overlay"><div class="overlay-wrap"><i class="icon-line-zoom-in"></i></div></div>
											</a>
										</div>
										@endforeach
									</div>
									<div class="clear"></div>
									<div class="section helpful_wrapper topmargin-sm nobottommargin nopadding">
									<p class="nomargin padding-xs clearfix">
										<span id='down_vote_count_{{$objReview->product_reviews_id}}' data-count="{{$objReview->down_vote}}" >-{{$objReview->down_vote}}</span>
										@if(isset($arrUserVotedReview[$objReview->product_reviews_id]))
											@if($arrUserVotedReview[$objReview->product_reviews_id]->down == 1 )
												<i id='down_vote' class="i-circled i-light icon-thumbs-down2 i-small nomargin vote-review voted" data-action = 'down' data-review-id="{{$objReview->product_reviews_id}}" style="float: none;"></i> 
											@else 
												<i id='down_vote' class="i-circled i-light icon-thumbs-down2 i-small nomargin vote-review" data-action = 'down' data-review-id="{{$objReview->product_reviews_id}}" style="float: none;"></i> 
											@endif

											@if($arrUserVotedReview[$objReview->product_reviews_id]->up  == 1)
												<i id='up_vote' class="i-circled i-light icon-thumbs-up2 i-small nomargin vote-review voted" data-action = 'up' data-review-id="{{$objReview->product_reviews_id}}" style="float: none;"></i> 
											@else
												<i id='up_vote' class="i-circled i-light icon-thumbs-up2 i-small nomargin vote-review" data-action = 'up' data-review-id="{{$objReview->product_reviews_id}}" style="float: none;"></i> 
											@endif
										@else
											<i id='down_vote' class="i-circled i-light icon-thumbs-down2 i-small nomargin vote-review" data-action = 'down' data-review-id="{{$objReview->product_reviews_id}}" style="float: none;"></i> 
											<i id='up_vote' class="i-circled i-light icon-thumbs-up2 i-small nomargin vote-review" data-action = 'up' data-review-id="{{$objReview->product_reviews_id}}" style="float: none;"></i> 
										@endif
										<span id='up_vote_count_{{$objReview->product_reviews_id}}' data-count="{{$objReview->up_vote}}">{{$objReview->up_vote}}  @lang('product.votes') <span class="hidden-sm hidden-xs">@lang('product.this review was helpful').</span> </span>
										<a href="{{route('Create Review',['product_slug'=>$arrDetail->product_slug])}}" class="border_bottom_dotted">@lang('product.Similar opinion? Write a review to help others')!</a>
										</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</li>
			@endforeach
			</ol>
			<div class="row clearfix topmargin-sm bottommargin-sm">
				<div class="col-sm-6">
				<p class="nomargin">
					<!-- Total Records {{ $arrProductReview->total() }} </br> -->
					<strong>{{$arrProductReview->currentPage()}} of {{$arrProductReview->lastPage()}} </strong> @lang('product.pages')
				</div>
				<div class="col-sm-6 pagination-custom">
					{{$arrProductReview->links()}}
				</div>
			</div>
			<div class="promo promo-light bottommargin">
				<h4 class="nomargin">@lang('product.Your review will help thousands of people')!</h4>
				<a href="{{route('Create Review',['product_slug'=>$arrDetail->product_slug])}}" target="_blank" class="button button-blue button-3d button-rounded nott"><i class="icon-line2-speech"></i> @lang('product.Share Your Review Now')</a>
			</div>
		</div>
	</div>

</section>


@endsection
@section( 'tailJs' )
	<script src="{{ asset('js/sweetalert.min.js') }}"></script>
	<script>
		var urlUpVote = '{{route("postUpVote")}}';
		var urlDownVote = '{{route("postDownVote")}}';
		var urlReport = '{{route("postReportFraud")}}';
		$('.report-fraud').on('click', function(){
			var objThis = $(this);
			var review_id = objThis.data('review-id');
			if(objThis.hasClass('reported')){
				var status = 0;
			}else{
				var status = 1;
			}
			if(objThis.closest('i').data('vote')){

			} else {
				var review_up_count = $('#up_vote_count_'+review_id);
				var review_down_count = $('#down_vote_count_'+review_id);
				// Ajax perform action
				$.ajax({
					method: "POST",
					url: urlReport,
					json:true,
					data: {
						_token: $('meta[name="csrf-token"]').attr('content'),
						product_review_id: review_id,
						url:window.location.href,
						status: status
					},
					beforeSend: function(  ) {
						$('#loading').show();
					}
				})
				.done(function( result ) {
					result = JSON.parse(result);
					if(result.status == "error") {
						swal({
							type: 'error',
							title: 'Oops...',
							text: 'Something went wrong!',
						});
					} else {
						if(status == 0){
							objThis.removeClass('reported');
						}else{
							objThis.addClass('reported');
						}
					}
				})
				.error(function(result) {
					swal({
						type: 'error',
						title: 'Oops...',
						text: 'Something went wrong!',
					})
				})
				.always(function() {
					$('#loading').hide();
				});
			}
		});

		$('.start_conversation_id').on('click', function(e) {
			e.preventDefault();
			var form = $(this).parents('form:first');
			form.submit();
		})

		$('.vote-review').on('click',function() {
			var objThis = $(this);
			var action = objThis.data('action');
			var review_id = objThis.data('review-id');
			if(objThis.closest('i').data('vote')){

			} else {
				var urlVote = "";
				var review_up_count = $('#up_vote_count_'+review_id);
				var review_down_count = $('#down_vote_count_'+review_id);
				if(action == 'up'){
					urlVote = urlUpVote;
				} else {
					urlVote = urlDownVote;
				}
				// Ajax perform action
				$.ajax({
					method: "POST",
					url: urlVote,
					json:true,
					data: {
						_token: $('meta[name="csrf-token"]').attr('content'),
						product_review_id: review_id,
						url:window.location.href
					},
					beforeSend: function(  ) {
						$('#loading').show();
					}
				})
				.done(function( result ) {
					result = JSON.parse(result);
					if(result.status == "error") {
						swal({
							type: 'error',
							title: 'Oops...',
							text: 'Something went wrong!',
						});
					} else {
						review_down_count.html("-"+result.totalDownCount);
						review_up_count.html(""+result.totalUpCount);
						objThis.addClass('voted');
						objThis.siblings().removeClass('voted');
					}
				})
				.error(function(result) {
					swal({
						type: 'error',
						title: 'Oops...',
						text: 'Something went wrong!',
					})
				})
				.always(function() {
					$('#loading').hide();
				});
			}
			
		})
	</script>
@endsection