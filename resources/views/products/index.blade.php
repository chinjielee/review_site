@extends( 'layouts.app' )

@section( 'content' )

<!-- Content==================
=== === === === === === === === === -->

<section id="slider" class="slider-parallax" style="background: url({{ asset('img/review_bg.jpg') }}) center;">
<a href="#" class="button button-full button-blue center tright">
<div class="container clearfix" style="font-size: 18px;">
If the listing you were looking for isn't here, you can create it!  <i class="icon-caret-right" style="top:4px;"></i>
</div>
</a>
	<div class="content-wrap nopadding">
		<div class="container clearfix">
			<div class="heading-block center">
				<h1 class="topmargin" style="color: #fff;">@lang('product.What are you reviewing')?</h1>
				<span style="color: #fff;">@lang('product.Search any products, services or movies on below to review')</span>
				<span>
					<form action="#" method="post" role="form" class="landing-wide-form clearfix">
						<div id='bloodhound' class="col_four_fifth nobottommargin norightmargin">
							<input id='product-search-text' type="text" class="form-control input-lg not-dark typeahead" value="" placeholder="Example: iPhone, Fridges, Washing Machines, TVs...">
						</div>
						<div class="col_one_fifth col_last nobottommargin">
							<button id='product-search' class="btn btn-lg btn-blue btn-block nomargin" value="submit" type="submit" style="">@lang('product.SEARCH')</button>
						</div>
					</form>
				</span>
			</div>
		</div>
	</div>

</section>

<!--Stagging No Search Result-->
<section id="content">
	<div class="content-wrap">
		<div class="container center clearfix" style="opacity: 0.3;">
		<i class="icon-line-search" style="font-size: 90px;"></i>
			<h1 class="nobottommargin">@lang('No Result Found')</h1>
			<h3>@lang('Search any keywords on top search bar')</h3>
		</div>
	</div>
</section>

<!--Stagging Search Result-->
<section id="content">

	<div class="content-wrap">

		<div class="container clearfix">

				<h3 class="center">Seach Results for « iphone »</h3>

				<div class="line topmargin-sm bottommargin-sm"></div>

				<div class="row clearfix">
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
				</div>
				
				<div class="row clearfix">
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
				</div>
				
				<div class="row clearfix">
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
					<div class="col-md-2 center bottommargin">
						<a href="#"><img src="{{ asset('img/Apple-iPhone-7.jpg') }}" alt="Product Name">
							<h5 class="topmargin-sm nobottommargin">Apple iPhone 7<br>
							<small>483 reviews</small>
							</h5>
							<div class="rating-stars"><span class="filled-stars" style="width: 100%;color: #fde16d;"><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span><span class="star"><i class="icon-thumbs-up2"></i></span></span></div>
							<a href="#" class="button button-3d button-rounded button-light button-mini nott">Write a review</a>
						</a>
					</div>
				</div>
			
			</div>
			
		</div>
	
</section>
@endsection
@section('tailJs')
<script>
	let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
	let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
	let search = 'product-search';
	let searchResult = 'product-result';
	let searchInput = 'product-search-text';
</script>
@endsection