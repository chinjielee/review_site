@extends('layouts.app')

@section('content')

<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">

			<div class="container clearfix">
				<ol class="breadcrumb">
					<li><a href="/">@lang('errors.Home')</a></li>
					<li class="active"><strong>@lang('errors.Page Not Found')</strong></li>
				</ol>
			</div>

		</section><!-- #page-title end -->

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">

					<div class="col_half nobottommargin">
						<div class="error404 center">404</div>
					</div>

					<div class="col_half nobottommargin col_last">

						<div class="heading-block nobottomborder">
							<h3 class="nott">@lang('errors.Ooopps.! The Page you were looking for, could not be found.')</h3>
						</div>

						<form action="#" method="get" role="form" class="nobottommargin">
							<div class="input-group input-group-lg">
								<input id='product-search-text' type="text" class="form-control" placeholder="@lang('errors.Find products, services or questions...')">
								<span class="input-group-btn">
									<button class="btn btn-blue" id="product-search" value="submit" type="submit">@lang('errors.Search')</button>
								</span>
							</div>
						</form>

						<div class="col_one_third widget_links topmargin nobottommargin">
							<ul>
								<li><a href="{{ route('About Us') }}">@lang('errors.About Us')</a></li>
								<li><a href="{{ route('All Categories') }}">@lang('errors.Explore Categories')</a></li>
								<li><a href="{{ route('Create Review') }}">@lang('errors.Write a Review')</a></li>
							</ul>
						</div>

						<div class="col_one_third widget_links topmargin nobottommargin">
							<ul>
								<li><a href="{{ route('login') }}">@lang('errors.Sign In')</a></li>
								<li><a href="{{ route('register') }}">@lang('errors.Register')</a></li>
								<li><a href="{{ route('Posting Guideline') }}">@lang('errors.Posting Guidelines')</a></li>
							</ul>
						</div>

						<div class="col_one_third widget_links topmargin nobottommargin col_last">
							<ul>
								<li><a href="{{ route('Term of Use') }}">@lang('errors.Terms of Use')</a></li>
								<li><a href="{{ route('Privacy Policy') }}">@lang('errors.Privacy Policy')</a></li>
								<li><a href="{{ route('Trademark Policy') }}">@lang('errors.Trademark Policy')</a></li>
							</ul>
						</div>

					</div>

				</div>

			</div>

		</section><!-- #content end -->
		
@endsection
@section( 'tailJs' )
			<script>
				let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
				let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
				let search = 'product-search';
				let searchResult = 'product-result';
				let searchInput = 'product-search-text';
			</script>
		@endsection