@extends('layouts.app')
<!-- Additional CSS -->
@section('headCss')

@endsection
<!-- Additional JS -->
@section('headJs')

@endsection
@section('content')


			<section id="slider" class="slider-parallax full-screen" style="background: url({{ asset('img/light_gradient_violet_bg.jpg') }}) center;">
			<div class="slider-parallax-inner">

				<div class="container clearfix">

					<div class="vertical-middle">

						<div class="heading-block center nobottomborder">
							<h1 data-animate="fadeInUp"><strong>Know better, choose better.</strong></h1>
							<span data-animate="fadeInUp" data-delay="300">Can't find what you're looking for? Search here...</span>
							<span>
							<form action="#" method="post" role="form" class="landing-wide-form clearfix">
							<div class="col_four_fifth nobottommargin norightmargin">
								<input type="text" class="form-control input-lg not-dark" value="" placeholder="Find products, services or questions">
							</div>
							<div class="col_one_fifth col_last nobottommargin">
								<button class="btn btn-lg btn-blue btn-block nomargin" value="submit" type="submit" style="">SEARCH</button>
							</div>
						</form>
							</span>
						</div>

					</div>

					<a href="#" data-scrollto="#section-features" class="one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

				</div>

			</div>

		</section>
           
            <!-- Content
            ============================================= -->
            <section id="content">

                <div class="content-wrap">

                    <div class="row clearfix">

                        <div class=" topmargin-sm">

                            <div class="heading-block center bottommargin-sm">
                                <h2>Most Reviewed Products</h2>
                                <span class="divcenter">Browse our most popular reviewed product categories</span>
                            </div>
                            
                            <div id="portfolio" class="portfolio portfolio-nomargin grid-container clearfix">

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/phone-category.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc center" style="position: absolute; top: 10%;">
							<h3>Phone</h3>
							<span>12,039 reweived</span>
						</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/watch-category.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc dark center" style="position: absolute; top: 10%;">
							<h3>Watch</h3>
							<span>10,039 reweived</span>
						</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/cars.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc dark center" style="position: absolute; top: 10%;">
							<h3>Car</h3>
							<span>8,018 reweived</span>
						</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/perfume.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc center" style="position: absolute; top: 10%;">
							<h3>Perfume</h3>
							<span>8,018 reweived</span>
						</div>
							</article>
							</div>
                        </div>

                    </div>
                    
					<div class="row clearfix common-height">

					<div class="col-md-6 center col-padding" style="background: url({{ asset('img/home-banner.png') }}) center center no-repeat; background-size: cover;">
						<div>&nbsp;</div>
					</div>

					<div class="col-md-6 center col-padding" style="background-color: rgb(242, 235, 221);">
						<div>
							<div class="heading-block">
								<span class="before-heading color bottommargin">Understandable &amp; Customizable.</span>
								<h3>Your Opinion Matters</h3>
								<span class="divcenter">Write your first review and help thousands of people choose better.</span>
								
								<div class="center topmargin-lg">
                            <a href="#" class="button button-3d button-rounded button-blue button-xlarge">Write A Review</a>
							</div>
							</div>
						</div>
					</div>

				</div>
                    
                    <div class="row clearfix topmargin-lg">

                        <div class=" topmargin-sm">

                            <div class="heading-block center bottommargin-sm">
                                <h2>Most Reviewed Services</h2>
                                <span class="divcenter">Browse our most popular reviewed product categories</span>
                            </div>
                        </div>

                    </div>
					
					<div class="section dark nomargin" style="background: url({{ asset('img/bg-main-10.jpg') }}) ; padding: 100px 0px;">
					
					<div class="container clearfix">

						<div class="col_one_fourth nobottommargin center" data-animate="bounceIn">
							<i class="i-plain i-large divcenter nobottommargin icon-time"></i>
							<div class="counter counter-lined"><span data-from="10" data-to="80" data-refresh-interval="50" data-speed="4000" data-comma="true"></span>+</div>
							<h5>Hours per Week</h5>
						</div>

						<div class="col_one_fourth nobottommargin center" data-animate="bounceIn" data-delay="200">
							<i class="i-plain i-large divcenter nobottommargin icon-code"></i>
							<div class="counter counter-lined"><span data-from="100" data-to="25841" data-refresh-interval="50" data-speed="2500" data-comma="true"></span></div>
							<h5>Lines of Code</h5>
						</div>

						<div class="col_one_fourth nobottommargin center" data-animate="bounceIn" data-delay="400">
							<i class="i-plain i-large divcenter nobottommargin icon-briefcase"></i>
							<div class="counter counter-lined"><span data-from="100" data-to="923" data-refresh-interval="50" data-speed="3500" data-comma="true"></span></div>
							<h5>Projects Completed</h5>
						</div>

						<div class="col_one_fourth nobottommargin center col_last" data-animate="bounceIn" data-delay="600">
							<i class="i-plain i-large divcenter nobottommargin icon-dribbble2"></i>
							<div class="counter counter-lined"><span data-from="25" data-to="214" data-refresh-interval="30" data-speed="2700" data-comma="true"></span></div>
							<h5>Shots on Dribbble</h5>
						</div>

					</div>

				</div>
                   
                    <div class="row clearfix">

                        <div>
                            
                            <div id="portfolio" class="portfolio portfolio-nomargin grid-container clearfix">

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/phone-category.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc center" style="position: absolute; top: 10%;">
							<h3>Phone</h3>
							<span>12,039 reweived</span>
						</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/watch-category.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc dark center" style="position: absolute; top: 10%;">
							<h3>Watch</h3>
							<span>10,039 reweived</span>
						</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/cars.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc dark center" style="position: absolute; top: 10%;">
							<h3>Car</h3>
							<span>8,018 reweived</span>
						</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/perfume.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc center" style="position: absolute; top: 10%;">
							<h3>Perfume</h3>
							<span>8,018 reweived</span>
						</div>
							</article>
							</div>
                        </div>

                    </div>
                    
                    <div class="container topmargin bottommargin clearfix">
                        <div class="heading-block center">
                                <h2>Top Questions</h2>
                                <span class="divcenter">Browse our most popular questions</span>
						</div>
						<!-- Portfolio Items
						============================================= -->
						<div id="portfolio" class="portfolio grid-container clearfix">

							<article class="portfolio-item pf-media pf-icons">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/cars.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="{{ asset('img/categories/cars.jpg') }}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
										<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3><a href="portfolio-single.html">Cars</a></h3>
									<span class="nomargin"><a href="#">14,559 reviews</a></span>
								</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/cars.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="{{ asset('img/categories/cars.jpg') }}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
										<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3><a href="portfolio-single.html">Cars</a></h3>
									<span class="nomargin"><a href="#">14,559 reviews</a></span>
								</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/cars.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="{{ asset('img/categories/cars.jpg') }}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
										<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3><a href="portfolio-single.html">Cars</a></h3>
									<span class="nomargin"><a href="#">14,559 reviews</a></span>
								</div>
							</article>
							
							<article class="portfolio-item pf-media pf-icons">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/cars.jpg') }}" alt="Open Imagination">
									</a>
									<div class="portfolio-overlay">
										<a href="{{ asset('img/categories/cars.jpg') }}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
										<a href="portfolio-single.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
									</div>
								</div>
								<div class="portfolio-desc">
									<h3><a href="portfolio-single.html">Cars</a></h3>
									<span class="nomargin"><a href="#">14,559 reviews</a></span>
								</div>
							</article>
							
						</div><!-- #portfolio end -->

                    </div>

                </div>

            </section><!-- #content end -->
            
            
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<!-- Additional Bottom  JS -->
@section('tailJs')
<script>
	let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
	let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
	let urlSearch = "{!!route('Search Result',[""])!!}";
	let search = 'product-search';
	let searchResult = 'product-result';
	let searchInput = 'product-search-text';
</script>
@endsection