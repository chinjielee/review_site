@extends( 'layouts.app' )
	<!-- Additional CSS -->
@section( 'headCss' )
		<style>
			.twitter-typeahead {
				position: inherit!important;
				display: block !important;
			}
			
			.tt-dataset-value {
				line-height: 15px;
				padding: 10px;
			}
			
			.tt-menu {
				background-color: #fff;
				width: 100%;
				text-align: left;
				max-height: 150px;
				overflow-y: auto;
			}
			
			.tt-dataset-value:hover {
				background: rgba(0, 101, 204, 0.1);
				width: 100%;
			}
		</style>
		@endsection
		<!-- Additional JS -->
		@section( 'headJs' )

		@endsection
		@section( 'content' )

		<section id="slider" class="full-screen" style="background: url({{ asset('img/light_gradient_violet_bg.jpg') }}) center;">
		
				<div class="container clearfix">

						<div class="heading-block center nobottomborder topmargin-lg">
							<h1 data-animate="fadeInUp"><strong>@lang('welcome.Know better, choose better').</strong></h1>
							<span data-animate="fadeInUp" data-delay="300">@lang('welcome.Can not find what you are looking for? Search here')...</span>
							<span>
								<form action="#" method="post" role="form" class="landing-wide-form clearfix">
									<div id='bloodhound' class="col_four_fifth nobottommargin norightmargin">
										<input id='product-search-text' type="text" class="form-control input-lg not-dark typeahead" value="" placeholder="@lang('welcome.Find products, services or movies')...">
									</div>
									<div class="col_one_fifth col_last nobottommargin">
										<button class="btn btn-lg btn-blue btn-block nomargin" id="product-search" value="submit" type="submit" style="">@lang('welcome.SEARCH')</button>
									</div>
								</form>
							</span>
						</div>
			</div>
			
			<div class="visible-lg">
			@foreach($jsonImage as $imglg)
				<a href="{{$imglg->url_link}}" class="featured-img-wrapper"><img src="{{ asset('img/featured-img/$imglg->image') }}" style="max-width: {{$imglg->large_width}}px;position: absolute;top: {{$imglg->large_top}}%;left: {{$imglg->large_left}}%;right:{{$imglg->large_right}}%;" class="animated"></a>
			@endforeach
			</div>
			<div class="visible-md">
			@foreach($jsonImage as $imgmd)
				<a href="{{$imgmd->url_link}}" class="featured-img-wrapper"><img src="{{ asset('img/featured-img/$imgmd->image') }}" style="max-width: {{$imgmd->middle_width}}px;position: absolute;top: {{$imgmd->middle_top}}%;left: {{$imgmd->middle_left}}%;right:{{$imgmd->middle_right}}%;" class="animated"></a>
			@endforeach
			</div>
			
		</section>

		@endsection
		@section( 'tailJs' )
			<script>
				let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
				let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
				let search = 'product-search';
				let searchResult = 'product-result';
				let searchInput = 'product-search-text';
			</script>
		@endsection