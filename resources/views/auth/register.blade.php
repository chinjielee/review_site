@extends('layouts.app')

@section('content')

<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">@lang('auth.Home')</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page"><strong>{{ ucwords(Route::currentRouteName()) }}</strong>
			</li>
		</ol>
	</div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
	<div class="content-wrap">
		<div class="container clearfix">
			<div class="col-md-6 divcenter nobottommargin clearfix">
				<h3 class="center">@lang('auth.Create your new account')</h3>
				<div class="panel panel-default nobottommargin">
					<div class="panel-body">
						@if(Session::get('errors')||count( $errors ) > 0)
							<div class="alert alert-danger">
							@foreach ($errors->all() as $error)
								<i class="icon-remove-sign"></i> {{ $error }} </br>
							@endforeach
							</div>
						@endif
						<form id="register-form" name="register-form" class="nobottommargin" action="{{ route('register') }}" method="post">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row bottommargin-sm">
								<div class="col-md-6">
									<label for="register-firstname">@lang('auth.First Name')*:</label>
									<input type="text" id="register-firstname" name="firstname" value="{{old('firstname')}}" class="sm-form-control {{ $errors->has('firstname') ? ' error' : '' }}" />
								</div>
								<div class="col-md-6">
									<label for="register-lastname">@lang('auth.Last Name')*:</label>
									<input type="text" id="register-lastname" name="lastname" value="{{old('lastname')}}" class="sm-form-control {{ $errors->has('lastname') ? ' error' : '' }}" />
								</div>
							</div>
							<div class="col_full">
								<label for="register-form-email">@lang('auth.Email Address')*:</label>
								<input type="email" id="register-form-email" name="email" value="{{old('email')}}" class="sm-form-control {{ $errors->has('email') ? ' error' : '' }}" />
							</div>
							<div class="col_full">
								<label for="password">@lang('auth.Password')*: </label>
								@if ($errors->has('password'))
									<span class="text-danger">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
								<input type="password" id="password" name="password" value="" class="sm-form-control {{ $errors->has('password') ? ' error' : '' }}" />
							</div>
							<div class="col_full">
								<label for="password_confirmation">@lang('auth.Confirm Password')*:</label>
								@if ($errors->has('password_confirmation'))
									<span class="text-danger">
										<strong>{{ $errors->first('password_confirmation') }}</strong>
									</span>
								@endif
								<input type="password" id="password_confirmation" name="password_confirmation" value="" class="sm-form-control {{ $errors->has('password_confirmation') ? ' error' : '' }}" />
							</div>
							<div class="row bottommargin-sm">
								<div class="col-md-4">
									<label for="register-form-mobile_code">@lang('auth.Country Code')*:</label>
									<select id="register-form-mobile_code" class="selectpicker" name="mobile_code">
										<option {{ (old("mobile_code") == '60' ? "selected":"") }}>60</option>
										<option {{ (old("mobile_code") == '65' ? "selected":"") }}>65</option>
									</select>
								</div>
								<div class="col-md-8">
									<label for="register-form-mobile">@lang('auth.Mobile')*:</label>
									<input type="number" id="register-form-mobile" name="mobile" value="{{old('mobile')}}" class="sm-form-control {{ $errors->has('mobile') ? ' error' : '' }}" />
								</div>
							</div>
							<div class="col_full">
								<label for="login-form-country">@lang('auth.Country of residence'):</label>
								<select class="selectpicker" name="country">
									<option {{ (old("country") == 'Singapore' ? "selected":"") }}>@lang('auth.Singapore')</option>
									<option {{ (old("country") == 'Malaysia' ? "selected":"") }}>@lang('auth.Malaysia')</option>
								</select>
							</div>
							<div class="row bottommargin-sm">
								<div class="col-md-6">
									<label for="login-form-username">@lang('auth.Gender'):</label>
									<select class="selectpicker" name="gender">
										<option {{ (old("gender") == 'Female' ? "selected":"") }}>@lang('auth.Female')</option>
										<option {{ (old("gender") == 'Male' ? "selected":"") }}>@lang('auth.Male')</option>
									</select>
								</div>

								<div class="col-md-6">
									<label for="login-form-dob">@lang('auth.Date of birth'):</label>
									<input type="text" value="{{old('dob')}}" class="sm-form-control tleft format {{ $errors->has('dob') ? ' error' : '' }}" name="dob" placeholder="YYYY-MM-DD">
								</div>
							</div>
							<div class="line line-sm"></div>
							<div class="col_full">
								<label for="login-form-qna1">@lang('auth.Security Question')</label>
								<select class="selectpicker bottommargin-xs" name='qna1' id='qna1'>
									<option value='0'>@lang('auth.Choose an Security Questions')</option>
								@foreach($arrSecurityQuestions->all() as $question)
									<option {{ (old("qna1") == $question->security_questions_id ? "selected":"") }} value="{{$question->security_questions_id}}">{{$question->questions}}</option>
								@endforeach
								</select>
								<input type="text" id="login-form-ans1" name="qna-ans1" value="{{old('qna-ans1')}}" class="sm-form-control" />
							</div>
							<!--
							<div class="col_full">
								<label for="login-form-qna2">Security Question 2:</label>
								<select class="selectpicker bottommargin-xs" name='qna2' id='qna2'>
									<option value='0'>Choose an Security Questions</option>
								@foreach($arrSecurityQuestions->all() as $question)
									<option value="{{$question->security_questions_id}}">{{$question->questions}}</option>
								@endforeach
								</select>
								<input type="text" id="login-form-ans2" name="qna-ans2" value="{{ old('qna-ans2') }}" class="sm-form-control" />
							</div>
							<div class="col_full">
								<label for="login-form-qna3">Security Question 3:</label>
								<select class="selectpicker bottommargin-xs" name='qna3' id='qna3'>
									<option value='0'>Choose an Security Questions</option>
								@foreach($arrSecurityQuestions->all() as $question)
									<option value="{{$question->security_questions_id}}">{{$question->questions}}</option>
								@endforeach
								</select>
								<input type="text" id="login-form-ans3" name="qna-ans3" value="{{old('qna-ans3')}}" class="sm-form-control" />
							</div>
							-->
							<div class="col_full nobottommargin">
								<input id="t&c" class="checkbox-style" name="t&c" type="checkbox" {{ old('t&c') ? 'checked' : '' }}>
								<label for="t&c" class="checkbox-style-3-label" style="text-transform: inherit; font-weight: normal;">I've agree to WIKÅBÖ's Terms of Use and Privacy Policy</label>
							</div>
							<div class="col_full nobottommargin">
								<button class="button button-3d button-blue button-large nomargin btn-block nott" id="login-form-submit" name="login-form-submit" value="login">@lang('auth.Register Now')</button>
							</div>
							<div class="line line-sm"></div>
							@if ($socialLogin == true)
							<div class="center">
								<h4 style="margin-bottom: 15px;">@lang('auth.or Sign in with'):</h4>
								<a href="{{url('/redirect')}}" class="button button-mini button-circle si-facebook si-colored">Facebook</a>
								<!-- <a href="#" class="button button-mini button-circle si-instagram si-colored">Instagram</a>
								<a href="{{url('/social-twitter/redirect')}}" class="button button-mini button-circle si-evernote si-colored">Twitter</a>
								<a href="#" class="button button-mini button-circle si-acrobat si-colored">Weibo</a> -->
							</div>
							@endif
						</form>
					</div>
				</div>			
			</div>
		</div>
	</div>
</section><!-- #content end -->

<!--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
-->
@endsection


