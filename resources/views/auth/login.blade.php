@extends('layouts.app')

@section('content')

<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
		<div class="container clearfix">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">@lang('auth.Home')</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page"><strong>{{ ucwords(Route::currentRouteName()) }}</strong>
				</li>
			</ol>
		</div>
	</section><!-- #page-title end -->

<!-- Content
		============================================= -->
		<section id="content">
			<div class="content-wrap">

				<div class="container clearfix">
					
					<div class="col-md-6 divcenter nobottommargin clearfix">
					
						<h3 class="center">@lang('auth.Please sign in to your account')</h3>
							<div class="panel panel-default nobottommargin">
								<div class="panel-body">	
								@if(Session::get('errors')||count( $errors ) > 0)
									<div class="alert alert-danger">
									@foreach ($errors->all() as $error)
										<i class="icon-remove-sign"></i> {{ $error }} </br>
									@endforeach
									</div>
								@endif
								<form id="login-form" name="login-form" class="nobottommargin" action="{{ route('login') }}" method="post">
									<input type="hidden" name="_token" value="{{ csrf_token() }}">
									<div class="col_full">
										<label for="email">@lang('auth.Email Address')*:</label>
										<input type="email" id="email" name="email" value="{{ old('email') }}" class="sm-form-control" required autofocus/>
									</div>

									<div class="col_full">
										<label for="password">@lang('auth.Password')*:</label>
										<input type="password" id="password" name="password" value="" class="sm-form-control" required/>
									</div>
										
									<div class="col_full nobottommargin">
										<input id="remember" class="checkbox-style" name="remember" type="checkbox" {{ old('remember') ? 'checked' : '' }}>
										<label for="remember" class="checkbox-style-3-label" style="text-transform: inherit; font-weight: normal;">@lang('auth.Remember Me')</label>
									
										<a href="{{ route('Forget Password') }}" class="fright">@lang('auth.Forgot Password')?</a>
									</div>
									<div class="col_full nobottommargin">
										<button class="button button-3d button-blue button-large nomargin btn-block nott" id="login-form-submit" name="login-form-submit" value="login">@lang('auth.Sign In')</button>
									</div>
											
									<div class="line line-sm"></div>
									<div class="center">
									<h4 style="margin-bottom: 15px;">@lang('auth.or Sign in with'):</h4>
									<a href="{{url('/redirect')}}" class="button button-mini button-circle si-facebook si-colored">Facebook</a>
									<!-- <a href="#" class="button button-mini button-circle si-instagram si-colored">Instagram</a>
									<a href="{{url('/social-twitter/redirect')}}" class="button button-mini button-circle si-evernote si-colored">Twitter</a>
									<a href="#" class="button button-mini button-circle si-acrobat si-colored">Weibo</a> -->
								</div>
								</form>
							</div>
						</div>
						<div class="center">
							@lang('layout.Not A Member')? 
							<a href="{{ route('register') }}">@lang('layout.Register Here')</a>
						</div>
					</div>
				</div>

			</div>

		</section><!-- #content end -->
		
<!--			
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('Forget Password') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
-->
@endsection
