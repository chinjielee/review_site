@extends('layouts.app')

@section('content')

<!-- Page Title
		============================================= -->
		<section id="page-title" class="page-title-mini">
		<div class="container clearfix">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="/">@lang('auth.Home')</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page"><strong>{{ ucwords(Route::currentRouteName()) }}</strong>
				</li>
			</ol>
		</div>
	</section><!-- #page-title end -->
<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
					<div class="col-md-6 divcenter nobottommargin clearfix">
							
						<h3 class="center">@lang('auth.Having trouble signing in')?</h3>
							
								<div class="panel panel-default nobottommargin">
									<div class="panel-body" style="padding: 40px;">
									@if (session('status'))
						                <div class="alert alert-success">
						                    {{ session('status') }}
						                </div>
						            @endif
										<form id="login-form" name="login-form" class="nobottommargin" action="{{ route('password.email') }}" method="post">
											<div class="col_full">
												<label for="email">@lang('auth.Email Address'):</label>
												@if ($errors->has('email'))
					                                <span class="help-block">
					                                    <strong>{{ $errors->first('email') }}</strong>
					                                </span>
					                            @endif
												<input type="email" id="email" name="email" value="{{ old('email') }}" class="form-control" required/>
					                            
											</div>
											
											<div class="col_full nobottommargin">
												<button type="submit" class="button button-3d button-blue button-large nomargin btn-block" id="login-form-submit" name="login-form-submit" value="login">@lang('auth.Continue')</button>
											</div>
											<input type="hidden" name="_token" value="{{ csrf_token() }}">
										</form>
									</div>
								</div>
								
					</div>
				</div>

			</div>

		</section><!-- #content end -->
		
<!--
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Reset Password</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
-->
@endsection
