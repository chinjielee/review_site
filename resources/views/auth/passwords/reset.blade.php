@extends('layouts.app')

@section('content')

<!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">
        <div class="container clearfix">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="/">@lang('auth.Home')</a>
                </li>
                <li class="breadcrumb-item active" aria-current="page"><strong>{{ ucwords(Route::currentRouteName()) }}</strong>
                </li>
            </ol>
        </div>
    </section><!-- #page-title end -->
<!-- Content
        ============================================= -->

<section id="content">

			<div class="content-wrap">

				<div class="container clearfix">
					
					<div class="col-md-6 divcenter nobottommargin clearfix">
					
						<h3 class="center">@lang('auth.Reset Password')</h3>
							<div class="panel panel-default nobottommargin">
								<div class="panel-body">	
								
                    <form class="form-horizontal" method="POST" action="{{ route('Forget Password') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-5 control-label">@lang('auth.Email Address')</label>

                            <div class="col-md-7">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-5 control-label">@lang('auth.Password')</label>

                            <div class="col-md-7">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <label for="password-confirm" class="col-md-5 control-label">@lang('auth.Confirm Password')</label>
                            <div class="col-md-7">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-7 col-md-offset-5">
                                <button type="submit" class="button button-3d button-blue button-large nomargin btn-block nott">
                                    @lang('auth.Reset Password')
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection
