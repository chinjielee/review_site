@extends( 'layouts.app' )
	<!-- Additional CSS -->
@section( 'headCss' )
		<style>
			.twitter-typeahead {
				position: inherit!important;
				display: block !important;
			}
			
			.tt-dataset-value {
				line-height: 15px;
				padding: 10px;
			}
			
			.tt-menu {
				background-color: #fff;
				width: 100%;
				text-align: left;
				max-height: 150px;
				overflow-y: auto;
			}
			
			.tt-dataset-value:hover {
				background: rgba(0, 101, 204, 0.1);
				width: 100%;
			}
		</style>
		@endsection
		<!-- Additional JS -->
		@section( 'headJs' )

		@endsection
		@section( 'content' )


		<section id="slider" class="slider-parallax full-screen" style="background: url({{ asset('img/light_gradient_violet_bg.jpg') }}) center;">
			<div class="slider-parallax-inner">

				<div class="container clearfix">

					<div class="vertical-middle">

						<div class="heading-block center nobottomborder">
							<h1 data-animate="fadeInUp"><strong>Know better, choose better.</strong></h1>
							<span data-animate="fadeInUp" data-delay="300">Can't find what you're looking for? Search here...</span>
							<span>
								<form action="#" method="post" role="form" class="landing-wide-form clearfix">
									<div id='bloodhound' class="col_four_fifth nobottommargin norightmargin">
										<input id='product-search-text' type="text" class="form-control input-lg not-dark typeahead" value="" placeholder="Find products, services or movies...">
									</div>
									<div class="col_one_fifth col_last nobottommargin">
										<button class="btn btn-lg btn-blue btn-block nomargin" id="product-search" value="submit" type="submit" style="">SEARCH</button>
									</div>
								</form>
							</span>
						</div>

					</div>

					<a href="#" data-scrollto="#section-features" class="one-page-arrow"><i class="icon-angle-down infinite animated fadeInDown"></i></a>

				</div>

			</div>

		</section>

		<!-- Content
		===
		=== === === === === === === === === === === === === === -->
		<section id="content">

			<div class="content-wrap">

				<div class="row clearfix">

					<div class=" topmargin-sm">

						<div class="heading-block center bottommargin-sm">
							<h2>Most Reviewed Products</h2>
							<span class="divcenter">Browse our most popular reviewed product categories</span>
						</div>

						<div id="portfolio" class="portfolio portfolio-nomargin grid-container clearfix">

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;" data-animate="fadeInUp" data-delay="0">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/mobile-phones.jpg') }}" alt="Open Imagination">
									</a>
								
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc center" style="position: absolute; top: 10%;">
									<h3>Mobile Phones</h3>
									<span>12,039 reweived</span>
								</div>
							</article>

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;" data-animate="fadeInUp" data-delay="100">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/wearable-tech.jpg') }}" alt="Open Imagination">
									</a>
								
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc dark center" style="position: absolute; top: 10%;">
									<h3>Wearable Tech</h3>
									<span>10,039 reweived</span>
								</div>
							</article>

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;" data-animate="fadeInUp" data-delay="200">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/cars.jpg') }}" alt="Open Imagination">
									</a>
								
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc dark center" style="position: absolute; top: 10%;">
									<h3>Car</h3>
									<span>8,018 reweived</span>
								</div>
							</article>

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;" data-animate="fadeInUp" data-delay="300">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/perfume.jpg') }}" alt="Open Imagination">
									</a>
								
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc center" style="position: absolute; top: 10%;">
									<h3>Perfume</h3>
									<span>8,018 reweived</span>
								</div>
							</article>
						</div>
					</div>

				</div>

				<div class="row clearfix common-height">

					<div class="col-md-6 center col-padding" style="background: url({{ asset('img/home-banner.png') }}) center center no-repeat; background-size: cover;" data-animate="fadeInLeft" data-delay="400">
						<div>&nbsp;</div>
					</div>

					<div class="col-md-6 center col-padding" style="background-color: rgb(242, 235, 221);" data-animate="fadeInRight" data-delay="400">
						<div>
							<div class="heading-block">
								<span class="before-heading color bottommargin">Understandable &amp; Customizable.</span>
								<h3>Your Opinion Matters</h3>
								<span class="divcenter">Write your first review and help thousands of people choose better.</span>

								<div class="center topmargin-lg">
									<a href="#" class="button button-3d button-rounded button-blue button-xlarge">Write A Review</a>
								</div>
							</div>
						</div>
					</div>

				</div>

				<div class="row clearfix topmargin-lg">

					<div class=" topmargin-sm">

						<div class="heading-block center bottommargin-sm">
							<h2>Most Reviewed Services</h2>
							<span class="divcenter">Browse our most popular reviewed product categories</span>
						</div>
					</div>

				</div>

				<div class="section dark nomargin" style="background: url({{ asset('img/bg-main-10.jpg') }}) ; padding: 100px 0px;">

					<div class="container clearfix">

						<div class="col_one_fourth nobottommargin center" data-animate="bounceIn">
							<div class="counter counter-lined"><span data-from="10" data-to="80" data-refresh-interval="50" data-speed="4000" data-comma="true"></span>
							</div>
							<h5><a href="#" class="white">Appliance Repairs</a></h5>
						</div>

						<div class="col_one_fourth nobottommargin center" data-animate="bounceIn" data-delay="200">
							<div class="counter counter-lined"><span data-from="100" data-to="25841" data-refresh-interval="50" data-speed="2500" data-comma="true"></span>
							</div>
							<h5><a href="#" class="white">Beauty Services</a></h5>
						</div>

						<div class="col_one_fourth nobottommargin center" data-animate="bounceIn" data-delay="400">
							<div class="counter counter-lined"><span data-from="100" data-to="923" data-refresh-interval="50" data-speed="3500" data-comma="true"></span>
							</div>
							<h5><a href="#" class="white">Education & Training</a></h5>
						</div>

						<div class="col_one_fourth nobottommargin center col_last" data-animate="bounceIn" data-delay="600">
							<div class="counter counter-lined"><span data-from="25" data-to="214" data-refresh-interval="30" data-speed="2700" data-comma="true"></span>
							</div>
							<h5><a href="#" class="white">Health Services</a></h5>
						</div>

					</div>

				</div>

				<div class="row clearfix">

					<div>

						<div id="portfolio" class="portfolio portfolio-nomargin grid-container clearfix">

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;" data-animate="fadeInUp" data-delay="0">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/mobile-phones.jpg') }}" alt="Open Imagination">
									</a>
								
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc center" style="position: absolute; top: 10%;">
									<h3>Mobile Phones</h3>
									<span>12,039 reweived</span>
								</div>
							</article>

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;" data-animate="fadeInUp" data-delay="100">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/wearable-tech.jpg') }}" alt="Open Imagination">
									</a>
								
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc dark center" style="position: absolute; top: 10%;">
									<h3>Wearable Tech</h3>
									<span>10,039 reweived</span>
								</div>
							</article>

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;" data-animate="fadeInUp" data-delay="200">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/cars.jpg') }}" alt="Open Imagination">
									</a>
								
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc dark center" style="position: absolute; top: 10%;">
									<h3>Car</h3>
									<span>8,018 reweived</span>
								</div>
							</article>

							<article class="portfolio-item pf-media pf-icons" style="position: relative !important;" data-animate="fadeInUp" data-delay="300">
								<div class="portfolio-image">
									<a href="portfolio-single.html">
										<img src="{{ asset('img/categories/perfume.jpg') }}" alt="Open Imagination">
									</a>
								
									<div class="portfolio-overlay">
										<a href="#" class="center-icon"><i class="material-icons list-icon">visibility</i></a>
									</div>
								</div>
								<div class="portfolio-desc center" style="position: absolute; top: 10%;">
									<h3>Perfume</h3>
									<span>8,018 reweived</span>
								</div>
							</article>

						</div>
					</div>

				</div>

				<div class="container topmargin bottommargin clearfix">
					<div class="heading-block center">
						<h2>Top Questions</h2>
						<span class="divcenter">Browse our most popular questions</span>
					</div>

					<div id="oc-testi" class="owl-carousel testimonials-carousel carousel-widget" data-margin="20" data-items-sm="1" data-items-md="2" data-items-xl="3">

						<div class="oc-item">
							<div class="testimonial">
								<div class="testi-image">
									<a href="#"><img src="http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" alt="Avatar name"></a>
								</div>
								<div class="testi-content">
									<p>Incidunt deleniti blanditiis quas aperiam recusandae consequatur ullam quibusdam cum libero illo rerum repellendus!</p>
									<div class="testi-meta">
										John Doe
										<span>Singapore</span>
									</div>
								</div>
							</div>
						</div>

						<div class="oc-item">
							<div class="testimonial">
								<div class="testi-image">
									<a href="#"><img src="http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" alt="Avatar name"></a>
								</div>
								<div class="testi-content">
									<p>Natus voluptatum enim quod necessitatibus quis expedita harum provident eos obcaecati id culpa corporis molestias.</p>
									<div class="testi-meta">
										Collis Ta'eed
										<span>Singapore</span>
									</div>
								</div>
							</div>
						</div>

						<div class="oc-item">
							<div class="testimonial">
								<div class="testi-image">
									<a href="#"><img src="http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" alt="Avatar name"></a>
								</div>
								<div class="testi-content">
									<p>Fugit officia dolor sed harum excepturi ex iusto magnam asperiores molestiae qui natus obcaecati facere sint amet.</p>
									<div class="testi-meta">
										Mary Jane
										<span>Malaysia</span>
									</div>
								</div>
							</div>
						</div>

						<div class="oc-item">
							<div class="testimonial">
								<div class="testi-image">
									<a href="#"><img src="http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" alt="Avatar name"></a>
								</div>
								<div class="testi-content">
									<p>Similique fugit repellendus expedita excepturi iure perferendis provident quia eaque. Repellendus, vero numquam?</p>
									<div class="testi-meta">
										Steve Jobs
										<span>Malaysia</span>
									</div>
								</div>
							</div>
						</div>

						<div class="oc-item">
							<div class="testimonial">
								<div class="testi-image">
									<a href="#"><img src="http://0.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?s=60" alt="Avatar name"></a>
								</div>
								<div class="testi-content">
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minus, perspiciatis illum totam dolore deleniti labore.</p>
									<div class="testi-meta">
										Jamie Morrison
										<span>Singapore</span>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>

			</div>

		</section> <!-- #content end -->

		@endsection
		@section( 'tailJs' )
			<script>
				let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
				let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
				let search = 'product-search';
				let searchResult = 'product-result';
				let searchInput = 'product-search-text';
			</script>
		@endsection