<div id="page-menu" class="product_submenu_wrapper">
		<div id="page-menu-wrap">
			<div class="container clearfix">
				<nav class="one-page-menu product_submenu center">
					<ul>
                        <li @if($pageInfo->subpage == 'review')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['review']}}">
								<div>@lang('service.Reviews') ({{$objProductCountDetail->reviewCount}})</div>
							</a>
						</li>
						<li @if($pageInfo->subpage == 'Question and Answer')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['qa']}}">
							<div>@lang('service.Q&amp;A') ({{$objProductCountDetail->qnaCount}})</div>
							</a>
						</li>
						<li @if($pageInfo->subpage == 'detail')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['detail']}}">
								<div>@lang('service.Service Details')</div>
							</a>
						</li>
						<!--<li @if($pageInfo->subpage == 'price')
                                class="active"
                            @endif>
							
							<a href="{{$pageInfo->url['price']}}">
								<div>@lang('service.Price')</div>
							</a>
						</li>-->
						<li>
							<a href="{{route('Compare',[$arrDetail->product_slug,'',''])}}">
								<div>@lang('service.Compare')</div>
							</a>
						</li>
						<li @if($pageInfo->subpage == 'stat')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['stat']}}">
								<div>@lang('service.Stats')</div>
							</a>
						</li>
						<li @if($pageInfo->subpage == 'photo')
                                class="active"
                            @endif>
							<a href="{{$pageInfo->url['photo']}}">
								<div>@lang('service.Service Photos')</div>
							</a>
						</li>
					</ul>
				</nav>
				<div id="page-submenu-trigger"><i class="icon-reorder"></i>
				</div>
			</div>
		</div>
	</div>