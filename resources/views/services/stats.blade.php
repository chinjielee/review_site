@extends( 'layouts.app' )

@section( 'content' )

<!-- Content
===
===
=== === === === === === === === === === === === === -->

<style>
	.pricing-title h3,
	label {
		color: #000;
	}
	
	.section {
		background-color: #F9F9F9;
	}
	
	#page-menu nav li.active a {
		background-color: #0256ae;
	}
	
	.pricing-box.pricing-extended .pricing-features li {
		padding: 0px 0 10px;
		font-size: inherit;
		width: 100%;
	}
	
	.pricing-price {
		color: #000;
		font-weight: 900;
	}
</style>
@include( 'products.product_breadcrumb' )
@include( 'products.top_summary_detail' )
@include( 'products.product_nav_bar' )
	<section id="content" style="margin-bottom: 0px;">

		<div class="content-wrap" style="padding: 40px 0px;">

			<div class="container clearfix">
				<div class="title-block">
					<h3><span>@lang('product.Stats')</span></h3>
					<span>@lang('product.Total Review')</span>
				</div>

				<!-- Charts Area
					============================================= -->
				<div class="col-md-8 bottommargin divcenter" style="max-width: 50%; min-height: 350px;">
					<canvas id="chart-0"></canvas>
					<hr>
					<canvas id="chart-1"></canvas>
					<div align="center">
						<b>1 = Very Poor&nbsp;&nbsp;&nbsp;2 = Poor&nbsp;&nbsp;&nbsp;3 = Neutral&nbsp;&nbsp;&nbsp;4 = Meet expectation&nbsp;&nbsp;&nbsp;5 = Impressive</b>
					</div>
				</div>
				<!-- Charts Area End -->
			</div>



		</div>

	</section>



@endsection

@section( 'tailJs' )

<script>
	var arrKey = [];
	var arrValue = [];
	let jsonString = '{!!$statDetail!!}';
	tempValue = JSON.parse(jsonString);
	tempValue = Object.values(tempValue);
	arrKey = tempValue.map(a => a.key);
	arrValue = tempValue.map(a => a.total);

	var arrKey2 = [];
	var arrValue2 = [];
	let jsonString2 = '{!!$statDetail2!!}';
	tempValue2 = JSON.parse(jsonString2);
	tempValue2 = Object.values(tempValue2);
	arrKey2 = tempValue2.map(a => a.key);
	arrValue2 = tempValue2.map(a => a.rating);

	var randomScalingFactor = function () {
		return Math.round( Math.random() * 100 );
	};

	var chartColors = window.chartColors;
	var color = Chart.helpers.color;
	var config = {
		data: {
			datasets: [ {
				data: arrValue,
				backgroundColor: [
					color( chartColors.red ).alpha( 0.5 ).rgbString(),
					color( chartColors.orange ).alpha( 0.5 ).rgbString(),
					color( chartColors.yellow ).alpha( 0.5 ).rgbString(),
					color( chartColors.green ).alpha( 0.5 ).rgbString(),
					color( chartColors.blue ).alpha( 0.5 ).rgbString(),
				],
				label: 'My dataset' // for legend
			} ],
			labels: arrKey
		},
		options: {
			responsive: true,
			legend: {
				position: 'right',
			},
			title: {
				display: true,
				text: 'Monthly Review Chart'
			},
			scale: {
				ticks: {
					beginAtZero: true
				},
				reverse: false
			},
			animation: {
				animateRotate: false,
				animateScale: true
			}
		}
	};

	var config2 = {
		data: {
			datasets: [ {
				data: arrValue2,
				backgroundColor: [
					color( chartColors.red ).alpha( 0.8 ).rgbString(),
					color( chartColors.orange ).alpha( 0.8 ).rgbString(),
					color( chartColors.yellow ).alpha( 0.8 ).rgbString(),
					color( chartColors.green ).alpha( 0.8 ).rgbString(),
					color( chartColors.blue ).alpha( 0.8 ).rgbString(),
				],
				label: 'My dataset' // for legend
			} ],
			labels: arrKey2
		},
		options: {
			responsive: true,
			legend: {
				position: 'right',
			},
			title: {
				display: true,
				text: 'Rating Breakdown Chart'
			},
			scale: {
				ticks: {
					beginAtZero: true,
					stepSize: 1
				},
				reverse: false
			},
			animation: {
				animateRotate: false,
				animateScale: true
			}
		}
	};

	window.onload = function () {
		var ctx = document.getElementById( "chart-0" );
		var ctx2 = document.getElementById( "chart-1" );
		window.myPolarArea = Chart.PolarArea( ctx, config );
		window.myPolarArea = Chart.PolarArea( ctx2, config2 );
	};
</script>

@endsection