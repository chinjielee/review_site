@extends( 'layouts.app' )

@section( 'content' )

<!-- Content
===
=== === === === === === === === === === === === === === -->
@include( 'services.product_breadcrumb' )
@include('services.top_summary_detail')
@include( 'services.product_nav_bar' )

<section id="content" style="margin-bottom: 0px;">

	<div class="content-wrap" style="padding: 40px 0px;">

		<div class="container clearfix">
			<div class="title-block">
				<h3><span>@lang('product.Price')</span></h3>
				<span></span>
			</div>
		</div>

	</div>

</section>

@endsection