@extends('layouts.app')

@section('content')
<!-- Page Title
		============================================= -->
		<section id="page-title">

			<div class="container clearfix">
				<h1 class="nott">{{ Route::currentRouteName() }}</h1>
				<ol class="breadcrumb">
					<li><a href="#">Home</a></li>
					<li class="active">{{ Route::currentRouteName() }}</li>
				</ol>
			</div>

		</section><!-- #page-title end -->

<!-- Content
		============================================= -->
		<section id="content">

			<div class="content-wrap">

				
			</div>

		</section><!-- #content end -->
		
@endsection
