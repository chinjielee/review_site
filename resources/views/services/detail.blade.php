@extends( 'layouts.app' )

@section( 'content' )


<script>
setRecentView();
function setRecentView(){
    var d = new Date();
    d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = "recent_view="+"{{$current_recent_view}}{{$view_token}}"+";" + expires + ";path=/";
}
</script>
<!-- Content
===
===
=== === === === === === === === === === === === === -->
@include( 'services.product_breadcrumb' )
@include('services.top_summary_detail')
@include( 'services.product_nav_bar' )
<section id="content" class="nobottommargin">

	<div class="content-wrap" style="padding-top: 40px;">

		<div class="container clearfix">
			<div class="title-block">
				<h3><span>@lang('service.Service Details')</span></h3>

				@isset($arrProductSpec->release_date)
					<span>{{$arrDetail->product_service_title}} - Released {{$arrProductSpec->release_date}} </span>
				@endisset
			</div>
			
			<table class="table table-bordered" width="100%">
			<thead>
					<tr>
						<th colspan="3"><h4 class="nomargin">{{$arrDetail->product_service_title}}</h4></th>
					</tr>
				</thead>
				<tbody>
				@foreach($arrProductSpec as $key => $val)
					<tr>
						<td  style="background-color: #f5f5f5;" class="color"><strong>{{ucfirst($key)}}</strong></td>
						@if(is_array($val))
							<td >
								<ul class="multiple-value-list">
									@foreach($val as $subVal)
										<li>{{$subVal}}</li>
									@endforeach
								</ul>
							</td>
						@else
							<td >{!!$val!!}</td>
						@endif
					</tr>
				@endforeach
					<!-- <tr>
					  <td>Announced</td>
					  <td>2016, September</td>
				  </tr>
					<tr>
					  <td>Status</td>
					  <td>Available. Released 2016, September</td>
				  </tr>
					<tr>
					  <td>Dimensions</td>
					  <td>138.3 x 67.1 x 7.1 mm (5.44 x 2.64 x 0.28 in)</td>
				  </tr>
					<tr>
					  <td>Weight</td>
					  <td>138 g (4.87 oz)</td>
				  </tr>
					<tr>
					  <td>Build</td>
					  <td>Front glass, aluminum body</td>
				  </tr>
					<tr>
					  <td>SIM</td>
					  <td>Nano-SIM</td>
				  </tr>
					<tr>
						<td>&nbsp;</td>
						<td><ul>
							<li>IP67 dust/water resistant (up to 1m for 30 mins)</li>
							<li>Apple Pay (Visa, MasterCard, AMEX certified)</li>
						</ul></td>
					</tr>
					<tr>
					  <td>Type</td>
					  <td>LED-backlit IPS LCD, capacitive touchscreen, 16M colors</td>
				  </tr>
				  <tr>
					  <td>Size</td>
					  <td>4.7 inches, 60.9 cm2 (~65.6% screen-to-body ratio)</td>
				  </tr>
				  <tr>
					  <td>Resolution</td>
					  <td>750 x 1334 pixels, 16:9 ratio (~326 ppi density)</td>
				  </tr>
				  <tr>
					  <td>Multitouch</td>
					  <td>Yes</td>
				  </tr>
				  <tr>
					  <td>Protection</td>
					  <td>Ion-strengthened glass, oleophobic coating</td>
				  </tr> -->
				</tbody>
			</table>
			@isset($arrProductSpec->website)
			<div class="promo promo-light bottommargin">
				<h4 class="nomargin">@lang('services.View more details on official website')</h3>
				<a href="{{$arrProductSpec->website}}" target="_blank" class="button button-blue button-3d button-rounded nott"><i class="icon-line-globe"></i> @lang('product.Official Website')</a>
			</div>
			@endisset

		</div>

	</div>

</section>
@endsection