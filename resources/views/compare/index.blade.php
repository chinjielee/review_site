@extends( 'layouts.app' )
@section( 'headCss' )
<link rel="stylesheet" href="{{ asset('css/loader.css') }}" type="text/css" />
<style>
	.compare-search {
		background: #e6effb;
		padding: 15px 5px;
		margin: 0 0px 15px;
		position: relative;
		z-index: 4;
	}
	
	.table> thead:first-child> tr:first-child> th {
		border: 1px solid #dddddd;
		width: 300px;
	}
	
	.table-compare,
	.table-compare th {
		text-align: center;
	}
	
	td:first-child {
		font-weight: bold;
		text-align: left;
		background-color: #f9f9f9;
		color: #06c;
	}
	@media (max-width: 767px) {
		.table-compare td:first-child {
			text-align: center;
			background-color: #f9f9f9;
			color: #06c;
			font-weight: bold;
		}
	}
	i.icon-thumbs-up2{
		font-size: 0.92em;
		text-shadow: none;
	}
	.compare-search-input{
		display:inline;
	}
	.compare_title_xs{
		text-transform: capitalize;
	}
	.empty-title {
		opacity: 0.3;
	}
</style>
@endsection
@section( 'content' )
<div id='loading' class="loading" style="display:none;">@lang('compare.Loading&#8230;')</div>
<!-- Page Title === === === === === === === === === === === === === === === -->
<section id="page-title" class="page-title-mini">
	<div class="container clearfix">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="/">@lang('compare.Home')</a>
			</li>
			<li class="breadcrumb-item active" aria-current="page"><strong>{{ Route::currentRouteName() }}</strong>
			</li>
		</ol>
	</div>
</section>
<!-- #page-title end -->
<!-- Content === === === === === === === === === === === === === === === -->
<section id="slider" class="slider-parallax" style="background: url({{ asset('img/review_bg.jpg') }}) center;">
	<div class="content-wrap nobottompadding">
		<div class="container clearfix">
			<div class="heading-block center">
				<h1 class="topmargin" style="color: #fff;">@lang('compare.Compare specs/details')</h1>
				<span style="color: #fff;">@lang('compare.Search any products, services or movies on below to compare')</span>
			</div>
		</div>
	</div>

</section>

<section id="content">

	<div class="content-wrap">

		<div class="container clearfix">
			<table class="table table-hover table-bordered table-compare nobottommargin" style="border: 0px;">
				<thead>
					<tr>
						<th class="hidden-xs" style="border: 0px;">&nbsp;</th>
							@if(empty($objProduct_1))
							<th class="hidden-xs">
								<div class="compare-search">
									<div class="nomargin clearfix">
										<p class="nomargin"><strong class="">@lang('compare.Compare with')</strong>
										</p>
										<div class="clearfix">
											<input type="text" name="wildcard-text" id='input_1' value="">
											<button id="search_product_1" class="button button-mini nomargin button-search" data-action='search' data-col='1'>@lang('compare.Search')</button>
										</div>
										<p class="nomargin">
											<small><i>@lang('compare.Please enter model name or part of it')</i></small>
										</p>
									</div>
								</div>
								<h4 id="product_title_1" class="bottommargin-xs empty-title">@lang('compare.Add a mobile phone')</h4>
								<img id="product_image_1" src="{{ asset('img/empty-compare.jpg') }}" alt="Product Name" width="120px" class="bottommargin-xs" style="opacity: 0.3;">
							@else
							<th>
								<div class="compare-search">
									<div class="nomargin clearfix">
										<p class="nomargin"><strong class="">@lang('compare.Compare with')</strong>
										</p>
										<div class="clearfix">
											<input type="text" name="wildcard-text" id='input_1' value="">
											<button class="button button-mini nomargin button-search" data-action='change' data-col='1'>@lang('compare.Change')</button>
										</div>
										<p class="nomargin">
											<small><i>@lang('compare.Please enter model name or part of it')</i></small>
										</p>
									</div>
								</div>
								<h4 id="product_title_1" class="bottommargin-xs">{{$objProduct_1->product_service_title}}</h4>
								@if(empty($objProduct_1->images))
									<img id="product_image_1" src="{{ asset('img/empty-compare.jpg') }}" alt="{{$objProduct_1->product_service_title}}" width="120px" class="bottommargin-xs" style="opacity: 0.3;">
								@else
									<img id="product_image_1" src="{{ $objProduct_1->images }}" alt="{{$objProduct_1->product_service_title}}" width="120px" class="bottommargin-xs">	
								@endif
							@endif
						</th>
						@if(empty($objProduct_2))
							<th>
								<div class="compare-search">
									<div class="nomargin clearfix">
										<p class="nomargin"><strong class="">@lang('compare.Compare with')</strong>
										</p>
										<div class="clearfix">
											<input type="text" name="wildcard-text" id='input_2' value="">
											<button id="search_product_2" class="button button-mini nomargin button-search" data-action='search' data-col='2'>@lang('compare.Search')</button>
										</div>
										<p class="nomargin">
											<small><i>@lang('compare.Please enter model name or part of it')</i></small>
										</p>
									</div>
								</div>
								<h4 id="product_title_2" class="bottommargin-xs empty-title">@lang('compare.Add a mobile phone')</h4>
								<img id="product_image_2" src="{{ asset('img/empty-compare.jpg') }}" alt="Product Name" width="120px" class="bottommargin-xs" style="opacity: 0.3;">
							@else
							<th>
								<div class="compare-search">
									<div class="nomargin clearfix">
										<p class="nomargin"><strong class="">@lang('compare.Change with')</strong>
										</p>
										<div class="clearfix">
											<input type="text" name="wildcard-text" id='input_2' value="">
											<button id="change_product_2" class="button button-mini nomargin button-search" data-action='change' data-col='2'>@lang('compare.Change')</button>
										</div>
										<p class="nomargin">
											<small><i>@lang('compare.Please enter model name or part of it')</i></small>
										</p>
									</div>
								</div>
								<h4 id="product_title_2" class="bottommargin-xs">{{$objProduct_2->product_service_title}}</h4>
								@if(empty($objProduct_2->images))
									<img id="product_image_2" src="{{ asset('images/product/') }}" alt="{{$objProduct_2->product_service_title}}" width="120px" class="bottommargin-xs" style="opacity: 0.3;">
								@else
									<img id="product_image_2" src="{{ $objProduct_2->images }}" alt="{{$objProduct_2->product_service_title}}" width="120px" class="bottommargin-xs">	
								@endif
							@endif
						</th>
						@if(empty($objProduct_3))
							<th class="hidden-xs">
								<div class="compare-search">
									<div class="nomargin clearfix">
										<p class="nomargin"><strong class="">@lang('compare.Compare with')</strong>
										</p>
										<div class="clearfix">
											<input type="text" name="wildcard-text" id='input_3' value="">
											<button id="search_product_3" class="button button-mini nomargin button-search" data-action='search' data-col='3'>@lang('compare.Search')</button>
										</div>
										<p class="nomargin">
											<small><i>@lang('compare.Please enter model name or part of it')</i></small>
										</p>
									</div>
								</div>
								<h4 id="product_title_3" class="bottommargin-xs empty-title">@lang('compare.Add a mobile phone')</h4>
								<img id="product_image_3" src="{{ asset('img/empty-compare.jpg') }}" alt="Product Name" width="120px" class="bottommargin-xs" style="opacity: 0.3;">
							@else
							<th class="hidden-xs">
								<div class="compare-search">
									<div class="nomargin clearfix">
										<p class="nomargin"><strong class="">@lang('compare.Change with')</strong>
										</p>
										<div class="clearfix">
											<input type="text" name="wildcard-text" id='input_3' value="">
											<button id="change_product_3" class="button button-mini nomargin button-search" data-action='change' data-col='3'>@lang('compare.Change')</button>
										</div>
										<p class="nomargin">
											<small><i>@lang('compare.Please enter model name or part of it')</i></small>
										</p>
									</div>
								</div>
								<h4 id="product_title_3" class="bottommargin-xs">{{$objProduct_3->product_service_title}}</h4>
								@if(empty($objProduct_3->images))
									<img id="product_image_3" src="{{ asset('img/empty-compare.jpg') }}" alt="{{$objProduct_3->product_service_title}}" width="120px" class="bottommargin-xs" style="opacity: 0.3;">
								@else
									<img id="product_image_3" src="{{ $objProduct_3->images }}" alt="{{$objProduct_3->product_service_title}}" width="120px" class="bottommargin-xs">	
								@endif
							@endif
						</th>
					</tr>
				</thead>
				<tbody id="compare_result">
					<tr class="visible-xs">
						<td colspan="2"><strong>@lang('compare.Protection')</strong>
						</td>
					</tr>
					<tr>
						<td class="hidden-xs compare_title_xs">@lang('compare.Protection')</td>
						<td>@lang('compare.Ion-strengthened glass, oleophobic coating')</td>
						<td>@lang('compare.Ion-strengthened glass, oleophobic coating')</td>
						<td class="hidden-xs hidden-sm">
						</td>
					</tr>
				</tbody>
			</table>

		</div>

	</div>
@include( 'compare.search_product_modal' )
</section> <!-- #content end -->
	<script>
		var totalProduct = {{$intTotalProduct}};
		var urlSearchProduct = '{{route("search.product",['',''])}}';
		var jsonProduct = '{!!$strCompareProductsAttribute!!}';
		var jsonBrands = '{!!$arrBrand!!}';
		var $objJson = "";
		var $objBrands = "";
		if(jsonProduct!='') {
			$objJson = JSON.parse(jsonProduct);
		}
		if(jsonBrands!='') {
			$objBrands = JSON.parse(jsonBrands);
		}
		
		$( document ).ready(function() {
			renderingCompareResultTable($objJson, $objBrands);
			initRating();
		});

		function initRating(){
			$(".show-rating-wikabo").rating({
				containerClass: 'is-heart',
				filledStar: '<i class="icon-thumbs-up2"></i>',
				emptyStar: '<i class="icon-thumbs-up"></i>',
				showClear:0,
				showCaption:0,
				displayOnly:1
			});
		}

		$('.button-search').on('click',function(){
			var objThis = $(this);
			var col = $(this).data('col');
			var action = $(this).data('action');
			var wildcard = objThis.siblings('input').val();
			$('#modal_product_result').empty();
			$('#loading').show();
			$.ajax({
				method: "GET",
				url: urlSearchProduct+"/"+wildcard+"/10",
				beforeSend: function(  ) {
					$('#loading').show();
				}
			})
			.done(function( data ) {
				if(data.length > 0 ){
					for(var i = 0 ; i<data.length; i++) {
						var ele = processProductSuggestion(data[i], col, action);
						$('#modal_product_result').append(ele);
					}
				} else {
					$('#modal_product_result').html("No result found");
				}
				$('#product_search_result').modal('show');
			})
			.fail(function(data) {
			  alert( "Keyword is invalid, please try again with another keyword" );
			})
			.always(function() {
				$('#loading').hide();
			});
			return false;
		});

		function bindProduct(ele){
			var colReplace = $(ele).data('col');
			var jsonDetail = $(ele).data('detail');
			var action = $(ele).data('action');
			var newJsonCompare = "";
			if(action === 'search'){
				totalProduct++;
			}
			replaceCompare($objJson, colReplace, jsonDetail);
			$('#product_search_result').modal('hide');
		}

		function replaceCompare(objJson , col, objProduct) {
			// processAttribute
			var jsonAttr = JSON.parse(objProduct.product_service_attributes);
			// Clean up replacing index
			$.each(objJson, function(attr, products) {
				attr = attr.toLowerCase();
				delete products[col];
				if(typeof jsonAttr[attr] !== "undefined"){
					products[col] = jsonAttr[attr];
				}
			});

			$.each(jsonAttr, function(attr, products) {
				attr = attr.toLowerCase();
				if(typeof(objJson[attr]) == "undefined") {
					objJson[attr] = {};
					objJson[attr][col] = {};
					objJson[attr][col] = products;
				}
				if(typeof(objJson[attr][col]) == "undefined") {
					objJson[attr][col] = "";
					objJson[attr][col] = products;
				}
			});
			// Compulsary field 
			objJson.brand[col] = objProduct.brands_id;
			objJson.rating[col] = objProduct.product_rating.overall;
			// Reassign all value to empty
			$.each(objJson, function(attrKey, attr) {
				for(var i = 1 ; i<=totalProduct; i++){
					if(typeof (attr[i]) === "undefined" || typeof (attr[i]) === "") {
						attr[i] = "";
					}
				}
			});
			// Process Binding general product info
			$('#product_title_'+col).html(objProduct.product_service_title).removeClass('empty-title');
			$('#search_product_'+col).data('action','change');
			$('#search_product_'+col).html('Change');
			if(typeof objProduct.images !== "undefined" && objProduct.images !== ""){
				$('#product_image_'+col).attr('src',objProduct.images);
				$('#product_image_'+col).attr('alt',objProduct.product_service_title);
			}
			renderingCompareResultTable(objJson, $objBrands);
		}

		function processProductSuggestion(data, col, action){
			$obj = $('#sample_result_list').clone();
			if(data.images !== "") {
				// Binding Image
				$obj.find('#sample_image').attr('src',data.images);	
			} else {
				$obj.find('#sample_image').remove();	
			}
			// Binding Title
			$obj.find('#sample_product_name').html(data.product_service_title);	

			// Binding Data
			$obj.find('button').attr('data-detail',''+JSON.stringify(data));

			//Bind Col
			$obj.find('button').attr('data-col',''+col);
			$obj.find('button').attr('data-action',''+action);
			return $obj;
		}

		function renderingCompareResultTable(objJsonResult, objBrands) {
			$ele = $('#compare_result');
			$ele.empty();
			var count = 1;
			$.each(objJsonResult, function(attr, products) {
				if(attr != 'image'){
					$bindingTemplate = "<tr class='visible-xs'><td colspan='"+totalProduct+"' class='compare_title_xs colspan-dynamic'><strong>"+attr+"</strong></td></tr><td class='hidden-xs compare_title_xs'><strong>"+attr+"</strong></td>";
					count = 0;
					$.each(products, function(key, product) {
						var td = '';
						if(count < 2){
							td = '<td>';
						}else{
							td = '<td class="hidden-xs">';
						}
						if(typeof(product) == "array" || typeof(product) == "object"){
							$bindingTemplate += td+"<ul class='multiple-value-list'>";
							for(var productinfo = 1 ; productinfo <= product.length; productinfo++) {
								$bindingTemplate += "<li>"+product[productinfo]+"</li>";
							}
							$bindingTemplate += "</ul></td>";
						} else if(product !== ""){
							if(attr == "brand") {
								if(typeof(objBrands[product]) !== 'undefined' && objBrands[product] != ''){
									$bindingTemplate += td+objBrands[product]+"</td>";
								} else if(product){
									$bindingTemplate += td+""+product+"</td>";
								}else{
									$bindingTemplate += td+"-</td>";
								}
							} else if(attr == "rating") {
								$bindingTemplate += td+"<input id='rating-overall' type='number' class='rating rating-input rating-loading show-rating-wikabo' value='"+product+"' max='5' data-stars='5' data-size='xs'></td>";
							} else {
								$bindingTemplate += td+""+product+"</td>";
							}
						} else {
							$bindingTemplate += td+"-</td>";
						}
						count++;
					});
					$('.colspan-dynamic').attr('colspan',count);
					$ele.append($bindingTemplate);
				}
			});
			initRating();
		}
	</script>
@endsection