<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'WIKÅBÖ') }}</title>
    <!-- Fonts -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/theme/bootstrap.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/style.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/swiper.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/dark.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/font-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/animate.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/magnific-popup.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/responsive.css') }}" type="text/css" />
    <!-- Bootstrap Switch CSS -->
	<link rel="stylesheet" href="{{ asset('css/theme/components/bs-switches.css') }}" type="text/css" />
	<!-- Radio Checkbox Plugin -->
	<link rel="stylesheet" href="{{ asset('css/theme/components/radio-checkbox.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/theme/components/timepicker.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/theme/components/daterangepicker.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/theme/components/datepicker.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/components/bs-select.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/components/bs-rating.css') }}" type="text/css" />
	<link rel="stylesheet" href="{{ asset('css/theme/components/select-boxes.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/fonts/material-icon/material-icons.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{ asset('css/theme/custom-style.css') }}" type="text/css" />
    @yield('headCss')
    <style>
    #primary-menu ul li > a span#notification_count {
       display: inline;
    }
    </style>
    <!-- Javascript -->
    <script type="text/javascript" src="{{ asset('js/theme/jquery.js') }}"></script>
    <script>
    var urlSearch = "{!!route('Search Result',[""])!!}";
    </script>
    @yield('headJs')
</head>

<body class="stretched">
        <!-- Document Wrapper
        ============================================= -->
        <div class="clearfix" id='wrapper'>
            <!-- Header
            ============================================= -->
            <header id="header" class="full-header">
                <div id="header-wrap">
                    
                    <div class="container clearfix">

                        <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

                        <!-- Logo
                        ============================================= -->
                        <div id="logo">
                            <a href="{{ route('home') }}" class="standard-logo" data-dark-logo="images/logo-dark.png"><img src ="{{ asset('images/wikabo-logo.png') }}"></a>
                            <a href="{{ route('home') }}" class="retina-logo" data-dark-logo="images/logo-dark@2x.png"><img src ="{{ asset('images/wikabo-logo.png') }}"></a>
                        </div><!-- #logo end -->
                        <!-- Primary Navigation
                        ============================================= -->
                        <nav id="primary-menu" class="light">
                            <ul>
                                <!-- <li><a href="{{ route('About Us') }}"><div class="nott">@lang('layout.About Us')</div></a></li> -->
                                <li class="mega-menu">
                                <a href="#"><div class="nott">@lang('layout.Explore Categories') <i class="icon-angle-down"></i></div></a>
									<div class="mega-menu-content style-2 clearfix">
                                        @foreach($share->catList as $CategoryLabelKey => $LabelList)
                                            @if(count($LabelList) > 0 )
                                                <ul class="mega-menu-column col-lg-4">
                                                
                                                <li class="mega-menu-title"><a href="{{route(ucfirst($CategoryLabelKey).' All List')}}"><strong><div class="">{{ucfirst($CategoryLabelKey)}}</strong></a></div>
                                                @foreach($LabelList as $category)
                                                    @if($loop->iteration %7 == 0 )
                                                        @if($loop->last)
                                                            <li><a href="{{route('Category '.ucfirst($CategoryLabelKey).' List',['category'=>$category->category_slug])}}"><div>{{ucfirst($category->categories_name)}}</div></a></li>
                                                            </ul>
                                                            </div>    
                                                        @else
                                                            </ul>
                                                            </div>
                                                            <div class="col-md-4 noleftpadding">
                                                            <ul>
                                                            <li><a href="{{route('Category '.ucfirst($CategoryLabelKey).' List',['category'=>$category->category_slug])}}"><div>{{ucfirst($category->categories_name)}}</div></a></li>
                                                        @endif
                                                    @elseif($loop->iteration == 1)
                                                        <div class="col-md-4 noleftpadding">
                                                        <ul>
                                                            <li><a href="{{route('Category '.ucfirst($CategoryLabelKey).' List',['category'=>$category->category_slug])}}"><div>{{ucfirst($category->categories_name)}}</div></a></li>
                                                        @if($loop->last)
                                                        </ul>
                                                        @endif
                                                    @elseif($loop->last)
                                                        <li><a href="{{route('Category '.ucfirst($CategoryLabelKey).' List',['category'=>$category->category_slug])}}"><div>{{ucfirst($category->categories_name)}}</div></a></li>
                                                        </ul>
                                                        </div>
                                                    @else
                                                        <li><a href="{{route('Category '.ucfirst($CategoryLabelKey).' List',['category'=>$category->category_slug])}}"><div>{{ucfirst($category->categories_name)}}</div></a></li>
                                                    @endif
                                                @endforeach    
                                            </li>
                                            </ul>
                                        @endif    
                                    @endforeach 
                                    </div>
                                </li>
                                <li><a href="{{ route('Review') }}"><div class="nott">@lang('layout.Write a Review')</div></a></li>
                                @if (Auth::guest())
                                    <li class="sign-in-menu">
                                        <a href="{{ route('login') }}" class="nopadding"><div class="button button-border button-circle button-blue"><div class="nott">@lang('layout.Sign In')</div></div></a>
                                    </li>
                                    <!-- <li class="sign-up-menu">
                                        <a href="{{ route('register') }}" class="nopadding"><div class="button button-border button-circle button-blue"><div class="nott">@lang('layout.Register')</div></div></a>
                                    </li> -->
                               @else
                                <li class="profile-menu"><a href="#" class="nopadding"><div class="button button-circle button-blue"><i class="icon-user4" style="top: 29%;"></i>{{ Auth::user()->fullname }} <i class="icon-angle-down" style="top: 30%;"></i></div></a>
                                    <ul>
                                        <li><a href="{{ route('Dashboard')}} "><div><i class="material-icons list-icon">dashboard</i>@lang('layout.Dashboard')</div></a></li>
                                        <li><a href="{{ route('Notification')}}"><div><i class="material-icons list-icon">notifications_none</i>@lang('layout.Notification') <span id="notification_count" class="badge badge-primary">{{$share->total}}</span></div></a></li>
                                        <li><a href="{{ route('Messenger')}}"><div><i class="material-icons list-icon">mail_outline</i>@lang('layout.Messenger') <span id="messenger_count" class="badge badge-primary"></span></div></a></li>
                                        <li>
                                            <a href="{{ route('logout')}}" >
                                                <div><i class="material-icons list-icon">settings_power</i>@lang('layout.Logout')</div>
                                            </a>
                                        </li>        
                                    </ul>
                                </li>
                               <!--  <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                </form> -->
                                @endif
                            </ul>
                            
                            <!-- Top Language
                            ============================================= -->
                            <div id="top-cart">
                                <a href="#" id="top-cart-trigger"><img src="{{ asset('img/flag/'.__('layout.language_flag').'.jpg') }}" style="max-width: 25px; width: 25px;"></a>
                                <div class="top-cart-content">
                                    <div class="top-cart-title">
                                        <h4>@lang('layout.Select Language')</h4>
                                    </div>
                                    <div class="top-cart-items">
                                        <div class="top-cart-item clearfix">
                                            <a href="{{ url('lang/en') }}">
                                                <div style="float: left; margin-right: 15px;">
                                                <img src="{{ asset('img/flag/united-states-flag.jpg') }}" style="max-width: 25px; width: 25px;">
                                                </div>
                                                <div class="top-cart-item-desc">
                                                    @lang('layout.English')
                                                </div>
                                            </a>
                                        </div>
                                        <div class="top-cart-item clearfix">
                                            <a href="{{ url('lang/cn') }}">
                                                <div style="float: left; margin-right: 15px;">
                                                <img src="{{ asset('img/flag/china-flag.jpg') }}" style="max-width: 25px; width: 25px;">
                                                </div>
                                                <div class="top-cart-item-desc">
                                                    @lang('layout.Chinese')
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- #top-language end -->

                            <!-- Top Search
                            ============================================= -->
                            <div id="top-search">
                                <a href="#" id="top-search-trigger"><i class="icon-search3" style="font-size: 16px;"></i><i class="icon-line-cross"></i></a>
                                <!-- <form action="search.html" method="get">
                                    <input type="text" name="q" class="form-control top-search-input" value="" placeholder="Find products, services or movies...">
                                </form> -->
                                <form role="form" class="" id="top-search-form">
                                    <div id='bloodhound'>
                                        <input id='product-search-text-top' type="text" class="form-control top-search-input typeahead" value="" placeholder="Find products, services or movies...">
                                    </div>
                                </form>
                            </div><!-- #top-search end -->

                        </nav><!-- #primary-menu end -->
                        
                    </div>

                </div>

            </header><!-- #header end -->
            
             @yield('content')
             
            
            <!-- Footer
            ============================================= -->
            <footer id="footer" class="nomargin">

                <div class="container">

                    <!-- Footer Widgets
                    ============================================= -->
                    <div class="footer-widgets-wrap clearfix">

                        <div class="col_two_third">

                            <div class="col_one_third">
                            
                                <div class="widget widget_links clearfix">

                                    <ul>
                                        <li><a href="{{ route('About Us') }}">@lang('layout.About Us')</a></li>
                                        <li><a href="{{ route('Contact Us') }}">@lang('layout.Contact Us')</a></li>
                                        <li><a href="{{ route('Terms of Service') }}">@lang('layout.Our Policy')</a></li>
                                        <!-- <li><a href="{{ route('All Categories') }}">@lang('layout.Explore Categories')</a></li>
                                        <li><a href="{{ route('Review') }}">@lang('layout.Write a Review')</a></li>
                                        <li><a href="{{ route('Posting Guideline') }}">@lang('layout.Posting Guidelines')</a></li> -->
                                    </ul>

                                </div>

                            </div>

                            <!-- <div class="col_one_third">

                                <div class="widget widget_links clearfix">

                                    <ul>
                                        <li><a href="{{ route('Terms of Service') }}">@lang('layout.Terms of Service')</a></li>
                                        <li><a href="{{ route('Term of Use') }}">@lang('layout.Terms of Use')</a></li>
                                        <li><a href="{{ route('Privacy Policy') }}">@lang('layout.Privacy Policy')</a></li>
                                        <li><a href="{{ route('Trademark Policy') }}">@lang('layout.Trademark Policy')</a></li>
                                    </ul>

                                </div>

                            </div>
                            <div class="col_one_third col_last">
                                <div class="widget widget_links clearfix">
                                
                                    <ul>
										<li><a href="{{ route('Acceptable Use Policy') }}">@lang('layout.Acceptable Use Policy')</a></li>
                                        <li><a href="{{ route('Disclaimer and Limitation of Liability') }}">@lang('layout.Disclaimer and Limitation of Liability')</a></li>
                                        <li><a href="{{ route('Contact Us') }}">@lang('layout.Contact Us')</a></li>
									</ul>
								</div>
                            </div> -->
					</div>
                        <div class="col_one_third col_last">

                            <div class="widget subscribe-widget clearfix">
                                <h5><strong>@lang('layout.Stay connected')</strong><br>
                                @lang('layout.Like our Facebook page to stay in the know on amazing new products').</h5>
                            </div>

                            <div class="widget clearfix" style="margin-bottom: -20px; margin-top: 0px;">

                                <div class="row">

                                    <div class="col-md-4 clearfix bottommargin-sm">
                                        <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                            <i class="icon-facebook"></i>
                                            <i class="icon-facebook"></i>
                                        </a>
                                        <a href="https://www.facebook.com/Wikabo-283021278916083/" target="_blank"><small style="display: inline-block; margin-top: 3px;"><strong>@lang('layout.Like us')</strong><br>@lang('layout.on Facebook')</small></a>
                                    </div>
                                    <div class="col-md-4 clearfix bottommargin-sm">
                                        <a href="#" class="social-icon si-dark si-colored si-instagram nobottommargin" style="margin-right: 10px;">
                                            <i class="icon-instagram"></i>
                                            <i class="icon-instagram"></i>
                                        </a>
                                        <a href=" https://www.instagram.com/wikabo_official/" target="_blank"><small style="display: inline-block; margin-top: 3px;"><strong>@lang('layout.Follow us')</strong><br>@lang('layout.on Instagram')</small></a>
                                    </div>

                                    <div class="col-md-4 clearfix bottommargin-sm">
                                        <a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                            <i class="icon-twitter"></i>
                                            <i class="icon-twitter"></i>
                                        </a>
                                        <a href="https://twitter.com/WIKABO1" target="_blank"><small style="display: inline-block; margin-top: 3px;"><strong>@lang('layout.Follow us')</strong><br>@lang('layout.on Twitter')</small></a>
                                    </div>

                                </div>

                            </div>

                        </div>

                    </div><!-- .footer-widgets-wrap end -->

                </div>

                <!-- Copyrights
                ============================================= -->
                <div id="copyrights">

                    <div class="container clearfix">

                        <div class="col_half">
                            Copyrights &copy; 2020 All Rights Reserved by WIKTUBE PTE. LTD.<br>
                            
                        </div>

                        <!-- <div class="col_half col_last tright">
                        
							<div class="copyright-links"><a href="{{ route('Copyright Policy') }}">@lang('layout.Copyright Policy')</a></div>
                       
                        </div> -->

                    </div>

                </div><!-- #copyrights end -->

            </footer><!-- #footer end -->
	</div>
	
<!--
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'WIKÅBÖ') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login123</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->fullname }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </div>
</body>-->
    
    <!-- Go To Top
        ============================================= -->
        <div id="gotoTop" class="icon-angle-up"></div>

    <!-- Scripts -->
    <!-- External JavaScripts
        ============================================= -->
        <script type="text/javascript" src="{{ asset('js/theme/plugins.js') }}"></script>
        <!-- Bootstrap Select Plugin -->
		<script type="text/javascript" src="{{ asset('js/theme/components/bs-select.js') }}"></script>

		<!-- Select Splitter Plugin -->
		<script type="text/javascript" src="{{ asset('js/theme/components/selectsplitter.js') }}"></script>
        
		<script type="text/javascript" src="{{ asset('js/theme/components/moment.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/theme/components/datepicker.js') }}"></script>
		<script type="text/javascript" src="{{ asset('js/theme/components/timepicker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/theme/components/daterangepicker.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/theme/components/star-rating.js') }}"></script>
        
		<!-- Select-Boxes Plugin -->
		<script type="text/javascript" src="{{ asset('js/theme/components/select-boxes.js') }}"></script>

		<!-- Select-Splitter Plugin -->
		<script type="text/javascript" src="{{ asset('js/theme/components/selectsplitter.js') }}"></script>
       
		<!-- Bootstrap Switch Plugin -->
		<script type="text/javascript" src="{{ asset('js/theme/components/bs-switches.js') }}"></script>
       
        <!-- Footer Scripts
        ============================================= -->
        <script type="text/javascript" src="{{ asset('js/theme/functions.js') }}"></script>
	
        <!-- Charts JS
        ============================================= -->
        <script type="text/javascript" src="{{ asset('js/theme/chart.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/theme/chart-utils.js') }}"></script>
        
        @yield('tailJs')
        <script>
            let urlAsProduct = "{!! route('autocomplete.product', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
            let urlAsCommment = "{!! route('autocomplete.comment', ['wildecard' => '%QUERY', 'limit'=>10]) !!}";
            var searchResult = 'product-result';
        </script>
        <script type="text/javascript" src="{{ asset('js/lib/typeahead.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/review/autoComplete.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/review/search.js') }}" defer></script>
        <script src="{{ asset('js/sweetalert.min.js') }}"></script>
        <script>
        checkCookie("wikabo-agree","open_sesame");
        @if(isset($share->total))
            checkNotification({{$share->total}});
        @endif

        function setCookie(cname, cvalue, exdays) {
            var d = new Date();
            d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
            var expires = "expires="+d.toUTCString();
            document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
        }

        function getCookie(cname) {
            var name = cname + "=";
            var ca = document.cookie.split(';');
            for(var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1);
                }
                if (c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }
            return "";
        }

        function checkCookie(cname,cvalue) {
            var wookie = getCookie(cname);
            if(cname == "wikabo-agree"){
                if (wookie == ""){
                    swal({
                        type: 'info',
                        title: 'Cookie Agreement',
                        allowOutsideClick: false,
                        html: 'By using the website, you are agreeing to our <a href="{{route("Privacy Policy")}}">Privacy Policy</a> , <a href="{{route("Term of Use")}}">Term &#38; Condition</a> and the use of cookies in accordance with our <a href="{{ route("Cookie Policy") }}">Cookie Policy</a>.',
                        confirmButtonText: 'I understand',
                    }).then(function(isConfirm) {
                        setCookie(cname, cvalue, 365);
                    });
                }
            }else{
                if(wookie == ""){
                    setCookie(cname, cvalue, 365);
                }else{
                    return wookie;
                }
            }
        }

        function checkNotification(count) {
            if(count > 0 ){
                $('#notification_count').show();
            }
        }

        </script>
</body>
</html>