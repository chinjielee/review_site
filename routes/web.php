<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Language Control
|--------------------------------------------------------------------------
*/
Route::get('lang/{locale}', function ($locale) {
    if (in_array($locale, \Config::get('app.locales'))) {
        Session::put('locale', $locale);
    }
    return redirect()->back();
});

/*
|--------------------------------------------------------------------------
| Landing Page
|--------------------------------------------------------------------------
*/
Route::get('/',['as' => 'home', 'uses' => 'HomeController@index']);

/*
|--------------------------------------------------------------------------
| Auth Page
|--------------------------------------------------------------------------
*/
Auth::routes();
Route::get('/signin', ['as' => 'login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/signin', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('/logout', ['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::post('/register', ['as' => 'register', 'uses' => 'Auth\RegisterController@register']);
Route::get('/forgot-password', ['as' => 'Forgot Password','uses' => 'Auth\ForgotPasswordController@showLinkRequestForm']);
Route::get('/reset-password/{token}', ['as' => 'Reset Password','uses' => 'Auth\ResetPasswordController@showResetForm']);
Route::get('/redirect', 'SocialAuthFacebookController@redirect');
Route::get('/callback', 'SocialAuthFacebookController@callback');
Route::get('/social-twitter/redirect', 'SocialAuthTwitterController@redirect');
Route::get('/social-twitter/callback', 'SocialAuthTwitterController@callback');
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verify');
Route::get('/password/reset/',['as'=>'Forget Password','uses'=>'Auth\ForgotPasswordController@showLinkRequestForm']);
/*
|--------------------------------------------------------------------------
| Statis Page / List
|--------------------------------------------------------------------------
*/
Route::get('/about-us', function(){
    return view('static.about-us');
})->name("About Us");

Route::get('/terms-of-use', function(){
    return view('static.terms-of-use');
})->name("Term of Use");

Route::get('/terms-of-service', function(){
    return view('static.terms-of-service');
})->name("Terms of Service");

Route::get('/disclaimer-and-limitation-of-liability', function(){
    return view('static.disclaimer-and-limitation-of-liability');
})->name("Disclaimer and Limitation of Liability");

Route::get('/privacy-policy', function(){
    return view('static.privacy-policy');
})->name("Privacy Policy");

Route::get('/posting-guideline', function(){
    return view('static.posting-guideline');
})->name("Posting Guideline");

Route::get('/acceptable-use-policy', function(){
    return view('static.acceptable-use-policy');
})->name("Acceptable Use Policy");

Route::get('/copyright-policy', function(){
    return view('static.copyright-policy');
})->name("Copyright Policy");

Route::get('/cookie-policy', function(){
    return view('static.cookie-policy');
})->name("Cookie Policy");

Route::get('/trademark-policy', function(){
    return view('static.trademark-policy');
})->name("Trademark Policy");

Route::get('/contact-us',['as' => 'Contact Us', 'uses' => 'ContactUsController@index']);
Route::post('/contact-us',['as' => 'Submit Contact Us', 'uses' => 'ContactUsController@handleForm']);

/*
|--------------------------------------------------------------------------
| Categories
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'categories'], function () {
    // Route::get('/', [
    //     'middleware' => 'auth',
    //     'uses' => 'ProfileController@getIndex',
    //     'as' => 'profile.index'
    // ]);
    Route::get('/',['as' => 'All Categories', 'uses' => 'CategoryController@CategoryIndex']);

    Route::get('/sub', function(){
        return view('categories.sub');
    })->name("Sub Categories List");

    Route::get('/{category_id}', function(){
        return view('categories.product-list');
    })->name("Category Product List");
});

/*
|--------------------------------------------------------------------------
| Browse Product
|--------------------------------------------------------------------------
*/
// ########################## Browse #################################
Route::group(['prefix' => 'browse'], function () {
    # -------------------- Product ------------------------ #
    Route::group(['prefix' => 'products'], function () {
        // 'products.index'
        Route::get('/',[
            'as' => 'Products All List',
            'uses' => 'CategoryController@Categorylist'
        ]);

        // 'products.detail'
        Route::get('/{product_slug}',[
            'as' => 'Products Detail1', 
            'uses' => 'ProductController@detail'
            ]);

        // 'products.qa'
        Route::get('/{product_slug}/qa',[
            'as' => 'Products Question and Answer1',
            'uses' => 'ProductController@qa'
        ]);
        
        // 'products.review'
        Route::get('{temp_product_id}/review/temp/',[
            'as' => 'Products Temp Review',
            'uses' => 'ProductController@tempReview',
        ]);

        // 'products.review'
        Route::get('/{product_slug}/review',[
            'as' => 'Products Review1',
            'uses' => 'ProductController@review',
        ]);
        
        // 'products.stats'
        Route::get('/{product_slug}/stats',[
            'as' => 'Products Stats1',
            'uses' => 'ProductController@stats'
        ]);
        
        // 'products.photos'
        Route::get('/{product_slug}/photos',[
            'as' => 'Products Photos1',
            'uses' => 'ProductController@photos'
        ]);

        // 'products.prices'
        Route::get('/{product_slug}/prices',[
            'as' => 'Products Prices1',
            'uses' => 'ProductController@prices'
        ]);

        // 'categories.list'
        Route::get('/{category}/list/',[
            'as' => 'Category Products List',
            'uses' => 'CategoryController@Categorylist'
        ]);

        Route::get('/{category}/{sub_category}/list/',[
            'as' => 'Sub Category Products List',
            'uses' => 'CategoryController@Categorylist'
        ]);
        
        // 'products.detail'
        Route::get('/{category}/{product_slug}',[
            'as' => 'Products Detail2',
            'uses' => 'ProductController@detail'
        ]);
        
        //products.qa
        Route::get('/{category}/{product_slug}/qa',[
            'as' => 'Products Question and Answer2',
            'uses' => 'ProductController@qa'
        ]);

        //products.review
        Route::get('/{category}/{product_slug}/review',[
            'as' => 'Products Review2',
            'uses' => 'ProductController@review'
        ]);

         //products.stats
         Route::get('/{category}/{product_slug}/stats',[
            'as' => 'Products Stats2',
            'uses' => 'ProductController@stats'
        ]);

        //products.photos
        Route::get('/{category}/{product_slug}/photos',[
            'as' => 'Products Photos2',
            'uses' => 'ProductController@photos'
        ]);

         //products.prices
         Route::get('/{category}/{product_slug}/prices',[
            'as' => 'Products Prices2',
            'uses' => 'ProductController@prices'
        ]);
        
        // products.detail
        Route::get('/{category}/{sub_category}/{product_slug}',[
            'as' => 'Products Detail3',
            'uses' => 'ProductController@detail'
        ]);
        
        // products.review
        Route::get('/{category}/{sub_category}/{product_slug}/review',[
            'as' => 'Products Review3',
            'uses' => 'ProductController@review',
        ]);
        
        // products.qa
        Route::get('/{category}/{sub_category}/{product_slug}/qa',[
            'as' => 'Products Question and Answer3',
            'uses' => 'ProductController@qa'
        ]);

        // products.stats
        Route::get('/{category}/{sub_category}/{product_slug}/stats',[
            'as' => 'Products Stats3',
            'uses' => 'ProductController@stats'
        ]);
        
        // products.photos
        Route::get('/{category}/{sub_category}/{product_slug}/photos',[
            'as' => 'Products Photos3',
            'uses' => 'ProductController@photos'
        ]);

        // products.prices
        Route::get('/{category}/{subservice_category}/{product_slug}/prices',[
            'as' => 'Products Prices3',
            'uses' => 'ProductController@prices'
        ]);
        Route::get('/{category}/{sub_category}/list/',[
            'as' => 'Products Sub Category List',
            'uses' => 'CategoryController@CategoryProductlist'
        ]);

    });

    # -------------------- Service ------------------------ #

    Route::group(['prefix' => 'services'], function () {
        // 'services.index'
        Route::get('/',[
            'as' => 'Services All List',
            'uses' => 'CategoryController@Categorylist'
        ]);

        // 'services.detail'
        Route::get('/{service}',[
            'as' => 'Services Detail1', 
            'uses' => 'ServiceController@detail'
            ]);

        // 'services.qa'
        Route::get('/{service}/qa',[
            'as' => 'Services Question and Answer1',
            'uses' => 'ServiceController@qa'
        ]);
        
        // 'services.review'
        Route::get('/{service}/review',[
            'middleware'=>'auth',
            'as' => 'Services Review1',
            'uses' => 'ServiceController@review'
        ]);
        // 'services.stats'
        Route::get('/{service}/stats',[
            'as' => 'Services Stats1',
            'uses' => 'ServiceController@stats'
        ]);
        
        // 'services.photos'
        Route::get('/{service}/photos',[
            'as' => 'Services Photos1',
            'uses' => 'ServiceController@photos'
        ]);

        // 'services.prices'
        Route::get('/{service}/prices',[
            'as' => 'Services Prices1',
            'uses' => 'ServiceController@prices'
        ]);
        // 'categories.list'
        Route::get('/{category}/list/',[
            'as' => 'Category Services List',
            'uses' => 'CategoryController@Categorylist'
        ]);
        
        Route::get('/{category}/{sub_category}/list/',[
            'as' => 'Sub Category Services List',
            'uses' => 'CategoryController@Categorylist'
        ]);

        // 'services.detail'
        Route::get('/{category}/{service}',[
            'as' => 'Services Detail2',
            'uses' => 'ServiceController@detail'
        ]);
        
        //services.qa
        Route::get('/{category}/{service}/qa',[
            'as' => 'Services Question and Answer2',
            'uses' => 'ServiceController@qa'
        ]);

        //services.review
        Route::get('/{category}/{service}/review',[
            'middleware'=>'auth',
            'as' => 'Services Review2',
            'uses' => 'ServiceController@review'
        ]);

        //services.stats
        Route::get('/{category}/{service}/stats',[
            'as' => 'Services Stats2',
            'uses' => 'ServiceController@stats'
        ]);

        //services.photos
        Route::get('/{category}/{service}/photos',[
            'as' => 'Services Photos2',
            'uses' => 'ServiceController@photos'
        ]);

        //services.prices
        Route::get('/{category}/{service}/prices',[
            'as' => 'Services Prices2',
            'uses' => 'ServiceController@prices'
        ]);
        
        // services.detail
        Route::get('/{category}/{sub_category}/{service}',[
            'as' => 'Services Detail3',
            'uses' => 'ServiceController@detail'
        ]);
        
        // services.review
        Route::get('/{category}/{sub_category}/{service}/review',[
            'middleware'=>'auth',
            'as' => 'Services Review3',
            'uses' => 'ServiceController@review'
        ]);
        
        // services.qa
        Route::get('/{category}/{sub_category}/{service}/qa',[
            'as' => 'Services Question and Answer3',
            'uses' => 'ServiceController@qa'
        ]);

        // services.stats
        Route::get('/{category}/{sub_category}/{service}/stats',[
            'as' => 'Services Stats3',
            'uses' => 'ServiceController@stats'
        ]);
        
        // services.photos
        Route::get('/{category}/{sub_category}/{service}/photos',[
            'as' => 'Services Photos3',
            'uses' => 'ServiceController@photos'
        ]);

        // services.prices
        Route::get('/{category}/{sub_category}/{service}/prices',[
            'as' => 'Services Prices3',
            'uses' => 'ServiceController@prices'
        ]);
        Route::get('/{category}/{sub_category}/list/',[
            'as' => 'Services Sub Category List',
            'uses' => 'CategoryController@CategoryProductlist'
        ]);
    });

    # -------------------- Movie ------------------------ #
    Route::group(['prefix' => 'movies'], function () {
        // 'movies.index'
        Route::get('/',[
            'as' => 'Movies All List',
            'uses' => 'CategoryController@Categorylist'
        ]);

        // 'movies.detail'
        Route::get('/{product_slug}',[
            'as' => 'Movies Detail1', 
            'uses' => 'ProductController@detail'
            ]);

        // 'movies.qa'
        Route::get('/{product_slug}/qa',[
            'as' => 'Movies Question and Answer1',
            'uses' => 'ProductController@qa'
        ]);
        
        // 'movies.review'
        Route::get('/{product_slug}/review',[
            'as' => 'Movies Review1',
            'uses' => 'ProductController@review',
        ]);
        // 'movies.stats'
        Route::get('/{product_slug}/stats',[
            'as' => 'Movies Stats1',
            'uses' => 'ProductController@stats'
        ]);
        
        // 'movies.photos'
        Route::get('/{product_slug}/photos',[
            'as' => 'Movies Photos1',
            'uses' => 'ProductController@photos'
        ]);

        // 'movies.prices'
        Route::get('/{product_slug}/prices',[
            'as' => 'Movies Prices1',
            'uses' => 'ProductController@prices'
        ]);

        // 'categories.list'
        Route::get('/{category}/list/',[
            'as' => 'Category Movies List',
            'uses' => 'CategoryController@Categorylist'
        ]);

        Route::get('/{category}/{sub_category}/list/',[
            'as' => 'Sub Category Movies List',
            'uses' => 'CategoryController@Categorylist'
        ]);

        
        // 'movies.detail'
        Route::get('/{category}/{product_slug}',[
            'as' => 'Movies Detail2',
            'uses' => 'ProductController@detail'
        ]);
        
        //movies.qa
        Route::get('/{category}/{product_slug}/qa',[
            'as' => 'Movies Question and Answer2',
            'uses' => 'ProductController@qa'
        ]);

        //movies.review
        Route::get('/{category}/{product_slug}/review',[
            'as' => 'Movies Review2',
            'uses' => 'ProductController@review'
        ]);

         //movies.stats
         Route::get('/{category}/{product_slug}/stats',[
            'as' => 'Movies Stats2',
            'uses' => 'ProductController@stats'
        ]);

        //movies.photos
        Route::get('/{category}/{product_slug}/photos',[
            'as' => 'Movies Photos2',
            'uses' => 'ProductController@photos'
        ]);

         //movies.prices
         Route::get('/{category}/{product_slug}/prices',[
            'as' => 'Movies Prices2',
            'uses' => 'ProductController@prices'
        ]);
        
        // movies.detail
        Route::get('/{category}/{sub_category}/{product_slug}',[
            'as' => 'Movies Detail3',
            'uses' => 'ProductController@detail'
        ]);
        
        // movies.review
        Route::get('/{category}/{sub_category}/{product_slug}/review',[
            'as' => 'Movies Review3',
            'uses' => 'ProductController@review',
        ]);
        
        // movies.qa
        Route::get('/{category}/{sub_category}/{product_slug}/qa',[
            'as' => 'Movies Question and Answer3',
            'uses' => 'ProductController@qa'
        ]);

        // movies.stats
        Route::get('/{category}/{sub_category}/{product_slug}/stats',[
            'as' => 'Movies Stats3',
            'uses' => 'ProductController@stats'
        ]);
        
        // movies.photos
        Route::get('/{category}/{sub_category}/{product_slug}/photos',[
            'as' => 'Movies Photos3',
            'uses' => 'ProductController@photos'
        ]);

        // movies.prices
        Route::get('/{category}/{sub_category}/{product_slug}/prices',[
            'as' => 'Movies Prices3',
            'uses' => 'ProductController@prices'
        ]);
        //categories.list
        Route::get('/{category}/{sub_category}/list/',[
            'as' => 'Movies Sub Category List',
            'uses' => 'CategoryController@CategoryProductlist'
        ]);
    });
});

/* ***************************** Searching ************************/
    // review.search
    Route::get('/search/{keyword}/',[
        'as' => 'Search Result',
        'uses' => 'SearchController@search'
    ]);
/*
|--------------------------------------------------------------------------
| Review Product
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'review'], function () {
    // review.index
    Route::get('/',[
        'as' => 'Review',
        'uses' => 'ReviewController@index'
    ]);
    /* ***************************** New Review ************************/
    // review.create
    Route::get('/create/',[
        'middleware'=>'auth',
        'as' => 'Create Review',
        'uses' => 'ReviewController@create'
    ]);

    Route::post('/create',[
        'middleware'=>'auth',
        'as' => 'Post Product Review',
        'uses' => 'ReviewController@postCreate'
    ]);

    // review.detail
    Route::get('/review',[
        'middleware'=>'auth',
        'as' => 'Review Detail',
        'uses' => 'ReviewController@review'
    ]);  

    Route::post('/review',[
        'middleware'=>'auth',
        'as' => 'Post Review Detail',
        'uses' => 'ReviewController@postReview'
    ]);  

    // review.thankyou
    Route::get('/thankyou',[
        'middleware'=>'auth',
        'as' => 'Review Thank You',
        'uses' => 'ReviewController@thankyou'
    ]);  
    /* ***************************** Review Existing ************************/
    // review.detail
    Route::get('/{product_slug}/review/',[
        'middleware'=>'auth',
        'as' => 'Review Product Detail',
        'uses' => 'ReviewController@review'
    ]);  

    // review.thankyou
    Route::get('/thankyou/',[
        'middleware'=>'auth',
        'as' => 'Review Thank You',
        'uses' => 'ReviewController@thankyou'
    ]);  
});

/*
|--------------------------------------------------------------------------
| Compare
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'compare'], function () {
    // Route::get('/', [
    //     'middleware' => 'auth',
    //     'uses' => 'ProfileController@getIndex',
    //     'as' => 'profile.index'
    // ]);
    Route::get('/', [
        'as' => 'Compare',
        'uses' => 'CompareController@index'
    ]);
    Route::get('/{product_1}/', [
        'as' => 'Compare',
        'uses' => 'CompareController@compare'
    ]);
    Route::get('/{product_1}/{product_2}/', [
        'as' => 'Compare',
        'uses' => 'CompareController@compare'
    ]);
    Route::get('/{product_1}/{product_2}/{product_3}/', [
        'as' => 'Compare',
        'uses' => 'CompareController@compare'
    ]);
});
/*
|--------------------------------------------------------------------------
| Logged In Page
|--------------------------------------------------------------------------
*/
    /*
    | Profile
    */
    Route::group(['prefix' => 'profile','middleware'=>'auth'], function () {
        // Route::get('/', [
        //     'middleware' => 'auth',
        //     'uses' => 'ProfileController@getIndex',
        //     'as' => 'profile.index'
        // ]);

        // profile.index
        Route::get('/',[
            'as' => 'Dashboard',
            'uses' => 'UserController@dashboard'
        ]);  
		

        // profile.my-account
        Route::get('/my-account',[
            'as' => 'My Profile',
            'uses' => 'UserController@account'
        ]);  

        // profile.reviews
        Route::get('/reviews',[
            'as' => 'My Reviews',
            'uses' => 'UserController@review'
        ]);  

        // profile.comments
        Route::get('/comments',[
            'as' => 'My Comments',
            'uses' => 'UserController@comment'
        ]); 

        // profile.questions
        Route::get('/questions',[
            'as' => 'My Questions',
            'uses' => 'UserController@question'
        ]);

        Route::get('/answers',[
            'as' => 'My Answers',
            'uses' => 'UserController@answers'
        ]);

        Route::get('/activity-log',[
            'as' => 'Activity Log',
            'uses' => 'UserController@activity'
        ]);
		
        Route::get('/messenger',[
            'as' => 'Messenger',
            'uses' => 'UserController@messenger'
        ]); 
        
        Route::get('/messenger-details/{channel_id}',[
            'as' => 'Messenger Details',
            'uses' => 'ChatController@getChatMessages'
        ]); 
		
        Route::get('/notification',[
            'as' => 'Notification',
            'uses' => 'UserController@notification'
        ]);
		
        Route::get('/notification-settings',[
            'as' => 'Notification Setting',
            'uses' => 'UserController@notificationSetting'
        ]);

        Route::get('/edit-password', function(){
            return view('profile.edit-password');
        })->name("Password");

        Route::post('/photo',[
            'as' => 'My Profile Photo',
            'uses' => 'UserController@update_avatar'
        ]);

        Route::post('/update/profile',[
            'as' => 'Profile Update',
            'uses' => 'UserController@update_profile'
        ]);

        Route::post('/update/address',[
            'as' => 'Address Update',
            'uses' => 'UserController@update_address'
        ]);

        Route::post('/update/password',[
            'as' => 'Password Update',
            'uses' => 'UserController@update_password'
        ]);

        Route::post('/update/review',[
            'as' => 'Review Update',
            'uses' => 'UserController@update_review'
        ]);

        Route::post('/update/comment',[
            'as' => 'Comment Update',
            'uses' => 'UserController@update_comment'
        ]);

        Route::post('/update/question',[
            'as' => 'Question Update',
            'uses' => 'UserController@update_question'
        ]);

        Route::post('/update/notification-settings',[
            'as' => 'Notification Setting Update',
            'uses' => 'UserController@update_notification_setting'
        ]);
    });

/*
|--------------------------------------------------------------------------
| Action 
|--------------------------------------------------------------------------
*/
// Searching Autocomplete
Route::group(['prefix' => 'autocomplete'], function () {
    Route::get('product/{wildcard}/{limit}/',[
        'as' => 'autocomplete.product',
        'uses' => 'AutoCompleteController@product'
    ]);

    Route::get('comment/{wildcard}/{limit}/',[
        'as' => 'autocomplete.comment',
        'uses' => 'AutoCompleteController@comment'
    ]);
});

Route::group(['prefix' => 'search'], function () {
    Route::get('product/{wildcard}/{limit}/',[
        'as' => 'search.product',
        'uses' => 'ProductController@searchProduct'
    ]);
});

    /*
    |--------------------------------------------------------------------------
    | Notifications
    |--------------------------------------------------------------------------
    */ 
Route::group(['prefix' => 'notifications'], function () {
    Route::get('', [
        'as' => 'notiGet',
        'uses' => 'NotificationController@getNoti'
    ]);
    Route::get('all', [
        'as' => 'notiGetAll',
        'uses' => 'NotificationController@getNotiAll'
    ]);
    
    Route::post('', [
        'middleware'=>'auth',
        'as' => 'notiSend',
        'uses' => 'NotificationController@sendNoti'
    ]);

    Route::patch('{id}/read', [
        'middleware'=>'auth',
        'as' => 'notiMarkRead',
        'uses' => 'NotificationController@markAsRead'
    ]);

    Route::post('mark-all-read', [
        'middleware'=>'auth',
        'as' => 'notiMarkAllRead',
        'uses' => 'NotificationController@markAllRead'
    ]);

    Route::post('{id}/dismiss', [
        'middleware'=>'auth',
        'as' => 'notiDismiss',
        'uses' => 'NotificationController@dismiss'
    ]);
});

    /*
    |--------------------------------------------------------------------------
    | Question & Answer on Product
    |--------------------------------------------------------------------------
    */

Route::group(['prefix' => 'product'], function () {
    Route::post('question', [
        'middleware'=>'auth',
        'as' => 'postQuestion',
        'uses' => 'ProductQuestionController@postQuestion'
    ]);

    Route::post('question/verify', [
        'middleware'=>'auth',
        'as' => 'postQuestionVerify',
        'uses' => 'ProductQuestionController@postQuestionVerify'
    ]);

    Route::post('answer', [
        'middleware'=>'auth',
        'as' => 'postAnswer',
        'uses' => 'ProductAnswerController@postAnswer'
    ]);
    Route::post('answer/verify', [
        'middleware'=>'auth',
        'as' => 'postAnswerVerify',
        'uses' => 'ProductAnswerController@postAnswerVerify'
    ]);
});

/*
    |--------------------------------------------------------------------------
    | Question & Answer on Product
    |--------------------------------------------------------------------------
    */

    Route::group(['prefix' => 'review'], function () {
        Route::post('upvote', [
            'middleware'=>'auth',
            'as' => 'postUpVote',
            'uses' => 'ReviewController@postUpVote'
        ]);
    
        Route::post('downvote', [
            'middleware'=>'auth',
            'as' => 'postDownVote',
            'uses' => 'ReviewController@postDownVote'
        ]);

        Route::post('report', [
            'middleware'=>'auth',
            'as' => 'postReportFraud',
            'uses' => 'ReviewController@postReportFraud'
        ]);
    });

/*
|--------------------------------------------------------------------------
| Upload
|--------------------------------------------------------------------------
*/
Route::post('review-image-upload', [
    'middleware'=>'auth',
    'as' => 'review.upload',
    'uses' => 'UploadImageController@reviewImageStore'
]);

Route::post('review/remove', [
    'middleware'=>'auth',
    'as' => 'review.remove',
    'uses' => 'UploadImageController@reviewDestroy'
]);

/*
|--------------------------------------------------------------------------
| Chat
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'chat'], function () {
    Route::post('create/', [
        'middleware'=>'auth',
        'as' => 'Post Chat',
        'uses' => 'ChatController@postCreateChat'
    ]);

    Route::post('send/', [
        'middleware'=>'auth',
        'as' => 'postSendMsg',
        'uses' => 'ChatController@postSendMsg'
    ]);
});
// SQL Debugger 
// Event::listen('Illuminate\Database\Events\QueryExecuted', function ($query) {
//     echo'<pre>';
//     var_dump($query->sql);
//     var_dump($query->bindings);
//     var_dump($query->time);
//     echo'</pre>';
// });